$(document).ready(function() {
    variableGLOBAL();
    var Servicio = {};

    if (getCookieV2("SolicitudTomada")) {
        Servicio = JSON.parse(getCookieV2("SolicitudTomada"));
        if (typeof Servicio.Proveedor == "undefined") {
            Servicio.Proveedor = obj_data_usuario();
        }

        $(".imgC").attr("src", "" + Servicio.Cliente.img_cli + "");
        $(".nomC").text(Servicio.Cliente.nom_cli);
        $(".desde").text(Servicio.desdeText);
        $(".hasta").text(Servicio.hastaText);
        $(".kmC").text(JSON.parse(Servicio.distancia_sol).text);
        $(".preC").text(formato.precio(Servicio.costo_sol));
        $(".LLegue").attr('id', Servicio.id);
        $(".BTCANCELARS").attr('id', Servicio.id);
        tltwha = Servicio.Cliente.tel_cli;
        if (tltwha) {
            tltwha = tltwha.replace("+", "");
        }
        $(".linkC").attr('href', 'https://wa.me/' + tltwha + '');
        $(".linkH").attr('href', 'historial.html?id=' + Servicio.id + '');
        $(".linkCcall").attr('href', 'tel:' + Servicio.Cliente.tel_cli + '');

        $(document).on('click', '.LLegue', function() {
            var bt = $(this);
            var sol = Servicio;
            sol.token = obj_data_usuario().token

            axios.post(DOMINIO + 'solicitud_servicios/llegue', sol, progress__event(bt))
                .then(function(response) {
                    location.href = "historial.html?id=" + sol.id;
                })
                .catch(function(error) {
                    console.log(error);

                });

        });
        $(document).on('click', '.BTCANCELARS', function() {
            var editar = {
                id: Servicio.id,
                token: obj_data_usuario().token,
                cliente: obj_data_usuario()
            }
            axios.post(DOMINIO + 'solicitud_servicios/cancelar-solicitud', editar, progress__event($(".BTCANCELARS")))
                .then(function(response) {
                    location.href = "historial.html?id=" + editar.id;
                })
                .catch(function(error) {
                    console.log(error);

                });

        });
    }
});