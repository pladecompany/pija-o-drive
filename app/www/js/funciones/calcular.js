$(document).ready(function() {
    convertirAlert();
    variableGLOBAL();
    var Servicio = {};

    if (getCookieV2("ServicioElegido")) {
        Servicio = JSON.parse(getCookieV2("ServicioElegido"));

        IMGtipoVeh = $("#IMGtipoVeh");
        IMGtipoVeh.attr("src", "img/transporte/" + Servicio.icono_veh + "");
        $("#desde").val(Servicio.desdeText);
        $("#hasta").val(Servicio.hastaText);
        //$("#carga").val(Servicio.carga);
        //$("#carga").formSelect();
        $(".KM").text(Servicio.distancia.text);
        var KMmin = parseFloat(Servicio.distancia.value / 1000);
        if (KMmin < Servicio.tarifas.km) {
            Servicio.calculo = parseFloat(Servicio.tarifas.precio);
        } else {
            Servicio.calculo = parseFloat(Servicio.distancia.value / 1000);
            Servicio.calculo = parseFloat(Servicio.calculo * Servicio.preciokm);
            var Durmin = parseFloat((Servicio.duracion.value / 60) * Servicio.preciomin);
            Servicio.calculo = parseFloat(Servicio.calculo + Durmin);
            Servicio.calculo = parseFloat(Servicio.calculo + Servicio.tarifas.banderazo);
        }
        
        setCookieV2("ServicioElegido", JSON.stringify(Servicio));
        $(".COSTO").text(formato.precio(Servicio.calculo));
        $(".DURACION").text(Servicio.duracion.text);

    }

    function onError(error) {
        M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
        setTimeout(function() { history.back(); }, 5500);

    };

    var mapa;
    DSERU = "";
    DIRDISU = "";



    correr_mapa_();

    function correr_mapa_() {
        if (!google) {
            alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");
        }
        DSERU = new google.maps.DirectionsService();
        DIRDISU = new google.maps.DirectionsRenderer();
        var styles = [
            { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
        ];

        var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

        var options = {
            zoom: 16,
            center: new google.maps.LatLng(23.0000000, -102.0000000),
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            backgroundColor: 'transparent'
        };
        geocoder = new google.maps.Geocoder();
        mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
        mapa.mapTypes.set('map_style', styledMap);
        mapa.setMapTypeId('map_style');

        $(document).on('click', '.gm-fullscreen-control', function() {
            screen_map('mapaservicio');
        });

        MAPA = mapa;
        DIRDISU.setMap(mapa);
        var data_map = {
            origin: "" + Servicio.lat1 + "," + Servicio.lon1 + "",
            destination: "" + Servicio.lat2 + "," + Servicio.lon2 + "",
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        };

        DSERU.route(data_map, function(response, status) {

            if (status === 'OK') {
                DIRDISU.setDirections(response);

            }
        });

    }



    $("#desde").on('change keyup click blur', function() {
        Servicio.desdeText = this.value;
        setCookieV2("ServicioElegido", JSON.stringify(Servicio));
    });

    $("#hasta").on('change keyup click blur', function() {
        Servicio.hastaText = this.value;
        setCookieV2("ServicioElegido", JSON.stringify(Servicio));
    });
    $("#carga").on('change', function() {
        Servicio.carga = this.value;
        setCookieV2("ServicioElegido", JSON.stringify(Servicio));
    });


    $(".BTACEPTAR").on('click', function() {
        if (!Servicio.id) {
            alert("Seleccione tipo de vehiculo");
            return;
        }
        /*if (!Servicio.carga) {
            alert("Seleccione que va a transportar");
            return;
        }
        if (!Servicio.cantidad) {
            alert("Ingrese la cantidad");
            return;
        }*/
        if (!Servicio.calculo) {
            alert("Imposible calcular el costo");
            return;
        }
        
        if (!Servicio.desdeText || !Servicio.hastaText) {
            alert("Configure una ruta con origen y destino");
            return;
        }
        if (!Servicio.desdeText) {
            alert("Seleccione la dirección de origen");
            return;
        }
        if (!Servicio.hastaText) {
            alert("Seleccione la dirección de destino");
            return;
        }

        //
        Servicio.cliente = obj_data_usuario();


        axios.post(DOMINIO + 'solicitud_servicios/agregar', Servicio, progress__event($(".BTACEPTAR")))
            .then(function(response) {

                if (response.data.r == false) {
                    alert(response.data.msg);
                } else {

                    response.data.millis = 300000;
                    setCookieV2("SOLICITUDNUEVA", JSON.stringify(response.data));
                    location.href = "esperandoChofer.html";
                }
            })
            .catch(function(error) {
                console.log(error);

            });
        return false;

    });




});