$(document).on('ready', function(){
  $("#save").on('click', function(){
    if($("#nue_cla").val().trim().length<8)
      Materialize.toast("Cantidad mínino requerido: 8", 5000);
    else{
      var data = {
        ida:getCookie('id'),
        id:getCookie('id'),
        token:getCookie('token'),
        pass: $("#act_cla").val(),
        pass_new: $("#nue_cla").val(),
        pass_new_con: $("#rep_cla").val()
      };

      axios.post(DOMINIO+'clientes/cambiar-clave',data)
        .then(response=>{
          Materialize.toast(response.data.msg, 5000);
          pass: $("#act_cla").val('');
          pass_new: $("#nue_cla").val('');
          pass_new_con: $("#rep_cla").val('');

        })
        .catch(err=>{
          console.log(err);
        })

    }
    return false;
  });
});
