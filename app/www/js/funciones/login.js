$(document).on('ready', function() {

    $("#btn_login").on('click', function() {

        var data = {
            correo: $("#email").val(),
            pass: $("#clave").val()
        };

        axios.post(DOMINIO + 'login/app', data, progress__event($("#btn_login")))
            .then(function(response) {

                if (response.data.r == false) {
                    alert(response.data.msg);
                } else {
                    setCookie("data_user", JSON.stringify(response.data.user));
                    setCookie("id", response.data.user.id);
                    setCookie("token", response.data.user.token);
                    location.href = "inicio.html";

                }

            })
            .catch(function(error) {
                //console.log(error);
                alert('No se encontró un usuario con este correo', 5000);

            });
        return false;
    });
    $("#BTFlogin").on('click', function() {
        facebookConnectPlugin.login(["public_profile", "email"],
            function(response) {

                if (response.authResponse.userID != '') {
                    facebookConnectPlugin.api(response.authResponse.userID + "/?fields=id,name,email,first_name,picture.width(720).height(720)", ["public_profile"],
                        function(response) {
                            loginOauth(response);
                            //alert(JSON.stringify(response));
                        },
                        function(response) {

                        });
                }

            },
            function(response) {
                console.log('ERROR:' + response);
            });
    });

    function loginOauth(data) {
        data.correo = data.email;
        axios.post(DOMINIO + 'login/app/Oauth', data, progress__event($("#BTFlogin")))
            .then(function(response) {

                if (response.data.r == false) {
                    alert(response.data.msg);
                } else {
                    setCookie("data_user", JSON.stringify(response.data.user));
                    setCookie("id", response.data.user.id);
                    setCookie("token", response.data.user.token);
                    location.href = "inicio.html";

                }

            })
            .catch(function(error) {
                //console.log(error);
                alert('No se encontró un usuario con este correo', 5000);

            });
    }
    document.addEventListener('deviceready', function() {
        facebookConnectPlugin.logout(function(s) { console.log(s) }, function(e) { console.log(e); });
    });
});