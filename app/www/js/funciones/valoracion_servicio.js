$(document).ready(function() {
    variableGLOBAL();
    var Servicio = {};

    if (getCookieV2("SolicitudValorada")) {
        if (obj_data_usuario().tablausuario == "clientes") {
            $(".VistaCliente").removeClass('hide');
        } else {
            $(".VistaProve").removeClass('s4');
            $(".VistaProve").addClass('s12');
        }
        Servicio = JSON.parse(getCookieV2("SolicitudValorada"));
        //console.log(Servicio);
        $(".imgP").attr("src", "" + Servicio.Proveedor.img_pro + "");
        $(".nomP").text(Servicio.Proveedor.nom_pro);
        $(".imgC").attr("src", "" + Servicio.Cliente.img_cli + "");
        $(".nomC").text(Servicio.Cliente.nom_cli);
        $(".desde").text(Servicio.desdeText);
        $(".hasta").text(Servicio.hastaText);
        $(".kmC").text(JSON.parse(Servicio.distancia_sol).text);
        $(".preC").text(formato.precio(Servicio.costo_sol));
        $(".DURACION").text(JSON.parse(Servicio.duracion_sol).text);
        tltwha = Servicio.Proveedor.tel_pro;
        if (tltwha) {
            tltwha = tltwha.replace("+", "");
        }
        $(".linkP").attr('href', 'whatsapp//send?phone=' + tltwha + '');

        if ((Servicio.puntuaciones == 1 && obj_data_usuario().tablausuario == "clientes") || (Servicio.puntuaciones == null && obj_data_usuario().tablausuario == "proveedores")) {
            $(".Puntuar").removeClass('hide');
        }
        if (Servicio.Proveedor.Detalles.Reputacion) {
            var repu = '',
                voto1 = '',
                voto2 = '',
                voto3 = '';
            var Re = Servicio.Proveedor.Detalles.Reputacion;

            for (var i = 0; i < Re; i++) {
                repu += '<i class="fa fa-star"></i> ';
            }


            $(".Reputacion").html(repu);

        }
        if (Servicio.Cliente.DetallesC) {
            var repu = '';
            var Re = Servicio.Cliente.DetallesC.voto2;

            for (var i = 0; i < Re; i++) {
                repu += '<i class="fa fa-star"></i> ';
            }


            $(".ReputacionC").html(repu);

        }

        $(document).on('click', '.BTpuntuar', function() {
            if (obj_data_usuario().tablausuario == "clientes") {
                var p = parseInt(((parseInt($("#voto1").val()) + parseInt($("#voto2").val()) + parseInt($("#voto3").val())) / 3).toFixed(0));

            } else {
                var p = parseInt($("#voto2").val()).toFixed(0);
            }
            var promedio = '';
            for (var i = 0; i < p; i++) {
                promedio += '<i class="fa fa-star"></i> ';
            }
            var editar = {
                id: Servicio.id,
                token: obj_data_usuario().token,
                comentario_rep: $("#comentario_rep").val(),
                voto1: $("#voto1").val(),
                voto2: $("#voto2").val(),
                voto3: $("#voto3").val(),
                id_proveedor: Servicio.Proveedor.id,
                promedio: promedio,
                tipo: obj_data_usuario().tablausuario,
                id_cliente: Servicio.Cliente.id

            }
            axios.post(DOMINIO + 'solicitud_servicios/puntuar-chofer-cliente', editar, progress__event($(".BTpuntuar")))
                .then(function(response) {
                    if (response.data.r == false) {
                        alert(response.data.msg);
                        return;
                    } else {
                        setCookieV2("SolicitudValorada", JSON.stringify({}));
                        setCookieV2("SolicitudVerificada", JSON.stringify({}));
                        location.href = "historial.html";
                    }
                })
                .catch(function(error) {
                    console.log(error);

                });
        });

    }

});