var Camera;
$(document).on('ready', function() {
    convertirAlert();
    variableGLOBAL();
    var LAT = null,
        LON = null;
    $(".telefono").mask("+570000000000");
    $(".cedula").mask("0000000000");
    $("#fec_nac_cli").on('change', function() {
        validarEdad(this.value);

    });
    $('.foto').on('click', function() {
        $('#imagen').click();
    });
    var img = "";
    opcion = false;

    $(".OpenCam").on('click', function() {
        if (!Camera) {
            $("#imagen").click();
            return;
        }
        var optionsC = {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: $(this).attr("tipo") == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: true,
            allowEdit: true
        }
        navigator.camera.getPicture(function(result) {
            onSuccess(result)
        }, function(error) {
            // alert(error);
        }, optionsC);
    });
    $("#imagen").on('change', function() {

        if (this.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
                onSuccess(evt.target.result.split(",")[1]);
            };
            reader.readAsDataURL(this.files[0]);
        }

    });
    $(".BTUBI").on('click', function() {
        setTimeout(function() { correr_mapa_(); }, 1200);
    });

    onSuccess = function onSuccess(imageURI) {

        document.getElementById('IMGPROFILE').src = "data:image/jpeg;base64," + imageURI;
        img = "data:image/jpeg;base64," + imageURI;

    }
    $(".BTreg").on('click', function() {
        if (img.trim().length < 1)
            alert('Debe seleccionar una imagen', 5000);
        else if ($("#nom_usu").val().trim().length < 3)
            alert("Ingresa tu nombre", 5000);
        else if ($("#ced_usu").val().trim().length < 8)
            alert("Ingresa tu cedula", 5000);
        else if ($("#tel_usu").val().trim().length < 6)
            alert("Completa tu teléfono WhatsApp", 5000);
        else if (!validarEmail($("#cor_usu").val()))
            alert("Ingresa un correo válido", 5000);
        else if ($("#con_usu").val().trim().length < 6)
            alert("Ingresa tu contraseña", 5000);
        else if ($("#con_usu_2").val().trim().length < 8)
            alert("Ingresa el confirmar contraseña", 5000);
        else if ($("#con_usu").val() != $("#con_usu_2").val())
            alert("Las contraseñas no coinciden", 5000);
        else if ($("#tip_usu").val() == "")
            alert("¿Dinos como deseas registrarte?");
        else if ($("#dir_usu").val().trim().length < 3)
            alert("Ingresa tu dirección", 5000);
        else if ($("#condi").is(':checked') == false)
            alert("Debes aceptar las condiciones de uso", 5000);
        else {

            if ($("#tip_usu").val() == "1") {
                route = "clientes";
            } else {
                route = "proveedores";
            }

            var formData = new FormData();
            formData.append('tip_usu', route);
            formData.append('img', img);
            formData.append('nom', $("#nom_usu").val());
            formData.append('cor', $("#cor_usu").val());
            formData.append('tel', $("#tel_usu").val());
            formData.append('cedula', $("#ced_usu").val());
            formData.append('pas', $("#con_usu").val());
            formData.append('dir_usu', $("#dir_usu").val());
            formData.append('lat', LAT);
            formData.append('lon', LON);
            axios.post(DOMINIO + '' + route, formData, progress__event($(".BTreg")))
                .then(function(response) {

                    if (response.data.r == false) {
                        alert(response.data.msg);
                    } else {
                        setCookie("data_user", JSON.stringify(response.data.user));
                        setCookie("id", response.data.user.id);
                        setCookie("token", response.data.user.token);
                        if (route == "clientes") {
                            location.href = "inicio.html";
                        } else {
                            location.href = "registro_proveedor.html";
                        }

                    }
                })
                .catch(function(error) {
                    console.log(error);

                });
            return false;
        }
    });
    var mapa = null;

    function correr_mapa_() {
        if (!mapa) {
            if (!google) {
                alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");
            }
            var styles = [
                { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
                { elementType: 'labels.text.fill', stylers: [{ color: '#e30016' }] }
            ];

            var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

            var options = {
                zoom: 16,
                center: new google.maps.LatLng(23.0000000, -102.0000000),
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                backgroundColor: 'transparent'
            };
            geocoder = new google.maps.Geocoder();

            mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(-78.9909352282, -4.29818694419, -66.8763258531, 12.4373031682));
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('FiltrarMapa'), { bounds: defaultBounds, types: ['establishment', 'geocode'] });
            autocomplete.setComponentRestrictions({ 'country': ['co'] });

            mapa.mapTypes.set('map_style', styledMap);
            mapa.setMapTypeId('map_style');

            google.maps.event.addListener(mapa, 'click', function(event) {

                LAT = event.latLng.lat();
                LON = event.latLng.lng();
                cargarDatam(LAT, LON);
            });
            MAPA = mapa;
            LocateGet();
        }

    }
    LocateGet();

    function LocateGet() {

        var device = document.addEventListener('deviceready', function() {
            cordova.plugins.locationAccuracy.canRequest(function(canRequest) {
                if (canRequest) {
                    cordova.plugins.locationAccuracy.request(function(success) {


                        navigator.geolocation.getCurrentPosition(function(position) {

                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });


                    }, function(error) {

                        navigator.geolocation.getCurrentPosition(function(position) {
                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });

                        if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {

                        }
                    }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);

                }
            });
        });
        if (!device) {
            navigator.geolocation.getCurrentPosition(function(position) {

                cargarDatam(position.coords.latitude, position.coords.longitude);
            }, function(error) {
                onError(error);
            });
        }

    }

    function onError(error) {
        M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
        
    };

    function cargarDatam(lat, lon) {

        if (!LAT && !LON) {
            LAT = lat;
            LON = lon;
        }

        if (mapa) {
            if (typeof marker != 'undefined')
                marker.setMap(null);

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lon),
                map: mapa
            });
            mapa.panTo(new google.maps.LatLng(lat, lon));
        }
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
                'location': { lat: parseFloat(lat), lng: parseFloat(lon) }
            },
            function g(results, status) {


                if (status == 'OK') {
                    $("#dir_usu").val(results[0].formatted_address);

                    return;
                } else {


                    return;
                }


            });

    }
    $("#FiltrarMapa").on('change', function() {
        var tis = this;
        setTimeout(function() {
            if (tis.value.trim() != "") {
                buscar_address(tis.value.trim());
                tis.value = "";
            }
        }, 100);
    });

    function buscar_address(address) {

        $("#FiltrarMapa_icon").addClass("fa-spinner fa-spin");
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address, componentRestrictions: { 'country': 'co' }, region: 'co' }, geocodeResult);
    }

    function geocodeResult(results, status) {
        //$("#FiltrarMapa").focus();
        $("#FiltrarMapa_icon").removeClass("fa-check");

        if (status == 'OK') {

            //addMarker(results[0].geometry.location.lat(),results[0].geometry.location.lng());
            LAT = results[0].geometry.location.lat();
            LON = results[0].geometry.location.lng();
            if (typeof marker != 'undefined')
                marker.setMap(null);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(LAT, LON),
                map: mapa
            });
            mapa.panTo(new google.maps.LatLng(LAT, LON));
            $("#FiltrarMapa_icon").addClass("fa-check");
            $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
            return;
        } else {
            $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
            // $("#FiltrarMapa").focus();
            return;
        }
        $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
        //$("#FiltrarMapa").focus();
    }

    $("#BTFreg").on('click', function() {

        if (!$("#tip_usu2").val()) {
            alert("¿Dinos como deseas registrarte?");
            return;
        }

        facebookConnectPlugin.login(["public_profile", "email"],
            function(response) {

                if (response.authResponse.userID != '') {
                    facebookConnectPlugin.api(response.authResponse.userID + "/?fields=id,name,email,first_name,picture.width(720).height(720)", ["public_profile"],
                        function(response) {
                            response.route = $("#tip_usu2").val();
                            loginOauth(response);
                            //alert(JSON.stringify(response));
                        },
                        function(response) {

                        });
                }

            },
            function(response) {
                console.log('ERROR:' + response);
            });
    });
    $("#btTip").on('click', function() {
        $('#TipoU').modal('open');
    });

    function loginOauth(data) {
        data.cor = data.email;
        if (data.route == "1") {
            route = "clientes";
        } else {
            route = "proveedores";
        }
        axios.post(DOMINIO + '' + route + '/Oauth', data, progress__event($("#BTFreg")))
            .then(function(response) {

                if (response.data.r == false) {
                    alert(response.data.msg);
                } else {
                    setCookie("data_user", JSON.stringify(response.data.user));
                    setCookie("id", response.data.user.id);
                    setCookie("token", response.data.user.token);
                    if (route == "clientes") {
                        location.href = "inicio.html";
                    } else {
                        location.href = "registro_proveedor.html";
                    }

                }

            })
            .catch(function(error) {
                //console.log(error);
                alert('No se encontró un usuario con este correo', 5000);

            });
    }
    document.addEventListener('deviceready', function() {
        facebookConnectPlugin.logout(function(s) { console.log(s) }, function(e) { console.log(e); });
    });

});