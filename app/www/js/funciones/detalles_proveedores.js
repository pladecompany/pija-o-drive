var Camera;
$(document).on('ready', function() {
    variableGLOBAL();
    var img1 = "";
    var img2 = "";
    var img3 = "";
    var img4 = "";
    $(".telefono").mask("+570000000000");
    tiposVeh__();
    var TiposVeh = [];

    function tiposVeh__() {
        var html = '';
        axios.get(DOMINIO + 'tipovehiculo/app/?token=' + obj_data_usuario().token, {})
            .then(function(response) {
                var data = response.data;
                html += '<option selected="" value="">Tipo de vehículo</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '" data-icon="img/transporte/' + data[i].icono_veh + '">' + data[i].nombre_veh + '</option>';
                }

                $("#id_tipo_vehiculo").html(html);
                $("#id_tipo_vehiculo").formSelect();

            })
            .catch(function(error) {

            });
    }
    $(".OpenCam").on('click', function() {
        if (!Camera) {
            $("#imagen_" + $(this).attr("img") + "").click();
            return;
        }
        var tip = $(this).attr("img");
        var optionsC = {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: $(this).attr("tipo") == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: true,
            allowEdit: true
        }
        navigator.camera.getPicture(function(result) {
            onSuccess(result, tip)
        }, function(error) {
            // alert(error);
        }, optionsC);
    });
    $(".BTimagen").on('change', function() {
        var tip = $(this).attr("img");
        if (this.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
                onSuccess(evt.target.result.split(",")[1], tip);
            };
            reader.readAsDataURL(this.files[0]);
        }

    });


    onSuccess = function onSuccess(imageURI, tipo) {

        document.getElementById('img_img_' + tipo + '').src = "data:image/jpeg;base64," + imageURI;
        if (tipo == 1) {
            img1 = "data:image/jpeg;base64," + imageURI;
        }
        if (tipo == 2) {
            img2 = "data:image/jpeg;base64," + imageURI;
        }
        if (tipo == 3) {
            img3 = "data:image/jpeg;base64," + imageURI;
        }
        if (tipo == 4) {
            img4 = "data:image/jpeg;base64," + imageURI;
        }


    }
    $(".BTlisto").on('click', function() {
        if (img1.trim().length < 1)
            alert('Debe seleccionar una imagen de frente', 5000);
        else if (img2.trim().length < 1)
            alert('Debe seleccionar una imagen lateral', 5000);
        else if (img3.trim().length < 1)
            alert('Debe seleccionar una imagen trasera', 5000);
        else if ($("#id_tipo_vehiculo").val() == "")
            alert("Seleccione tipo de vehículo", 5000);
        else if ($("#marca").val().trim().length < 3)
            alert("Ingresa la marca", 5000);
        else if ($("#matricula").val().trim().length < 3)
            alert("Ingresa la matrícula", 5000);
        else if ($("#referencia").val().trim().length < 3)
            alert("Completa la referencia personal", 5000);
        else if ($("#tel_referencia").val().trim().length < 6)
            alert("Completa el teléfono de la referencia personal", 5000);
        else if (img4.trim().length < 1)
            alert('Debe seleccionar una imagen de su cédula', 5000);
        else {


            var formData = new FormData();
            formData.append('img1', img1);
            formData.append('img2', img2);
            formData.append('img3', img3);
            formData.append('img4', img4);

            formData.append('id_tipo_vehiculo', $("#id_tipo_vehiculo").val());
            formData.append('marca', $("#marca").val());
            formData.append('matricula', $("#matricula").val());
            formData.append('referencia', $("#referencia").val());
            formData.append('tel_referencia', $("#tel_referencia").val());
            formData.append('token', obj_data_usuario().token);
            formData.append('id', obj_data_usuario().id);

            axios.post(DOMINIO + 'proveedores/detalles/agregar', formData, progress__event($(".BTlisto")))
                .then(function(response) {

                    if (response.data.r == false) {
                        alert(response.data.msg);
                    } else {
                        setCookie("data_user", JSON.stringify(response.data));
                        setCookie("id", response.data.id);
                        setCookie("token", response.data.token);
                        location.href = "inicio.html";
                    }
                })
                .catch(function(error) {
                    console.log(error);

                });
            return false;
        }
    });

});