$('.modal').modal({ inDuration: 0, outDuration: 0 });
convertirAlert();
variableGLOBAL();

var CookieArray = [];
var limit = 3;

obtener_soli();

function obtener_soli() {

    var html = '';
    axios.get(DOMINIO + 'solicitud_servicios/propiasP/?limit=' + limit + '&token=' + obj_data_usuario().token + '&id=' + obj_data_usuario().id, {})
        .then(function(response) {
            var data = response.data;

            $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_soli(); " class="view_more"><center>Cargar más...</center></div>');

            if (data.length > 0 && data.length < 3 || data.length == 0)
                $(".view_more").addClass("hide");

            for (var i = 0; i < data.length; i++) {
                insertarSoli(data[i], 1);
            }

            if (limit == 3)
                $(".Soli").scrollTop($('.Soli')[0].scrollHeight);

            if (data.length == 0)
                $(".Soli").html(sin_resultados());

            limit = (limit * 2);
            $('.ver-img-pro').materialbox();
        })
        .catch(function(error) {
            $(".view_more").addClass("hide");
            $(".Soli").html(sin_resultados());

        });
}


buscarID = function buscarID(id) {

    for (var i = 0; i < CookieArray.length; i++) {
        if (CookieArray[i].id == id)
            return CookieArray[i];
    }
    return false;
}
insertarSoli = function insertarSoli(data, op) {
    if (buscarID(data.id)) {
        return false;
    }
    CookieArray.push(data);


    var html = '';
    html += '<div class="row m-1 BTVER" id="' + data.id + '" style="border: 1px solid #ddd; border-radius: .5rem;padding: .1rem;">';
    html += '	<a href="#det_solicitud" class="modal-trigger clr_red">';
    html += '	<div class="col s2 center">';
    html += '		<div class="input-field">';
    html += '				<img src="' + data.Cliente.img_cli + '" class="img-historial img-cover" width="100%" ' + imgenpordefecto() + '>';
    html += '		</div>';
    html += '		</div>';
    html += '		<div class="col s10">';
    html += '		<h6 class="clr_red text-bold">' + data.Cliente.nom_cli + '</h6>';
    //if (data.estatus_sol == 3 && data.puntuaciones == 0) {
    //html += '   <h6 class="clr_black text-bold">' + ESTATUSP[6] + '</h6>';
    //} else {
    html += '   <h6 class="clr_black text-bold">' + ESTATUSP[data.estatus_sol] + '</h6>';
    //}

    html += '			<h6 class="clr_black"><i class="fa fa-phone"></i> ' + ((data.Cliente.tel_cli) ? data.Cliente.tel_cli : 'N/A') + '</h6>';
    html += '           <h6 class="clr_black" style="visibility:hidden;">...</h6>'; //html += '          <h6 class="clr_black">Transportar (' + data.cantidad_sol + ') ' + data.carga_sol + '</h6>';

    if (data.Proveedor.ReviewRecibida && data.estatus_sol == 3) {
        var p = parseInt(((parseInt(data.Proveedor.ReviewRecibida.voto1) + parseInt(data.Proveedor.ReviewRecibida.voto2) + parseInt(data.Proveedor.ReviewRecibida.voto3)) / 3).toFixed(0));
        var promedio = '';
        for (var h = 0; h < p; h++) {
            promedio += '<i class="fa fa-star"></i> ';
        }
        html += '      <h6 class="clr_black">Puntuación recibida: ' + ((promedio) ? promedio : 'Ninguna') + '</h6>';
        if (data.Proveedor.ReviewRecibida.comentario_rep) {
            html += '      <h6 class="clr_black">Comentario del cliente: <b>' + data.Proveedor.ReviewRecibida.comentario_rep + '</b></h6>';
        }

    }
    html += '		</div>';
    html += '	</a>';
    if (data.estatus_sol == 2) {
        html += '<div class="row center">';
        html += '<a style="margin-top: 1.2rem;" id="' + data.id + '" href="#!" class="btn btn-red btn-small Vermapa">Ver mapa <i class="fa fa-map-marker"></i></a>';
        html += '</div>';
        html += '<div class="row center">';
        html += '<button type="button" id="' + data.id + '" class="btn btn-red btn-small LLegue">Ya llegué <i class="fas fa-map-marker-alt"></i></button>';
        html += '</div>';

    }
    if (data.puntuaciones == null && data.estatus_sol == 7) {
        html += '<div class="row center">';
        html += '<button type="button" id="' + data.id + '" style="margin-top:0.5rem;" class="btn btn-red btn-small BTVALORAR bt__' + data.id + '">Valorar</button>';
        html += '</div>';
    }
    html += '      <span class="right clr_red timeago" title="' + data.fecha_sol + '"></span>';

    html += '</div>';

    if (op)
        $(".Soli").append(html);
    else
        $(".Soli").prepend(html);

}

function sin_resultados() {
    var html = '';
    html += '<div class="row mt-1" style="border: 1px solid #ddd; border-radius: .5rem;padding: .1rem;">';
    html += '    <a href="#!">';
    html += '	<div class="col s12 center">';
    html += '                <h6 class="clr_black">';
    html += ' No se encontraron resultados. ';
    html += '                </h6>';
    html += '            </div>';
    html += '    </a>';
    html += '</div>';
    return html;
}
var Ver = null;
$(document).on('click', '.BTVER', function() {
    Ver = buscarID(this.id);
    VERSOLI(Ver);
});

function VERSOLI(Ver) {
    $(".Leye1").addClass('hide');
    $(".Leye2").addClass('hide');

    var html = '';
    html += '<img src="' + Ver.Cliente.img_cli + '" class="img-Verhistorial img-cover mt-2" ' + imgenpordefecto() + '>';
    html += '<h6 class="text-bold"><span>CLIENTE:</span> <span class="clr_red">' + Ver.Cliente.nom_cli + '</span></h6>';
    html += '<h6 class="clr_black"><i class="fa fa-phone"></i> ' + ((Ver.Cliente.tel_cli) ? Ver.Cliente.tel_cli : 'N/A') + '</h6>';
    //if (Ver.estatus_sol == 3 && Ver.puntuaciones == 0) {
    //    html += '   <h6 class="clr_black text-bold">' + ESTATUSP[6] + '</h6>';
    //} else {
    html += '   <hr><h6 class="text-bold"><span>ESTATUS: </span> <span class="clr_red">' + ESTATUSP[Ver.estatus_sol] + '</span></h6>';
    //}
    //html += '<hr><h6 class="clr_black">Transportar (' + Ver.cantidad_sol + ') ' + Ver.carga_sol + '</h6>';
    html += '<hr><h6 class="text-justify"><b>DESDE:</b> ' + Ver.desdeText + ',<hr><b>HASTA:</b> ' + Ver.hastaText + '</h6>';
    html += '<hr><h6 class="text-bold">DISTANCIA APROXIMADA: <span class="clr_red">' + JSON.parse(Ver.distancia_sol).text + '</span></h6>';
    html += '<hr>';
    if (Ver.estatus_sol == 2) {
        html += '<div class="row">';
        html += '<div class="col s6 center">';
        html += '<a id="' + Ver.id + '" href="#!" class="btn btn-red btn-small Vermapa">Ver mapa <i class="fa fa-map-marker"></i></a>';
        html += '</div>';
        html += '<div class="col s6 center">';
        html += '<button type="button" id="' + Ver.id + '" class="btn btn-red btn-small LLegue">Ya llegué <i class="fas fa-map-marker-alt"></i></button>';
        html += '</div>';
        html += '</div>';
    }
    if (Ver.estatus_sol != 3 && Ver.estatus_sol != 5) {
        html += '<button style="margin:0.5rem;" type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTCANCELARS">cancelar</button>';
    }
    html += '<div class="col s12 center">';
    html += '    <h5>El costo del Transporte</h5>';
    html += '    <h4 class="clr_red text-bold mb-0">$ ' + formato.precio(Ver.costo_sol) + '</h4>';
    html += '</div>';
    if (Ver.estatus_sol == 6) {
        html += '<h6 class="text-center">Viaje realizado</h6>';
        html += '<button type="button" id="' + Ver.id + '" class="btn btn-red btn-small FINVIAJE">Finalizar viaje <i class="fas fa-map-marker-alt"></i></button>';
    }
    var buttons = '';
    if (Ver.puntuaciones == null) {
        buttons += '<button type="button" id="' + Ver.id + '" style="margin-top:0.5rem;" class="btn btn-red btn-small ' + ((Ver.estatus_sol == 7) ? "" : "hide") + ' BTVALORAR bt__' + Ver.id + '">Valorar</button>';
    }


    $(".CuerpoSoli").html(html);
    $(".Buttons").html(buttons);
    if (obj_data_usuario().lat_pro && obj_data_usuario().lon_pro && Ver.estatus_sol <= 2) {
        $(".Leye2").removeClass('hide');
    } else {
        $(".Leye1").removeClass('hide');
    }

    if (Ver.estatus_sol <= 2 || (Ver.estatus_sol >= 4 && Ver.estatus_sol <= 5)) {
        $("#mapaservicio").removeClass("hide");
    } else {
        $("#mapaservicio").addClass("hide");
        $(".Leye1").addClass('hide');
        $(".Leye2").addClass('hide');
    }
    setTimeout(function() {
        if (Ver.estatus_sol <= 2 || (Ver.estatus_sol >= 4 && Ver.estatus_sol <= 5)) {
            correr_mapa_();
        }
    }, 450);
}

$(document).on('click', '.BTCANCELARS', function() {
    var editar = {
        id: this.id,
        token: obj_data_usuario().token,
        cliente: obj_data_usuario()
    }
    axios.post(DOMINIO + 'solicitud_servicios/cancelar-solicitud', editar, progress__event($(".BTCANCELARS")))
        .then(function(response) {
            CookieArray = [];
            obtener_soli();
            ID = editar.id;
            ABRIR_SOLICITUD();
        })
        .catch(function(error) {
            console.log(error);

        });

});
$(document).on('click', '.Vermapa', function() {

    setCookieV2("ServicioMapa", JSON.stringify(buscarID(this.id)));
    location.href = "Vermapa.html";

});
$(document).on('click', '.FINVIAJE', function() {
    var bt = $(this);
    var sol = buscarID(this.id);
    sol.token = obj_data_usuario().token

    axios.post(DOMINIO + 'solicitud_servicios/finviaje', sol, progress__event(bt))
        .then(function(response) {
            $(".bt__" + sol.id + "").removeClass("hide");
            bt.remove();
        })
        .catch(function(error) {
            console.log(error);

        });

});
$(document).on('click', '.LLegue', function() {
    var bt = $(this);
    var sol = buscarID(this.id);
    sol.token = obj_data_usuario().token

    axios.post(DOMINIO + 'solicitud_servicios/llegue', sol, progress__event(bt))
        .then(function(response) {
            $(".BTVER").remove();
            CookieArray = [];
            obtener_soli();
            ID = sol.id;
            ABRIR_SOLICITUD();
        })
        .catch(function(error) {
            console.log(error);

        });

});
$(document).on('click', '.BTVALORAR', function() {
    Ver = buscarID(this.id);
    setCookieV2("SolicitudValorada", JSON.stringify(Ver));
    location.href = "valoracion_servicio.html";

});

function onError(error) {
    M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
    setTimeout(function() { history.back(); }, 5500);

};

var mapa;
DSERU = "";
DIRDISU = "";



function correr_mapa_() {
    if (!mapa) {
        if (!google) {
            alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");

        }
        DSERU = new google.maps.DirectionsService();
        DIRDISU = new google.maps.DirectionsRenderer();
        var styles = [
            { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
        ];

        var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

        var options = {
            zoom: 16,
            center: new google.maps.LatLng(23.0000000, -102.0000000),
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            backgroundColor: 'transparent'
        };
        geocoder = new google.maps.Geocoder();

        mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
        mapa.mapTypes.set('map_style', styledMap);
        mapa.setMapTypeId('map_style');


        MAPA = mapa;
        DIRDISU.setMap(mapa);
    }
    if (DIRDISU) {
        DIRDISU.setMap(null);
        DIRDISU = new google.maps.DirectionsRenderer();
        DIRDISU.setMap(mapa);
    }
    var data_map = {
        origin: "" + Ver.lat1 + "," + Ver.lon1 + "",
        destination: "" + Ver.lat2 + "," + Ver.lon2 + "",
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
    };
    if (obj_data_usuario().lat_pro && obj_data_usuario().lon_pro && Ver.estatus_sol <= 2) {
        data_map.origin = "" + obj_data_usuario().lat_pro + "," + obj_data_usuario().lon_pro + "";
        data_map.waypoints = [{
            location: "" + Ver.lat1 + "," + Ver.lon1 + "",
            stopover: true
        }]
    }

    DSERU.route(data_map, function(response, status) {
        if (status === 'OK') {
            DIRDISU.setDirections(response);
        }
    });

}

var ID = null;

if (GETV('id') && GETV('id') != "") {
    ID = GETV('id');
}
if (ID) {
    alert("Cargando solicitud...");
    ABRIR_SOLICITUD(true);
}

function ABRIR_SOLICITUD(op) {
    axios({
            method: 'GET',
            url: DOMINIO + 'solicitud_servicios/propiasP/?solicitud=' + ID + '&limit=' + limit + '&token=' + obj_data_usuario().token + '&id=' + obj_data_usuario().id
        }).then(function(r) {
            if (r.data) {
                if (r.data.length > 0) {
                    Ver = r.data[0];
                    $(".toast").remove();
                    if (op) {
                        $("#det_solicitud").modal("open");
                    }
                    VERSOLI(Ver);

                }
            }
        })
        .catch(function(res) {
            console.log(res);
        });

}