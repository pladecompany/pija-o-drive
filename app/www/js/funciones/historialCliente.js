$('.modal').modal({ inDuration: 0, outDuration: 0 });
convertirAlert();
variableGLOBAL();

var CookieArray = [];
var limit = 3;

obtener_soli();

function obtener_soli() {

    var html = '';
    axios.get(DOMINIO + 'solicitud_servicios/propiasC/?limit=' + limit + '&token=' + obj_data_usuario().token + '&id=' + obj_data_usuario().id, {})
        .then(function(response) {
            var data = response.data;

            $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_soli(); " class="view_more"><center>Cargar más...</center></div>');

            if (data.length > 0 && data.length < 3 || data.length == 0)
                $(".view_more").addClass("hide");

            for (var i = 0; i < data.length; i++) {
                insertarSoli(data[i], 1);
            }

            if (limit == 3)
                $(".Soli").scrollTop($('.Soli')[0].scrollHeight);

            if (data.length == 0)
                $(".Soli").html(sin_resultados());

            limit = (limit * 2);
            $('.ver-img-pro').materialbox();
        })
        .catch(function(error) {
            $(".view_more").addClass("hide");
            $(".Soli").html(sin_resultados());

        });
}


buscarID = function buscarID(id) {

    for (var i = 0; i < CookieArray.length; i++) {
        if (CookieArray[i].id == id)
            return CookieArray[i];
    }
    return false;
}
insertarSoli = function insertarSoli(data, op) {
    if (buscarID(data.id)) {
        return false;
    }
    CookieArray.push(data);


    var html = '';
    html += '<div class="row m-1 BTVER" id="' + data.id + '" style="border: 1px solid #ddd; border-radius: .5rem;padding: .1rem;">';
    html += '	<a href="#det_solicitud" class="modal-trigger clr_red" >';
    html += '	<div class="col s2 center">';
    html += '		<div class="input-field">';
    if (data.Proveedor) {
        html += '        <img src="' + data.Proveedor.img_pro + '" class="img-historial img-cover" width="100%" ' + imgenpordefecto() + '>';
    } else {
        html += '        <img src="img/logo.png" width="100%" class="img-historial img-cover" ' + imgenpordefecto() + '>';
    }
    html += '		</div>';
    html += '		</div>';
    html += '		<div class="col s10">';
    if (data.Proveedor) {
        html += '    <h6 class="clr_red text-bold">' + data.Proveedor.nom_pro + '</h6>';
    }

    html += '<h6 class="clr_black text-bold">' + ESTATUSC[data.estatus_sol] + '</h6>';

    //html += '			<h6 class="clr_black">Transportar (' + data.cantidad_sol + ') ' + data.carga_sol + '</h6>';
    if (data.Proveedor) {
        html += '			<h6 class="clr_black"><i class="fa fa-phone"></i> ' + ((data.Proveedor.tel_pro) ? data.Proveedor.tel_pro : 'N/A') + '</h6>';
    } else {
        html += '      <h6 class="clr_black"><i class="fa fa-phone"></i> N/A</h6>';
    }

    if (data.Cliente.ReviewRecibida && (data.estatus_sol == 3 || data.estatus_sol == 7)) {
        var p = parseInt(data.Cliente.ReviewRecibida.voto2).toFixed(0);
        var promedio = '';
        for (var h = 0; h < p; h++) {
            promedio += '<i class="fa fa-star"></i> ';
        }
        html += '      <h6 class="clr_black">Puntuación recibida: ' + ((promedio) ? promedio : 'Ninguna') + '</h6>';
        if (data.Cliente.ReviewRecibida.comentario_rep) {
            html += '      <h6 class="clr_black">Comentario del conductor: <b>' + data.Cliente.ReviewRecibida.comentario_rep + '</b></h6>';
        }

    }

    html += '		</div>';
    html += '	</a>';
    if (data.puntuaciones == 1 && (data.Cliente.ReviewRecibida && data.estatus_sol == 7)) {
        html += '<div class="row center">';
        html += '<button type="button" id="' + data.id + '" class="btn btn-red btn-small BTVALORAR">Valore su viaje</button>';
        html += '</div>';
    }
    html += '			<span class="right clr_red timeago" title="' + data.fecha_sol + '"></span>';
    html += '</div>';


    if (op)
        $(".Soli").append(html);
    else
        $(".Soli").prepend(html);
}

function sin_resultados() {
    var html = '';
    html += '<div class="row mt-1" style="border: 1px solid #ddd; border-radius: .5rem;padding: .1rem;">';
    html += '    <a href="#!">';
    html += '	<div class="col s12 center">';
    html += '                <h6 class="clr_black">';
    html += ' No se encontraron resultados. ';
    html += '                </h6>';
    html += '            </div>';
    html += '    </a>';
    html += '</div>';
    return html;
}
var Ver = null;
$(document).on('click', '.BTVER', function() {
    Ver = buscarID(this.id);
    VERSOLI(Ver);
});

function VERSOLI(Ver,oprun) {
    $(".Leye1").addClass('hide');
    $(".Leye2").addClass('hide');
    var html = '';
    if (Ver.Proveedor) {

        html += '<img src="' + Ver.Proveedor.img_pro + '"  class="img-Verhistorial img-cover mt-2" ' + imgenpordefecto() + '>';
        //html += '<img src="' + Ver.Proveedor.Detalles.img_fre + '" onerror="this.src=\'img/carro.jpg\'"  class="img-Verhistorial img-cover mt-2">';
        html += '<h6 class="text-bold">CONDUCTOR: <span class="clr_red">' + Ver.Proveedor.nom_pro + '</span></h6>';
        html += '<h6 class="clr_black"><i class="fa fa-phone"></i> ' + ((Ver.Proveedor.tel_pro) ? Ver.Proveedor.tel_pro : 'N/A') + '</h6>';
        html += '<hr><h6 class="text-bold"> ESTATUS: <span class="clr_red">' + ESTATUSC[Ver.estatus_sol] + '</span></h6>';
        if (Ver.Proveedor.Detalles.Reputacion && Ver.estatus_sol == 1) {
            html += '<h6 class="clr_black">';
            var Re = Ver.Proveedor.Detalles.Reputacion;
            for (var i = 0; i < Re; i++) {
                html += '<i class="fa fa-star"></i> ';
            }
            html += '</h6>';
        }

        if (Ver.Proveedor.Detalles) {

            html += '<div class="row col s12">';

            html += '    <div class="row col s12 center">';
            //html += '    <div class="row col s6">';
            //html += '       <button type="button" style="margin-right:0.8rem;margin-top:0.8rem;" id="' + Ver.id + '" class="btn btn-red btn-small BTCAR">TRANSPORTE</button>';
            //html += '    </div>';
            //html += '    <div class="row col s12 center">';
            html += '       <button type="button" style="margin-left:0.8rem;margin-top:0.8rem;" id="' + Ver.id + '" class="btn btn-red btn-small VERMAS">CONTACTAR</button>';
            //html += '    </div>';
            html += '    </div>';

            html += '    <div class="row col s12 center">';
            html += '        <p><span class="text-bold">Transporte</p>';
            html += '        <hr>';
            html += '        <p><span class="text-bold">MARCA: </span> <span class="marP"></span></p>';
            html += '        <hr>';
            html += '        <p><span class="text-bold">PLACA: </span> <span class="mtrP"></span></p>';
            html += '        <hr>';
            html += '        <center>';
            html += '           <div class="carousel" style="width: 100%;">';
            html += '                <a class="carousel-item" href="#one!">';
            html += '                    <img src="carro.jpg" onerror="this.src=\'img/carro.jpg\'" class="imgPC_1 img-carro">';
            html += '               </a>';
            html += '                <a class="carousel-item" href="#two!">';
            html += '                   <img src="carro.jpg" onerror="this.src=\'img/carro.jpg\'" class="imgPC_2 img-carro">';
            html += '                </a>';
            html += '               <a class="carousel-item" href="#three!">';
            html += '                   <img src="carro.jpg" onerror="this.src=\'img/carro.jpg\'" class="imgPC_3 img-carro">';
            html += '                </a>';
            html += '            </div>';
            html += '       </center>';
            html += '    </div>';

            html += '</div>';

        }

    } else {
        html += '<img src="" width="20%" ' + imgenpordefecto() + '>';
        html += '<h6 class="clr_black"><i class="fa fa-phone"></i> N/A</h6>';


    }

    //html += '<hr><h6 class="clr_black">Transportar (' + Ver.cantidad_sol + ') ' + Ver.carga_sol + '</h6>';
    html += '<hr><h6 class="text-justify"><b>DESDE:</b> ' + Ver.desdeText + '<br><hr><b>HASTA:</b> ' + Ver.hastaText + '</h6>';
    html += '<hr><h6 class="text-bold">DISTANCIA APROXIMADA: <span class="clr_red">' + JSON.parse(Ver.distancia_sol).text + '</span></h6>';
    html += '<hr>';
    html += '<div class="col s12 center">';
    html += '    <h5>El costo del Transporte</h5>';
    html += '    <h4 class="clr_red text-bold m-0">$ ' + formato.precio(Ver.costo_sol) + '</h4>';
    html += '</div>';

    var buttons = '';
    if (Ver.estatus_sol == 1) {
        buttons += '<button type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTACEPTAR">Aceptar</button>';
        buttons += '<button type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTRECHAZAR">Rechazar conductor</button><br><br>';
    }
    if (Ver.puntuaciones == 1) {
        buttons += '<h5 class="clr_red text-bold m-0">Viaje ya finalizado</h5> <br> <h6 class="clr_red">Estamos creando comunidad califique su viaje para seguir utilizando Pijaosdrive. </h6> <button type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTVALORAR">Valore su viaje</button>';
    }

    if (Ver.estatus_sol == 4 || Ver.estatus_sol == 5) {
        buttons += '<button type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTRECHAZAR">Reenviar</button>';
    }

    if (Ver.estatus_sol != 3 && Ver.estatus_sol != 5) {
        buttons += '<button style="margin:0.5rem;" type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTCANCELARS">cancelar</button>';
    }


    $(".CuerpoSoli").html(html);
    $(".Buttons").html(buttons);

    if (Ver.Proveedor) {
        if (Ver.Proveedor.lat_pro && Ver.Proveedor.lon_pro && Ver.estatus_sol <= 2) {
            $(".Leye2").removeClass('hide');
        } else {
            $(".Leye1").removeClass('hide');
        }
    }
    if (Ver.Proveedor) {
        if (Ver.Proveedor.Detalles) {
            $(".imgPC_1").attr("src", "" + Ver.Proveedor.Detalles.img_fre + "");
            $(".imgPC_2").attr("src", "" + Ver.Proveedor.Detalles.img_lat + "");
            $(".imgPC_3").attr("src", "" + Ver.Proveedor.Detalles.img_tra + "");
            $(".mtrP").text(Ver.Proveedor.Detalles.matri_veh);
            $(".marP").text(Ver.Proveedor.Detalles.marca_veh);
            $('.carousel').carousel();
        }
    }

    if (Ver.estatus_sol <= 2 || (Ver.estatus_sol >= 4 && Ver.estatus_sol <= 5)) {
        $("#mapaservicio").removeClass("hide");
    } else {
        $("#mapaservicio").addClass("hide");
        $(".Leye1").addClass('hide');
        $(".Leye2").addClass('hide');
    }
    var tim = ((oprun==1)?2100:450);
    setTimeout(function() {
        if (Ver.estatus_sol <= 2 || (Ver.estatus_sol >= 4 && Ver.estatus_sol <= 5)) {
            correr_mapa_();
        }
    }, tim);
}

$(document).on('click', '.BTCAR', function() {
    $(".DivTra").toggleClass("hide");
    $('.carousel').carousel();
});
$(document).on('click', '.BTVALORAR', function() {
    Ver = buscarID(this.id);
    setCookieV2("SolicitudValorada", JSON.stringify(Ver));
    location.href = "valoracion_servicio.html";

});

$(document).on('click', '.BTRECHAZAR', function() {
    var editar = {
        id: this.id,
        token: obj_data_usuario().token,
        cliente: obj_data_usuario()
    }
    axios.post(DOMINIO + 'solicitud_servicios/rechazar-chofer-solicitud', editar, progress__event($(".BTRECHAZAR")))
        .then(function(response) {

            if (response.data.r == false) {
                alert(response.data.msg);
                return;
            } else {
                response.data.millis = 300000;
                setCookieV2("SOLICITUDNUEVA", JSON.stringify(response.data));
                location.href = "esperandoChofer.html";
            }
        })
        .catch(function(error) {
            console.log(error);

        });

});
$(document).on('click', '.BTCANCELARS', function() {
    var editar = {
        id: this.id,
        token: obj_data_usuario().token,
        cliente: obj_data_usuario()
    }
    axios.post(DOMINIO + 'solicitud_servicios/cancelar-solicitud', editar, progress__event($(".BTCANCELARS")))
        .then(function(response) {
            $(".BTVER").remove();
            CookieArray = [];
            obtener_soli();

            ID = editar.id;
            ABRIR_SOLICITUD();
        })
        .catch(function(error) {
            console.log(error);

        });

});


$(document).on('click', '.VERMAS', function() {
    if (Ver) {
        setCookieV2("SolicitudVerificada", JSON.stringify(Ver));
        location.href = "aceptar_servicio.html";
    }
});

$(document).on('click', '.BTACEPTAR', function() {
    var editar = {
        id: this.id,
        token: obj_data_usuario().token
    }
    axios.post(DOMINIO + 'solicitud_servicios/aceptar-chofer-solicitud', editar, progress__event($(".BTACEPTAR")))
        .then(function(response) {
            if (response.data.r == false) {
                alert(response.data.msg);
                return;
            } else {
                setCookieV2("SolicitudVerificada", JSON.stringify(Ver));
                location.href = "aceptar_servicio.html";
            }
        })
        .catch(function(error) {
            console.log(error);

        });

});

function onError(error) {
    M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
    setTimeout(function() { history.back(); }, 5500);

};

var mapa;
DSERU = "";
DIRDISU = "";



function correr_mapa_() {
    if (!mapa) {
        if (!google) {
            alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");

        }
        DSERU = new google.maps.DirectionsService();
        DIRDISU = new google.maps.DirectionsRenderer();
        var styles = [
            { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
        ];

        var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

        var options = {
            zoom: 16,
            center: new google.maps.LatLng(23.0000000, -102.0000000),
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            backgroundColor: 'transparent'
        };
        geocoder = new google.maps.Geocoder();

        mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
        mapa.mapTypes.set('map_style', styledMap);
        mapa.setMapTypeId('map_style');


        MAPA = mapa;
        DIRDISU.setMap(mapa);
    }
    if (DIRDISU) {
        DIRDISU.setMap(null);
        DIRDISU = new google.maps.DirectionsRenderer();
        DIRDISU.setMap(mapa);
    }
    var data_map = {
        origin: "" + Ver.lat1 + "," + Ver.lon1 + "",
        destination: "" + Ver.lat2 + "," + Ver.lon2 + "",
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
    };
    if (Ver.Proveedor) {
        if (Ver.Proveedor.lat_pro && Ver.Proveedor.lon_pro && Ver.estatus_sol <= 2) {
            data_map.origin = "" + Ver.Proveedor.lat_pro + "," + Ver.Proveedor.lon_pro + "";
            data_map.waypoints = [{
                location: "" + Ver.lat1 + "," + Ver.lon1 + "",
                stopover: true
            }]
        }
    }

    DSERU.route(data_map, function(response, status) {
        if (status === 'OK') {
            DIRDISU.setDirections(response);
        }
    });

}
var ID = null;

if (GETV('id') && GETV('id') != "") {
    ID = GETV('id');
}
if (ID) {
    alert("Cargando solicitud...");
    ABRIR_SOLICITUD(true);
}

function ABRIR_SOLICITUD(op) {
    axios({
            method: 'GET',
            url: DOMINIO + 'solicitud_servicios/propiasC/?solicitud=' + ID + '&limit=' + limit + '&token=' + obj_data_usuario().token + '&id=' + obj_data_usuario().id
        }).then(function(r) {
            if (r.data) {
                if (r.data.length > 0) {
                    Ver = r.data[0];
                    $(".toast").remove();
                    if (op) {
                        $("#det_solicitud").modal("open");
                    }
                    VERSOLI(Ver,1);

                }
            }
        })
        .catch(function(res) {
            console.log(res);
        });

}
