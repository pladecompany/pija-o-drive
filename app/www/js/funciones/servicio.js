$(document).ready(function() {
    convertirAlert();
    variableGLOBAL();
    var Servicio = {};

    if (getCookieV2("ServicioElegido")) {
        Servicio = JSON.parse(getCookieV2("ServicioElegido"));
        IMGtipoVeh = $("#IMGtipoVeh");
        IMGtipoVeh.attr("src", "img/transporte/" + Servicio.icono_veh + "");

    }
    tiposVeh__();
    var TiposVeh = [];

    function tiposVeh__() {
        var html = '';
        axios.get(DOMINIO + 'tipovehiculo/app/?token=' + obj_data_usuario().token, {})
            .then(function(response) {
                var data = response.data;
                html += '<option selected="" value="">Tipo de vehículo</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '" data-icon="img/transporte/' + data[i].icono_veh + '">' + data[i].nombre_veh + '</option>';
                }

                $("#id_tipo_vehiculo").html(html);
                $("#id_tipo_vehiculo").formSelect();

            })
            .catch(function(error) {

            });
    }
    domiss__();
    var misDo = [];

    function domiss__() {
        var html = '';
        axios.get(DOMINIO + 'domicilios/?id=' + obj_data_usuario().id + '&limit=todas&token=' + obj_data_usuario().token, {})
            .then(function(response) {
                var data = response.data;
                html += '<option selected="" value="" disabled>Elegir</option>';
                misDo = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].dir_dom + '</option>';
                }

                $("#id_domicilio1").html(html);

                $("#id_domicilio2").html(html);


            })
            .catch(function(error) {

            });
    }

    function Misdo(id) {
        for (var i = 0; i < misDo.length; i++) {
            if (misDo[i].id == id) {
                return misDo[i];
            }
        }
        return false
    }

    $(".BTPRE").on('click', function() {
        if (misDo.length <= 0) {
            alert("Será redirigido a Mis direcciones favoritas, ya que no posee ninguna.");
            setTimeout(function() { location.href = "mis_domicilios.html"; }, 3500);
        }
    });
    $("#id_domicilio1").on('change blur', function() {
        if (this.value) {
            var dir = Misdo(this.value);
            addMarker(dir.lat_dom, dir.lon_dom, 1, 2);
            $("#domi_1").modal("close");
            $("#FiltrarMapa1").val(dir.nom_dom);
        }
    });

    $("#id_domicilio2").on('change blur', function() {
        if (this.value) {
            var dir = Misdo(this.value);
            addMarker(dir.lat_dom, dir.lon_dom, 2, 2);
            $("#domi_2").modal("close");
            $("#FiltrarMapa2").val(dir.nom_dom);
        }
    });

    function returnTipo(id) {
        for (var i = 0; i < TiposVeh.length; i++) {
            if (TiposVeh[i].id == id) {
                return TiposVeh[i];
            }
        }
        return false;
    }
    $("#id_tipo_vehiculo").on('change', function() {
        setCookieV2("ServicioElegido", JSON.stringify(returnTipo(this.value)));
        Servicio = JSON.parse(getCookieV2("ServicioElegido"));
        IMGtipoVeh.attr("src", "img/transporte/" + Servicio.icono_veh + "");
    });

    function onError(error) {
        M.toast({ html: "Ocurrió un error accediendo a tu ubicación... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>" }, 5000);  
    };

    var mapa;
    DSERU = "";
    DIRDISU = "";
    var markers = [];


    correr_mapa_();

    function correr_mapa_() {
        if (!mapa) {
            if (!google) {
                alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");
            }
            DSERU = new google.maps.DirectionsService();
            DIRDISU = new google.maps.DirectionsRenderer();
            var styles = [
                { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
                { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
            ];

            var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

            var options = {
                zoom: 10,
                center: new google.maps.LatLng(24.1156735, -72.9301367),
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                backgroundColor: 'transparent'
            };
            geocoder = new google.maps.Geocoder();

            mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(-78.9909352282, -4.29818694419, -66.8763258531, 12.4373031682));
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('FiltrarMapa1'), { bounds: defaultBounds, types: ['establishment', 'geocode'] });
            autocomplete.setComponentRestrictions({ 'country': ['co'] });
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('FiltrarMapa2'), { bounds: defaultBounds, types: ['establishment', 'geocode'] });
            autocomplete.setComponentRestrictions({ 'country': ['co'] });

            mapa.mapTypes.set('map_style', styledMap);
            mapa.setMapTypeId('map_style');

            $(document).on('click', '.gm-fullscreen-control', function() {
                screen_map('mapaservicio');
            });
            google.maps.event.addListener(mapa, 'click', function(event) {
                addMarker(event.latLng.lat(), event.latLng.lng(), null, 1);
            });
            MAPA = mapa;
            DIRDISU.setMap(mapa);
        }
        var device = document.addEventListener('deviceready', function() {
            cordova.plugins.locationAccuracy.canRequest(function(canRequest) {
                if (canRequest) {
                    cordova.plugins.locationAccuracy.request(function(success) {


                        navigator.geolocation.getCurrentPosition(function(position) {

                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });


                    }, function(error) {

                        navigator.geolocation.getCurrentPosition(function(position) {
                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });

                        if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {

                        }
                    }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);

                }
            });
        });
        if (!device) {
            navigator.geolocation.getCurrentPosition(function(position) {

                cargarDatam(position.coords.latitude, position.coords.longitude);
            }, function(error) {
                onError(error);
            });
        }
    }
    var lastMarker, mar1, mar2;

    function addMarker(lat, lon, ad, op) {


        if (op == 1) {

            if (markers.length == 2) {
                if (mar1)
                    mar1.setMap(null);
                if (mar2)
                    mar2.setMap(null);
                markers = [];
            }
            if (markers.length <= 1) {

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lon),
                    map: mapa
                });

                markers.push(marker);

                if (markers.length == 1) {
                    $(".desde").text("");
                    $(".hasta").text("");
                    Servicio.desdeText = "";
                    Servicio.hastaText = "";
                    if (DIRDISU != null) {
                        DIRDISU.setMap(null);
                        DIRDISU = new google.maps.DirectionsRenderer();
                        DIRDISU.setMap(mapa);
                    }
                    mar1 = marker;
                    codeAddress(lat, lon, 1);
                    //onSuccess(); 
                    mapa.panTo(new google.maps.LatLng(lat, lon));
                }
                if (markers.length == 2) {
                    codeAddress(lat, lon, 2);
                    onSuccess();
                    mar2 = marker;

                }

            }

        } else {
            if (markers.length == 2) {

                delete markers[1];
                markers.splice(1, 1);
            }
            if (DIRDISU != null) {
                DIRDISU.setMap(null);
                DIRDISU = new google.maps.DirectionsRenderer();
                DIRDISU.setMap(mapa);

            }
            if (ad == 1) {
                if (mar1)
                    mar1.setMap(null);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lon),
                    map: mapa
                });

                markers.push(marker);
                mapa.panTo(new google.maps.LatLng(lat, lon));
                mar1 = marker;
            }
            if (ad == 2) {
                if (mar2)
                    mar2.setMap(null);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lon),
                    map: mapa
                });

                markers.push(marker);
                mapa.panTo(new google.maps.LatLng(lat, lon));
                mar2 = marker;
            }
            codeAddress(lat, lon, ad);
            onSuccess();

        }


    }
    // Sets the map on all markers in the array.
    function setMapOnAll(mapa) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(mapa);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    $("#FiltrarMapa1").on('change', function() {
        var tis = this;
        setTimeout(function() {
            if (tis.value.trim() != "") {
                buscar_address(tis.value.trim(), 1);
                //tis.value = "";
            }
        }, 100);
    });

    $("#FiltrarMapa2").on('change', function() {
        var tis = this;
        setTimeout(function() {
            if (tis.value.trim() != "") {
                buscar_address(tis.value.trim(), 2);
                //tis.value = "";
            }
        }, 100);
    });

    function buscar_address(address, op) {

        $("#FiltrarMapa_icon" + op + "").addClass("fa-spinner fa-spin");
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address, componentRestrictions: { 'country': 'co' }, region: 'co' }, geocodeResult);

        function geocodeResult(results, status) {
            //$("#FiltrarMapa").focus();
            $("#FiltrarMapa_icon" + op + "").removeClass("fa-check");

            if (status == 'OK') {

                addMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), op, 2);

                $("#FiltrarMapa_icon" + op + "").addClass("fa-check");
                $("#FiltrarMapa_icon" + op + "").removeClass("fa-spinner fa-spin");
                return;
            } else {
                $("#FiltrarMapa_icon" + op + "").removeClass("fa-spinner fa-spin");
                //$("#FiltrarMapa").focus();
                return;
            }
            $("#FiltrarMapa_icon" + op + "").removeClass("fa-spinner fa-spin");
            //$("#FiltrarMapa").focus();
        }
    }


    $("#carga").on('change', function() {
        Servicio.carga = this.value;
    });
    $("#can_ser").on('change keyup click blur', function() {
        Servicio.cantidad = this.value;
    });

    $(".BTCAL").on('click', function() {
        if (!Servicio.id) {
            alert("Seleccione tipo de vehiculo");
            return;
        }

        /*if (!Servicio.carga) {
            alert("Seleccione que va a transportar");
            return;
        }
        if (!Servicio.cantidad) {
            alert("Ingrese la cantidad");
            return;
        }*/
        if (!Servicio.desdeText || !Servicio.hastaText) {
            alert("Configure una ruta con origen y destino");
            return;
        }
        if (!Servicio.desdeText) {
            alert("Seleccione la dirección de origen");
            return;
        }
        if (!Servicio.hastaText) {
            alert("Seleccione la dirección de destino");
            return;
        }

        setCookieV2("ServicioElegido", JSON.stringify(Servicio));
        location.href = 'calcular.html';
    });

    $(".DELMARKER").on('click', function() {
        deleteMarkers();
        if (DIRDISU != null) {
            DIRDISU.setMap(null);
            DIRDISU = new google.maps.DirectionsRenderer();
            DIRDISU.setMap(mapa);
        }
        Servicio.lat1 = null;
        Servicio.lon1 = null;
        Servicio.lat2 = null;
        Servicio.lon2 = null;
        $(".desde").text("");
        $(".hasta").text("");
        Servicio.desdeText = "";
        Servicio.hastaText = "";
        $("#FiltrarMapa1").val('');
        $("#FiltrarMapa2").val('');
        if (mar1)
            mar1.setMap(null);
        if (mar2)
            mar2.setMap(null);
        if (lastMarker)
            lastMarker.setMap(null);
    });

    var onSuccess = function() {


        var data_map = {
            origin: "" + Servicio.lat1 + "," + Servicio.lon1 + "",
            destination: "" + Servicio.lat2 + "," + Servicio.lon2 + "",
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        };

        DSERU.route(data_map, function(response, status) {

            if (status === 'OK') {
                direccion = response.routes[0].legs[0];


                $(".desde").text(direccion.start_address);
                Servicio.desdeText = direccion.start_address;

                $(".hasta").text(direccion.end_address);
                Servicio.hastaText = direccion.end_address;

                Servicio.distancia = direccion.distance;
                Servicio.duracion = direccion.duration;

                DIRDISU.setDirections(response);
                deleteMarkers();
            }
        });


    };

    function cargarDatam(lat, lon) {

        addMarker(lat, lon, null, 1);
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
                'location': { lat: parseFloat(lat), lng: parseFloat(lon) }
            },
            function g(results, status) {
                //$("#FiltrarMapa").focus();
                $("#FiltrarMapa_icon1").removeClass("fa-check");

                if (status == 'OK') {
                    $("#FiltrarMapa1").val(results[0].formatted_address);
                    $("#FiltrarMapa_icon1").addClass("fa-check");
                    $("#FiltrarMapa_icon1").removeClass("fa-spinner fa-spin");
                    return;
                } else {
                    $("#FiltrarMapa_icon1").removeClass("fa-spinner fa-spin");
                    //$("#FiltrarMapa").focus();
                    return;
                }
                $("#FiltrarMapa_icon1").removeClass("fa-spinner fa-spin");
                //$("#FiltrarMapa").focus();
            });
    }

    function codeAddress(lat, lon, tipo) {
        if (tipo == 1) {
            Servicio.lat1 = lat;
            Servicio.lon1 = lon;

        }
        if (tipo == 2) {
            Servicio.lat2 = lat;
            Servicio.lon2 = lon;
        }
    }
});