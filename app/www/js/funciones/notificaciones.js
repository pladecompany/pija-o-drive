$(document).ready(function() {
    variableGLOBAL();


    var CookieArray = [];
    var limit = 3;
    obtener_mis_notis = function obtener_mis_notis() {

        var html = '';
        axios.get(DOMINIO + 'notificaciones/?id=' + obj_data_usuario().id + '&limit=' + limit + '&token=' + obj_data_usuario().token + '&tipo=' + obj_data_usuario().tablausuario, {})
            .then(function(response) {
                var data = response.data;

                $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_mis_notis(); " class="view_more"><center>Cargar más...</center></div>');

                if (data.length > 0 && data.length < 3 || data.length == 0)
                    $(".view_more").addClass("hide");


                for (var i = 0; i < data.length; i++) {

                    CargarNoti(data[i], 1);
                }

                if (limit == 3)
                    $(".misNotificaciones").scrollTop($('.misNotificaciones')[0].scrollHeight);

                if (data.length == 0)
                    $(".misNotificaciones").html(sin_resultados());

                limit = (limit * 2);

            })
            .catch(function(error) {
                $(".view_more").addClass("hide");
                $(".misNotificaciones").html(sin_resultados());
            });
    }




    obtener_mis_notis();

    buscarID = function buscarID(id) {

        for (var i = 0; i < CookieArray.length; i++) {
            if (CookieArray[i].id == id)
                return true;
        }
        return false;
    }
    CargarNoti = function CargarNoti(data, op) {

        if (buscarID(data.id)) {
            return false;
        }
        CookieArray.push(data);
        var html = '';

        html += '<div class="row boxNotificaciones mb-1 VN vernotificacion' + data.id + '" id_usable="'+data.id_usable+'" noti="' + data.id + '" tipo="' + data.tipo_noti + '">';
        html += '    <div class="col s12">';
        html += '<i style="margin:0.5rem;" class="fa right fa-times BORRAR clr_red" noti="' + data.id + '"></i>';
        html += '        <h6 class="clr_red">' + data.tit_noti + '</h6>';
        html += '        <p>' + data.des_noti + '</p>';
        html += '<span class="timeago right" title="' + data.fec_reg_noti + '"></span>';
        html += '    </div>';
        html += '</div>';

        if (op)
            $(".misNotificaciones").append(html);
        else
            $(".misNotificaciones").prepend(html);
    }

    function sin_resultados() {
        var html = '';
        html += '<div class="row boxNotificaciones mb-1">';
        html += '    <div class="col s12">';
        html += '        <h6 class="clr_red text-center">Sin resultados</h6>';
        html += '    </div>';
        html += '</div>';
        return html;
    }
    $(document).on('click', '.BORRAR', function() {
        var id = $(this).attr("noti");
        var i = $(this);

        $(i).removeClass('fa-times');
        $(i).addClass('fa-spinner fa-spin');

        axios.get(DOMINIO + 'notificaciones/del/' + id, progress__event())
            .then(function(response) {
                $(".vernotificacion" + id).remove();
                $(i).removeClass('fa-spinner fa-spin');
                mis_notiss();
            })
            .catch(function(error) {
                $(i).removeClass('fa-spinner fa-spin');
            });
    });
    $(document).on('click', '.VN', function() {
        var tipo = $(this).attr("tipo");
        var id = $(this).attr("noti");
        var id_usable = $(this).attr("id_usable");
        var i = $(this).find('i');

        $(i).removeClass('fa-times');
        $(i).addClass('fa-spinner fa-spin');

        axios.get(DOMINIO + 'notificaciones/vista/' + id, progress__event())
            .then(function(response) {
                $(".vernotificacion" + id).remove();
                $(i).removeClass('fa-spinner fa-spin');
                if (tipo == "solicitud-cambios-push") {
                    location.href = "historial.html?id="+id_usable;
                } else if (tipo == "solicitud-nuevo-push") {
                    location.href = "inicio.html";
                } else if (tipo == "verificar-push") {
                    var obj = obj_data_usuario();
                    if (obj_data_usuario().tablausuario == "clientes")
                        obj.est_cli = 1;
                    else
                        obj.est_pro = 1;
                    setCookie("data_user", JSON.stringify(obj));
                    location.href = "inicio.html";
                } else if (tipo == "verificar-pro-push") {
                    var obj = obj_data_usuario();
                    obj.estatus_verificado = 1;
                    setCookie("data_user", JSON.stringify(obj));
                    location.href = "inicio.html";
                }
                mis_notiss();
            })
            .catch(function(error) {
                $(i).removeClass('fa-spinner fa-spin');

            });
    });

});