$(document).ready(function() {
    convertirAlert();
    variableGLOBAL();
    var Servicio = {};

    if (getCookieV2("ServicioMapa")) {
        Servicio = JSON.parse(getCookieV2("ServicioMapa"));
    } else {
        history.back();
    }

    function onError(error) {
        M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
        setTimeout(function() { history.back(); }, 5500);

    };

    var mapa;
    DSERU = "";
    DIRDISU = "";


    $(document).on('click', '.BTVER', function() {
        var html = '';
        html += '<img src="' + Servicio.Cliente.img_cli + '" width="20%" ' + imgenpordefecto() + '>';
        html += '<h6 class="clr_red text-bold">Solicitud de ' + Servicio.Cliente.nom_cli + '</h6>';
        html += '<h6 class="clr_black"><i class="fa fa-phone"></i> ' + ((Servicio.Cliente.tel_cli) ? Servicio.Cliente.tel_cli : 'N/A') + '</h6>';
        html += '    <h6 class="clr_black text-bold">' + ESTATUSP[Servicio.estatus_sol] + '</h6>';
        //html += '<hr><h6 class="clr_black">Transportar (' + Servicio.cantidad_sol + ') ' + Servicio.carga_sol + '</h6>';
        html += '<hr><h6><b>Desde:</b> ' + Servicio.desdeText + ', <b>hasta:</b> ' + Servicio.hastaText + '</h6>';
        html += '<hr><h6>Son ' + JSON.parse(Servicio.distancia_sol).text + '</h6>';
        html += '<hr><h6>Tiempo aproximado <i class="fa fa-clock"></i> ' + JSON.parse(Servicio.duracion_sol).text + '</h6>';
        html += '<hr>';
        html += '<div class="col s12 center">';
        html += '    <h5>El costo del Transporte</h5>';
        html += '    <h4 class="clr_red text-bold">$ ' + formato.precio(Servicio.costo_sol) + '</h4>';
        html += ' <p class="m-0 Leye1 hide">Punto A: Ubicación de origen</p>';
        html += ' <p class="m-0 Leye1 hide">Punto B: Ubicación de destino</p>';
        html += ' <p class="m-0 Leye2 hide">Punto A: Mi Ubicación actual</p>';
        html += ' <p class="m-0 Leye2 hide">Punto B: Ubicación de origen</p>';
        html += ' <p class="m-0 Leye2 hide">Punto C: Ubicación de destino</p>';
        html += '</div>';

        $(".CuerpoSoli").html(html);
        if (obj_data_usuario().lat_pro && obj_data_usuario().lon_pro) {
            $(".Leye2").removeClass('hide');
        } else {
            $(".Leye1").removeClass('hide');
        }
    });
    correr_mapa_();

    function correr_mapa_() {
        if (!google) {
            alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");
        }
        DSERU = new google.maps.DirectionsService();
        DIRDISU = new google.maps.DirectionsRenderer();
        var styles = [
            { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
        ];

        var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

        var options = {
            zoom: 16,
            center: new google.maps.LatLng(Servicio.lat1, Servicio.lon1),
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            backgroundColor: 'transparent'
        };
        geocoder = new google.maps.Geocoder();
        mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
        mapa.mapTypes.set('map_style', styledMap);
        mapa.setMapTypeId('map_style');

        MAPA = mapa;
        DIRDISU.setMap(mapa);
        ActualizarMapa();
    }

    setInterval(function() {
        ActualizarMapa();
    }, 60000);

    function ActualizarMapa() {
        $(".Leye1").addClass('hide');
        $(".Leye2").addClass('hide');
        var data_map = {
            origin: "" + Servicio.lat1 + "," + Servicio.lon1 + "",
            destination: "" + Servicio.lat2 + "," + Servicio.lon2 + "",
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        };
        if (obj_data_usuario().lat_pro && obj_data_usuario().lon_pro) {
            $(".Leye2").removeClass('hide');
            data_map.origin = "" + obj_data_usuario().lat_pro + "," + obj_data_usuario().lon_pro + "";
            data_map.waypoints = [{
                location: "" + Servicio.lat1 + "," + Servicio.lon1 + "",
                stopover: true
            }]
        } else {
            $(".Leye1").removeClass('hide');
        }
        DSERU.route(data_map, function(response, status) {
            if (status === 'OK') {
                DIRDISU.setDirections(response);
            }
        });
    }
});