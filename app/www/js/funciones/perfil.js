var Camera;
$(document).on('ready', function() {
    convertirAlert();
    variableGLOBAL();

    var LAT = null,
        LON = null;
    $(".telefono").mask("+570000000000");
    $(".cedula").mask("0000000000");
    $('.IMGVEH').materialbox();
    $('#IMGPROFILE').materialbox();

    $('.foto').on('click', function() {
        $('#imagen').click();
    });
    var img = "";
    opcion = false;
    $("#nom_usu").val(obj_data_usuario().nom_usu);
    if (obj_data_usuario().tablausuario == "clientes") {
        $("#ced_usu").val(obj_data_usuario().cedula_cli);
        $("#tel_usu").val(obj_data_usuario().tel_cli);
        $("#cor_usu").val(obj_data_usuario().cor_cli);
        $("#dir_usu").val(obj_data_usuario().dir_cli);
        $(".TIP_USU").text("Cliente");
        $(".COD_USU").text(obj_data_usuario().id);
    } else {
        $("#dir_usu").val(obj_data_usuario().dir_pro);
        $(".ProveedorDiv").removeClass('hide');
        $("#marca").val(obj_data_usuario().Detalles.marca_veh);
        $("#matricula").val(obj_data_usuario().Detalles.matri_veh);
        $("#referencia").val(obj_data_usuario().Detalles.referencia_personal);
        $("#tel_referencia").val(obj_data_usuario().Detalles.tel_referencia);
        $("#img_img_1").attr("src", "" + obj_data_usuario().Detalles.img_fre + "");
        $("#img_img_2").attr("src", "" + obj_data_usuario().Detalles.img_lat + "");
        $("#img_img_3").attr("src", "" + obj_data_usuario().Detalles.img_tra + "");
        $("#img_img_4").attr("src", "" + obj_data_usuario().Detalles.img_cedula + "");
        $(".nombre_veh").text(obj_data_usuario().Detalles.Vehiculo.nombre_veh);
        $("#icono_veh").attr("src", "img/transporte/" + obj_data_usuario().Detalles.Vehiculo.icono_veh + "");
        $("#ced_usu").val(obj_data_usuario().cedula_pro);
        $("#tel_usu").val(obj_data_usuario().tel_pro);
        $("#cor_usu").val(obj_data_usuario().cor_pro);
        $(".TIP_USU").text("Conductor");
        $(".COD_USU").text(obj_data_usuario().id);
    }

    $(".OpenCam").on('click', function() {
        if (!Camera) {
            $("#imagen").click();
            return;
        }
        var optionsC = {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: $(this).attr("tipo") == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: true,
            allowEdit: true
        }
        navigator.camera.getPicture(function(result) {
            onSuccess(result)
        }, function(error) {
            // alert(error);
        }, optionsC);
    });
    $("#imagen").on('change', function() {

        if (this.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
                onSuccess(evt.target.result.split(",")[1]);
            };
            reader.readAsDataURL(this.files[0]);
        }

    });
    $(".BTUBI").on('click', function() {
        setTimeout(function() { correr_mapa_(); }, 1200);
    });

    onSuccess = function onSuccess(imageURI) {

        $(".IMGPROFILE").attr("src", "data:image/jpeg;base64," + imageURI);
        img = "data:image/jpeg;base64," + imageURI;
        if (obj_data_usuario().tablausuario == "clientes") {
            route = "clientes/editar/imagen";
        } else {
            route = "proveedores/editar/imagen";
        }
        var formData = new FormData();
        formData.append('id', obj_data_usuario().id);
        formData.append('img', img);
        axios.post(DOMINIO + '' + route, formData, progress__event())
            .then(function(response) {

                if (response.data.r == false) {
                    alert(response.data.msg);
                } else {
                    setCookie("data_user", JSON.stringify(response.data.user));
                    setCookie("id", response.data.user.id);
                    setCookie("token", response.data.user.token);

                }
            })
            .catch(function(error) {
                console.log(error);

            });
        return false;
    }
    $(".BTedit").on('click', function() {
        //if(img.trim().length<1)
        //alert('Debe seleccionar una imagen', 5000);
        //else
        if ($("#nom_usu").val().trim().length < 3)
            alert("Ingresa tu nombre", 5000);
        else if ($("#ced_usu").val().trim().length < 8)
            alert("Ingresa tu cedula", 5000);
        else if ($("#tel_usu").val().trim().length < 6)
            alert("Completa tu teléfono", 5000);
        else if ($("#dir_usu").val().trim().length < 3)
            alert("Ingresa tu dirección", 5000);
        else {

            if (obj_data_usuario().tablausuario == "clientes") {
                route = "clientes/editar";
            } else {
                route = "proveedores/editar";
            }

            var formData = new FormData();
            formData.append('id', obj_data_usuario().id);
            formData.append('tip_usu', route);
            formData.append('nom', $("#nom_usu").val());
            formData.append('tel', $("#tel_usu").val());
            formData.append('cedula', $("#ced_usu").val());
            formData.append('dir_usu', $("#dir_usu").val());
            formData.append('lat', LAT);
            formData.append('lon', LON);
            axios.post(DOMINIO + '' + route, formData, progress__event($(".BTedit")))
                .then(function(response) {

                    if (response.data.r == false) {
                        alert(response.data.msg);
                    } else {
                        setCookie("data_user", JSON.stringify(response.data.user));
                        setCookie("id", response.data.user.id);
                        setCookie("token", response.data.user.token);

                    }
                })
                .catch(function(error) {
                    console.log(error);

                });
            return false;
        }
    });
    var mapa = null;

    function correr_mapa_() {
        if (!mapa) {
            if (!google) {
                alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");
            }
            var styles = [
                { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
                { elementType: 'labels.text.fill', stylers: [{ color: '#f5821f' }] }
            ];

            var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

            var options = {
                zoom: 16,
                center: new google.maps.LatLng(23.0000000, -102.0000000),
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                backgroundColor: 'transparent'
            };
            geocoder = new google.maps.Geocoder();

            mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(-78.9909352282, -4.29818694419, -66.8763258531, 12.4373031682));
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('FiltrarMapa'), { bounds: defaultBounds, types: ['establishment', 'geocode'] });
            autocomplete.setComponentRestrictions({ 'country': ['co'] });

            mapa.mapTypes.set('map_style', styledMap);
            mapa.setMapTypeId('map_style');

            google.maps.event.addListener(mapa, 'click', function(event) {

                LAT = event.latLng.lat();
                LON = event.latLng.lng();



                if (typeof marker != 'undefined')
                    marker.setMap(null);
                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: mapa
                });
            });
            MAPA = mapa;
        }
        var device = document.addEventListener('deviceready', function() {
            cordova.plugins.locationAccuracy.canRequest(function(canRequest) {
                if (canRequest) {
                    cordova.plugins.locationAccuracy.request(function(success) {


                        navigator.geolocation.getCurrentPosition(function(position) {

                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });


                    }, function(error) {

                        navigator.geolocation.getCurrentPosition(function(position) {
                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });

                        if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {

                        }
                    }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);

                }
            });
        });
        if (!device) {
            navigator.geolocation.getCurrentPosition(function(position) {

                cargarDatam(position.coords.latitude, position.coords.longitude);
            }, function(error) {
                onError(error);
            });
        }
    }

    function cargarDatam(lat, lon) {

        if (typeof marker != 'undefined')
            marker.setMap(null);
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lon),
            map: mapa
        });
        mapa.panTo(new google.maps.LatLng(lat, lon));
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
                'location': { lat: parseFloat(lat), lng: parseFloat(lon) }
            },
            function g(results, status) {


                if (status == 'OK') {
                    $("#dir_usu").val(results[0].formatted_address);

                    return;
                } else {


                    return;
                }


            });
    }
    $("#FiltrarMapa").on('change', function() {
        var tis = this;
        setTimeout(function() {
            if (tis.value.trim() != "") {
                buscar_address(tis.value.trim());
                tis.value = "";
            }
        }, 100);
    });

    function buscar_address(address) {

        $("#FiltrarMapa_icon").addClass("fa-spinner fa-spin");
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address, componentRestrictions: { 'country': 'co' }, region: 'co' }, geocodeResult);
    }

    function geocodeResult(results, status) {
        //$("#FiltrarMapa").focus();
        $("#FiltrarMapa_icon").removeClass("fa-check");

        if (status == 'OK') {

            //addMarker(results[0].geometry.location.lat(),results[0].geometry.location.lng());
            LAT = results[0].geometry.location.lat();
            LON = results[0].geometry.location.lng();
            if (typeof marker != 'undefined')
                marker.setMap(null);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(LAT, LON),
                map: mapa
            });
            mapa.panTo(new google.maps.LatLng(LAT, LON));
            $("#FiltrarMapa_icon").addClass("fa-check");
            $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
            return;
        } else {
            $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
            //$("#FiltrarMapa").focus();
            return;
        }
        $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
        //$("#FiltrarMapa").focus();
    }
});