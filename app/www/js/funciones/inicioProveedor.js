if (obj_data_usuario().tablausuario == "proveedores") {
    if (obj_data_usuario().est_pro == 1 && obj_data_usuario().estatus_verificado == 1) {
        $('.modal').modal({ inDuration: 0, outDuration: 0 });
    }
}
convertirAlert();
variableGLOBAL();

var CookieArray = [];
var limit = 3;
setCookieV2("SolicitudTomada", JSON.stringify({}));
obtener_soli();

function obtener_soli() {

    var html = '';
    axios.get(DOMINIO + 'solicitud_servicios/proveedor/?limit=' + limit + '&token=' + obj_data_usuario().token + '&id_tipo_vehiculo=' + obj_data_usuario().Detalles.id_tipo_vehiculo, {})
        .then(function(response) {
            var data = response.data;

            $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_soli(); " class="view_more"><center>Cargar más...</center></div>');

            if (data.length > 0 && data.length < 3 || data.length == 0)
                $(".view_more").addClass("hide");

            for (var i = 0; i < data.length; i++) {
                insertarSoli(data[i], 1);
            }

            if (limit == 3)
                $(".SoliINI").scrollTop($('.SoliINI')[0].scrollHeight);

            if (data.length == 0)
                $(".SoliINI").html(sin_resultados());

            limit = (limit * 2);
            $('.ver-img-pro').materialbox();
        })
        .catch(function(error) {
            $(".view_more").addClass("hide");
            $(".SoliINI").html(sin_resultados());

        });
}
getKilometros = function(lat1, lon1, lat2, lon2) {
    rad = function(x) { return x * Math.PI / 180; }
    var R = 6378.137; //Radio de la tierra en km
    var dLat = rad(lat2 - lat1);
    var dLong = rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d.toFixed(3); //Retorna tres decimales
}

buscarID = function buscarID(id) {

    for (var i = 0; i < CookieArray.length; i++) {
        if (CookieArray[i].id == id)
            return CookieArray[i];
    }
    return false;
}
insertarSoli = function insertarSoli(data, op) {
    if (parseInt(getKilometros(obj_data_usuario().lat_pro, obj_data_usuario().lon_pro, data.lat1, data.lon1)) <= 5) {
        if (buscarID(data.id)) {
            return false;
        }
        CookieArray.push(data);


        var html = '';
        html += '<div class="row m-0 BTVER" id="' + data.id + '" style="border: 1px solid #ddd; border-radius: .5rem;padding: .1rem;">';
        html += '	<a href="#det_solicitud" class="modal-trigger clr_red">';
        html += '	<div class="col s2 center">';
        html += '		<div class="input-field">';
        html += '				<img src="' + data.Cliente.img_cli + '" ' + imgenpordefecto() + ' class="img-historial img-cover">';
        html += '		</div>';
        html += '		</div>';
        html += '		<div class="col s10">';
        html += '		<h6 class="clr_red text-bold">' + data.Cliente.nom_cli + '</h6>';
        html += '     <h6 class="text-bold"><b class="clr_black">ESTATUS: </b><b style="clr_red">' + ESTATUSP[data.estatus_sol] + '</b></h6>';
        html += '     <h6 class="text-bold"><b class="clr_black">RECOJER EN: </b><b style="clr_red">' + data.desdeText + '</b></h6>';
        html += '			<h6><b class="clr_black">DISTANCIA APROXIMADA: </b> <b style="clr_red">' + JSON.parse(data.distancia_sol).text + '</b> / <b class="clr_black">TIEMPO APROXIMADO: </b><b style="clr_red"><i class="fa fa-clock"></b></i> ' + JSON.parse(data.duracion_sol).text + '</h6>';
        html += '			<span class="right clr_red timeago" title="' + data.fecha_sol + '"></span>';
        html += '		</div>';
        html += '	</a>';
        html += '</div>';

        if (op)
            $(".SoliINI").append(html);
        else
            $(".SoliINI").prepend(html);

    }
}

function sin_resultados() {
    var html = '';
    html += '<div class="row mt-1 NORESULTS" style="border: 1px solid #ddd; border-radius: .5rem;padding: .1rem;">';
    html += '    <a href="#!">';
    html += '	<div class="col s12 center">';
    html += '                <h6 class="clr_black">';
    html += ' No se encontraron resultados. ';
    html += '                </h6>';
    html += '            </div>';
    html += '    </a>';
    html += '</div>';
    return html;
}
var Ver = null;
$(document).on('click', '.BTVER', function() {
    $(".Leye1").addClass('hide');
    $(".Leye2").addClass('hide');
    Ver = buscarID(this.id);

    var html = '';
    html += '<img src="' + Ver.Cliente.img_cli + '" class="img-historial img-cover' + imgenpordefecto() + '>';
    html += '<h6 class="clr_black text-bold">CLIENTE: ' + Ver.Cliente.nom_cli + '</h6>';
    html += '<h6 class="text-center"><b class="clr_black">ESTATUS: </b><b class="clr_red">' + ESTATUSP[Ver.estatus_sol] + '</b></h6>';
    //)html += '<hr><h6 class="clr_black">Transportar (' + Ver.cantidad_sol + ') ' + Ver.carga_sol + '</h6>';
    html += '<hr><h6 class="text-justify"><b class="clr_black">DESDE:</b> ' + Ver.desdeText + ', <br><b>HASTA:</b> ' + Ver.hastaText + '</h6>';
    html += '<hr><h6 class="text-center"><b class="clr_black">DISTANCIA APROXIMADA: </b> <b class="clr_red">' + JSON.parse(Ver.distancia_sol).text + '</b></h6>';
    html += '<hr><h6 class="text-center"><b class="clr_black">TIEMPO APROXIMADO </b> <b class="clr_red"><i class="fa fa-clock"></i> ' + JSON.parse(Ver.duracion_sol).text + '</b></h6>';
    html += '<hr>';
    html += '<div class="col s12 center">';
    html += '    <h5>El costo del Transporte</h5>';
    html += '    <h4 class="clr_red text-bold">$ ' + formato.precio(Ver.costo_sol) + '</h4>';
    html += '</div>';

    var buttons = '';
    buttons += '<button type="button" id="' + Ver.id + '" class="btn btn-red btn-small BTACEPTAR">Aceptar</button>';
    buttons += '<button type="button" class="btn btn-red btn-small modal-close">Cancelar</button>';


    $(".CuerpoSoli").html(html);
    $(".Buttons").html(buttons);
    if (obj_data_usuario().lat_pro && obj_data_usuario().lon_pro) {
        $(".Leye2").removeClass('hide');
    } else {
        $(".Leye1").removeClass('hide');
    }
    setTimeout(function() {
        correr_mapa_();
    }, 450);
});
$(document).on('click', '.BTACEPTAR', function() {
    var editar = {
        id: this.id,
        id_proveedor: obj_data_usuario().id,
        token: obj_data_usuario().token,
        chofer: obj_data_usuario()
    }
    axios.post(DOMINIO + 'solicitud_servicios/tomar-solicitud', editar, progress__event($(".BTACEPTAR")))
        .then(function(response) {

            if (response.data.r == false) {
                alert(response.data.msg);
                obtener_soli();
                $("#det_solicitud").modal("close");
                return;
            } else {
                setCookieV2("SolicitudTomada", JSON.stringify(Ver));
                location.href = "solicitado_servicio.html";
            }
        })
        .catch(function(error) {
            console.log(error);

        });

});

function onError(error) {
    M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
    setTimeout(function() { history.back(); }, 5500);

};

var mapa;
DSERU = "";
DIRDISU = "";



function correr_mapa_() {
    if (!mapa) {
        if (!google) {
            alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");

        }
        DSERU = new google.maps.DirectionsService();
        DIRDISU = new google.maps.DirectionsRenderer();
        var styles = [
            { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#294dce' }] }
        ];

        var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

        var options = {
            zoom: 16,
            center: new google.maps.LatLng(23.0000000, -102.0000000),
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            backgroundColor: 'transparent'
        };
        geocoder = new google.maps.Geocoder();

        mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
        mapa.mapTypes.set('map_style', styledMap);
        mapa.setMapTypeId('map_style');


        MAPA = mapa;
        DIRDISU.setMap(mapa);
    }
    if (DIRDISU) {
        DIRDISU.setMap(null);
        DIRDISU = new google.maps.DirectionsRenderer();
        DIRDISU.setMap(mapa);
    }
    var data_map = {
        origin: "" + Ver.lat1 + "," + Ver.lon1 + "",
        destination: "" + Ver.lat2 + "," + Ver.lon2 + "",
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
    };
    if (obj_data_usuario().lat_pro && obj_data_usuario().lon_pro) {
        data_map.origin = "" + obj_data_usuario().lat_pro + "," + obj_data_usuario().lon_pro + "";
        data_map.waypoints = [{
            location: "" + Ver.lat1 + "," + Ver.lon1 + "",
            stopover: true
        }]
    }

    DSERU.route(data_map, function(response, status) {
        if (status === 'OK') {
            DIRDISU.setDirections(response);
        }
    });

}

$(document).on('click', '.CONSULTAR', function() {
    var editar = {
        id: obj_data_usuario().id
    }
    axios.post(DOMINIO + 'proveedores/Actualizar-sesion', editar, progress__event($(".CONSULTAR")))
        .then(function(response) {
            if (response.data.r == false) {
                //alert(response.data.msg);
            } else {
                $('#confirmacion').modal('close');
                $('#confirmacion2').modal('close');
                setCookie("data_user", JSON.stringify(response.data));
            }
        })
        .catch(function(error) {
            console.log(error);

        });
});