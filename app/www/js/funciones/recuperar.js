$(document).on('ready', function() {
    $("#btn_recu").on('click', function() {
        var data = {
            cor: $("#email").val(),
        };

        axios.post(DOMINIO + 'clientes/recuperar/clave', data, progress__event($("#btn_recu")))
            .then(function(response) {

                alert(response.data.msg);
                if (response.data.r) {
                    $("#email").val("");
                    setTimeout(function() { location.href = "login.html"; }, 2300);
                }
            })
            .catch(function(error) {
                console.log(error);
            });
        return false;
    });
});