$(document).ready(function() {
    variableGLOBAL();


    var CookieArray = [];
    var limit = 3;
    obtener_Domicilios = function obtener_Domicilios() {

        var html = '';
        axios.get(DOMINIO + 'domicilios/?id=' + obj_data_usuario().id + '&limit=' + limit + '&token=' + obj_data_usuario().token + '&tipo=' + obj_data_usuario().tablausuario, {})
            .then(function(response) {
                var data = response.data;

                $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_Domicilios(); " class="view_more"><center>Cargar más...</center></div>');

                if (data.length > 0 && data.length < 3 || data.length == 0)
                    $(".view_more").addClass("hide");


                for (var i = 0; i < data.length; i++) {

                    CargarNoti(data[i], 1);
                }

                if (limit == 3)
                    $(".misDomicilios").scrollTop($('.misDomicilios')[0].scrollHeight);

                if (data.length == 0)
                    $(".misDomicilios").html(sin_resultados());

                limit = (limit * 2);

            })
            .catch(function(error) {
                $(".view_more").addClass("hide");
                $(".misDomicilios").html(sin_resultados());
            });
    }




    obtener_Domicilios();

    buscarID = function buscarID(id) {

        for (var i = 0; i < CookieArray.length; i++) {
            if (CookieArray[i].id == id)
                return true;
        }
        return false;
    }
    CargarNoti = function CargarNoti(data, op) {

        if (buscarID(data.id)) {
            return false;
        }
        CookieArray.push(data);
        var html = '';

        html += '<div class="row boxNotificaciones mb-1 verdomi" noti="' + data.id + '">';
        html += '    <div class="col s12">';
        html += '<i style="margin:0.5rem;" class="fa right fa-times BORRAR clr_red" noti="' + data.id + '"></i>';
        html += '        <h6 class="clr_red">' + data.dir_dom + '</h6>';
        html += '        <p>' + data.nom_dom + '</p>';
        html += '    </div>';
        html += '</div>';

        if (op)
            $(".misDomicilios").append(html);
        else
            $(".misDomicilios").prepend(html);
    }

    function sin_resultados() {
        var html = '';
        html += '<div class="row boxNotificaciones mb-1">';
        html += '    <div class="col s12">';
        html += '        <h6 class="clr_red text-center">Sin resultados</h6>';
        html += '    </div>';
        html += '</div>';
        return html;
    }
    $(document).on('click', '.BORRAR', function() {
        var id = $(this).attr("noti");
        var i = $(this);

        $(i).removeClass('fa-times');
        $(i).addClass('fa-spinner fa-spin');

        axios.get(DOMINIO + 'domicilios/del/' + id, progress__event())
            .then(function(response) {
                $(".verdomi" + id).remove();
                $(i).removeClass('fa-spinner fa-spin');
                obtener_Domicilios();
            })
            .catch(function(error) {
                $(i).removeClass('fa-spinner fa-spin');
            });
    });
    $(".BTUBI").on('click', function() {
        $("#FiltrarMapa").val("");
        $("#des_dom").val("");
        LAT = null;
        LON = null;
        $("#modal-ubicacion").modal("open");
        setTimeout(function() { correr_mapa_(); }, 1200);
    });
    var mapa = null;

    function correr_mapa_() {
        if (!mapa) {
            if (!google) {
                alert("Ocurrio un problema cargando el mapa... <button class='btn btn-red btn-small left' onclick='location.reload();'>Reintentar</button>");
            }
            var styles = [
                { featureType: "poi.business", elementType: "labels", stylers: [{ visibility: "off" }] },
                { elementType: 'labels.text.fill', stylers: [{ color: '#f5821f' }] }
            ];

            var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

            var options = {
                zoom: 16,
                center: new google.maps.LatLng(23.0000000, -102.0000000),
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                backgroundColor: 'transparent'
            };
            geocoder = new google.maps.Geocoder();

            mapa = new google.maps.Map(document.getElementById('mapaservicio'), options);
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(-78.9909352282, -4.29818694419, -66.8763258531, 12.4373031682));
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('FiltrarMapa'), { bounds: defaultBounds, types: ['establishment', 'geocode'] });
            autocomplete.setComponentRestrictions({ 'country': ['co'] });

            mapa.mapTypes.set('map_style', styledMap);
            mapa.setMapTypeId('map_style');

            google.maps.event.addListener(mapa, 'click', function(event) {

                LAT = event.latLng.lat();
                LON = event.latLng.lng();
                cargarDatam(LAT, LON);

                if (typeof marker != 'undefined')
                    marker.setMap(null);
                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: mapa
                });
            });
            MAPA = mapa;
        }
        var device = document.addEventListener('deviceready', function() {
            cordova.plugins.locationAccuracy.canRequest(function(canRequest) {
                if (canRequest) {
                    cordova.plugins.locationAccuracy.request(function(success) {


                        navigator.geolocation.getCurrentPosition(function(position) {

                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });


                    }, function(error) {

                        navigator.geolocation.getCurrentPosition(function(position) {
                            cargarDatam(position.coords.latitude, position.coords.longitude);
                        }, function(error) {
                            onError(error);
                        });

                        if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {

                        }
                    }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);

                }
            });
        });
        if (!device) {
            navigator.geolocation.getCurrentPosition(function(position) {

                cargarDatam(position.coords.latitude, position.coords.longitude);
            }, function(error) {
                onError(error);
            });
        }
    }
function onError(error) {
    M.toast({ html: 'Ocurrió un error accediendo a tu ubicación' }, 5000);
    

};
    function cargarDatam(lat, lon) {

        if (typeof marker != 'undefined')
            marker.setMap(null);
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lon),
            map: mapa
        });
        mapa.panTo(new google.maps.LatLng(lat, lon));
        geocoder = new google.maps.Geocoder();
        $("#FiltrarMapa_icon").addClass("fa-spinner fa-spin");
        geocoder.geocode({
                'location': { lat: parseFloat(lat), lng: parseFloat(lon) }
            },
            function g(results, status) {


                if (status == 'OK') {
                    $("#FiltrarMapa").val(results[0].formatted_address);
                    LAT = lat;
                    LON = lon;
                    $("#FiltrarMapa_icon").addClass("fa-check");
                    $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
                    return;
                } else {


                    return;
                }


            });
    }
    $("#FiltrarMapa").on('change', function() {
        var tis = this;
        setTimeout(function() {
            if (tis.value.trim() != "") {
                buscar_address(tis.value.trim());
                //tis.value = "";
            }
        }, 100);
    });

    function buscar_address(address) {

        $("#FiltrarMapa_icon").addClass("fa-spinner fa-spin");
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address, componentRestrictions: { 'country': 'co' }, region: 'co' }, geocodeResult);
    }

    function geocodeResult(results, status) {
        //$("#FiltrarMapa").focus();
        $("#FiltrarMapa_icon").removeClass("fa-check");

        if (status == 'OK') {

            //addMarker(results[0].geometry.location.lat(),results[0].geometry.location.lng());
            LAT = results[0].geometry.location.lat();
            LON = results[0].geometry.location.lng();
            if (typeof marker != 'undefined')
                marker.setMap(null);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(LAT, LON),
                map: mapa
            });
            mapa.panTo(new google.maps.LatLng(LAT, LON));
            $("#FiltrarMapa_icon").addClass("fa-check");
            $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
            return;
        } else {
            $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
            //$("#FiltrarMapa").focus();
            return;
        }
        $("#FiltrarMapa_icon").removeClass("fa-spinner fa-spin");
        //$("#FiltrarMapa").focus();
    }
    $(".BTSAVEDOM").on('click', function() {

        if (!LAT)
            alert("Elija una ubicacón", 5000);
        else if (!LON)
            alert("Elija una ubicacón", 5000);
        else if ($("#FiltrarMapa").val().trim().length < 1)
            alert("Completa la dirección", 5000);
        else if ($("#des_dom").val().trim().length < 2)
            alert("Ingresa la descripción del domicilio: Alias.", 5000);
        else {

            var formData = new FormData();
            formData.append('id', obj_data_usuario().id);
            formData.append('token', obj_data_usuario().token);
            formData.append('nom_dom', $("#FiltrarMapa").val());
            formData.append('dir_dom', $("#des_dom").val());
            formData.append('lat', LAT);
            formData.append('lon', LON);
            axios.post(DOMINIO + 'domicilios/add', formData, progress__event($(".BTSAVEDOM")))
                .then(function(response) {

                    if (response.data.r == false) {
                        alert(response.data.msg);
                    } else {
                        $("#modal-ubicacion").modal("close");
                        $(".boxNotificaciones").remove();
                        CookieArray = [];
                        obtener_Domicilios();
                    }
                })
                .catch(function(error) {
                    console.log(error);

                });
            return false;
        }
    });
});