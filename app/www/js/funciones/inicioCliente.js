 variableGLOBAL();
 var __OpcionesVeh = $("#OpcionesVeh");
 var TiposVeh = [];
 tiposVeh__();

 function tiposVeh__() {
     var html = '';
     axios.post(DOMINIO + 'tipovehiculo/app/?token=' + obj_data_usuario().token, {}, progress__event())
         .then(function(response) {
             var data = response.data;

             TiposVeh = data;
             for (var i = 0; i < data.length; i++) {
                 if (data[i].preciokm > 0) {
                     html += '<a href="#!" onclick="BTServicio(this.id);" class="col s4 mb-2" id="' + data[i].id + '">';
                     html += '	<div class="boxTransporte__body">';
                     html += '		<img src="img/transporte/' + data[i].icono_veh + '" class="w100">';
                     html += '		<h6 class="center clr_red">' + data[i].nombre_veh + '</h6>';
                     html += '	</div>';
                     html += '</a>';
                 }
             }

             __OpcionesVeh.html(html);


         })
         .catch(function(error) {

         });
 }

 function returnTipo(id) {
     for (var i = 0; i < TiposVeh.length; i++) {
         if (TiposVeh[i].id == id) {
             return TiposVeh[i];
         }
     }
     return false;
 }

 function BTServicio(id) {
     var buscar = {
         id: obj_data_usuario().id,
         veh: id,
         token: obj_data_usuario().token
     }
     axios.post(DOMINIO + 'tipovehiculo/obtener', buscar, progress__event())
         .then(function(response) {

             if (response.data.r == false) {
                 alert(response.data.msg);
             } else {

                 setCookieV2("ServicioElegido", JSON.stringify(response.data));
                 location.href = "servicio.html";
             }
         })
         .catch(function(error) {
             console.log(error);

         });
 }


 $(document).on('click', '.CONSULTAR', function() {
     var editar = {
         id: obj_data_usuario().id
     }
     axios.post(DOMINIO + 'clientes/Actualizar-sesion', editar, progress__event($(".CONSULTAR")))
         .then(function(response) {
             if (response.data.r == false) {
                 //alert(response.data.msg);
             } else {
                 $('#confirmacion').modal('close');
                 setCookie("data_user", JSON.stringify(response.data));
             }
         })
         .catch(function(error) {
             console.log(error);

         });

 });