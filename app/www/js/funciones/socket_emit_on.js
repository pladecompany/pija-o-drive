	function variableGLOBAL() {
	    DOMINIO = "https://inthecompanies.com:2030/";
	    //DOMINIO = "http://localhost:2030/";
	    ESTATUSC = ["ESPERANDO CONDUCTOR", "PENDIENTE POR APROBAR", "EN CAMINO", "FINALIZADA", "VENCIDA", "CANCELADA", "EL CONDUCTOR YA LLEGÓ", "VIAJE REALIZADO, VALORACIÓN PENDIENTE"];
	    ESTATUSP = ["ESPERANDO CONDUCTOR", "ESPERANDO ACEPTACIÓN", "EN CAMINO", "FINALIZADA", "VENCIDA", "CANCELADA", "VALORACIÓN PENDIENTE", "VIAJE FINALIZADO"];
	    socket = io(DOMINIO);
	    formato = {
	        separador: ",", // separador para los miles
	        sepDecimal: '.', // separador para los decimales
	        formatear: function(num) {
	            num += '';
	            var splitStr = num.split('.');
	            var splitLeft = splitStr[0];
	            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
	            var regx = /(\d+)(\d{3})/;
	            while (regx.test(splitLeft)) {
	                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
	            }
	            return this.simbol + splitLeft + splitRight;
	        },
	        precio: function(num, simbol) {
	            this.simbol = simbol || '';

	            var num = num.toString();
	            num = num.replace(/,/g, ".");
	            num = Math.round(num * 100) / 100;


	            return this.formatear(num.toFixed(0));

	        }
	    }
	}

	window.onload = function() {

	    variableGLOBAL();


	    if (getCookie('id') != null && getCookie('id') != "" && validar_sesion == "true") {
	        var dataU = JSON.parse(getCookie('data_user'));
	        socket.emit("conectar", dataU);

	        if (home == "true") {
	            geo();
	        }




	        socket.on('conectadosOnline', function(usuarios) {
	            console.log(usuarios);
	        });
	        socket.on('Recibiralerta', function(usuario) {
	            //console.log("mensaje enviado por admin");
	            idu = obj_data_usuario().id;
	            tusu = obj_data_usuario().tablausuario;
	            //console.log(usuario);
	            if (usuario.id == idu && usuario.tablausuario == tusu)
	                alert("Mensaje del Admin: <br>" + usuario.msj, 10000);
	        });
	        socket.on('RECIBIRMENSAJE', function(mensaje) {
	            recibirmensaje(mensaje);
	        });
	        socket.on('RECIBIR-NUEVA-SOLICITUD', function(data) {
	            $(".NORESULTS").remove();

	            if (document.querySelector(".SoliINI")) {
	                insertarSoli(data);
	            }
	            alert("Nueva solicitud de servicio.");
	        });
	        socket.on('RECIBIR-NUEVA-NOTIFICACION', function(data) {

	            if (document.querySelector("#EsperandoRespuesta")) {
	                if (data.vencida == true) {
	                    alert("Su solicitud se venció, no hay conductores disponibles.");
	                    setTimeout(function() {
	                        location.href = "inicio.html";
	                    }, 1500);
	                }
	                if (data.tomada == true) {
	                    alert("Su solicitud fue aceptada por un conductor.");
	                    setTimeout(function() {
	                        location.href = "historial.html";
	                    }, 1500);
	                }
	            } else {
	                document.querySelector(".campanaNoti").classList.add("text-rojo");
	                document.querySelector(".campanaNoti").classList.add("animated");
	                document.querySelector(".campanaNoti").classList.remove("text-gray");
	                if (data.llegada == true) {
	                    $('.modal').modal({ inDuration: 0, outDuration: 0 });
	                    $("#llegoPIJAO").modal("open");
	                } else {
	                    alert("Tienes una nueva notificación.");
	                }


	            }
	        });

	        mis_notiss();

	    } else {
	        if (validar_sesion == "true") {
	            salir();
	        }
	    }
	}

	function obj_data_usuario() {

	    return JSON.parse(getCookie('data_user'));

	}

	function get_dominio() {
	    variableGLOBAL();
	    return DOMINIO;

	}

	mis_notiss = function mis_notiss() {

	    axios.get(DOMINIO + 'notificaciones/?id=' + obj_data_usuario().id + '&token=' + obj_data_usuario().token + '&tipo=' + obj_data_usuario().tablausuario, {})
	        .then(function(response) {
	            var data = response.data;

	            if (data.length > 0) {
	                document.querySelector(".campanaNoti").classList.add("text-rojo");
	                document.querySelector(".campanaNoti").classList.add("animated");
	                document.querySelector(".campanaNoti").classList.remove("text-gray");
	            } else {
	                document.querySelector(".campanaNoti").classList.add("text-gray");
	                document.querySelector(".campanaNoti").classList.remove("animated");
	                document.querySelector(".campanaNoti").classList.remove("text-rojo");
	            }

	        })
	        .catch(function(error) {});
	}

	function salir() {
	    window.localStorage.clear();
	    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
	    location.href = 'index.html';
	}
