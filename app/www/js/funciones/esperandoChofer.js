$(document).ready(function() {
  variableGLOBAL();
  var Servicio = {};
  var intervalRun;
  
	if(getCookieV2("SOLICITUDNUEVA")){
		Servicio = JSON.parse(getCookieV2("SOLICITUDNUEVA"));
		$(".desde").text(Servicio.desdeText);
		$(".hasta").text(Servicio.hastaText);
		$(".kmC").text(JSON.parse(Servicio.distancia_sol).text);
		$(".preC").text(formato.precio(Servicio.costo_sol));
		intervalRun = setInterval(function() { $(".Regresiva").text(correrRelog(Servicio.millis)); }, 1000);

		$(document).on('click','.BTcancelar',function(){ 
			
	        var data = {
	            id: Servicio.id,
	            token: obj_data_usuario().token,
	            tipo:obj_data_usuario().tablausuario,
	            id_cliente: obj_data_usuario().id

	        }
	        axios.post(DOMINIO+'solicitud_servicios/cancelar-solicitud', data, progress__event($(".BTcancelar")))
	        .then(function (response) {
	           if(response.data.r == false){
	              alert(response.data.msg);
	              return;
	          }else{
	             setCookieV2("SOLICITUDNUEVA", JSON.stringify({}));  
	             setCookieV2("SOLICITUDNUEVA", JSON.stringify({}));  
	             location.href = "inicio.html";
	           }
	        })
	        .catch(function (error) {
	          console.log(error);

	        });
    	});
    } 
    function correrRelog(millis){
    	  
		  var minutes = Math.floor(millis / 60000);
		  var seconds = ((millis % 60000) / 1000).toFixed(0);
		  millis = (millis-1000);
		  Servicio.millis = millis;
		  setCookieV2("SOLICITUDNUEVA", JSON.stringify(Servicio));

		  if(millis<=180000) {
		  	$("#EsperandoRespuesta").css('border', 'solid 1px orange');
		  	$(".spanRe").css('color', 'orange');
		  }
		  if(millis<=90000) {
		  	$("#EsperandoRespuesta").css('border', 'solid 1px red');
		  	$(".spanRe").css('color', 'red');
		  }
		  if(millis<=0) {
    	  	clearTimeout(intervalRun);
    	  	setTimeout(function(){ 
						location.href = "inicio.html";
			}, 300);
    	  	return "00:00";
    	  }
		  return (minutes < 10 ? '0' : '') + minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
	} 
 
});