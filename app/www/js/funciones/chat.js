$(document).on('ready', function(){
  variableGLOBAL();
  function scroll(){
	 $(".padreBoxChat").scrollTop(parseInt($(".padreBoxChat")[0].scrollHeight)+99999);
  }
  if(obj_data_usuario().tablausuario=="proveedores")
    envio=2;
  else
    envio=1;
  axios.get(DOMINIO+'chats/admin/obtener-chats?envio='+envio+'&id_usuario='+obj_data_usuario().id+'&token='+obj_data_usuario().token)
  .then(res=>{
  	datos =res.data.reverse()
  	for(var i=0; i<res.data.length;i++){
  		data = datos[i]
      if(data.tabla_usuario != 'admin'){
  			tipo = 'other'
  			img = 'img/logoapp.png'
  		}else{
  			tipo = 'self'
  			//img = obj_data_usuario().img_pro
        img = data.img;
  		}
  		var html = ''
	  	html += '<li class="'+tipo+'">'
	  	html += 	'<div class="avatar">'
	  	html += 		'<img src="'+img+'" draggable="false" class="img-cover" />'
	  	html += 	'</div>'
	  	html += 	'<div class="msg">'
	  	html += 	'	<p>'+data.mensaje+'</p>'
	  	html += 	'	<span class="timeago" title="'+data.hora+'"></span>'
	  	html += 	'</div>'
	  	html += '</li>'
	  	$('.mainChat').append(html)

	  	$('img').on('error',function(){
                $(this).attr('src','img/user.png')
        })

  	}
    $(".timeago").timeago();
  	setTimeout(function(){
  		scroll()
  	},100)
  })

  recibirmensaje = function recibirmensaje(mensaje){
  	var html = ''
  	html += '<li class="other">'
	  	html += 	'<div class="avatar">'
	  	html += 		'<img src="img/user.png" draggable="false" class="img-cover"/>'
	  	html += 	'</div>'
	  	html += 	'<div class="msg">'
	  	html += 	'	<p>'+mensaje.mensaje+'</p>'
	  	html += 	'	<span class="timeago" title="'+mensaje.hora+'"></span>'
	  	html += 	'</div>'
	  	html += '</li>'
	  	$('.mainChat').append(html)
  	setTimeout(function(){
  		scroll()
  	},100)

  }
//Detecta al pegar y formatea el texto
$("#mensaje").bind('paste', function (e){
    $(e.target).keyup(getInput);
});
//Detecta al presionar las teclas y calcula la altura para subir el content del chat
$('#mensaje').on('keyup',function(){
  valor = $(this).parent()[0].scrollHeight;
  valor +=100
  $('.padreBoxChat').css('height','calc(100vh - '+valor+'px)')
  scroll()
})
//Detecta al eliminar las teclas y calcula la altura para subir el content del chat

$('#mensaje').on('keydown',function(){
  valor = $(this).parent()[0].scrollHeight;
  valor +=100
  $('.padreBoxChat').css('height','calc(100vh - '+valor+'px)')
  scroll()
})
function getInput(e){
    //content = e.originalEvent.clipboardData.getData('text/plain');
    var inputText = $(e.target).html();
    $('#mensaje').html(inputText.replace(/<[^>]*>?/g, ''))
    $('#mensaje').unbind('keyup');

}

  $(document).on('click', '.BTenviar', function(){
  	if($("#mensaje").html().trim()!=""){
      if(obj_data_usuario().tablausuario=="proveedores")
        enviousu=2;
      else
        enviousu=1;
	  	var data = {
	  		id_usuario: obj_data_usuario().id,
	  		mensaje: $("#mensaje").html(),
	  		tabla_usuario: "admin",
	  		envio: enviousu,
	  		tablausuario: obj_data_usuario().tablausuario
	  	};
      if(obj_data_usuario().tablausuario=='clientes')
        imgchat=obj_data_usuario().img_cli;
      else
        imgchat=obj_data_usuario().img_pro;
	  	hora = moment().format("YYYY-MM-DD HH:mm:ss")
	  	var html =''
	  	html += '<li class="self">'
	  	html += 	'<div class="avatar">'
	  	html += 		'<img src="'+imgchat+'" draggable="false" class="img-cover" />'
	  	html += 	'</div>'
	  	html += 	'<div class="msg">'
	  	html += 	'	<p>'+data.mensaje+'</p>'
	  	html += 	'	<span class="timeago" title="'+hora+'"></span>'
	  	html += 	'</div>'
	  	html += '</li>'
	  	$('.mainChat').append(html)
	  	//CON ESTO ENVIAMOS EL EMIT AL API Y LO RECIBIMOS EN EL APP.JS 
	  	socket.emit("ENVIARMENSAJE",data);
	  	$("#mensaje").html("");
      $(".timeago").timeago();
  	}
        $('#mensaje').keydown()
	  	$('img').on('error',function(){
                $(this).attr('src','img/user.png')
        })
  		setTimeout(function(){
  		scroll()
  	},100)
  });

});