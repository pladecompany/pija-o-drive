$(document).ready(function() {
    variableGLOBAL();
    var Servicio = {};

    if (getCookieV2("SolicitudVerificada")) {
        Servicio = JSON.parse(getCookieV2("SolicitudVerificada"));

        $(".imgP").attr("src", "" + Servicio.Proveedor.img_pro + "");
        $(".nomP").text(Servicio.Proveedor.nom_pro);
        $(".imgPC_1").attr("src", "" + Servicio.Proveedor.Detalles.img_fre + "");
        $(".imgPC_2").attr("src", "" + Servicio.Proveedor.Detalles.img_lat + "");
        $(".imgPC_3").attr("src", "" + Servicio.Proveedor.Detalles.img_tra + "");
        $(".mtrP").text(Servicio.Proveedor.Detalles.matri_veh);
        $(".marP").text(Servicio.Proveedor.Detalles.marca_veh);
        $(".preP").text(formato.precio(Servicio.costo_sol));
        $(".btnback").attr('href', 'historial.html?id=' + Servicio.id + '');

        if (Servicio.estatus_sol <= 2) {
            $(".DivMen").removeClass("hide");
        } else {
            $(".DivMen").addClass("hide");
        }
        if (Servicio.estatus_sol != 3 && Servicio.estatus_sol != 5) {
            $(".BTCANCELARS").removeClass("hide");
        } else {
            $(".BTCANCELARS").addClass("hide");
        }
        tltwha = Servicio.Proveedor.tel_pro;
        if (tltwha) {
            tltwha = tltwha.replace("+", "");
        }
        $(".linkP").attr('href', 'https://wa.me/' + tltwha + '');
        $(".linkPcall").attr('href', 'tel:' + Servicio.Proveedor.tel_pro + '');

        if (Servicio.puntuaciones == null) {
            $(".Puntuar").removeClass('hide');
        }
        if (Servicio.Proveedor.Detalles.Reputacion) {
            var repu = '',
                voto1 = '',
                voto2 = '',
                voto3 = '';
            var Re = Servicio.Proveedor.Detalles.Reputacion;

            for (var i = 0; i < Re; i++) {
                repu += '<i class="fa fa-star"></i> ';
            }

            var R = Servicio.Proveedor.Detalles.voto1;

            for (var i = 0; i < R; i++) {
                voto1 += '<i class="fa fa-star"></i> ';
            }
            var R = Servicio.Proveedor.Detalles.voto2;

            for (var i = 0; i < R; i++) {
                voto2 += '<i class="fa fa-star"></i> ';
            }
            var R = Servicio.Proveedor.Detalles.voto3;

            for (var i = 0; i < R; i++) {
                voto3 += '<i class="fa fa-star"></i> ';
            }
            $(".Reputacion").html(repu);
            $(".voto1").html(voto1);
            $(".voto2").html(voto2);
            $(".voto3").html(voto3);

        }
        $(document).on('click', '.BTVALORAR', function() {

            setCookieV2("SolicitudValorada", JSON.stringify(Servicio));
            location.href = "valoracion_servicio.html";

        });
        Mensajes__();
        var mensajes = [];

        function Mensajes__() {
            var html = '';
            axios.get(DOMINIO + 'mensajes/cliente/?token=' + obj_data_usuario().token, {})
                .then(function(response) {
                    var data = response.data;
                    html += '<option selected="" value="" disabled>Elija un mensaje</option>';
                    mensajes = data;
                    for (var i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].tit_men + '</option>';
                    }

                    $("#mensaje").html(html);



                })
                .catch(function(error) {

                });
        }

        function mensajes_(id) {
            for (var i = 0; i < mensajes.length; i++) {
                if (mensajes[i].id == id) {
                    return mensajes[i];
                }
            }
            return false
        }
        var tit = null;
        var des = null;

        $("#mensaje").on('change', function() {
            if (this.value) {
                var men = mensajes_(this.value);
                tit = men.tit_men;
                des = men.des_men;
                $(".BTSENDM").removeClass("hide");
                $(".TEXTB").text(men.des_men);
            } else {
                $(".BTSENDM").addClass("hide");
                $(".TEXTB").text("");
            }
        });
        $(document).on('click', '.BTCANCELARS', function() {
            var editar = {
                id: Servicio.id,
                token: obj_data_usuario().token,
                cliente: obj_data_usuario()
            }
            axios.post(DOMINIO + 'solicitud_servicios/cancelar-solicitud', editar, progress__event($(".BTCANCELARS")))
                .then(function(response) {
                    location.href = "historial.html?id=" + editar.id;
                })
                .catch(function(error) {
                    console.log(error);

                });

        });
        $(".BTSENDM").on('click', function() {
            if (!tit || !des) {
                return;
            }
            var enviar = {
                id: Servicio.id,
                id_proveedor: Servicio.Proveedor.id,
                token: obj_data_usuario().token,
                usuario: obj_data_usuario(),
                tit: tit,
                des: des
            }
            axios.post(DOMINIO + 'solicitud_servicios/mensaje-preestablecido', enviar, progress__event($(".BTSENDM")))
                .then(function(response) {

                    if (response.data.r == true) {
                        alert(response.data.msg);
                        return;
                    }
                })
                .catch(function(error) {
                    console.log(error);

                });
        });


    }

});