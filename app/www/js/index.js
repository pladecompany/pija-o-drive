var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },
    onDeviceReady: function() {
        var este = this;
        this.receivedEvent('deviceready');
        //cordova.plugins.firebase.messaging.requestPermission();
        window.FirebasePlugin.grantPermission();
        //cordova.plugins.firebase.messaging.getToken().then(function(tokenFirebase) {
        window.FirebasePlugin.getToken(function(tokenFirebase) {
            var tablausuario = null;
            if ((getCookie('id') != "" && getCookie('id') != "null" && getCookie('id') != null)) {
                tablausuario = JSON.parse(getCookie('data_user')).tablausuario;
            }
            var data = {
                id_usuario: ((getCookie('id') != "" && getCookie('id') != "null" && getCookie('id') != null) ? getCookie('id') : null),
                tokenFirebase: tokenFirebase,
                tablausuario: tablausuario
            };
            axios({
                method: 'post',
                url: DOMINIO + "notificaciones/agregar-eliminar-firebase",
                data: data,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
            }).then(function(resp) { console.log(resp); }).catch(function(resp) { console.log(resp); });
        });
        //cordova.plugins.firebase.messaging.onTokenRefresh(function() {
        //    cordova.plugins.firebase.messaging.getToken().then(function(tokenFirebase) {
        window.FirebasePlugin.onTokenRefresh(function(tokenFirebase) {
            var tablausuario = null;
            if ((getCookie('id') != "" && getCookie('id') != "null" && getCookie('id') != null)) {
                tablausuario = JSON.parse(getCookie('data_user')).tablausuario;
            }
            var data = {
                id_usuario: ((getCookie('id') != "" && getCookie('id') != "null" && getCookie('id') != null) ? getCookie('id') : null),
                tokenFirebase: tokenFirebase,
                tablausuario: tablausuario
            };
            axios({
                method: 'post',
                url: DOMINIO + "notificaciones/agregar-eliminar-firebase",
                data: data,
                config: { headers: { 'Content-Type': 'multipart/form-data' } }
            }).then(function(resp) { console.log(resp); }).catch(function(resp) { console.log(resp); });
        });
        //});
        //cordova.plugins.firebase.messaging.onMessage(function(data) {
        window.FirebasePlugin.onMessageReceived(function(data) {
            // App abierta
            // alert(JSON.stringify(data));
            var googleMID = new Date().getTime();

            if (data.google.message_id) {
                googleMID = data.google.message_id;
            }
            document.querySelector(".campanaNoti").classList.add("text-rojo");
            document.querySelector(".campanaNoti").classList.add("animated");
            document.querySelector(".campanaNoti").classList.remove("text-gray");

            cordova.plugins.notification.local.isScheduled("" + googleMID + "", function(isScheduled) {

                if (!isScheduled && getCookie("googleMID") != googleMID) {
                    setCookie("googleMID", googleMID);
                    cordova.plugins.notification.local.schedule({
                        id: "" + googleMID + "",
                        title: data.title,
                        message: data.body,
                        text: data.body,
                        at: new Date(),
                        smallIcon: "www/img/logoapp.png",
                        icon: "www/img/logoapp.png",
                        data: data
                    });
                    cordova.plugins.notification.local.onclick = function(id, state, data) {
                        este.clickNotification(data);
                    }
                }
            });

        });
        /*cordova.plugins.firebase.messaging.onBackgroundMessage(function(data) {
            // App cerrada
            document.querySelector(".campanaNoti").classList.add("text-rojo");
            document.querySelector(".campanaNoti").classList.add("animated");
            document.querySelector(".campanaNoti").classList.remove("text-moradoOscuro");
            este.clickNotification(data);
        });*/
        cordova.plugins.locationAccuracy.canRequest(function(canRequest) {
            if (canRequest) {
                cordova.plugins.locationAccuracy.request(function(success) {

                }, function(error) {


                    if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {

                    }
                }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);

            }
        }, handleError);
        cordova.plugins.diagnostic.requestCameraAuthorization({
            successCallback: function(status) {
                console.log("Successfully requested camera authorization: authorization was " + status);
                checkState();
            },
            errorCallback: handleError,
            externalStorage: true
        });
        cordova.plugins.diagnostic.requestExternalStorageAuthorization(function(status) {
            console.log("Successfully requested SDCARD authorization: authorization was " + status);
            checkState();
        }, handleError);

        function handleError(error) {
            var msg = "Error: " + error;
            console.log(msg);
            // alert(msg);
        }

        function checkState() {
            // alert("cheking");
        }
    },
    clickNotification: function(data) {

        //alert(data.tipo_noti);

        if (data.tipo_noti == "solicitud-cambios-push") {
            location.href = "historial.html";
        } else if (data.tipo_noti == "solicitud-nuevo-push") {
            location.href = "inicio.html";
        } else if (data.tipo_noti == "verificar-push") {
            var obj = obj_data_usuario();
            if (obj_data_usuario().tablausuario == "clientes")
                obj.est_cli = 1;
            else
                obj.est_pro = 1;
            setCookie("data_user", JSON.stringify(obj));
            location.href = "inicio.html";
        } else if (data.tipo_noti == "verificar-pro-push") {
            var obj = obj_data_usuario();
            obj.estatus_verificado = 1;
            setCookie("data_user", JSON.stringify(obj));
            location.href = "inicio.html";
        } else if (data.tipo_noti == "nuevo-mensaje-chat") {
            location.href = "contacto.html";
        }

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();

/* 
Please tells me if it works:

modify:
mavenCentral()
jcenter()

to:
maven { url "https://maven.google.com" }
jcenter { url "http://jcenter.bintray.com/"}


classpath 'com.android.tools.build:gradle:3.0.0'
classpath 'com.google.gms:google-services:3.0.0'

in files:
platforms/android/build.gradle
platforms/android/app/build.gradle
platforms/android/CordovaLib/build.gradle
platforms/android/cordovasuportgoogleservice/nombreapp-build.gradle
*/