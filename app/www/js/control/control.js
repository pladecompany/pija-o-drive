if (RegistroDetalles == "true")
    RegistroDetalles = RegistroDetalles;
else
    var RegistroDetalles = "";
if (NOTI == "true")
    NOTI = NOTI;
else
    var NOTI = "";

if (validar_sesion == "true")
    validar_sesion = validar_sesion;
else
    var validar_sesion = "";

if (home == "true")
    home = home;
else {
    var home = "";
}

if (getCookie('data_user')) {

    datausu = JSON.parse(getCookie('data_user'));
    if (!datausu.Detalles && RegistroDetalles == "" && datausu.tablausuario == "proveedores") {
        location.href = "registro_proveedor.html";
    }

    if (home != "true") {
        if (datausu.tablausuario == "proveedores") {
            if (RegistroDetalles == "" && datausu.Detalles) {
                if (datausu.est_pro != 1)
                    location.href = "inicio.html";
                if (datausu.estatus_verificado != 1)
                    location.href = "inicio.html";
            }


        } else {
            if (datausu.est_cli != 1)
                location.href = "inicio.html";
        }
    }




}



//ARCHIVOS REQUERIDOS
document.write("<script src='js/jquery.min.js'></script>");
document.write("<script src='js/materialize.js'></script>");
document.write("<script src='js/script.js'></script>");
document.write("<script src='js/control/document_ready.js'></script>");
document.write("<script src='js/axios.min.js'></script>");
document.write('<script src="js/moment/jquery.livequery.min.js"></script>');
document.write('<script src="js/moment/jquery.timeago.js">');
document.write('<script src="js/validCampo.js"></script>');
document.write('<script src="js/socket/socket.js"></script>');
document.write('<script src="js/funciones/socket_emit_on.js"></script>');
document.write('<script type="text/javascript" src="js/mask.js"></script>');

function convertirAlert() {
    alert = function alert(a) { M.toast({ html: a }, 5000); }
}
var intervalEnproceso = null;

function UbiAct(ubicacionActual) {
    variableGLOBAL();
    datausu = JSON.parse(getCookie('data_user'));
    if (datausu.tablausuario == "proveedores") {
        datausu.lat_pro = ubicacionActual.lat;
        datausu.lon_pro = ubicacionActual.lon;
    } else {
        datausu.lat_cli = ubicacionActual.lat;
        datausu.lon_cli = ubicacionActual.lon;
    }
    setCookie("data_user", JSON.stringify(datausu));
    ubicacionActual.id_usuario = datausu.id;

    axios.post(DOMINIO + datausu.tablausuario + '/ubicacionActual', ubicacionActual).then(response => {
        if (response.data.r == false) {
            //alert(response.data.msg);
        } else {
            if (response.data.Activas) {
                if (document.getElementById("ENPROCESO")) document.getElementById("ENPROCESO").innerText = "(" + response.data.Activas + ")";
            } else {
                if (document.getElementById("ENPROCESO")) document.getElementById("ENPROCESO").innerText = "";
            }
            if (response.data.id) {
                setCookie("data_user", JSON.stringify(response.data));
            }

            if (response.data.Sinvalorar && datausu.tablausuario == "proveedores") {

                if (response.data.Sinvalorar > 0) {
                    //if (document.querySelector(".EleINI1")) document.querySelector(".EleINI1").classList.add("hide");
                    //if (document.querySelector(".SoliINI")) document.querySelector(".SoliINI").classList.add("hide");
                    if (document.querySelector(".VALpendiente")) {
                        document.querySelector(".VALpendiente").classList.remove("hide");
                        $(".linkHP").attr('href', 'historial.html?id=' + response.data.SinvalorarId + '');
                    }

                } else {
                    if (document.querySelector(".EleINI1")) document.querySelector(".EleINI1").classList.remove("hide");
                    if (document.querySelector(".SoliINI")) {
                        document.querySelector(".SoliINI").classList.remove("hide");
                        if (obj_data_usuario().tablausuario == "proveedores") {
                            if (obj_data_usuario().est_pro == 1 && obj_data_usuario().estatus_verificado == 1) {
                                $('.modal').modal({ inDuration: 0, outDuration: 0 });
                            }
                        }
                    }
                    if (document.querySelector(".VALpendiente")) document.querySelector(".VALpendiente").classList.add("hide");
                }
                return;
            } else {
                if (document.querySelector(".EleINI1")) document.querySelector(".EleINI1").classList.remove("hide");
                if (document.querySelector(".SoliINI")) {
                    if (document.querySelector(".SoliINI")) {
                        document.querySelector(".SoliINI").classList.remove("hide");
                        if (obj_data_usuario().tablausuario == "proveedores") {
                            if (obj_data_usuario().est_pro == 1 && obj_data_usuario().estatus_verificado == 1) {
                                $('.modal').modal({ inDuration: 0, outDuration: 0 });
                            }
                        }
                    }
                }
                if (document.querySelector(".VALpendiente")) document.querySelector(".VALpendiente").classList.add("hide");
                if (datausu.tablausuario == "proveedores") {
                    return;
                }
            }
            if (response.data.Sinvalorar && datausu.tablausuario == "clientes") {
                if (response.data.Sinvalorar > 0) {
                    //if (document.querySelector(".EleINI1")) document.querySelector(".EleINI1").classList.add("hide");
                    //if (document.querySelector(".EleINI2")) document.querySelector(".EleINI2").classList.add("hide");
                    //if (document.querySelector(".EleINI3")) document.querySelector(".EleINI3").classList.add("hide");
                    if (document.querySelector(".VALpendiente")) {
                        document.querySelector(".VALpendiente").classList.remove("hide");
                        $(".linkHC").attr('href', 'historial.html?id=' + response.data.SinvalorarId + '');
                    }

                } else {
                    if (document.querySelector(".EleINI1")) document.querySelector(".EleINI1").classList.remove("hide");
                    if (document.querySelector(".EleINI2")) document.querySelector(".EleINI2").classList.remove("hide");
                    if (document.querySelector(".EleINI3")) document.querySelector(".EleINI3").classList.remove("hide");
                    if (document.querySelector(".VALpendiente")) document.querySelector(".VALpendiente").classList.add("hide");
                }
            } else {
                if (document.querySelector(".EleINI1")) document.querySelector(".EleINI1").classList.remove("hide");
                if (document.querySelector(".EleINI2")) document.querySelector(".EleINI2").classList.remove("hide");
                if (document.querySelector(".EleINI3")) document.querySelector(".EleINI3").classList.remove("hide");
                if (document.querySelector(".VALpendiente")) document.querySelector(".VALpendiente").classList.add("hide");
            }
        }
    });
}

function geo() {

    var device = document.addEventListener("deviceready", function() {
        if (canRequest == "") {
            cordova.plugins.locationAccuracy.canRequest(canrequest, ErrorCant);
        } else {
            canrequest();
        }

        function canrequest() {
            canRequest = 1;
            cordova.plugins.locationAccuracy.request(
                function(success) {
                    navigator.geolocation.getCurrentPosition(
                        function(pos) {

                            UbiAct({ lat: pos.coords.latitude, lon: pos.coords.longitude });
                        },
                        function(error) {
                            console.log(error);

                        }
                    );
                },
                function(error) {
                    console.log(error);
                    navigator.geolocation.getCurrentPosition(
                        function(pos) {
                            UbiAct({ lat: pos.coords.latitude, lon: pos.coords.longitude });
                        },
                        function(error) {
                            console.log(error);

                        }
                    );
                    if (
                        error.code !==
                        cordova.plugins.locationAccuracy
                        .ERROR_USER_DISAGREED
                    ) {

                    }
                },
                cordova.plugins.locationAccuracy
                .REQUEST_PRIORITY_HIGH_ACCURACY
            );
        }

        function ErrorCant() {
            console.log(error);

        }
    });
    if (!device) {
        navigator.geolocation.getCurrentPosition(
            function(pos) {
                UbiAct({ lat: pos.coords.latitude, lon: pos.coords.longitude });
            },
            function(error) {
                console.log(error);

            }
        );
    }

}

function progress__event(bt) {
    var html = "";
    if (bt) {

        bt.attr('disabled', true);
        html = bt.html();
        bt.html("<i class='fa fa-spin fa-spinner'></i>");
    }
    var configprogress = {
        onUploadProgress: progressEvent => {
            EstatusProgress(Math.floor((progressEvent.loaded * 100) / progressEvent.total), bt , html);
        }
    };
    return configprogress;
}

function EstatusProgress(value, bt, html) {

    var ProgressP = document.querySelector("#Progress__P");
    var ProgressC = document.querySelector("#Progress__C");
    if (ProgressP && ProgressC) {
        ProgressP.style.display = "block";
        if (value < 100) {
            ProgressC.style.width = value + '%';

        } else {
            if (bt) {
                bt.attr('disabled', false);
                bt.html(html);
            }
            ProgressC.style.width = value + '%';
            setTimeout(function() {
                ProgressP.style.display = "none";
                ProgressC.style.width = '0%';
            }, 500);
        }
    } else {
        if (bt) {
            bt.attr('disabled', false);
            bt.html(html);
        }
    }
}

function GETV(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function screen_map(map_id) {
    $("#" + map_id + "").toggleClass('map_full');
    $(".formulario_inputs").toggleClass('inputs_forms_map');
}

function handleFileSelect(file) {
    var file = file.target.files[0]; // FileList object
    var reader = new FileReader();
    reader.onloadend = function(evt) {
        // console.log("read success");
        // console.log(evt.target.result);
        $('.foto_perfil').attr('src', evt.target.result);
    };
    reader.readAsDataURL(file);


}

function handleFileSelect_otros(file, img) {
    var file = file.target.files[0]; // FileList object
    var reader = new FileReader();
    reader.onloadend = function(evt) {
        // console.log("read success");
        // console.log(evt.target.result);

        $(img).attr('src', evt.target.result);
    };
    reader.readAsDataURL(file);


}

function replaceURL(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp, "<a target='_blanck' href='$1'>$1</a>");


}

function imgenpordefecto() {
    var img = "this.src ='img/logo.png'";
    var erroryes = 'onerror="' + img + '"';
    return erroryes;
}

var ajax_timeout;

//enviar op=1 para mostrar cargando y img=1 para mostrar img cargando 
function modal_wait(html, op, img) {
    $('#close_modal_wait_bt').click();
    $('#close_modal_wait_bt').click();
    if (op == 1) {
        if (img == "0")
            $("#img_ok").css("display", "none");
        else
            $("#img_ok").css("display", "inline-block");

        $("#message-text").html(html);

        setTimeout(function() { $("#open_modal_wait_bt").click(); }, 1000);

        ajax_timeout = setTimeout(function() { modal_wait('', 0, 0); }, 60000); //

    } else {
        $("#img_ok").css("display", "inline-block");
        $("#Modal_Loading_Body").html("");
        $('#close_modal_wait_bt').click();
        clearTimeout(ajax_timeout);
    }
}

function modal_wait_no_vence(html, op, img) {
    $('#close_modal_wait_bt').click();
    $('#close_modal_wait_bt').click();
    if (op == 1) {
        if (img == "0")
            $("#img_ok").css("display", "none");
        else
            $("#img_ok").css("display", "inline-block");

        $("#message-text").html(html);

        setTimeout(function() { $("#open_modal_wait_bt").click(); }, 1000);



    } else {
        $("#img_ok").css("display", "inline-block");
        $("#Modal_Loading_Body").html("");
        $('#close_modal_wait_bt').click();

    }
}

//enviar op=1 para mostrar boton ok
function modal_alert(html, op) {
    $('#close_modal_alert_bt').click();
    $('#close_modal_alert_bt').click();
    if (op == "0")
        $("#close_modal_alert_bt").css("display", "none");
    else
        $("#close_modal_alert_bt").css("display", "block");

    $("#Modal_Alert_Body").html(html);
    if ($("#Modal_Alert").css("display") == "none")
        setTimeout(function() { $("#open_modal_alert_bt").click(); }, 1000);

}

function Modal_Filter() {
    setTimeout(function() { $("#open_modal_filter_bt").click(); }, 500)
}

function noti_alert(html) {
    setTimeout(function() {
        Materialize.toast(html, 4500);
    }, 300);
}

function validarEmail(email) {
    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    // Se utiliza la funcion test() nativa de JavaScript
    var valor = regex.test(email.trim());

    if (valor == true) {
        return valor;
    } else {
        return valor;
    }
}

function calcular_edad(birthday) {
    var year, month, day, age, year_diff, month_diff, day_diff;
    var myBirthday = new Date();
    var today = new Date();
    var array = birthday.split("-");

    year = array[0];
    month = array[1];
    day = array[2];
    year_diff = today.getFullYear() - year;
    month_diff = (today.getMonth() + 1) - month;
    day_diff = today.getDate() - day;

    if (month_diff < 0) {
        year_diff--;
    } else if ((month_diff === 0) && (day_diff < 0)) {
        year_diff--;
    }

    return year_diff;
}



//------------------------------------COOKIES
function setCookie(nombre, valor, caducidad) {
    //Si no tenemos caducidad para la cookie, la definimos a 31 dias
    setCookieV2(nombre, valor, caducidad);
}

function getCookie(nombre) {
    return getCookieV2(nombre);
}

//COOKIE TEMPORAL 
function setCookieV2(nombre, valor, caducidad) {
    window.localStorage.setItem(nombre, valor);

}

function getCookieV2(nombre) {
    if (window.localStorage.getItem(nombre) == null || window.localStorage.getItem(nombre) == "undefined")
        return "";
    else
        return window.localStorage.getItem(nombre);
}

function recibirmensaje(mensaje) {
    alert('Soporte te ha escrito <a href="contacto.html" style="color:#fff;padding:1px 5px;font-weight:600; padding-right:7px;background-color:var(--orange);border-radius: 5px;margin-left:10px;">Ver</a>');
    //Materialize.toast('Nuevo mensaje', 4000)
}