$(document).on('ready', function() {
    $('#side-header').load('components/header.html');
    $('#side-nav').load('components/nav.html');
    $('body').prepend('<div id="ProgressBar"></div>');
    $('#ProgressBar').load('components/progres.html');

    convertirAlert();
    if (getCookie('data_user')) {
        datausu = JSON.parse(getCookie('data_user'));
        if (home == "true" && document.getElementById('MainHome')) {
            if (datausu.tablausuario == "proveedores") {
                $('#MainHome').load('inicioProveedor.html');
            } else {
                $('#MainHome').load('inicioCliente.html');
            }
        }
        if (document.getElementById('MainHistorial')) {
            if (datausu.tablausuario == "proveedores") {
                $('#MainHistorial').load('hProveedor.html');
            } else {
                $('#MainHistorial').load('hCliente.html');
            }
        }
        geo();
        setInterval(function() {
            geo();
        }, 60000);
        setTimeout(function() { $('#llegoPIJAO.modal').modal(); }, 300);
    }
    $('.timeago').livequery(function() {
        $(this).timeago();
    });
});