$(document).ready(function() {
    M.AutoInit();
    $('.modal').modal({ inDuration: 0, outDuration: 0 });
    $('select').formSelect();
    $('.tabs').tabs();
    $("select").css("display", "block");
    $('.sidenav').sidenav();
    $('.dropdown-trigger').dropdown();
});