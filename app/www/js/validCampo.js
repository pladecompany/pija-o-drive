(function($) {
    $.fn.validCampo = function(cadena) {
        $(this).on({
            keypress : function(e){
                var key = e.which,
                    keye = e.keyCode,
                    tecla = String.fromCharCode(key).toLowerCase(),
                    letras = cadena;
                if(letras.indexOf(tecla)==-1 && keye!=9&& (key==37 || keye!=37)&& (keye!=39 || key==39) && keye!=8 && (keye!=46 || key==46) || key==161){
                    e.preventDefault();
                }
            }
        });
    };
    $(document).on('blur keyup keydown', '.text', function(){
        $(this).validCampo("abcdefghijklmnñopqrstuvwxyz ");
    });
    $(document).on('blur keyup keydown', '.number', function(){
        $(this).validCampo("1234567890");
    });
    $(document).on('blur keyup keydown', '.number-2', function(){
        $(this).validCampo("1234567890.");
    });
    $(document).on('blur keyup keydown', '.number-1', function(){
        $(this).validCampo("1234567890-");
    });
    $(document).on('blur keyup keydown', '.text-number-', function(){
        $(this).validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");
    });
    $(document).on('blur keyup keydown', '.text-number-1', function(){
        $(this).validCampo("1234567890abcdefghijklmnñopqrstuvwxyz- ");
    });
   
})( jQuery );
