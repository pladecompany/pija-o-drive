$(document).ready(function() {
    listClientes();

    function listClientes() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'clientes',
            type: 'GET',
            data: data,
            success: function(data) {
                console.log(data);
                actualizar_tabla_clientes(data)
            }

        })
    }

    function actualizar_tabla_clientes(data) {
        var table = $('.clientes').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            if (datos.img_cli)
                var imgc = "<img src='" + datos.img_cli + "'; " + imgenpordefecto() + " width='45px' class='circle' height='45px' style='border: 1px solid #ccc; padding: 1px'>";
            else
                var imgc = "<img src=''; " + imgenpordefecto() + " width='45px' class='circle' height='45px' style='border: 1px solid #ccc; padding: 1px'>";
            if (datos.est_cli == 1) {
                sts = 'Activo';
                col = '#4caf50';
            } else if (datos.est_cli == 2) {
                sts = 'Bloqueado';
                col = '#ff2828';
            } else {
                sts = 'Pausado';
                col = '#ffc82b';
            }
            tltwha = datos.tel_cli;
            if (tltwha) {
                tltwha = tltwha.replace("+", "");
            }
            viajes = 0;
            puntos = 0;
            if (datos.DetallesC && datos.DetallesC.Reputacion)
                puntos = datos.DetallesC.Reputacion;
            if (datos.DetallesC && datos.DetallesC.Viajes_total)
                viajes = datos.DetallesC.Viajes_total;

            puntos = '<a class="btn btn-orange ver-puntos" id="' + datos.id + '" name="Puntos de ' + datos.nom_cli + '">' + puntos + ' <i class="fa fa-star"></i></a>';
            viajes = '<a class="btn btn-orange ver-puntos" id="' + datos.id + '" name="Viajes de ' + datos.nom_cli + '">' + viajes + ' <i class="fa fa-taxi"></i></a>';

            var btnsts = '<a class="btn btn-orange sts_c sts_c_' + datos.id + '" id="' + datos.id + '" name="' + datos.est_cli + '"><i class="fa fa-cog"></i></a>'
            var btnpush = '<a title="PUSH" class="btn btn-orange msj_cli" id="' + datos.id + '"><i class="fa fa-envelope"></i></a>';
            var btnwha = '<a title="Whatsapp" class="btn btn-orange" href="https://api.whatsapp.com/send?phone=' + tltwha + '" target="_blank"><i class="fab fa-whatsapp"></i></a>';
            var option = '<a class="btn btn-orange delete_c" id="' + datos.id + '"><i class="fa fa-trash"></i></a> <a class="btn btn-orange btPrint" id="' + datos.id + '"><i class="fa fa-file-pdf"></i></a> <a class="btn btn-orange" href="?op=chat&envio=1&id_usuario=' + datos.id + '&nombre=' + datos.nom_cli + ' (cliente)"><i class="fa fa-comments"></i></a>';
            var row = [count += 1,datos.id, imgc, "<textarea id='cli_ver_" + datos.id + "' style='display:none;'>" + JSON.stringify(datos) + "</textarea>" + datos.nom_cli, datos.tel_cli, datos.cedula_cli, datos.cor_cli, viajes, puntos, '<span class="txt-sts-' + datos.id + '" style="color:' + col + ';">' + sts + '</span> ' + btnsts, btnpush+' '+btnwha+' '+ option]
            table.row.add(row).draw().node();
        }
    }

    $(document).on('click', ".sts_c", function() {
        var idc = this.id;
        var ids = this.name;
        $("#status").modal('open');
        $("#id_cli").val(idc);
        $("#sts_edit").val(ids).trigger("change");
    });
    $(document).on('click', ".save-sts-cli", function() {
        if ($("#comentario").val().trim() == "") {
            M.toast({ html: "Debes ingresar un comentario." }, 5000);
            return;
        }
        var idc = $("#id_cli").val();
        var ids = $("#sts_edit").val();
        if (ids == "1")
            stsT = 'Activo';
        else if (ids == "2")
            stsT = 'Bloqueado';
        else
            stsT = 'Pausado';
        var formData = new FormData();
        formData.append("id", idc);
        formData.append("est_cli", ids);
        formData.append("comentario", $("#comentario").val());
        formData.append("stsT", stsT);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        $.ajax({
            url: dominio + "clientes/editar-status",
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.r) {
                    $("#comentario").val('');
                    txt = $('select[name="sts_edit"] option:selected').text();
                    $(".txt-sts-" + idc).html(txt);
                    if (ids == 1) {
                        col = '#4caf50';
                    } else if (ids == 2) {
                        col = '#ff2828';
                    } else {
                        col = '#ffc82b';
                    }
                    $(".txt-sts-" + idc).css('color', col);
                    $(".sts_c_" + idc).attr("name", ids);
                    $("#status").modal('close');

                    var obj = JSON.parse($("#cli_ver_" + idc).val());
                    obj.est_cli = ids;
                    $("#cli_ver_" + idc).val(JSON.stringify(obj));

                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });

    $(document).on('click', ".msj_cli", function() {
        var idc = this.id;
        $("#mensaje").modal('open');
        $("#id_cli_msj").val(idc);
    });
    $(document).on('click', ".save-msj-cli", function() {
        $(this).attr("disabled", true);
        var idc = $("#id_cli_msj").val();
        var msj = $("#msj_cli").val();
        var formData = new FormData();
        formData.append("id", idc);
        formData.append("msj", msj);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        $.ajax({
            url: dominio + "clientes/enviar-push",
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                $(".save-msj-cli").attr("disabled", false);
                if (data.r) {
                    $("#msj_cli").val('')
                    $("#mensaje").modal('close');
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });




    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click', '.delete_c', function() {
        var idv = this.id;
        var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

        M.toast({ html: toastContent }, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        console.log('click');
        id = this.id
        var data = {
            ida: user.id,
            token: user.token
        };
        M.Toast.dismissAll();
        $.ajax({
            url: dominio + 'clientes/delete/' + id,
            type: 'GET',
            data: data,
            success: function(data) {
                if (data.msj) {
                    M.toast({ html: data.msj }, 10000);
                    listClientes();
                } else {
                    M.toast({ html: "Error al eliminar." }, 10000);

                }
            }
        })

    })

    $(document).on('click', '.btPrint', function() {

        var cliente = $(this).attr("id");
        if (cliente) {
            $("#tablainfo").val($("#cli_ver_" + cliente).val());
            $("#formprint")[0].submit();
        }

    });
    $(document).on('click', ".ver-puntos", function() {

        var idp = this.id;
        var idn = this.name;
        $("#puntos").modal('open');
        $("#id_ver_puntos").val(idp);
        $("#id_reg_pun").val(idp);
        $(".name-puntos").html(idn);

        listarpuntuacion(idp);

    });
    $(document).on('change', "#mes_pun , #ano_pun", function() {
        listarpuntuacion($('#id_reg_pun').val());
    });

    function listarpuntuacion(idp) {
        var table = $('#table_p').DataTable({
            'processing': true,
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        $("#btn_pdf_punta").hide();
        $("#tablainfopuntos").val('');

        data = {
            ida: user.id,
            token: user.token,
            id: idp,
            mes: $("#mes_pun").val(),
            ano: $("#ano_pun").val()
        }
        $.ajax({
            url: dominio + 'solicitud_servicios/propiasC/admin',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                if (data.length > 0) {
                    $("#btn_pdf_punta").show();
                    info = JSON.stringify(data);
                    $("#tablainfopuntos").val(info);
                } else {
                    M.toast({ html: "No se encontro ningun resultado." }, 5000);
                }


                for (var i = 0; i < data.length; i++) {
                    if(data[i].Cliente.ReviewRecibida){
                        //lim = '<center><span class="btn btn-orange ">'+data[i].Cliente.ReviewRecibida.voto1+' <i class="fa fa-star"></i></span></center>';
                        ama = '<center><span class="btn btn-orange ">' + data[i].Cliente.ReviewRecibida.voto2 + ' <i class="fa fa-star"></i></span></center>';
                        //pun = '<center><span class="btn btn-orange ">'+data[i].Cliente.ReviewRecibida.voto3+' <i class="fa fa-star"></i></span></center>';

                        var row = [count += 1, data[i].Proveedor.nom_pro, data[i].Proveedor.tel_pro, ama, '<i class="fa fa-comments"></i>: ' + data[i].Cliente.ReviewRecibida.comentario_rep]
                        table.row.add(row).draw().node();
                    }
                }
            }

        })
    }
});