$(document).ready(function(){
    listproveedores();
    listpagos();
    listplanes();

    planprove = [];
    selectplanes = [];
    preplanes = [];
    //funcion para quitar tabindex del modal y funcione el select2
    document.getElementById("form").removeAttribute("tabindex"); 
    function listproveedores(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'proveedores',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_tabla_proveedores(data)
            }

        })
    }

    function actualizar_tabla_proveedores(data){
        veri=0;
        for(var i=0;i<data.length;i++){
            veri++;
            datos = data[i];
            planprove[datos.id]=datos.id_plan;
            $("#pro").append('<option value="'+datos.id+'">'+datos.nom_pro+' ('+datos.cedula_pro+')</option>');
            if(veri==data.length){
                $('#pro').select2({    
                  language: {
                    noResults: function() {
                      return "No hay resultado";        
                    },
                    searching: function() {
                      return "Buscando..";
                    }
                  }
                });         
            }
        }
        
    }
    function listplanes(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'planes',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_select_planes(data);
            }

        });
    }
    function actualizar_select_planes(data){
        
        for(var i=0;i<data.length;i++){
            datos = data[i];
            selectplanes[datos.id]=datos.tit_pla;
            preplanes[datos.id]=datos.pre_pla;
        }
    }

    function listpagos(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'pagos',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_tabla_pagos(data)
            }

        })
    }

    function actualizar_tabla_pagos(data){
        var table = $('.pagost').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i];
            datos = data[i];
            if(datos.Proveedor.img_pro)
              var imgc = "<img src='"+datos.Proveedor.img_pro+"'; "+imgenpordefecto()+" width='45px' class='circle' height='45px' style='border: 1px solid #ccc; padding: 1px'>"; 
            else
              var imgc = "<img src=''; "+imgenpordefecto()+" width='45px' class='circle' height='45px' style='border: 1px solid #ccc; padding: 1px'>";
            fec = moment(datos.fecha_reg).format("DD-MM-YYYY")
            pago='';
            if(datos.estatus==1)
                pago='checked';
            var sts='<div class="switch">'
                +'<label>'
                +'  No'
                +'  <input type="checkbox" '+pago+' class="status" id="'+datos.id+'">'
                +'  <span class="lever"></span>'
                +'  Si'
                +'</label>'
              +'</div>';
            var option = '<a class="btn btn-orange edit-pago" id="'+datos.id+'"><i class="fa fa-eye"></i></a> <a class="btn btn-orange delete_c" id="'+datos.id+'"><i class="fa fa-trash"></i></a>';
            var row = [count+=1,imgc,"<textarea id='pago_s_"+datos.id+"' style='display:none;'>" + JSON.stringify(datos) + "</textarea>"+datos.Proveedor.nom_pro,datos.Proveedor.cedula_pro,datos.Proveedor.tel_pro,datos.plan,"$ "+datos.valor,fec,sts,option]
            table.row.add(row).draw().node();
        }
    }
    $(document).on('change', "#pro", function(){
        idp=$(this).val();
        $("#infoplan").html('');
        $("#pla").val('');
        $("#pre").val('');
        if(idp!=''){
            plan=planprove[idp];
            if(plan){
                txtplan=selectplanes[plan];
                preplan=preplanes[plan];
                $("#pla").val(txtplan);
                $("#infoplan").html(txtplan);
                $("#pre").val(preplan);
            }else{
                $("#infoplan").html('Sin plan');
                $("#pre").val('');
            }
        }
    });

    $(document).on('click', ".edit-pago", function(){
        idplan = $(this).attr("id");
        var obj = JSON.parse($("#pago_s_"+idplan).val());
        $("#id_pago").val(obj.id);
        $("#pro").val(obj.id_proveedor).trigger("change");
        $("#pro").attr("disabled",true);
        $("#pre").val(obj.valor);
        $("#infoplan").html(obj.plan);
        $("#pla").val(obj.plan);
        fec = moment(obj.fecha_reg).format("YYYY-MM-DD");
        $("#fecha").val(fec);
        $("#con").val(obj.concepto);
        //$("#tit").val(obj.tit_pla);
        $("#form").modal('open');
    });
    
    $(document).on('click', ".btn-save", function(){
        idp=$("#id_pago").val();
        var formData = new FormData();
        formData.append("pro" ,$("#pro").val());
        formData.append("pla" ,$("#pla").val());
        formData.append("pre" ,$("#pre").val());
        formData.append("con" ,$("#con").val());
        formData.append("fecha" ,$("#fecha").val());
        formData.append("id" , idp);
        formData.append("ida" ,user.id);
        formData.append("token" ,user.token);
        if(idp!='')
            url ="pagos/editar";
        else
            url ="pagos";
        $.ajax({
            url: dominio + "" +url, 
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
                if(data.r){
                    listpagos();
                    $("#form").modal('close');
                    $(".limpiar_form").val('');
                    M.toast({html:data.msg}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
                
            },
            error: function(xhr, status, errorThrown){
              console.log(xhr);
            }
        });
    });
    
    $(document).on('change','.status',function(){
        idp=$(this).prop("id");
        if ($(this).is(':checked') ) {
            valor=1;
        } else {
            valor=0;
        }
        var formData = new FormData();
        formData.append("id" ,idp);
        formData.append("estatus" ,valor);
        formData.append("ida" ,user.id);
        formData.append("token" ,user.token);
        $.ajax({
            url: dominio + "pagos/editar-status", 
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
                if(data.r){
                    var obj = JSON.parse($("#pago_s_"+idp).val());
                    obj.estatus=ids;
                    $("#pago_s_"+idp).val(JSON.stringify(obj));

                    M.toast({html:data.msg}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
                
            },
            error: function(xhr, status, errorThrown){
              console.log(xhr);
            }
        });
    });
    
    
    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click','.delete_c',function(){
      var idv = this.id;
      var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
      id = this.id
      var data = {
        ida: user.id,
        token: user.token
      };
      M.Toast.dismissAll();
      $.ajax({
        url:dominio+'pagos/delete/'+id,
        type:'GET',
        data:data,
        success:function(data){
            if(data.msj){
                M.toast({html:data.msj}, 10000);
                listpagos();
            }else{
                M.toast({html:"Error al eliminar."}, 10000);
              
            }
        }
      })

    })
});

