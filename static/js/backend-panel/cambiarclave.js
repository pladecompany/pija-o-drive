document.write("<script src='../static/js/back/general.js'></script>");


$(document).ready(function(){
$('#perfil').attr('src',dominio+user.img_usu)

//submit al cambiar clave
    $("#cambiar_clave").on('submit', function(e){
        e.preventDefault();
        $('#save').prop('disabled', true);
        $('#save1').addClass('fa-spinner fa-spin');

        var data = new FormData(this);
        data.append("id", user.id);
        $.ajax({
          url: dominio + "usuarios/cambiar-clave",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
        success: function(data){
          M.toast({html:data.msj}, 10000);
          if (data.msj == "Ha cambiado su contraseña satisfactoriamente.") {
              $('#cambiar_clave').trigger("reset");
          }
        },
        error: function(xhr, status, errorThrown){
            //M.toast({html:err.data.msj}, 10000);
          console.log("Errr : ");
        }
        });

    });

});
