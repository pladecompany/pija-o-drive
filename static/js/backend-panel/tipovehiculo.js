$(document).ready(function(){
   
  tiposVeh__();
  var TiposVeh = [];
    function tiposVeh__(){
        var html = '';              
         
         $.ajax({
            url : dominio+'tipovehiculo/panel/?token='+user.token,
            type: 'GET',
            data:{},
            success:function(response){
               TiposVeh = response;
               actualizar_tabla_tipo(response);
            },
            error: function(xhr, status, errorThrown){
                console.log(xhr);
            }

        });
  }
  function returnTipo(id){
    for (var i = 0; i < TiposVeh.length; i++) {
        if (TiposVeh[i].id==id){
          return TiposVeh[i];
        }
    }
    return false;
  }
   function actualizar_tabla_tipo(data){
        var table = $('.tablaTipo').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i];
            var option = '<a href="#modalVehiculos" class="btn btn-orange BTEdit" id="'+datos.id+'"><i class="fa fa-eye"></i></a> <a class="btn btn-orange BTDel" id="'+datos.id+'"><i class="fa fa-trash"></i></a>';
            var row = [count+=1,`<img width="40px" src="../static/img/transporte/${datos.icono_veh}">`,datos.nombre_veh,option]
            table.row.add(row).draw().node();
        }
    }
   
  $(document).on('click', '.BTNuevo', function() {
    $(".btsave").removeClass('hide');
    $(".btedit").addClass('hide');
    $("#form")[0].reset();    
    $("#modalVehiculos").modal('open');  
  });

  $(document).on('click', '.BTEdit', function() {
    $("#form")[0].reset();    
    $(".btsave").addClass('hide');
    $(".btedit").removeClass('hide');
    $("#modalVehiculos").modal('open');
    var Tipov = returnTipo(this.id);
    $("#id_tipo_ve").val(Tipov.id);
    $("#nombre_veh").val(Tipov.nombre_veh); 
    $("#icono_veh").val(Tipov.icono_veh); 
  });
  $(document).on('click', '.BTsubmit', function() {
      
        var formData = new FormData();
        formData.append("nombre_veh" ,$("#nombre_veh").val());
        formData.append("icono_veh" ,$("#icono_veh").val());
        formData.append("id" ,$("#id_tipo_ve").val());
        
        formData.append("token" ,user.token);
        if($(this).attr("tipo")=="2")
            url ="tipovehiculo/editar";
        else
            url ="tipovehiculo/agregar";
        $.ajax({
            url: dominio + "" +url, 
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
                if(data.r){
                    tiposVeh__();
                    $("#modalVehiculos").modal('close');
                    
                    M.toast({html:data.msg}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
                
            },
            error: function(xhr, status, errorThrown){
              console.log(xhr);
            }
        });
  });
  $(document).on('click','.BTDel',function(){
      var idv = this.id;
      var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
      id = this.id
      var data = {
        ida: user.id,
        token: user.token
      };
      M.Toast.dismissAll();
      $.ajax({
        url:dominio+'tipovehiculo/delete/'+id,
        type:'GET',
        data:data,
        success:function(data){
            if(data.msj){
                M.toast({html:data.msj}, 10000);
                 tiposVeh__();
            }else{
                M.toast({html:"Error al eliminar."}, 10000);
              
            }
        }
      })

    })
  
});
