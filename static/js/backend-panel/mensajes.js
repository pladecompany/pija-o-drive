$(document).ready(function(){
    listmsjs();
    function listmsjs(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'mensajes',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_tabla_msj(data)
            }

        })
    }

    function actualizar_tabla_msj(data){
        var table = $('.msj').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i];
            var option = '<a class="btn btn-orange edit-msj" id="'+datos.id+'"><i class="fa fa-eye"></i></a> <a class="btn btn-orange delete_c" id="'+datos.id+'"><i class="fa fa-trash"></i></a>';
            var row = [count+=1,"<textarea id='msj_s_"+datos.id+"' style='display:none;'>" + JSON.stringify(datos) + "</textarea>"+datos.tit_men,datos.des_men,option]
            table.row.add(row).draw().node();
        }
    }
    $(document).on('click', ".edit-msj", function(){
        idm = $(this).attr("id");
        var obj = JSON.parse($("#msj_s_"+idm).val());
        $("#id_msj").val(obj.id);
        $("#des").val(obj.des_men);
        $("#tit").val(obj.tit_men);
        $("#form").modal('open');
    });
    
    $(document).on('click', ".btn-save", function(){
        idp=$("#id_msj").val();
        var formData = new FormData();
        formData.append("tit" ,$("#tit").val());
        formData.append("des" ,$("#des").val());
        formData.append("id" , idp);
        formData.append("ida" ,user.id);
        formData.append("token" ,user.token);
        if(idp!='')
            url ="mensajes/editar";
        else
            url ="mensajes";
        $.ajax({
            url: dominio + "" +url, 
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
                if(data.r){
                    listmsjs();
                    $("#form").modal('close');
                    $(".limpiar_form").val('');
                    M.toast({html:data.msg}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
                
            },
            error: function(xhr, status, errorThrown){
              console.log(xhr);
            }
        });
    });
    
    
    
    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click','.delete_c',function(){
      var idv = this.id;
      var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
      id = this.id
      var data = {
        ida: user.id,
        token: user.token
      };
      M.Toast.dismissAll();
      $.ajax({
        url:dominio+'mensajes/delete/'+id,
        type:'GET',
        data:data,
        success:function(data){
            if(data.msj){
                M.toast({html:data.msj}, 10000);
                listmsjs();
            }else{
                M.toast({html:"Error al eliminar."}, 10000);
              
            }
        }
      })

    })
});

