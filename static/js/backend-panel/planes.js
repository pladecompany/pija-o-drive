$(document).ready(function(){
    listplanes();
   
    function listplanes(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'planes',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_tabla_planes(data)
            }

        })
    }

    function actualizar_tabla_planes(data){
        var table = $('.plan').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i];
            var option = '<a class="btn btn-orange edit-plan" id="'+datos.id+'"><i class="fa fa-eye"></i></a> <a class="btn btn-orange delete_c" id="'+datos.id+'"><i class="fa fa-trash"></i></a>';
            var row = [count+=1,"<textarea id='plan_s_"+datos.id+"' style='display:none;'>" + JSON.stringify(datos) + "</textarea>"+datos.tit_pla,datos.des_pla,"$ "+formato.precio2(datos.pre_pla),option]
            table.row.add(row).draw().node();
        }
    }
    $(document).on('click', ".edit-plan", function(){
        idplan = $(this).attr("id");
        var obj = JSON.parse($("#plan_s_"+idplan).val());
        $("#id_plan").val(obj.id);
        //precio=formato.precio2(obj.pre_pla);
        $("#pre").val(obj.pre_pla);
        $("#des").val(obj.des_pla);
        $("#tit").val(obj.tit_pla);
        $("#form").modal('open');
    });
    
    $(document).on('click', ".btn-save", function(){
        idp=$("#id_plan").val();
        var formData = new FormData();
        formData.append("tit" ,$("#tit").val());
        formData.append("des" ,$("#des").val());
        formData.append("pre" ,$("#pre").val());
        formData.append("id" , idp);
        formData.append("ida" ,user.id);
        formData.append("token" ,user.token);
        if(idp!='')
            url ="planes/editar";
        else
            url ="planes";
        $.ajax({
            url: dominio + "" +url, 
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
                if(data.r){
                    listplanes();
                    $("#form").modal('close');
                    $(".limpiar_form").val('');
                    M.toast({html:data.msg}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
                
            },
            error: function(xhr, status, errorThrown){
              console.log(xhr);
            }
        });
    });
    
    
    
    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click','.delete_c',function(){
      var idv = this.id;
      var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
      id = this.id
      var data = {
        ida: user.id,
        token: user.token
      };
      M.Toast.dismissAll();
      $.ajax({
        url:dominio+'planes/delete/'+id,
        type:'GET',
        data:data,
        success:function(data){
            if(data.msj){
                M.toast({html:data.msj}, 10000);
                listplanes();
            }else{
                M.toast({html:"Error al eliminar."}, 10000);
              
            }
        }
      })

    })
});

