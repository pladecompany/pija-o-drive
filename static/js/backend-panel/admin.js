$(document).ready(function(){
    
    if(user){
        if(user.tip_adm==2)
            window.location = "../panel/";  
    }
    listadmin();
   
    function listadmin(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'admin',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_tabla_admin(data)
            }

        })
    }

    function actualizar_tabla_admin(data){
        var table = $('.admin').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i];
            if(datos.tip_adm==1)
                tipouser='Administrador';
            else
                tipouser='Normal';
            var option = '<a class="btn btn-orange edit-admin" id="'+datos.id+'"><i class="fa fa-edit"></i></a> <a class="btn btn-orange delete_c" id="'+datos.id+'"><i class="fa fa-trash"></i></a>';
            var row = [count+=1,"<textarea id='admin_s_"+datos.id+"' style='display:none;'>" + JSON.stringify(datos) + "</textarea>"+datos.correo,tipouser,option]
            table.row.add(row).draw().node();
        }
    }
    $(document).on('click', ".edit-admin", function(){
        idplan = $(this).attr("id");
        var obj = JSON.parse($("#admin_s_"+idplan).val());
        $("#id_admin").val(obj.id);
        $("#cor_adm").val(obj.correo);
        $("#con_adm").val('');
        $("#tip_adm").val(obj.tip_adm);
        $('#tip_adm').formSelect();
        $("#modalAdmin").modal('open');
    });
    
    $(document).on('click', ".btn-save", function(){
        idp=$("#id_admin").val();
        var formData = new FormData();
        formData.append("cor" ,$("#cor_adm").val());
        formData.append("pass" ,$("#con_adm").val());
        formData.append("tip" ,$("#tip_adm").val());
        formData.append("id" , idp);
        formData.append("ida" ,user.id);
        formData.append("token" ,user.token);
        if(idp!='')
            url ="admin/editar";
        else
            url ="admin";
        $.ajax({
            url: dominio + "" +url, 
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
                if(data.r){
                    listadmin();
                    $("#modalAdmin").modal('close');
                    $(".limpiar_form").val('');
                    M.toast({html:data.msg}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
                
            },
            error: function(xhr, status, errorThrown){
              console.log(xhr);
            }
        });
    });
    
    
    
    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click','.delete_c',function(){
      var idv = this.id;
      var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
      id = this.id
      var data = {
        ida: user.id,
        token: user.token
      };
      M.Toast.dismissAll();
      $.ajax({
        url:dominio+'admin/delete/'+id,
        type:'GET',
        data:data,
        success:function(data){
            if(data.msj){
                M.toast({html:data.msj}, 10000);
                listadmin();
            }else{
                M.toast({html:"Error al eliminar."}, 10000);
              
            }
        }
      })

    })
});

