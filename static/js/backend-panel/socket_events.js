var socket = io(dominio);

if(window.localStorage.getItem("user") != null) {

  var dataU = JSON.parse(window.localStorage.getItem("user"));
  dataU.tablausuario = "admins";
  socket.emit("conectar",dataU);
  socket.on('RECIBIRMENSAJE-ADMIN',function(mensaje){
            if(mensaje.envio==2)
                url='&envio=2&id_usuario='+mensaje.id_usuario+'&nombre='+mensaje.nombre+'%20(proveedor)';
            else
                url='&envio=1&id_usuario='+mensaje.id_usuario+'&nombre='+mensaje.nombre+'%20(cliente)';
    		M.toast({html:'Nuevo mensaje <a href="?op=chat'+url+'" style="color:#fff;padding:1px 5px;font-weight:600; padding-right:7px;background-color:var(--orange);border-radius: 5px;margin-left:10px;">Ver</a>'}, 5000);
            
            if($('#id_usuario').val() == mensaje.id_usuario && $('#envio').val() == mensaje.envio){
                var audioElement1 = document.createElement('audio');
                audioElement1.setAttribute('src', '../static/audio/ring2.mp3');
                audioElement1.volume = 0.1;
                audioElement1.play();
                if(mensaje.img == null){
                    img = mensaje.img;
                }else{
                    img = mensaje.img;
                }
                socket.emit('ACTUALIZAR-VISTO',mensaje);
                $('#mensajes').append(`
                <li class="boxChat__left">
                    <div class="avatar">
                        <img src="${img}" ${imgenpordefecto()} class="img-cover w100" draggable="false">
                    </div>
                    <div class="msg">
                        <p class="m-0">${mensaje.mensaje}</p>
                        <span class="timeago" title="${mensaje.hora}"></span>
                    </div>
                </li>
				`);
                $("#content_messages").scrollTop($("#content_messages")[0].scrollHeight);


                envio = $('.envio');
                id_usuario = $('.id_usuario');
                    for(var i =0;i<envio.length;i++){
                        if(id_usuario[i].value == mensaje.id_usuario && envio[i].value == mensaje.envio){
                            div1 = $(`#${mensaje.envio}-${mensaje.id_usuario}`);
                            //count = parseInt($(`#${mensaje.envio}-${mensaje.id_usuario} > .chat > .badge`).html())+1
                            div = $(`#${mensaje.envio}-${mensaje.id_usuario}`).remove();
                            if(mensaje.envio == 1){
                                usu = 'cliente'
                                img = mensaje.img;
                                nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`
                            }else{
                                usu = 'proveedor'
                                img = mensaje.img;
                                nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`
                            }
                            $('.listado').prepend(`
                            <div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
		                        <div class="row mb-0">
		                            <div class="col s3">
		                                <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
		                            </div>

		                            <div class="col s9 chat" id="${mensaje.id}">
		                                ${nombre}
		                                <p id="msj">${mensaje.mensaje}</p>
		                                <span class="timeago" title="${mensaje.hora}"></span>
		                                <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
		                                <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
		                                <!--<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;"></span>-->
		                            </div>
		                        </div>
		                    </div> 
                            `);
                            
                            
                        }
                    }

            }else{
                //$('#msj1').css('color','red');
                /*num = $('#num').html()
                con =parseInt(num)+1;
                $('#num').html(con)*/
                var count = 0;
                var audioElement = document.createElement('audio');

                audioElement.setAttribute('src', '../static/audio/ring.mp3');

                audioElement.play();
                    envio = $('.envio')
                    id_usuario = $('.id_usuario')
                    if(envio.length != 0 && id_usuario.length != 0){
                        for(var i =0;i<envio.length;i++){
                            if(id_usuario[i].value == mensaje.id_usuario && envio[i].value == mensaje.envio){
                                div1 = $(`#${mensaje.envio}-${mensaje.id_usuario}`);
                                if($(`#${mensaje.envio}-${mensaje.id_usuario} > .row > .chat > .badge`).html() != undefined){
                                    count += parseInt($(`#${mensaje.envio}-${mensaje.id_usuario} > .row > .chat > .badge`).html())+1;
                                    badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                                }else{
                                    count += 1
                                    badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                                }
                                div = $(`#${mensaje.envio}-${mensaje.id_usuario}`).remove();
                                if(mensaje.envio == 1){
                                    usu = 'cliente';
                                    img = mensaje.img;
                                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                }else{
                                    usu = 'proveedor';
                                    img = mensaje.img;
                                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                }
                                $('.listado').prepend(`
                                <div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
			                        <div class="row mb-0">
			                            <div class="col s3">
			                                <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
			                            </div>

			                            <div class="col s9 chat" id="${mensaje.id}">
			                                ${nombre}
			                                <p id="msj">${mensaje.mensaje}</p>
			                                <span class="timeago" title="${mensaje.hora}"></span>
			                                <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
			                                <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
			                                ${badge}
			                            </div>
			                        </div>
			                    </div> 
                                `);
                                
                                
                            }
                        } 
                    }
                        nuevo = $(`#${mensaje.envio}-${mensaje.id_usuario}`);
                        if(nuevo.length == 0 && $('#bus_men').val()==''){
                        	count += 1
                            badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                            if(mensaje.envio == 1){
                                usu = 'cliente';
                                img = mensaje.img;
                                nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                            }else{
                                usu = 'proveedor';
                                img = mensaje.img;
                                nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                            }
                            $('.listado').prepend(`
                            	<div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
			                        <div class="row mb-0">
			                            <div class="col s3">
			                                <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
			                            </div>

			                            <div class="col s9 chat" id="${mensaje.id}">
			                                ${nombre}
			                                <p id="msj">${mensaje.mensaje}</p>
			                                <span class="timeago" title="${mensaje.hora}"></span>
			                                <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
			                                <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
			                                ${badge}
			                            </div>
			                        </div>
			                    </div> 
                            `);
                        }else if(nuevo.length == 0 && $('#bus_men').val() != ''){
                        	array = usuarios[0];
                            contador = 0;
                            for(var  i = 0;i<array.length;i++){
                                if(array[i].id_usuario == mensaje.id_usuario && array[i].envio == mensaje.envio){
                                    count = array[i].pendientes+1
                                    badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                                    if(mensaje.envio == 1){
                                        usu = 'cliente';
                                        img = mensaje.img;
                                        nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                    }else{
                                        usu = 'proveedor';
                                        img = mensaje.img;
                                        nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                    }
                                    html = `
                                    <div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
				                        <div class="row mb-0">
				                            <div class="col s3">
				                                <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
				                            </div>

				                            <div class="col s9 chat" id="${mensaje.id}">
				                                ${nombre}
				                                <p id="msj">${mensaje.mensaje}</p>
				                                <span class="timeago" title="${mensaje.hora}"></span>
				                                <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
				                                <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
				                                ${badge}
				                            </div>
				                        </div>
				                    </div>
                                    `;
                                    var usuario = {
                                        nombre:mensaje.nombre.toLowerCase(),
                                        id_usuario:mensaje.id_usuario,
                                        envio:mensaje.envio,
                                        pendientes:count,
                                        html:html
                                    }
                                    array.splice(i, 1)
                                    contador +=1;
                                    usuario1 = false;


                                count +=1
                                badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                                if(mensaje.envio == 1){
                                    usu = 'cliente';
                                    img = mensaje.img;
                                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                }else{
                                    usu = 'proveedor';
                                    img = mensaje.img;
                                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                }
                                html = `
                                <div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
			                        <div class="row mb-0">
			                            <div class="col s3">
			                                <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
			                            </div>

			                            <div class="col s9 chat" id="${mensaje.id}">
			                                ${nombre}
			                                <p id="msj">${mensaje.mensaje}</p>
			                                <span class="timeago" title="${mensaje.hora}"></span>
			                                <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
			                                <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
			                                ${badge}
			                            </div>
			                        </div>
			                    </div> 
                                `
                                usuario1 = {
                                    nombre:mensaje.nombre.toLowerCase(),
                                    id_usuario:mensaje.id_usuario,
                                    envio:mensaje.envio,
                                    pendientes:count,
                                    html:html
                                }
                                
                            }
                            if(usuario){
                                array.unshift(usuario);
                            }
                            if(usuario1){
                                array.splice(0, 0, usuario1);
                            }
                        }
                        
                    }else {
                    	if($('#bus_men').val()==''){
                            count += 1
                            count2 = mensaje.pendientes;
                            badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count2+'</span>';
                            if(mensaje.envio == 1){
                                usu = 'cliente';
                                img = mensaje.img;
                                nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                            }else{
                                usu = 'proveedor';
                                img = mensaje.img;
                                nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                            }
                            $(`#${mensaje.envio}-${mensaje.id_usuario}`).remove();
                            $('.listado').prepend(` 
                            	<div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
				                    <div class="row mb-0">
				                        <div class="col s3">
				                            <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
				                        </div>
				                        <div class="col s9 chat" id="${mensaje.id}">
				                            ${nombre}
				                            <p id="msj">${mensaje.mensaje}</p>
				                            <span class="timeago" title="${mensaje.hora}"></span>
				                            <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
				                            <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
				                            ${badge}
				                        </div>
				                    </div>
				                </div>
                            `);
                        }
                        else if($('#bus_men').val()!=''){
                            array = usuarios[0];
                            //console.log('nuevo mensaje',array)
                            contador = 0;
                            for(var  i = 0;i<array.length;i++){
                                if(array[i].id_usuario == mensaje.id_usuario && array[i].envio == mensaje.envio){
                                    count = array[i].pendientes+1
                                    badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                                    if(mensaje.envio == 1){
                                        usu = 'cliente';
                                        img = mensaje.img;
                                        nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                    }else{
                                        usu = 'proveedor';
                                        img = mensaje.img;
                                        nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                    }
                                    html = `
                                    <div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
					                    <div class="row mb-0">
					                        <div class="col s3">
					                            <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
					                        </div>
					                        <div class="col s9 chat" id="${mensaje.id}">
					                            ${nombre}
					                            <p id="msj">${mensaje.mensaje}</p>
					                            <span class="timeago" title="${mensaje.hora}"></span>
					                            <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
					                            <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
					                            ${badge}
					                        </div>
					                    </div>
					                </div>
                                    `;
                                    var usuario = {
                                        nombre:mensaje.nombre.toLowerCase(),
                                        id_usuario:mensaje.id_usuario,
                                        envio:mensaje.envio,
                                        pendientes:count,
                                        html:html
                                    }
                                    array.splice(i, 1);
                                    contador += 1;
                                    
                                    usuario1 = false;

                                }
                            }
                            if(usuario){
                                array.unshift(usuario);
                            }
                            if(contador==0){
                                count +=1
                                badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+count+'</span>';
                                if(mensaje.envio == 1){
                                    usu = 'cliente';
                                    img = mensaje.img;
                                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                }else{
                                    usu = 'proveedor';
                                    img = mensaje.img;
                                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${mensaje.nombre} (${usu})</p></a>`;
                                }
                                html = `
                                	<div class="boxChat__cont boxList" id="${mensaje.envio}-${mensaje.id_usuario}">
					                    <div class="row mb-0">
					                        <div class="col s3">
					                            <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
					                        </div>
					                        <div class="col s9 chat" id="${mensaje.id}">
					                            ${nombre}
					                            <p id="msj">${mensaje.mensaje}</p>
					                            <span class="timeago" title="${mensaje.hora}"></span>
					                            <input type="number"  value="${mensaje.envio}" class="envio" style="display:none">
					                            <input type="number"  value="${mensaje.id_usuario}" class="id_usuario" style="display:none">
					                            ${badge}
					                        </div>
					                    </div>
					                </div>
                                `;
                                usuario1 = {
                                    nombre:mensaje.nombre.toLowerCase(),
                                    id_usuario:mensaje.id_usuario,
                                    envio:mensaje.envio,
                                    pendientes:count,
                                    html:html
                                }
                            }
                            
                            if(usuario1){
                                array.splice(0, 0, usuario1);
                            }
                        }
                    }
            }
            $(".timeago").timeago();
    	});
    
    socket.on('Recibiralerta', function(data) {
        //console.log(data);
        $("#icon_notifi").removeClass("fa-bell-slash");
        $("#icon_notifi").addClass("fa-bell");
        M.toast({html:data.tit_noti}, 5000);
        
    });
    
}
