$(document).ready(function() {
var CookieArray = [];
var limit = 5;
obtener_mis_notis = function obtener_mis_notis(){
    
    var html = ''; 
    //OBTENER LOS MODULOS QUE PERTENECE EL USUARIO
       
    axios.get(dominio+'notificaciones/admin/?limit='+limit+"&token="+user.token+"&idu="+user.id, {})
    .then(function (response) {
        var data = response.data;
        
        $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_mis_notis(); " class="view_more"><center>Cargar más...</center></div>');
        
        if(data.length > 0 && data.length < 5 || data.length == 0)
            $(".view_more").addClass("hide");

        for(i=data.length-1;i>=0;i--){
            CargarNoti(data[i]);
        }
         
        if(limit==5)
               $(".misNotificaciones").scrollTop($('.misNotificaciones')[0].scrollHeight);
        
        if(data.length == 0)
            $(".misNotificaciones").html(sin_resultados());

        limit= (limit*2);
        
    })
    .catch(function (error) {
        $(".view_more").addClass("hide");
        $(".misNotificaciones").html(sin_resultados());
    });
    }


 
 obtener_mis_notis();

buscarID = function buscarID(id){

    for (var i = 0; i < CookieArray.length; i++) {
        if(CookieArray[i].id == id)
            return true;    
    }
    return false;
} 
CargarNoti = function CargarNoti(data){
                     moment.locale("es");
                    if (buscarID(data.id)) {
                        return false;
                    }
                    CookieArray.push(data);
                    var html = '';
                        viewsts='no-vist';
                        if(data.est_noti==1)
                            viewsts='';
                        html += '<a class="anclaCard vernotificacion" noti="'+data.id+'" tipo="'+data.tipo_noti+'" href="#!">';
                        html += '<div class="row boxNotificaciones noti-'+data.id+' '+viewsts+'">';
                        html += '    <div class="col s1 p-1 flexCenter">';
                        html += '        <img src="../static/img/logo.png" width="90%" class="img-cover">';
                        html += '    </div>';

                        html += '    <div class="col s11">';
                        html += '        <h6 class="clr_blue">'+data.tit_noti+'</h6>';
                        html += '        <p style="color:rgba(0, 0, 0, 0.87);font-weight:400;">'+data.des_noti+'</p>';
                        html += '        <time class="right"><i class="fa fa-clock"></i> '+ moment(data.fec_reg_noti).toNow()+'</time>';
                        html += '    </div>';
                        html += '</div>';
                        html += '</a>';

                        $(".misNotificaciones").prepend(html);
}
function sin_resultados(){
        var html = '';
            html += '     <div class="card card-notifi">';
            html += '         <div class="row m-0">';
            html += '             <div class="col s12">';
            html += '                 <p class="center">No se encontraron resultados</p>';
            html += '             </div>';
            html += '         </div>';
            html += '     </div>';
         return html;
    }
$(document).on('click', '.vernotificacion', function(){ 
    var tipo = $(this).attr("tipo");
    var id = $(this).attr("noti");
    var i = $(this).find('i');

    $(i).addClass('fa-spinner fa-spin');
   
    axios.get(dominio+'notificaciones/vista/'+id, {})
    .then(function (response) {
        $(".noti-"+id).removeClass('no-vist');
        $(i).removeClass('fa-spinner fa-spin');
        if (tipo == "verificar-chofer-admin"){
            location.href = "?op=proveedores";
        }

        /*ultimas_notis();*/
    })
    .catch(function (error) {
        $(i).removeClass('fa-spinner fa-spin');

    });
});

});