document.write("<script src='static/js/backend-panel/general.js'></script>");
$(document).ready(function () {
  $("#btn_entrar").on('click', function(){
    $('#btn_entrar').prop('disabled', true);
    $('#login1').addClass('fa-spinner fa-spin');
    var data = {
      correo: $("#correo").val(),
      pass: $("#pass").val()
    };


    axios.post(dominio+'login/admin', data)
      .then(function (response) {
        if(response.data.auth == true){
          window.localStorage.setItem("user", JSON.stringify(response.data.user));
          data = response.data.user;
          window.location = "panel/";         
        }else {
          M.toast({html:response.data.msj}, 4000);
          $('#login1').removeClass('fa-spinner fa-spin');
          $('#btn_entrar').prop('disabled', false);
        }
      })
      .catch(function (error) {
        M.toast({html:'No se encontró un admin con este correo.'}, 10000);
        $('#login1').removeClass('fa-spinner fa-spin');
        $('#btn_entrar').prop('disabled', false);
      });
      return false;
  });
});
