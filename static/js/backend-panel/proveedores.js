$(document).ready(function() {
    listplanes();
    selectplanes = [];

    function listproveedores() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'proveedores',
            type: 'GET',
            data: data,
            success: function(data) {
                console.log(data);
                actualizar_tabla_proveedores(data)
            }

        })
    }

    function actualizar_tabla_proveedores(data) {
        var table = $('.prove').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            if (datos.img_pro)
                var imgc = "<img src='" + datos.img_pro + "'; " + imgenpordefecto() + " width='45px' class='circle' height='45px' style='border: 1px solid #ccc; padding: 1px'>";
            else
                var imgc = "<img src=''; " + imgenpordefecto() + " width='45px' class='circle' height='45px' style='border: 1px solid #ccc; padding: 1px'>";
            imgc += " " + datos.nom_pro;

            if (datos.Detalles)
                veh = "<img src='../static/img/transporte/" + datos.Detalles.Vehiculo.icono_veh + "'; width='45px'>";
            else
                veh = '';

            if (datos.est_pro == 1) {
                sts = 'Activo';
                col = '#4caf50';
            } else if (datos.est_pro == 2) {
                sts = 'Bloqueado';
                col = '#ff2828';
            } else {
                sts = 'Pausado';
                col = '#ffc82b';
            }

            if (datos.estatus_verificado == 1)
                sts1 = 'Verificado';
            else if (datos.estatus_verificado == 2)
                sts1 = 'Rechazado';
            else
                sts1 = 'Pendiente';


            tltwha = datos.tel_pro;
            if (tltwha) {
                tltwha = tltwha.replace("+", "");
            }

            if (datos.id_plan) {
                plan = selectplanes[datos.id_plan];
            } else
                plan = 'Sin plan';

            viajes = 0;
            puntos = 0;
            if (datos.Detalles && datos.Detalles.Reputacion)
                puntos = datos.Detalles.Reputacion;
            if (datos.Detalles && datos.Detalles.Viajes_total)
                viajes = datos.Detalles.Viajes_total;

            puntos = '<a class="btn btn-orange ver-puntos" id="' + datos.id + '" name="Puntos de ' + datos.nom_pro + '">' + puntos + ' <i class="fa fa-star"></i></a>';
            viajes = '<a class="btn btn-orange ver-puntos" id="' + datos.id + '" name="Viajes de ' + datos.nom_pro + '">' + viajes + ' <i class="fa fa-taxi"></i></a>';


            var btnsts = '<a class="btn btn-orange right sts_c sts_c_' + datos.id + '" id="' + datos.id + '" name="' + datos.est_pro + '"><i class="fa fa-cog"></i></a>';
            var btnsts1 = '<a class="btn btn-orange right sts_c1 sts_c_1' + datos.id + '" id="' + datos.id + '" name="' + datos.estatus_verificado + '"><i class="fa fa-cog"></i></a>';

            var btnplan = '<a class="btn btn-orange right plan_pro plan_pro_' + datos.id + '" id="' + datos.id + '" name="' + datos.id_plan + '"><i class="fa fa-cog"></i></a>';
            var btnpush = '<a title="PUSH" class="btn btn-orange msj_cli" id="' + datos.id + '"><i class="fa fa-envelope"></i></a>';
            var btnwhat = '<a title="Whatsapp" class="btn btn-orange" href="https://api.whatsapp.com/send?phone=' + tltwha + '" target="_blank"><i class="fab fa-whatsapp"></i></a>';
            var option = '<a class="btn btn-orange ver-prove" id="' + datos.id + '"><i class="fa fa-eye"></i></a> <a class="btn btn-orange delete_c" id="' + datos.id + '"><i class="fa fa-trash"></i></a> <a class="btn btn-orange btPrint" id="' + datos.id + '"><i class="fa fa-file-pdf"></i></a> <a class="btn btn-orange" href="?op=chat&envio=2&id_usuario=' + datos.id + '&nombre=' + datos.nom_pro + ' (proveedor)"><i class="fa fa-comments"></i></a>';
            var row = [count += 1,datos.id, imgc, veh, datos.tel_pro,"<textarea id='prove_ver_" + datos.id + "' style='display:none;'>" + JSON.stringify(datos) + "</textarea>" + ((datos.cedula_pro) ? datos.cedula_pro : 'S/A'), viajes, puntos, '<span class="txt-sts-' + datos.id + '" style="color:' + col + ';">' + sts + '</span> ' + btnsts, '<span class="txt-sts-1' + datos.id + '">' + sts1 + '</span> ' + btnsts1, '<span class="txt-plan-' + datos.id + '">' + plan + '</span> ' + btnplan, btnpush+' '+btnwhat+' '+option]

            table.row.add(row).draw().node();
        }
    }


    function listplanes() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'planes',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_select_planes(data);
            }

        });
        listproveedores();
    }

    function actualizar_select_planes(data) {

        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            selectplanes[datos.id] = datos.tit_pla;
            $("#plan_edit").append('<option value="' + datos.id + '">' + datos.tit_pla + '</option>')
        }
    }

    $(document).on('click', ".plan_pro", function() {
        var idp = this.id;
        var ids = this.name;
        $("#planes").modal('open');
        $("#id_prove_plan").val(idp);
        $("#plan_edit").val(ids).trigger("change");
    });
    $(document).on('click', ".save-plan-pro", function() {

        var idp = $("#id_prove_plan").val();
        var ids = $("#plan_edit").val();
        var formData = new FormData();
        formData.append("idp", idp);
        formData.append("id_plan", ids);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        $.ajax({
            url: dominio + "proveedores/editar-plan",
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.r) {
                    txt = $('select[name="plan_edit"] option:selected').text();
                    $(".txt-plan-" + idp).html(txt);
                    $(".plan_pro_" + idp).attr("name", ids);
                    $("#planes").modal('close');
                    var obj = JSON.parse($("#prove_ver_" + idp).val());
                    obj.id_plan = ids;
                    $("#prove_ver_" + idp).val(JSON.stringify(obj));
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });

    $(document).on('click', ".sts_c1", function() {
        var idc = this.id;
        var ids = this.name;
        $("#status1").modal('open');
        $("#id_cli1").val(idc);
        $("#sts_edit1").val(ids).trigger("change");
    });
    $(document).on('click', ".sts_c", function() {
        var idc = this.id;
        var ids = this.name;
        $("#status").modal('open');
        $("#id_cli").val(idc);
        $("#sts_edit").val(ids).trigger("change");
    });
    $(document).on('click', ".save-sts-cli1", function() {

        var idc = $("#id_cli1").val();
        var ids = $("#sts_edit1").val();
        var formData = new FormData();
        formData.append("id", idc);
        formData.append("estatus_verificado", ids);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        $.ajax({
            url: dominio + "proveedores/editar-status",
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.r) {
                    txt = $('select[name="sts_edit1"] option:selected').text();
                    $(".txt-sts-1" + idc).html(txt);
                    $(".sts_c_1" + idc).attr("name", ids);
                    $("#status1").modal('close');

                    var obj = JSON.parse($("#prove_ver_" + idc).val());
                    obj.estatus_verificado = ids;
                    $("#prove_ver_" + idc).val(JSON.stringify(obj));

                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });
    $(document).on('click', ".save-sts-cli", function() {


        if ($("#comentario").val().trim() == "") {
            M.toast({ html: "Debes ingresar un comentario." }, 5000);
            return;
        }
        var idc = $("#id_cli").val();
        var ids = $("#sts_edit").val();
        if (ids == "1")
            stsT = 'Activo';
        else if (ids == "2")
            stsT = 'Bloqueado';
        else
            stsT = 'Pausado';

        var formData = new FormData();
        formData.append("id", idc);
        formData.append("est_pro", ids);
        formData.append("comentario", $("#comentario").val());
        formData.append("ida", user.id);
        formData.append("stsT", stsT);
        formData.append("token", user.token);
        $.ajax({
            url: dominio + "proveedores/editar-status-normal",
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                $("#comentario").val('');
                if (data.r) {
                    txt = $('select[name="sts_edit"] option:selected').text();
                    $(".txt-sts-" + idc).html(txt);
                    if (ids == 1) {
                        col = '#4caf50';
                    } else if (ids == 2) {
                        col = '#ff2828';
                    } else {
                        col = '#ffc82b';
                    }
                    $(".txt-sts-" + idc).css('color', col);
                    $(".sts_c_" + idc).attr("name", ids);
                    $("#status").modal('close');

                    var obj = JSON.parse($("#prove_ver_" + idc).val());
                    obj.est_pro = ids;
                    $("#prove_ver_" + idc).val(JSON.stringify(obj));

                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });
    $(document).on('click', ".ver-prove", function() {
        idprove = $(this).attr("id");
        var obj = JSON.parse($("#prove_ver_" + idprove).val());
        $(".nom_pro").html(obj.nom_pro);
        $(".tlf_pro").html(obj.tel_pro);
        $(".ced_pro").html(obj.cedula_pro);
        $(".dir_pro").html(obj.dir_pro);
        $(".ema_pro").html(obj.cor_pro);
        if (obj.est_pro == 1)
            sts = 'Activo';
        else if (obj.est_pro == 2)
            sts = 'Bloqueado';
        else
            sts = 'Pendiente';

        if (obj.estatus_verificado == 1)
            sts1 = 'Verificado';
        else if (obj.estatus_verificado == 2)
            sts1 = 'Rechazado';
        else
            sts1 = 'Pendiente';

        $(".que_pro").html('0');
        $(".via_pro").html('0');
        $(".pun_pro").html('0');
        $(".sts_pro").html(sts);
        $(".stsf_pro").html(sts1);


        $(".wha_pro").html(obj.whatsapp_pro);
        $(".cod_pro").html('0');
        $(".doc_pro").html('0');
        if (datos.id_plan) {
            plan = selectplanes[obj.id_plan];
        } else
            plan = 'Sin plan';
        $(".pla_pro").html(plan);
        $(".ver_img1").html('<img src="' + obj.img_pro + '" ' + imgenpordefecto() + ' width="40%" class="border p-3">');

        if (obj.Detalles) {
            $(".tip_pro").html(obj.Detalles.Vehiculo.nombre_veh);
            $(".mar_pro").html(obj.Detalles.marca_veh);
            $(".mat_pro").html(obj.Detalles.matri_veh);
            $(".ref_pro").html(obj.Detalles.referencia_personal);
            $(".tel_referencia").html(obj.Detalles.tel_referencia);

            $(".ver_img2").html('<img src="' + obj.Detalles.img_fre + '" ' + imgenpordefecto() + ' width="40%" class="border p-3">');
            $(".ver_img3").html('<img src="' + obj.Detalles.img_lat + '" ' + imgenpordefecto() + ' width="40%" class="border p-3">');
            $(".ver_img4").html('<img src="' + obj.Detalles.img_tra + '" ' + imgenpordefecto() + ' width="40%" class="border p-3">');
            $(".ver_img5").html('<img src="' + obj.Detalles.img_cedula + '" ' + imgenpordefecto() + ' width="40%" class="border p-3">');
            $(".ver-com-pro").show();
        } else {
            $(".mar_pro , .mat_pro , .ref_pro , .tip_pro").html('Sin registrar');
            $(".ver-com-pro").hide();
        }
        $("#modalProveedor").modal('open');

    });
    $(document).on('click', ".msj_cli", function() {
        var idc = this.id;
        $("#mensaje").modal('open');
        $("#id_cli_msj").val(idc);
    });
    $(document).on('click', ".save-msj-cli", function() {
        $(this).attr("disabled", true);
        var idc = $("#id_cli_msj").val();
        var msj = $("#msj_cli").val();
        var formData = new FormData();
        formData.append("id", idc);
        formData.append("msj", msj);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        $.ajax({
            url: dominio + "proveedores/enviar-push",
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                $(".save-msj-cli").attr("disabled", false);
                if (data.r) {
                    $("#msj_cli").val('')
                    $("#mensaje").modal('close');
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });




    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click', '.delete_c', function() {
        var idv = this.id;
        var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

        M.toast({ html: toastContent }, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {
            ida: user.id,
            token: user.token
        };
        M.Toast.dismissAll();
        $.ajax({
            url: dominio + 'proveedores/delete/' + id,
            type: 'GET',
            data: data,
            success: function(data) {
                if (data.msj) {
                    M.toast({ html: data.msj }, 10000);
                    listproveedores();
                } else {
                    M.toast({ html: "Error al eliminar." }, 10000);

                }
            }
        })

    })

    $(document).on('click', '.btPrint', function() {

        var prove = $(this).attr("id");
        if (prove) {
            //buscar nombre del plan
            var obj = JSON.parse($("#prove_ver_" + prove).val());
            if (obj.id_plan) {
                plan = selectplanes[obj.id_plan];
            } else
                plan = 'Sin plan';
            $("#tablaplan").val(plan);
            $("#tablainfo").val($("#prove_ver_" + prove).val());
            $("#formprint")[0].submit();
        }

    });

    $(document).on('click', ".ver-puntos", function() {

        var idp = this.id;
        var idn = this.name;
        $("#puntos").modal('open');
        $("#id_ver_puntos").val(idp);
        $("#id_reg_pun").val(idp);
        $(".name-puntos").html(idn);
        listarpuntuacion(idp);
    });
    $(document).on('change', "#mes_pun , #ano_pun", function() {
        listarpuntuacion($('#id_reg_pun').val());
    });

    function listarpuntuacion(idp) {
        var table = $('#table_p').DataTable({
            'processing': true,
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        $("#btn_pdf_punta").hide();
        $("#tablainfopuntos").val('');

        data = {
            ida: user.id,
            token: user.token,
            id: idp,
            mes: $("#mes_pun").val(),
            ano: $("#ano_pun").val()
        }
        $.ajax({
            url: dominio + 'solicitud_servicios/propiasP/admin',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                if (data.length > 0) {
                    $("#btn_pdf_punta").show();
                    info = JSON.stringify(data);
                    $("#tablainfopuntos").val(info);
                } else {
                    M.toast({ html: "No se encontro ningun resultado." }, 5000);
                }

                for (var i = 0; i < data.length; i++) {
                    if(data[i].Proveedor.ReviewRecibida){
                        lim = '<center><span class="btn btn-orange ">' + data[i].Proveedor.ReviewRecibida.voto1 + ' <i class="fa fa-star"></i></span></center>';
                        ama = '<center><span class="btn btn-orange ">' + data[i].Proveedor.ReviewRecibida.voto2 + ' <i class="fa fa-star"></i></span></center>';
                        pun = '<center><span class="btn btn-orange ">' + data[i].Proveedor.ReviewRecibida.voto3 + ' <i class="fa fa-star"></i></span></center>';

                        //"<textarea id='prove_ver_"+datos.id+"' style='display:none;'>" + JSON.stringify(datos) + "</textarea>"
                        var row = [count += 1, data[i].Cliente.nom_cli, data[i].Cliente.tel_cli, lim, ama, pun, '<i class="fa fa-comments"></i>: ' + data[i].Proveedor.ReviewRecibida.comentario_rep]
                        table.row.add(row).draw().node();
                    }
                }
            }

        })
    }
});