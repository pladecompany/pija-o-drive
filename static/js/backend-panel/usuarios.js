$(document).ready(function(){
  var datos = []
  var tablaUsuarios = $('#tabla_usuarios').DataTable({
    "language": {
    "url": "../static/lib/JTables/Spanish.json"
    }
  });


  // console.log('entro a usuarios');
  obtenerUsuarios();
  modulos();


  function obtenerUsuarios(){

      var data = {
        ida: user.id,
        token: user.token
      }

      $.ajax({
          url: dominio + "usuarios/",
          type: "GET",
          data: data,
          dataType: "json",

          success: function(data){
            if(data.length == 0){
              tablaUsuarios.clear();
              tablaUsuarios.draw();
            }
            actualizarDataTabla(data);
          },
          error: function(xhr, status, errorThrown){
            console.log("Errr : ");
          }
      });
  }

  function modulos(){
    var data= {
      ida:user.id,
      token:user.token
    }
    $.ajax({
      url :dominio+"modulos/",
      type:"GET",
      data:data,
      dataType:"json",
      success:function(data){
        for(var i = 0;i<data.length;i++){
          $('.modulos').append(`
            <div class="col s12 m6 l4 center">
						<h6 class="bg-blue py-2">${data[i].descripcion}</h6>
						<p class="">
							<label>
								<input type="checkbox" class="filled-in modulos_check" name="mod_pro" id="mod_pro${data[i].id}" />
                <span></span>
                <input type="number" value="${data[i].id}" class="mod_pro${data[i].id} hide hidden">
							</label>
						</p>
					</div>
          
          `)
        }
      }
    })
  }

  function actualizarDataTabla(data){
    tablaUsuarios.clear();
    for(var i = 0 ; i < data.length; i++){
      var obj = data[i];
      // console.log(obj)
      if(obj.est_usu == 1)
        estado = 'Activo';
      else
        estado = 'Bloqueado';
      var row ="";
      row+='<a href="#" onclick="return false;" id="'+obj.id+'" name="" class="btn-transparente bt_editar"><i class="fas fa-edit "></i></a><a onclick="return false;" id="'+obj.id+'" name="" class="btn-transparente bt_eliminar"><i class="fas fa-trash "></i></a>';
      var row = [obj.nom_usu + " "+obj.ape_usu, obj.cor_usu, obj.sex_usu, obj.tlf_usu, estado, row];
      // datos.push({nombre:obj.nom_usu,correo:obj.cor_usu});
      tablaUsuarios.row.add(row).draw().node();
    }
  }
// $('.buscar').on('click',function(){
//   var min = $('#min').val()
//     var max = $('#max').val()
//     var cate = $('#select').val()
//     if(min.length<1 || max.length<1){
        // M.toast('Introduce fecha inicio y fecha fin',2000)
//     }else{
//         tabla.clear().draw();
//         acum = 0;
//         for(var i = 0;i<datos.length;i++){
//             var data = datos[i]
//             if(moment(data.fecha).format('YYYY-MM-DD') >= min && moment(data.fecha).format('YYYY-MM-DD') <=max){
//                 var row = [acum+=1,data.nombre,moment(data.fecha).format('L'),data.correo];
//                 tabla.row.add(row).draw().node();
//             }
//         }
//     }
//   })

  // Captura evento de envio de formulario de registro
  $("body").on('submit', '#formulario_usuario', function(e){
    var cor = $("#cor_usu").val();
    if(!validar_mail(cor)){
      var toastContent = '<span>Introduzca un correo válido</span>';
      M.toast({html:toastContent}, 4000);
      return;
    }


    e.preventDefault();
    var formData = new FormData();
    //formData.append(this);
    var method = $("#formulario_usuario").attr('method');
    var idu = $("#idu").val();
    formData.append("idu" ,idu);
    formData.append("nom_usu" , $("#nom_usu").val());
    formData.append("ape_usu", $("#ape_usu").val());
    formData.append("cor_usu", $("#cor_usu").val());
    formData.append("sex_usu", $("#sex_usu").val());
    formData.append("tlf_usu", $("#tlf_usu").val());
    formData.append("est_usu", $("#est_usu").prop('checked'));
    formData.append("pas_usu", $("#pas_usu").val());
    formData.append("pas_usu_conf", $("#pas_usu_conf").val());
    let imagefile = document.querySelector('#img_usu');
    formData.append("img_usu", imagefile.files[0]);
    formData.append("token", user.token);
    formData.append("ida", user.id);
    var modulos =[];
    
    modul = $(".modulos :input:checkbox:checked")
    for(var i = 0;i<modul.length;i++){
      id = modul[i].id
      id_modulo = $('.'+id).val()
      modulos.push(id_modulo)

    }
    console.log(modulos)
    formData.append("modulos",modulos);
    // formData.push(data);
    if(method == "POST")
      url = dominio + "usuarios/add";
      else if(method == "PUT")
      url = dominio + "usuarios/edit/" + idu;

    $.ajax({
        url: url, 
        type: method,
        cache: false,
        dataType: 'json',
        contentType: false,
        processData: false,
        data: formData,
        success: function(data){
          if(data.r == true){
            $("#usuarios").modal('close');
            obtenerUsuarios();
            M.toast({html:data.msj}, 10000);
          }else{
            M.toast({html:data.msj}, 10000);
          }
        },
        error: function(xhr, status, errorThrown){
          // console.log(xhr);
          M.toast({html:xhr.responseJSON.msg}, 10000);
        }
    });
    modulos = [];
    return false;
  });

    // Abre modal para agregar un registro

  $("body").on('click', '.btn_nuevo', function(){
    // $("#usuarios").modal('open');
    $("#idu").val("");
    $("#nom_usu").val("");
    $("#ape_usu").val("");
    $("#cor_usu").val("");
    $("#sex_usu").val("Femenino");
    $("#est_usu").prop("checked", false);
    $("#tlf_usu").val("");
    $("#pas_usu").val("");
    $("#pas_usu_conf").val("");
    $(".profile-pic").attr('src', '../static/img/user.png');
    $(".modulos_check").prop('checked', false);
    $("#formulario_usuario").attr('method', 'POST');
  });

  $('body').on('click','.bt_eliminar',function(){
      var idu = this.id;
      var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" data-id="'+idu+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
  });

  $('body').on('click','.conf_si',function(){
      var data = {
        ida: user.id,
        token: user.token
      };
      idu = $(this).data('id');
      M.Toast.dismissAll();
    $.ajax({ 
          url: dominio + "usuarios/delete/" + idu,
          type: "DELETE",
          data: data,
          dataType: "json",
          success: function(data){
            obtenerUsuarios();
            if(data.r == true){
                obtenerUsuarios();
              M.toast({html:data.msj}, 10000);
              // console.log('true');
            }else{
              M.toast({html:data.msj}, 10000);
              // console.log('false');
            }
          },
          error: function(xhr, status, errorThrown){
            M.toast("Error: eliminando", 10000);
          }
      });
  })

  
    // Abre modal para editar un registro
    $('body').on('click', '.bt_editar', function(){
      var data = {
        ida: user.id,
        token: user.token
      };
      $("#pas_usu").val("");
      $("#pas_usu_conf").val("");

      $("#usuarios").modal('open');
      $("#formulario_usuario").attr('method', 'PUT');
      var idc = this.id;
      $("#idu").val(idc);
      // console.log(idc);
      $.ajax({
          url: dominio + "usuarios/" + idc,
          type: "GET",
          data: data,
          dataType: "json",
          success: function(data){
            if(data.r == false){
              $("#formulario_usuario").attr('method', 'POST');
              $("#usuarios").modal('close');
            }else{
              $("#nom_usu").val(data.user.nom_usu);
              $("#ape_usu").val(data.user.ape_usu);
              $("#cor_usu").val(data.user.cor_usu);
              $("#sex_usu").val(data.user.sex_usu);
              $("#tlf_usu").val(data.user.tlf_usu);
              $("#est_usu").prop('checked',data.user.est_usu);
              if(data.user.img_usu != null)
                $('.profile-pic').attr('src',dominio+ data.user.img_usu);
              else
                $(".profile-pic").attr('src', '../static/img/user.png');
              
              modula = $(".modulos :input:checkbox")
              for (var a = 0; a < modula.length; a++) {
                id = modula[a].id
                $("#" + id).prop('checked', false);;

              }

              for(var i = 0;i<data.modulos.length;i++){
                modul = $(".modulos :input:checkbox")
                for (var a = 0; a < modul.length; a++) {
                  id = modul[a].id
                  id_modulo = 'mod_pro'+data.modulos[i].id_modulo
                  if(id == id_modulo){
                    $("#" + id).prop('checked','checked');
                  }
                  
                }
              }
            }
            // console.log(data);
          },
          error: function(xhr, status, errorThrown){
            console.log("Errr : ");
          }
      });
    });

});
