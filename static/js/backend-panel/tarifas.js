$(document).ready(function(){
    var horario=0;
    tiposVeh__();
    listtarifas();
   
    function listtarifas(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'tarifas',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_tarifas(data)
            },
            error: function(xhr, status, errorThrown){
                console.log(xhr);
            }
        });
    }
    function actualizar_tarifas(data){
        $("#bande").val(data.banderazo);
        $("#km_min").val(data.km);
        $("#pre_km").val(data.precio);
    }

    function tiposVeh__(){
            var html = '';              
             
             $.ajax({
                url : dominio+'tipovehiculo/panel/?token='+user.token,
                type: 'GET',
                data:{},
                success:function(response){
                   actualizar_tipo_vehiculo(response);
                },
                error: function(xhr, status, errorThrown){
                    console.log(xhr);
                }

            });
    }
    function actualizar_tipo_vehiculo(data){
        //console.log(data);
        acumveh=data.length-1;
        for(var i=0;i<data.length;i++){
            datos = data[i];
            $('#listar_vehiculos').append(`<div class="col s12 m12 l6">
                <div class="row border flexCenter m-2">
                    <div class="col s12 m2 flexCenter">
                        <img src="../static/img/transporte/${datos.icono_veh}" class="trans--img">
                    </div>
                    <div class="col s12 m5">
                    <h6 class="text-bold">${datos.nombre_veh}</h6>
                    </div>
                    <div class="col s12 m5">
                        <a class="btn btn-orange ver-calen" name="${datos.nombre_veh}" id="${datos.id}"><i class="fa fa-calendar m-0"></i></a>
                    </div>
                    
                </div>
            </div>`);   
        }
        
    }
    $(document).on('click', ".add-horario", function(){
        
        $('.list_hor').append(`
                <div class="col s3 horario-${horario} offset-m1">
                    <div class="form-group">
                        <label>Desde</label>
                        <input id="des" name="des[${horario}]" readonly type="text" class="timepicker" placeholder="">
                    </div>
                </div>
                <div class="col s3 horario-${horario}">
                    <div class="form-group">
                        <label>Hasta</label>
                        <input id="has" name="has[${horario}]" readonly type="text" class="timepicker" placeholder="">
                    </div>
                </div>
                <div class="col s2 horario-${horario}">
                    <div class="form-group">
                        <label>Precio Km($)</label>
                        <input type="text" name="pre_tar[${horario}]" class="number">
                    </div>
                </div>
                <div class="col s2 horario-${horario}">
                    <div class="form-group">
                        <label>Precio MIN ($)</label>
                        <input type="text" name="pre_min[${horario}]" class="number">
                    </div>
                </div>
                <div class="col s1 horario-${horario}">
                    <div class="form-group">
                        <br>
                        <a class="btn btn-orange eli-calen" id="${horario}"><i class="fa fa-times m-0"></i></a>
                    </div>
                </div>
        `);   
        horario++;
        $('.timepicker').timepicker({
            twelveHour: false 
        });
    });
    $(document).on('click', ".eli-calen", function(){
        idh=$(this).attr('id');
        $(".horario-"+idh).remove();
    });
    $(document).on('click', ".ver-calen", function(){
        idh=$(this).attr('id');
        name=$(this).attr('name');
        $("#id_auto").val(idh);
        $(".tit-hor").html(name);
        $('.list_hor').html('');
        horario=0;
        $.ajax({
            url : dominio+'tarifas/'+idh+'?token='+user.token,
            type: 'GET',
            data:{},
            success:function(response){
               //console.log(response);
               actualizar_horario(response);
            },
            error: function(xhr, status, errorThrown){
                console.log(xhr);
            }
        });
        $("#horario").modal('open');
    });
    function actualizar_horario(data){
        //console.log(data);
        for(var i=0;i<data.length;i++){
            datos = data[i];
            hdes=moment("2019-08-01 "+datos.hor_des).format('HH:mm');
            hhas=moment("2019-08-01 "+datos.hor_has).format('HH:mm');
            $('.list_hor').append(`
                <div class="col s3 horario-${horario} offset-m1">
                    <div class="form-group">
                        <label>Desde</label>
                        <input id="des" name="des[${horario}]" readonly type="text" class="timepicker" placeholder="" value="${hdes}">
                    </div>
                </div>
                <div class="col s3 horario-${horario}">
                    <div class="form-group">
                        <label>Hasta</label>
                        <input id="has" name="has[${horario}]" readonly type="text" class="timepicker" placeholder="" value="${hhas}">
                    </div>
                </div>
                <div class="col s2 horario-${horario}">
                    <div class="form-group">
                        <label>Precio Km($)</label>
                        <input type="text" name="pre_tar[${horario}]" class="number" value="${datos.preciokm}">
                    </div>
                </div>
                <div class="col s2 horario-${horario}">
                    <div class="form-group">
                        <label>Precio MIN ($)</label>
                        <input type="text" name="pre_min[${horario}]" class="number" value="${datos.preciomin}">
                    </div>
                </div>
                <div class="col s1 horario-${horario}">
                    <div class="form-group">
                        <br>
                        <a class="btn btn-orange eli-calen" id="${horario}"><i class="fa fa-times m-0"></i></a>
                    </div>
                </div>

            `); 
            horario++;  
        }
        $('.timepicker').timepicker({
            twelveHour: false 
        });
    }
    $(document).on('click', ".btn-save-hor", function(){
        txthor=$(".list_hor").html();
        if(txthor.trim()==''){
            M.toast({html:"Debes agregar al menos un horario"}, 5000);
        }else{
            var formData = new FormData(document.getElementById("form_tarifa"));
            formData.append("ida" ,user.id);
            formData.append("token" ,user.token);
            $.ajax({
                url: dominio + "tarifas", 
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    if(data.r){
                        $("#horario").modal('close');
                        M.toast({html:data.msj}, 5000);
                    }else
                        M.toast({html:data.msj}, 5000);
                    
                },
                error: function(xhr, status, errorThrown){
                  console.log(xhr);
                }
            });
        }
    });

    $(document).on('click', ".btn-save-tar", function(){
            var formData = new FormData(document.getElementById("form_tarifa_con"));
            formData.append("ida" ,user.id);
            formData.append("token" ,user.token);
            $.ajax({
                url: dominio + "tarifas/config", 
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    if(data.r){
                        M.toast({html:data.msg}, 5000);
                    }else
                        M.toast({html:data.msg}, 5000);
                    
                },
                error: function(xhr, status, errorThrown){
                  console.log(xhr);
                }
            });
    });    
    
    
    
});

