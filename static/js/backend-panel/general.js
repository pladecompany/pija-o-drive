var dominio = "https://inthecompanies.com:2030/";
//var dominio = "http://localhost:2030/";
var user = JSON.parse(window.localStorage.getItem('user'));

if (user) {
    if (user.tip_adm == 1)
        $("#ver-admin").show();
}

function imgenpordefecto() {
    var img = "this.src ='../static/img/user.png'";
    var erroryes = 'onerror="' + img + '"';
    return erroryes;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
op = getParameterByName('op');
if (op == 'usuarios') {
    document.write("<script src='../static/js/backend-panel/usuarios.js'></script>");
}

if (op == 'perfil') {
    document.write("<script src='../static/js/backend-panel/perfil.js'></script>");
}

if (op == 'mensajes') {
    document.write("<script src='../static/js/backend-panel/chats.js'></script>");
}

//clientes
if (op == 'clientes') {
    document.write("<script src='../static/js/backend-panel/clientes.js'></script>");
}
if (op == 'conductores') {
    document.write("<script src='../static/js/backend-panel/proveedores.js'></script>");
}

if (op == 'tipovehiculo') {
    document.write("<script src='../static/js/backend-panel/tipovehiculo.js'></script>");
}

if (op == 'planes') {
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write("<script src='../static/js/backend-panel/planes.js'></script>");
}

if (op == 'tarifas') {
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write("<script src='../static/js/moment.min.js'></script>");
    document.write("<script src='../static/js/backend-panel/tarifas.js'></script>");
}

if (op == 'chat') {
    document.write("<script src='../static/js/axios.min.js'></script>");
    document.write("<script src='../static/js/moment.min.js'></script>");
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write('<script type="text/javascript" src="../static/js/moment/jquery.timeago.js"></script>');
    document.write("<script src='../static/js/backend-panel/chats.js'></script>");
}

//notificaciones
if (op == 'notificaciones') {
    document.write("<script src='../static/js/axios.min.js'></script>");
    document.write("<script src='../static/js/moment.min.js'></script>");
    document.write("<script src='../static/js/moment.es.js'></script>");
    document.write("<script src='../static/js/backend-panel/notificaciones.js'></script>");
}

if (op == 'controldepagos') {
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write("<script src='../static/js/moment.min.js'></script>");
    document.write("<script src='../static/js/backend-panel/pagos.js'></script>");
}

if (op == 'administradores') {
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write("<script src='../static/js/backend-panel/admin.js'></script>");
}

if (op == 'mensaje') {
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write("<script src='../static/js/backend-panel/mensajes.js'></script>");
}

if (op == 'mensajesmasivos') {
    document.write("<script src='../static/js/funciones.js'></script>");
    document.write("<script src='../static/js/backend-panel/mensajesmasivos.js'></script>");
}