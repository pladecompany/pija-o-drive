$(document).ready(function(){
    $(document).on('change', "#tip_usu", function(){
        val = $(this).val();
        if(val==1){
            $(".fil_con").show();
            $(".fil_usu").hide();
            $("#bus_con").attr('disabled',false);
            $('#bus_con').formSelect();
        }else if(val==2){
            $(".fil_usu").show();
            $(".fil_con").hide();
            $("#bus_usu").attr('disabled',false);
            $('#bus_usu').formSelect();
        }else{
            $(".fil_usu").show();
            $(".fil_con").hide();
            $("#bus_usu").val("").trigger("change");
            $("#bus_usu").attr('disabled',true);
            $('#bus_usu').formSelect();
            $("#can").hide();
            $("#pun").val("").trigger("change");
            $("#pun").attr('disabled',true);
            $('#pun').formSelect();
        }
    });
    $(document).on('change', "#bus_usu , #bus_con", function(){
        val = $(this).val();
        if(val==1){
            $("#can").show();
            $("#pun").hide();
            $("#pun").attr('disabled',false);
            $('#pun').formSelect('destroy');
            
        }else{
            $("#can").hide();
            $("#pun").attr('disabled',false);
            $('#pun').formSelect();
        }
    });
    $(document).on('click', ".btn-bus", function(){
        if($('#tip_usu').val()==''){
            M.toast({ html: "Debes seleccionar el tipo de usuario" }, 10000);
        }/*else if($('#tip_usu').val()=='1' && $('#bus_con').val()==''){
            M.toast({ html: "Debes seleccionar como deseas buscar" }, 10000);
        }else if($('#tip_usu').val()=='2' && $('#bus_usu').val()==''){
            M.toast({ html: "Debes seleccionar como deseas buscar" }, 10000);
        }else if(($('#tip_usu').val()=='2' && $('#bus_usu').val()==1 || $('#tip_usu').val()=='1' && $('#bus_con').val()==1) && $('#can').val().trim()==''){
            M.toast({ html: "Debes ingresar la Puntuación/Cantidad" }, 10000);
        }else if(($('#tip_usu').val()=='2' && $('#bus_usu').val()>1 || $('#tip_usu').val()=='1' && $('#bus_con').val()>1) && $('#pun').val()==''){
            M.toast({ html: "Debes seleccionar la Puntuación/Cantidad" }, 10000);
        }else if($('#cond').val()==''){
            M.toast({ html: "Debes seleccionar la condición" }, 10000);
        }*/else{
            if($('#tip_usu').val()=='2')
                listClientes();
            else
                listproveedores();
        }

    });

    function listClientes() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'clientes',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_clientes(data)
            }

        })
    }

    function actualizar_tabla_clientes(data) {
        var table = $('.tabla').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        $(".txt-val").html('Amabilidad');

        var buscar = $("#bus_usu").val();
        if(buscar==1)
            var valorb = $("#can").val();
        else
            var valorb = $("#pun").val();
        var con = $("#cond").val();
        
        var resultado = 0;
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            /*if (datos.est_cli == 1) {
                paso = 0;
                viajes = 0;
                puntos = 0;
                if (datos.DetallesC && datos.DetallesC.Reputacion)
                    puntos = datos.DetallesC.Reputacion;
                if (datos.DetallesC && datos.DetallesC.Viajes_total)
                    viajes = datos.DetallesC.Viajes_total;

                if(buscar==1)
                    verificar=viajes;
                else
                    verificar=puntos;

                if(con==1 && verificar<valorb)
                    paso = 1;
                else if(con==2 && verificar<=valorb)
                    paso = 1;
                else if(con==3 && verificar==valorb)
                    paso = 1;
                else if(con==4 && verificar>valorb)
                    paso = 1;
                else if(con==5 && verificar>=valorb)
                    paso = 1;

                if(paso==1){
                    resultado++;
                    puntos = puntos + ' <i class="fa fa-star"></i>';
                    viajes = viajes + ' <i class="fa fa-taxi"></i>';
                    
                    check = '<label><input class="chek-usu" checked name="condi['+datos.id+']" id="condi" type="checkbox" value="clientes"/><span></span></label>';
                    var row = [check,datos.id, datos.nom_cli, datos.cor_cli, 'Cliente',viajes, puntos, puntos]
                    table.row.add(row).draw().node();
                } 
            }
            veri=data.length-1;
            if(veri==i && resultado==0)
                M.toast({ html: "No se encontro ningun resultado." }, 10000);*/
            if (datos.est_cli == 1) {
                viajes = 0;
                puntos = 0;
                if (datos.DetallesC && datos.DetallesC.Reputacion)
                    puntos = datos.DetallesC.Reputacion;
                if (datos.DetallesC && datos.DetallesC.Viajes_total)
                    viajes = datos.DetallesC.Viajes_total;
                puntos = puntos + ' <i class="fa fa-star"></i>';
                viajes = viajes + ' <i class="fa fa-taxi"></i>';
                        
                check = '<label><input class="chek-usu" checked name="condi['+datos.id+']" id="condi" type="checkbox" value="clientes"/><span></span></label>';
                var row = [check,datos.id, datos.nom_cli, datos.cor_cli, 'Cliente',viajes, puntos, puntos]
                table.row.add(row).draw().node();
            }
            
        }  
    }
    function listproveedores() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'proveedores',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_proveedores(data)
            }

        })
    }

    function actualizar_tabla_proveedores(data) {
        var table = $('.tabla').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        $(".txt-val").html('Limpieza / Amabilidad / Puntualidad');
        var buscar = $("#bus_con").val();
        if(buscar==1)
            var valorb = $("#can").val();
        else
            var valorb = $("#pun").val();
        var con = $("#cond").val();
        
        var resultado = 0;
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            

            /*if (datos.est_pro == 1) {
                paso = 0;
                viajes = 0;
                puntos = 0;
                puntos1 = 0;
                puntos2 = 0;
                puntos3 = 0;
                if (datos.Detalles && datos.Detalles.Reputacion)
                    puntos = datos.Detalles.Reputacion;
                if (datos.Detalles && datos.Detalles.voto1)
                    puntos1 = datos.Detalles.voto1;
                if (datos.Detalles && datos.Detalles.voto2)
                    puntos2 = datos.Detalles.voto2;
                if (datos.Detalles && datos.Detalles.voto3)
                    puntos3 = datos.Detalles.voto3;
                if (datos.Detalles && datos.Detalles.Viajes_total)
                    viajes = datos.Detalles.Viajes_total;
                if(buscar==1)
                    verificar=viajes;
                else if(buscar==3)
                    verificar=puntos1;
                else if(buscar==4)
                    verificar=puntos2;
                else if(buscar==5)
                    verificar=puntos3;
                else
                    verificar=puntos;

                if(con==1 && verificar<valorb)
                    paso = 1;
                else if(con==2 && verificar<=valorb)
                    paso = 1;
                else if(con==3 && verificar==valorb)
                    paso = 1;
                else if(con==4 && verificar>valorb)
                    paso = 1;
                else if(con==5 && verificar>=valorb)
                    paso = 1;

                if(paso==1){
                    resultado++;
                    puntos = puntos + ' <i class="fa fa-star"></i></a>';
                    puntos1 = puntos1 + ' <i class="fa fa-star"></i></a>';
                    puntos2 = puntos2 + ' <i class="fa fa-star"></i></a>';
                    puntos3 = puntos3 + ' <i class="fa fa-star"></i></a>';
                    viajes = viajes + ' <i class="fa fa-taxi"></i></a>';

                    check = '<label><input class="chek-usu"  checked name="condi['+datos.id+']" id="condi" type="checkbox" value="proveedores"/><span></span></label>';
                    var row = [check,datos.id, datos.nom_pro, datos.cor_pro, 'Conductor', viajes, puntos, puntos1+" / "+puntos2+" / "+puntos3]

                    table.row.add(row).draw().node();
                }
            } 
            veri=data.length-1;
            if(veri==i && resultado==0)
                M.toast({ html: "No se encontro ningun resultado." }, 10000);
            */
            if (datos.est_pro == 1) {
                viajes = 0;
                puntos = 0;
                puntos1 = 0;
                puntos2 = 0;
                puntos3 = 0;
                if (datos.Detalles && datos.Detalles.Reputacion)
                    puntos = datos.Detalles.Reputacion;
                if (datos.Detalles && datos.Detalles.voto1)
                    puntos1 = datos.Detalles.voto1;
                if (datos.Detalles && datos.Detalles.voto2)
                    puntos2 = datos.Detalles.voto2;
                if (datos.Detalles && datos.Detalles.voto3)
                    puntos3 = datos.Detalles.voto3;
                if (datos.Detalles && datos.Detalles.Viajes_total)
                    viajes = datos.Detalles.Viajes_total;
                
                
                puntos = puntos + ' <i class="fa fa-star"></i></a>';
                puntos1 = puntos1 + ' <i class="fa fa-star"></i></a>';
                puntos2 = puntos2 + ' <i class="fa fa-star"></i></a>';
                puntos3 = puntos3 + ' <i class="fa fa-star"></i></a>';
                viajes = viajes + ' <i class="fa fa-taxi"></i></a>';

                check = '<label><input class="chek-usu"  checked name="condi['+datos.id+']" id="condi" type="checkbox" value="proveedores"/><span></span></label>';
                var row = [check,datos.id, datos.nom_pro, datos.cor_pro, 'Conductor', viajes, puntos, puntos1+" / "+puntos2+" / "+puntos3]

                table.row.add(row).draw().node();
                
            } 
        }
    }
    $(document).on('click', ".btn-save", function(){
        listuser=0;
        $(".chek-usu").each(function(){ 
            if( $(this).is(':checked') )
                listuser++;
        });
        if($('#tit').val().trim()==''){
            M.toast({ html: "Debes ingresar un titulo" }, 10000);
        }else if($('#des').val().trim()==''){
            M.toast({ html: "Debes ingresar una descripción" }, 10000);
        }else if(listuser==0){
            M.toast({ html: "Debes seleccionar al menos un Usuario" }, 10000);
        }else{
            $('.btn-save').html('Enviando. . .');
            $('.btn-save').attr("disabled",true);
            var formData = new FormData(document.getElementById("form_msj"));
            formData.append("tit" ,$('#tit').val());
            formData.append("des" ,$('#des').val());
            formData.append("ida" ,user.id);
            formData.append("token" ,user.token);
            $.ajax({
                url: dominio + "mensajes/masivos", 
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    $('.btn-save').html('Enviar');
                    $('.btn-save').attr("disabled",false);
                    if(data.r){
                        $("#modal").modal('close');
                        $('#tit').val('');
                        $('#des').val('');
                        M.toast({html:data.msj}, 5000);
                    }else
                        M.toast({html:data.msj}, 5000);
                    
                },
                error: function(xhr, status, errorThrown){
                  console.log(xhr);
                }
            });
        }

    });
});

