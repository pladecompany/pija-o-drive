$(document).ready(function(){
    console.log("Modulo chats");
    axiosChats();

    
    //Cargar Listado de usuario al chat
    function axiosChats(){

        axios.get(dominio+'chats/admin/obtener-usuarios?id='+user.id+'&token='+user.token)
        .then(function (response) {
            data = response.data
            for(var i=0;i<data.length;i++){
                datos = data[i]
                if(datos.pendientes>0){
                    var badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+datos.pendientes+'</span>'
                }else{
                    var badge= ''
                }
                if(datos.envio ==1){
                    usu = 'cliente'
                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${datos.nombre} (${usu})</p></a>`
                    img = datos.img
                }else{
                    usu = 'proveedor'
                    nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${datos.nombre} (${usu})</p></a>`
                    img = datos.img
                }
                $('.listado').append(` 
                    <div class="boxChat__cont boxList" id="${datos.envio}-${datos.id_usuario}">
                        <div class="row mb-0">
                            <div class="col s3">
                                <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
                            </div>

                            <div class="col s9 chat" id="${datos.id}">
                                ${nombre}
                                <p id="msj">${datos.mensaje}</p>
                                <span class="timeago" title="${datos.hora}"></span>
                                <input type="number"  value="${datos.envio}" class="envio" style="display:none">
                                <input type="number"  value="${datos.id_usuario}" class="id_usuario" style="display:none">
                                ${badge}
                            </div>
                        </div>
                    </div>
                `);

            }
            $(".timeago").timeago();
            if($("#enviobuscar").val()!=0)
                abrirchattabla();
        })
        .catch(function (error) {
            console.log(error);

        });
    }

$("#enviar").bind('paste', function (e){
    $(e.target).keyup(getInput);
});

function getInput(e){
    //content = e.originalEvent.clipboardData.getData('text/plain');
    var inputText = $(e.target).html();
    $('#enviar').html(inputText.replace(/<[^>]*>?/g, ''))
    $(e.target).unbind('keyup');
}

$(document).on('click','.boxList',function(){
    
    men = $('#men').css('display','block')
    $('#mensajes').html('');
    $('#mensajes').append('<center><i class="fa fa-spinner fa-spin"></i></center>')
    $('#init').remove()
    envio = $(`#${this.id} > .row > .chat > .envio`).val()

    nombre = $(`#${this.id} > .row > .chat > .link > .nombre`).text()
    if(!nombre)
        nombre = $(`#${this.id} > .row > .chat > .nombre`).text()
    id_usuario = $(`#${this.id} > .row > .chat > .id_usuario`).val()

    
    
    /*pen1 = $('#num').html()
    pen2 = $(`#${this.id} > .row > .chat > .badge`).html()
    
    if(parseInt(pen1)>0){
        resta = parseInt(pen1)-parseInt(pen2)
        if(resta == 0){
            $('#msj1').css('color','#fff')
        }
        $('#num').html(resta) 
    }*/
    $(`#${this.id} > .row > .chat > .badge`).remove()
    
    $('#nombre').html(nombre)
    
    $('#id_usuario').val(id_usuario)
    $('#envio').val(envio)
    $('img').on('error',function(){
        $(this).attr('src','../static/img/user.png')
    });

    axios.get(dominio+'chats/admin/obtener-chats?envio='+envio+'&id_usuario='+id_usuario+'&token='+user.token)
        .then(res=>{
            datos = res.data.reverse()
            $('#mensajes').html('');
            coun1 = 0
            for(var i = 0;i<datos.length;i++){
                data=datos[i]
                if(data.tabla_usuario == 'admin'){
                    tipo = 'boxChat__left'
                    if(envio ==2){
                        img = data.img
                    }else{
                        img = data.img
                    }
                    if(coun1 ==0){
                        $('#msj-chat').attr('src',img)
                        coun1 +=1;
                    }
                }else{
                    tipo = 'boxChat__right'
                    img = '../static/img/user.png'
                }
                $('#mensajes').append(`
                <li class="${tipo}">
                    <div class="avatar">
                        <img src="${img}" ${imgenpordefecto()} class="img-cover w100 circle" style="width: 40px;height:40px;" draggable="false">
                    </div>
                    <div class="msg">
                        <p class="m-0">${data.mensaje}</p>
                        <span class="timeago" title="${data.hora}"></span>
                    </div>
                </li>
                `)

            }
        $(".timeago").timeago();
        scrollToBottom()
        })
    input = $('#bus_men').val()
    if(input.length>0){
        $('#bus_men').val('');
        $('#bus_men').keyup();
    }
})
function abrirchattabla(){
    
    men = $('#men').css('display','block')
    $('#mensajes').html('');
    $('#mensajes').append('<center><i class="fa fa-spinner fa-spin"></i></center>')
    $('#init').remove()
    envio = $("#enviobuscar").val()
    nombre = $("#nombrebuscar").val()
    id_usuario = $("#id_usuariobuscar").val()
    
    $(`#${id_usuario} > .row > .chat > .badge`).remove()
    
    $('#nombre').html(nombre)
    $('#id_usuario').val(id_usuario)
    $('#envio').val(envio)
    

    axios.get(dominio+'chats/admin/obtener-chats?envio='+envio+'&id_usuario='+id_usuario+'&token='+user.token)
        .then(res=>{
            //console.log(res);
            datos = res.data.reverse()
            $('#mensajes').html('');
            coun1 = 0
            for(var i = 0;i<datos.length;i++){
                data=datos[i]
                if(data.tabla_usuario == 'admin'){
                    tipo = 'boxChat__left'
                    if(envio ==2){
                        img = data.img
                    }else{
                        img = data.img
                    }
                    if(coun1 ==0){
                        $('#msj-chat').attr('src',img)
                        coun1 +=1;
                    }
                }else{
                    tipo = 'boxChat__right'
                    img = '../static/img/user.png'
                }
                $('#mensajes').append(`
                <li class="${tipo}">
                    <div class="avatar">
                        <img src="${img}" ${imgenpordefecto()} class="img-cover w100 circle" style="width: 40px;height:40px;" draggable="false">
                    </div>
                    <div class="msg">
                        <p class="m-0">${data.mensaje}</p>
                        <span class="timeago" title="${data.hora}"></span>
                    </div>
                </li>
                `)

            }
        $(".timeago").timeago();
        scrollToBottom()
        })
    input = $('#bus_men').val()
    if(input.length>0){
        $('#bus_men').val('');
        $('#bus_men').keyup();
    }
}
function scrollToBottom(){
   $("#content_messages").scrollTop($("#content_messages")[0].scrollHeight);
}
usuarios = []
var searchArray = function(arr, regex) {
  var matches=[], i;
  for (i=0; i<arr.length; i++) {
    if (arr[i].nombre.match(regex)) matches.push(arr[i]);
  }
  if(usuarios.length<1){
    usuarios.push(arr)
  }
  return matches;
};
$("#bus_men" ).keyup(function() {
    //if($(".listado > div").length >0){

      nombre = $('.nombre')
      var ss = [];
      for(var i=0; i<nombre.length;i++){
        nombres = $(nombre[i]).html()
        html= $(nombre[i]).parent().parent().parent().parent()
        filter1 = $(nombre[i]).parent().parent()
        id_usuario1 = $(`#${filter1[0].id} > .id_usuario`).val()
        envio1 = $(`#${filter1[0].id} > .envio`).val()
        pendientes = $(`#${filter1[0].id} > .badge`).html()
        if (pendientes == undefined){
            pendientes=0;
        }
        ss.push({pendientes:parseInt(pendientes),id_usuario:parseInt(id_usuario1),envio:parseInt(envio1),nombre:nombres.toLowerCase(),html:html})


      }
      filtro = $(this).val().toLowerCase()
      if(filtro != ''){
        result = searchArray(ss, filtro); // => ['german shepard', 'chihuahua']


          socket.emit('ENCONTRAR-USUARIO',filtro)
          socket.on('USUARIO-ENCONTRADO',function(data){
            if(data.length>0){
                $('.listado').html('')
                for(var i = 0; i<data.length;i++){
                    datos = data[i]
                    if(datos.pendientes>0){
                        var badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">'+datos.pendientes+'</span>'

                    }else{
                        var badge = ''
                    }
                    if(datos.envio ==1){
                        usu = 'cliente'
                        nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${datos.nombre} (${usu})</p></a>`
                        img = datos.img
                        if(datos.mensaje != null){
                            mensaje = datos.mensaje
                        }else{
                            mensaje = 'No hay mensajes con este usuario'
                        }
                    }else{
                        usu = 'proveedor'
                        nombre = `<a href="" onclick="return false;" class="link"><p class="nombre" style="font-weight:700;">${datos.nombre} (${usu})</p></a>`
                        img = datos.img
                        if(datos.mensaje != null){
                            mensaje = datos.mensaje
                        }else{
                            mensaje = 'No hay mensajes con este usuario'
                        }
                    }
                    
                    $('.listado').append(` 
                        <div class="boxChat__cont boxList" id="${datos.envio}-${datos.id_usuario}">
                            <div class="row mb-0">
                                <div class="col s3">
                                    <img src="${img}" ${imgenpordefecto()} class="boxChat--img img-cover w100">
                                </div>

                                <div class="col s9 chat" id="${datos.id}">
                                    ${nombre}
                                    <p id="msj">${mensaje}</p>
                                    <span class="timeago" title="${datos.hora}"></span>
                                    <input type="number"  value="${datos.envio}" class="envio" style="display:none">
                                    <input type="number"  value="${datos.id_usuario}" class="id_usuario" style="display:none">
                                    ${badge}
                                </div>
                            </div>
                        </div>`);

                    $('img').on('error',function(){
                            $(this).attr('src','../static/img/user.png')
                    })
                    //ss.push({pendientes:parseInt(pendientes),id_usuario:parseInt(id_usuario1),envio:parseInt(envio1),nombre:nombres.toLowerCase(),html:html})
                }   
            }else{
                $('.listado').html('<div><center><p style="color:#fff;font-weight:bold;">NO SE ENCONTRÓ "'+filtro+'"</p></center></div>')

            }
          })
      }else{

            $('.listado').html('')
            for(var i = 0;i<usuarios[0].length;i++){
                //console.log(usuarios[i].html)
                $('.listado').append(usuarios[0][i].html)
            }

      }
    //}
    

});
$('.send').on('click',function(){
    $(this).attr('disabled',true);
    if($("#enviar").html().trim()!=""){
        mensaje = $('#enviar').html()
        id_usuario = $('#id_usuario').val()
        envio = $('#envio').val()
        if (parseInt(envio) ==1){
            tabla_usuario = "clientes"
            tablausuario = "clientes"
        }else{
            tabla_usuario = "proveedores"
            tablausuario = "proveedores"

        }
        var data = {
            id_usuario: id_usuario,
            mensaje: mensaje,
            tabla_usuario: tabla_usuario,
            envio: envio,
            tablausuario:tablausuario,
        };
        hora = moment().format("YYYY-MM-DD HH:mm:ss")
        $('#mensajes').append(` 
            <li class="boxChat__right">
                <div class="avatar">
                    <img src="../static/img/user.png" class="img-cover w100" draggable="false">
                </div>
                <div class="msg">
                    <p class="m-0">${mensaje}</p>
                    <span class="timeago" title="${hora}"></span>
                </div>
            </li>
        `)
        scrollToBottom()
        socket.emit("ENVIARMENSAJE",data);
        $("#enviar").html("");
        $(".timeago").timeago();
    }
    $(this).attr('disabled',false);

});
 
});

