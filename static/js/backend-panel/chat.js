$(document).ready(function() {

  var user = JSON.parse(window.localStorage.getItem('user'));

  $(document).on('click', ".bt_abrir_chat", function(){
    var id_ses = user.id;
    var id_usu = $(this).attr('id');
    $("#bt_enviar").attr('idr', id_usu);
    $("#documento").attr('id_r', id_usu);
    $("#mensajes").html("");
    $("#listChat").show();
    $("#listUser").hide();
    $("#contador_" + id_usu).remove();
    socket.emit("buscarMensajes", {ids: id_ses, idu: id_usu});
  });

  $("#documento").on('change', function(){
      var id_r = $(this).attr('id_r');
        var toastContent = '<span>Archivo seleccionado correctamente ¿Deseas enviarlo?</span><br><button class="btn-flat toast-action conf_si_adjuntar" id="'+ id_r+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';
        M.toast({
            html: toastContent
        }, 4000);
  });

  
  $(document).on('click', '.conf_si_adjuntar', function(){
      M.Toast.dismissAll()
      var method = $(this).attr('method');
      var url = $(this).attr('action');
      var formData = new FormData();
      let file = document.querySelector('#documento');
      var id_r =this.id;
      formData.append("documento", file.files[0]);
      formData.append("token", user.token);
      formData.append("ida", user.id);
      formData.append("idr", id_r);
      var url = dominio + "chat/adjuntar";

       $.ajax({
          url: url, 
          type: "POST",
          cache: false,
          dataType: 'json',
          contentType: false,
          processData: false,
          data: formData,
          success: function(data){
            console.log(data);
          },
          error: function(xhr, status, errorThrown){
            console.log(xhr);
          }
       });
  });


  $("#bt_enviar").click(function(){
    var msj = $("#caja_mensaje").text().trim();
    var idr = $(this).attr('idr');

    if(msj.length > 0){
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds()
      var data = {
        idr: idr,
        usuario: user,
        mensaje: msj,
        fec_reg_chat: formatted_date
      };
      $("#mensajes").append(mensajeChat(data, "self"));
      $("#caja_mensaje").text('');
      socket.emit("nuevoMensaje", data);
      $('.timeago').timeago();
    }else{
    }
  });

  $("#bt_salir").click(function(){
      socket.emit("desconectar",user);
  });

});
