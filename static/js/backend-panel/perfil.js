$(document).ready(function(){
    perfil()
    function perfil(){
        var user = JSON.parse(window.localStorage.getItem('user'));
        $('#nombre').val(user.nom_usu)
        $('#apellido').val(user.ape_usu)
        $('#correo').val(user.cor_usu)
        $('#telefono').val(user.tlf_usu)
        $('#sexo').val(user.sex_usu)
        $('#imagen').attr('src',dominio+user.img_usu)
        $('#imagen').on('error',function(){
            $(this).attr('src','../static/img/user.png')
        })
    }

    $('#update_perfil').on('submit',function(e){
        e.preventDefault();
        nombre = $('#nombre').val()
        apellido = $('#apellido').val()
        correo = $('#correo').val()
        telefono = $('#telefono').val()
        sexo = $('#sexo').val()
        if (nombre.length<3){
            M.toast({html:'Por favor ingresa un nombre.'})
            return false;
        }
        if(apellido.length<3){
            M.toast({html:'Por favor ingresa un apellido.'})
            return false;
        }

        var data = new FormData(this);
        data.append("id", user.id);
        $.ajax({
          url: dominio + "usuarios/edit-usuario/"+user.id,
          type: "PUT",
          data: data,
          contentType: false,
          processData: false,
        success: function(data){
          M.toast({html:data.msj}, 10000);
          console.log(data);
          if (data.msj == "Se modificó correctamente") {
              //$('#update_perfil').trigger("reset");
              window.localStorage.setItem("user", JSON.stringify(data));
              perfil()
          }
        },
        error: function(xhr, status, errorThrown){
            //M.toast({html:err.data.msj}, 10000);
          console.log("Errr : ");
        }
        });
    })
});
