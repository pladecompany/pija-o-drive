$(document).ready(function(){
    //listproveedores();
    
    tiposVeh__();
    function tiposVeh__(){
            var html = '';              
             
             $.ajax({
                url : dominio+'tipovehiculo/panel/?token='+user.token,
                type: 'GET',
                data:{},
                success:function(response){
                   actualizar_tipo_vehiculo(response);
                },
                error: function(xhr, status, errorThrown){
                    console.log(xhr);
                }

            });
    }
    function actualizar_tipo_vehiculo(data){
        acumveh=data.length-1;
        for(var i=0;i<data.length;i++){
            datos = data[i];
            $('#listar_vehiculos').append(`<div class="col s12 m12 l6">
                <div class="row border flexCenter m-2">
                    <div class="col s12 m2 flexCenter">
                         <img src="../static/img/transporte/${datos.icono_veh}" class="trans--img">
                    </div>

                    <div class="col s12 m10">
                         <div class="row mb-0">
                            <div class="col s12 m3">
                                <br><h5 class="center veh_${datos.id}">0</h5>
                            </div>

                            <div class="col s12 m3">
                                <h6 class="center"><span class="trans-act veh_${datos.id}_1">0</span></h6> 
                                <h6 class="center">Activo</h6>
                            </div>

                            <div class="col s12 m3">
                                <h6 class="center"><span class="trans-blo veh_${datos.id}_2">0</span></h6> 
                                <h6 class="center">Bloqueado</h6>
                            </div>

                            <div class="col s12 m3">
                                <h6 class="center"><span class="trans-pau veh_${datos.id}_0">0</span></h6> 
                                <h6 class="center">Pausado</h6>
                            </div>
                         </div>
                    </div>
                </div>
            </div>`);   
            if(acumveh==i)
                listproveedores();
        }
        
    }

    function listproveedores(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'proveedores',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                acumular_vehiculos(data)
            }

        })
    }

    function acumular_vehiculos(data){
        for(var i=0;i<data.length;i++){
            datos = data[i];
            if(datos.Detalles){
                var tip_veh=datos.Detalles.id_tipo_vehiculo;
                var sts_veh=datos.est_pro;
                var tot_veh_a=$(".veh_"+tip_veh).html();
                var tot_veh_i=$(".veh_"+tip_veh+"_"+sts_veh).html();
                tot_veh_a++;
                tot_veh_i++;
                $(".veh_"+tip_veh).html(tot_veh_a)
                $(".veh_"+tip_veh+"_"+sts_veh).html(tot_veh_i)
            }
        }
    }

});

