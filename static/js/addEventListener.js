document.addEventListener('DOMContentLoaded', function(){
    const boxHeader = document.querySelector('#boxHeader');
    const boxNav = document.querySelector('#boxNav');
    const tiggerNav = document.querySelector('#boxHamburguesa');
    const boxVideo = document.querySelector('#boxHeader__video');
    const btnComoFunciona = document.querySelector('#comoFunciona');
    const boxComoFunciona = document.querySelector('#boxHeader__comoFunciona');
    const closeComoFunciona = document.querySelector('#closeComoFunciona');
    
    const mediaquery = window.matchMedia("(min-width: 992px)");
    if (mediaquery.matches) {
        if (boxNav && boxHeader) {
            boxHeader.setAttribute('style', `padding-top: ${boxNav.offsetHeight}px;`)
        }

        if (boxNav && boxVideo) {
            boxVideo.setAttribute('style', `top: ${boxNav.offsetHeight}px; height: calc(100vh - ${boxNav.offsetHeight}px);`)
        }

        if (boxNav && boxComoFunciona) {
            boxComoFunciona.setAttribute('style', `top: ${boxNav.offsetHeight}px;`);
        }
    }

    if (btnComoFunciona && closeComoFunciona && boxComoFunciona) {
        btnComoFunciona.addEventListener('click', function(){
            boxComoFunciona.classList.toggle('active');
        });

        closeComoFunciona.addEventListener('click', function(){
            if (boxComoFunciona.classList.contains('active')) {
                boxComoFunciona.classList.remove('active');
            }
        });
    }

    if (boxNav && tiggerNav) {
        tiggerNav.addEventListener('click', function(){
            var icon = this.children;
            if(icon[0].classList.contains('mdi-navigation-menu')) {
                icon[0].classList.remove('mdi-navigation-menu');
                icon[0].classList.add('mdi-navigation-close');
            } else {
                icon[0].classList.remove('mdi-navigation-close');
                icon[0].classList.add('mdi-navigation-menu');
            }
            boxNav.classList.toggle('active');
        });
    }
});

// Inicializa Slider
function iniciarSlider(el) {
    var flkty = new Flickity( `${el}`, {
        cellAlign: 'left',
        contain: true,
        pageDots: false
    });
}

function iniciarSliderTwo(el) {
    var flkty = new Flickity( `${el}`, {
        cellAlign: 'left',
        contain: true,
        autoPlay: 5000,
        groupCells: 3
    });
}

function iniciarSliderThree(el) {
    var flkty = new Flickity( `${el}`, {
        cellAlign: 'left',
        contain: true,
        autoPlay: 6000,
        groupCells: 1
    });
}