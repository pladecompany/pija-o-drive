const moment = require("moment");
const bcrypt = require("bcryptjs");
exports.seed = async function(knex, Promise) {
    // Deletes ALL existing entries
    console.log("Borrando Datos...");
    let y = 0;
    let x = true;

    x = await knex("admins").del();
    x = await knex("reputacion").del();
    x = await knex("solicitud_servicios").del();
    x = await knex("detalles_proveedores").del();
    x = await knex("pagos").del();
    x = await knex("proveedores").del();
    x = await knex("plan").del();
    x = await knex("horario_tarifa").del();
    x = await knex("tipovehiculo").del();
    x = await knex("clientes").del();
    x = await knex("firebases").del();


    await knex("admins").insert({
        id: 1,
        user: "admin",
        tip_adm: 1,
        correo: "admin@master.com",
        pass: bcrypt.hashSync("12345", 8)
    });

    /*await knex("tipovehiculo").insert({
        id: 1,
        nombre_veh: "Bicicleta",
        icono_veh: "bicycle.png",
        est_veh: 1,
        preciokm: 0
    });*/
    await knex("tipovehiculo").insert({
        id: 2,
        nombre_veh: "Carro",
        icono_veh: "car.png",
        est_veh: 1,
        preciokm: 0
    });
    await knex("tipovehiculo").insert({
        id: 3,
        nombre_veh: "Scooter",
        icono_veh: "scooter.png",
        est_veh: 1,
        preciokm: 0
    });
    /*await knex("tipovehiculo").insert({
        id: 4,
        nombre_veh: "Camión Grua",
        icono_veh: "crane-truck.png",
        est_veh: 1,
        preciokm: 0
    });*/
    await knex("tipovehiculo").insert({
        id: 5,
        nombre_veh: "Pickup",
        icono_veh: "pickup-truck.png",
        est_veh: 1,
        preciokm: 0
    });
    await knex("tipovehiculo").insert({
        id: 6,
        nombre_veh: "Camión",
        icono_veh: "truck.png",
        est_veh: 1,
        preciokm: 0
    });
    //var Veh = [1, 2, 3, 4, 5, 6];
    var Veh = [2, 3, 5, 6];
    for (var index = 0; index < Veh.length; index++) {
        await knex("horario_tarifa").insert({
            "id_tipo_vehiculo": Veh[index],
            "preciokm": (4 * Veh[index]),
            "preciomin": (10),
            "hor_des": "00:00:00",
            "hor_has": "08:00:00"
        });
        await knex("horario_tarifa").insert({
            "id_tipo_vehiculo": Veh[index],
            "preciokm": (1 * Veh[index]),
            "preciomin": (10),
            "hor_des": "08:01:00",
            "hor_has": "16:00:00"
        });
        await knex("horario_tarifa").insert({
            "id_tipo_vehiculo": Veh[index],
            "preciokm": (2 * Veh[index]),
            "preciomin": (10),
            "hor_des": "16:01:00",
            "hor_has": "22:00:00"
        });
        await knex("horario_tarifa").insert({
            "id_tipo_vehiculo": Veh[index],
            "preciokm": (3 * Veh[index]),
            "preciomin": (10),
            "hor_des": "22:01:00",
            "hor_has": "23:59:00"
        });

    }
};
