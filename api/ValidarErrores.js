const { ValidationError, NotFoundError } = require("objection");
const AR = require("./ApiResponser");
const PladeValidationError = require("./Errores").ValidationError;
const { AuthError } = require("./Errores");
const { UniqueViolationError } = require("objection-db-errors");

module.exports = function errorHandler(err, req, res, next) {
    function enviarError(code, msg, tipo, data = {}) {
        res.status(code).send({ msg, tipo, data });
    }

    if (err instanceof ValidationError) {
        //Variable de errores que se va a enviar
        let errores = {
            requeridos: []
        };
        switch (err.type) {
            //Manejar Errores de Validación
            case "ModelValidation":
                //En caso de que haya parametros requeridos:
                for (let [property, data] of Object.entries(err.data)) {
                    console.log(data[0].params);
                    switch (data[0].keyword) {
                        case "required":
                            errores["requeridos"].push(data[0].params.missingProperty);
                            break;
                    }
                }
                enviarError(
                    400,
                    "Por favor, verifique que ha ingresado todo correctamente.",
                    "ErrorDeValidación",
                    errores
                );
                break;

            case "RelationExpression":
                enviarError(400, err.message, "InvalidRelationExpression");
                break;

            case "UnallowedRelation":
                enviarError(400, err.message, "UnallowedRelation");
                break;

            case "InvalidGraph":
                enviarError(400, err.message, "InvalidGraph");
                break;

            default:
                enviarError(400, err.message, err.type);
                break;
        }
    } else if (err instanceof UniqueViolationError) {
        enviarError(409, "Ya existe un registro similar.", "ErrorDBUnico", {
            columnas: err.columns,
            tabla: err.table,
            constraint: err.constraint
        });
    } else if (err instanceof NotFoundError) {
        enviarError(404, err.message, "NotFound");
    } else if (err instanceof PladeValidationError) {
        enviarError(400, err.message, "ErrorDeValidacion");
    } else if (err instanceof AuthError) {
        enviarError(403, err.message, "ErrorDeAutorizacion");
    } else {
        res.status(500).send(err);
        //res.status(500).send({ msg: err.message, tipo: err.type, data: {} });
    }
};
