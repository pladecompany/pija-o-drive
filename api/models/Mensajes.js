"use strict";

const { Modelo } = require("./Modelo");

class Mensajes extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "mensajes_pre";
    }

}

module.exports = Mensajes;