"use strict";

const { Modelo } = require("./Modelo");

class Reputacion extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "reputacion";
    }

}

module.exports = Reputacion;