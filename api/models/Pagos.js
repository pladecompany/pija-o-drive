"use strict";

const { Modelo } = require("./Modelo");
const Proveedores = require("../models/Proveedores");

class Pagos extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "pagos";
    }

    $afterGet(queryContext) {
	    return this.ag();
	}

	async info_proveedor() {
	    
		if(this.id_proveedor){
			var Pr = await Proveedores.query()
		      .where("id", this.id_proveedor)
			  .catch(err => {
		      });
			this.Proveedor = Pr[0];
		}
		

	}
	async ag() {
	    await this.info_proveedor();
	}

}

module.exports = Pagos;