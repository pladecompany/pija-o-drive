"use strict";

const { Modelo } = require("./Modelo");

class Tokencuentas extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "token_cuentas";
    }

}

module.exports = Tokencuentas;