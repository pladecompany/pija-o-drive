"use strict";

const { Modelo } = require("./Modelo");

class Planes extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "plan";
    }

}

module.exports = Planes;