"use strict";

const { Modelo } = require("./Modelo");
const Horario = require("../models/Horario");
const moment = require("moment");

class Tipovehiculo extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "tipovehiculo";
    }

    $afterGet(queryContext) {
        return this.ag();
    }
    async tipovehiculo() {
        var P = await Horario.query()
            .where("id_tipo_vehiculo", this.id)
            .catch(err => {});
        var hora = moment().format("HH:mm:ss");
        for (var i = 0; i < P.length; i++) {
            var e = P[i];
            if (hora >= e.hor_des && hora <= e.hor_has) {
                this.preciokm = e.preciokm;
                this.preciomin = e.preciomin;
                this.Horarioaplicado = e;
                break;
            }
        }
    }
    async ag() {
        await this.tipovehiculo();
    }
}

module.exports = Tipovehiculo;