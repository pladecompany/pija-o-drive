"use strict";

const { Modelo } = require("./Modelo");
const Tipovehiculo = require("../models/Tipovehiculo");

class DetallesP extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "detalles_proveedores";
    }
    $afterGet(queryContext) {
	    return this.ag();
	}

	async petallesp() {
	    var P = await Tipovehiculo.query()
		      .where("id", this.id_tipo_vehiculo)
			  .catch(err => {
		      });
		this.Vehiculo = P[0];

	}
	async ag() {
	    await this.petallesp();
	}
}

module.exports = DetallesP;