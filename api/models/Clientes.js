"use strict";

const { Modelo } = require("./Modelo");
const Reputacion = require("../models/Reputacion");

class Clientes extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "clientes";
    }
    $afterGet(queryContext) {
	    return this.ag();
	}

	async cliente() {
	    

		var R = await Reputacion.query()
		      .where("id_cliente", this.id)
			  .catch(err => {
		      });
		if(R.length>0){
			var voto1=0,voto2=0,voto3=0;
			for (var i = 0; i < R.length; i++) {
				voto1 = (voto1 + R[i].voto1);
				voto2 = (voto2 + R[i].voto2);
				voto3 = (voto3 + R[i].voto3);
			}
			
			voto1 = (voto1/R.length);
			voto2 = (voto2/R.length);
			voto3 = (voto3/R.length);
			this.DetallesC = {};
			this.DetallesC.voto1 = parseInt(voto1.toFixed(0));
			this.DetallesC.voto2 = parseInt(voto2.toFixed(0));
			this.DetallesC.voto3 = parseInt(voto3.toFixed(0));
			//this.DetallesC.Reputacion = parseInt(((voto1+voto2+voto3)/3).toFixed(0));
			this.DetallesC.Reputacion = parseInt(voto2.toFixed(0));
			this.DetallesC.Viajes_total = R.length;
		}

	}
	async ag() {
	    await this.cliente();
	}
}

module.exports = Clientes;