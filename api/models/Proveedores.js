"use strict";

const { Modelo } = require("./Modelo");
const DetallesP = require("../models/DetallesP");
const Reputacion = require("../models/Reputacion");

class Proveedores extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "proveedores";
    }
	
	$afterGet(queryContext) {
	    return this.ag();
	}

	async proveedor() {
	    var P = await DetallesP.query()
		      .where("id_proveedor", this.id)
			  .catch(err => {
		      });

		this.Detalles = P[0];

		var R = await Reputacion.query()
		      .where("id_proveedor", this.id)
			  .catch(err => {
		      });
		if(R.length>0){
			var voto1=0,voto2=0,voto3=0;
			for (var i = 0; i < R.length; i++) {
				voto1 = (voto1 + R[i].voto1);
				voto2 = (voto2 + R[i].voto2);
				voto3 = (voto3 + R[i].voto3);
			}
			
			voto1 = (voto1/R.length);
			voto2 = (voto2/R.length);
			voto3 = (voto3/R.length);
			this.Detalles.voto1 = parseInt(voto1.toFixed(0));
			this.Detalles.voto2 = parseInt(voto2.toFixed(0));
			this.Detalles.voto3 = parseInt(voto3.toFixed(0));
			this.Detalles.Reputacion = parseInt(((voto1+voto2+voto3)/3).toFixed(0));
			this.Detalles.Viajes_total = R.length;
		}

	}
	async ag() {
	    await this.proveedor();
	}
}

module.exports = Proveedores;