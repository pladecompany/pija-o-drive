"use strict";

const { Modelo } = require("./Modelo");

class Mis_domicilios extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "mis_domicilios";
    }

}

module.exports = Mis_domicilios;