"use strict";

const { Modelo } = require("./Modelo");

class Notificaciones extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "notificaciones";
    }

}

module.exports = Notificaciones;