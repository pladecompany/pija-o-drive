"use strict";

const { Modelo } = require("./Modelo");
const Tipovehiculo = require("../models/Tipovehiculo");
const Clientes = require("../models/Clientes");
const Proveedores = require("../models/Proveedores");
const Reputacion = require("../models/Reputacion");

class Solicitud_servicios extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "solicitud_servicios";
    }
	
	$afterGet(queryContext) {
	    return this.ag();
	}

	async solicitud_servicios() {
	    var P = await Tipovehiculo.query()
		      .where("id", this.id_tipo_veh)
			  .catch(err => {
		      });
		this.Vehiculo = P[0];
		
		var C = await Clientes.query()
		      .where("id", this.id_cliente)
			  .catch(err => {
		      });
		this.Cliente = C[0];
		var RC = await Reputacion.query()
		      .where({id_cliente:this.id_cliente, id_solicitud:this.id})
			  .catch(err => {
		      });
		if(RC.length>0){
				this.Cliente.ReviewRecibida = RC[0];			
		}


		if(this.id_proveedor){
			var Pr = await Proveedores.query()
		      .where("id", this.id_proveedor)
			  .catch(err => {
		      });
			this.Proveedor = Pr[0];
			var RP = await Reputacion.query()
		      .where({id_proveedor:this.id_proveedor, id_solicitud:this.id})
			  .catch(err => {
		      });
			if(RP.length>0){
					this.Proveedor.ReviewRecibida = RP[0];			
			}
		}
		

	}
	async ag() {
	    await this.solicitud_servicios();
	}
}

module.exports = Solicitud_servicios;