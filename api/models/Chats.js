"use strict";

const { Modelo } = require("./Modelo");

class Chats extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "chats";
    }

}

module.exports = Chats;
