"use strict";

const { Modelo } = require("./Modelo");

class Horario extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "horario_tarifa";
    }

}

module.exports = Horario;