//Importar librerias
const Knex = require("knex");
const morgan = require("morgan");
const express = require("express");
const promiseRouter = require("express-promise-router");
const https = require("https");
var bodyParser = require('body-parser');
var cron = require('node-cron');
const os = require("os");
const Server = os.hostname();
const knexConfig = require("../knexfile");
var fs = require("fs");
if (Server == knexConfig.Hosname) {
    var privateKey = fs.readFileSync('/etc/ssl/private/inthecompanies.key', 'utf8');
    var certificate = fs.readFileSync('/etc/ssl/inthecompanies_com.crt', 'utf8');
    var ca = fs.readFileSync('/etc/ssl/inthecompanies_com.ca-bundle', 'utf8');
    var credentials = { ca, key: privateKey, cert: certificate };
}


//Importar Config
const { Model } = require("objection");

//Importar Validador de errores
const ValidadorDeErrores = require("./ValidarErrores");
var knex;

if (Server == knexConfig.Hosname) {
    knex = Knex(knexConfig.production);
} else {
    knex = Knex(knexConfig.development);
}

Model.knex(knex);

const router = promiseRouter();

//Iniciar Express
const app = express()

.use(morgan('dev'))
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: true }))
    .use(router)
    .set("json spaces", 2);
app.all('*', function(req, res, next) {
    var responseSettings = {
        "AccessControlAllowOrigin": req.headers.origin,
        "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
        "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
        "AccessControlAllowCredentials": true
    };
    res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    res.header("Access-Control-Allow-Origin", responseSettings.AccessControlAllowOrigin);
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        next();
    }


});

//Definir las rutas en la carpeta "routes"
definirRutas([
    "login",
    "clientes",
    "proveedores",
    "admin",
    "notificaciones",
    "planes",
    "tipovehiculo",
    "tarifas",
    "chats",
    "solicitud_servicios",
    "pagos",
    "domicilios",
    "mensajes"
]);
app.use(ValidadorDeErrores);

//Definir rutas de archivos
app.get("/public/uploads/:filename", function(req, res) {
    console.log(req.params.filename);
    res.sendFile(__dirname + `/public/uploads/${req.params.filename}`);
});
app.get("/assets/:filename", function(req, res) {
    console.log(req.params.filename);
    res.sendFile(__dirname + `/assets/${req.params.filename}`);
});

//Iniciar el servidor en un puerto
if (Server == knexConfig.Hosname) {
    var httpsServer = https.createServer(credentials, app);
    httpsServer.listen(knexConfig.PORT);
} else {
    var server = app.listen(knexConfig.PORT, () => {
        console.log(
            "API Corriendo en puerto: %s",
            server.address().port
        );
    });
}

//--------------------------------------------INICIO SOCKET-------------------
if (Server == knexConfig.Hosname) {
    var io = require("socket.io")(httpsServer);
} else {
    var io = require("socket.io")(server);
}

const USUARIOSC = []; // Usuarios conectados en la app
global.USUARIOSC = USUARIOSC; // para usar el arreglo de usuarios conectados en cualquier parte del api...(donde sea)
global.IO = io; // para hacer emits desde cualquier parte del api (de donde sea)
global.NUEVASSOLICITUDES = [];
io.on("connection", function(socket) {
    /**
     * Le dice al client que está conectado.
     * De momento no se usa, pero probablemente la necesite en un futuro
     */
    socket.emit("CONNECT");

    socket.on("conectar", require("./socketEvents/conectar")(socket));

    socket.on("desconectar", require("./socketEvents/desconectar")(socket));

    socket.on("disconnect", require("./socketEvents/disconnect")(socket));

    socket.on("ENVIARMENSAJE", require("./socketEvents/enviarmensaje")(socket));
    socket.on("ACTUALIZAR-VISTO", require("./socketEvents/actualizarvisto")(socket));
    socket.on("ENCONTRAR-USUARIO", require("./socketEvents/encontrarusuario")(socket));
});

//-----------------------------------------------------------------FIN SOCKET

function definirRutas(rutas) {
    rutas.forEach(ruta => {
        app.use(`/${ruta}`, require(`./routes/${ruta}`));
    });
}
var RunninSol = require("./routes/RunninSol");
var Run = new RunninSol();

cron.schedule('* * * * *', async() => {
    await Run.SolicitudesVencidas();
});