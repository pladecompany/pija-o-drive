//llamar al modelo de Usuarios
const Clientes = require("../models/Clientes");
const Proveedores = require("../models/Proveedores");
const Tokencuentas = require("../models/Tokencuentas");
const Solicitud_servicios = require("../models/Solicitud_servicios");
const _ = require("lodash");
const Notificacion = require("../models/Notificaciones");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();

const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");



const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");
const validarMail = require("../functions/validateMail");
const mailer = require("../mails/Mailer");
const mail2 = require("../mails/confirmacion");
const mail3 = require("../mails/recuperarPass");
//Llamar al helper de errores
const { AuthError, ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer({
    limits: {
        fieldSize: 8 * 1024 * 1024,
    }
});
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");


var router = express.Router();
router.post(
    "/ubicacionActual", upload.none(), async(req, res) => {
        data = req.body;

        update = {
            lat_cli: data.lat,
            lon_cli: data.lon
        };
        //console.log(data);
        await Clientes.query()
            .updateAndFetchById(data.id_usuario, update)
            .then(async usuario => {
                const pr = await Clientes.query()
                    .findById(usuario.id)
                    .catch(err => {
                        console.log(err);
                    });
                pr.tablausuario = "clientes";
                pr.nom_usu = pr.nom_cli;
                const Soll = await Solicitud_servicios.query()
                    .where({ id_cliente: usuario.id, estatus_sol: 2 })
                    .catch(err => {
                        console.log(err);
                    });
                const Solld = await Solicitud_servicios.query()
                    .where({ id_cliente: usuario.id, estatus_sol: 7, puntuaciones: 1 })
                    .catch(err => {
                        console.log(err);
                    });
                if (Soll.length > 0) {
                    pr.Activas = Soll.length;
                }
                if (Solld.length > 0) {
                    pr.Sinvalorar = Solld.length;
                    pr.SinvalorarId = Solld[0].id;
                }
                AR.enviarDatos(pr, res);
            })
            .catch(err => {
                res.send(err);
            });
    }
);

router.post(
    "/Actualizar-sesion", upload.none(), async(req, res) => {
        data = req.body;

        const pr = await Clientes.query()
            .findById(data.id)
            .catch(err => {
                res.send(err);
            });
        pr.tablausuario = "clientes";
        pr.nom_usu = pr.nom_cli;
        return AR.enviarDatos(pr, res);
    }
);

router.get(
    "/verificar", upload.none(), async(req, res) => {
        data = req.query;

        if (!data.token) {
            return AR.enviarDatos({ msg: "Token no detectado." }, res, 200);
        }

        update = {
            est_cli: 1
        };
        updatee = {
            est_token: 1
        };
        await Tokencuentas.query()
            .where("token", data.token)
            .update(updatee)
            .then(async u => {

                var usuario = {};
                if (u == 1) {
                    const token = await Tokencuentas.query()
                        .where({ token: data.token })
                        .then(async resp => {

                            if (resp.length != 0) {
                                if (resp[0].token == data.token) {
                                    await Clientes.query()
                                        .updateAndFetchById(resp[0].id_usuario, update)
                                    await Tokencuentas.query()
                                        .delete()
                                        .where("token", data.token)
                                }
                            }
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    usuario.r = u;
                    usuario.msg = "Cuenta verificada exitosamente.";
                } else {
                    usuario.r = u;
                    usuario.msg = "Token vencido.";
                }

                AR.enviarDatos(usuario, res, 200);
            })
            .catch(err => {
                res.send(err);
            });
    });

router.post(
    "/Oauth",
    upload.none(),
    async(req, res, next) => {
        data = req.body;
        data.nom = data.name;
        data.img = data.picture.data.url;
        valido = true;
        let err = false;
        try {

            if (!data.cor) {
                err = "Debe ingresar un correo electrónico.";
            }

            if (!validarMail(data.cor)) {
                err = 'Por favor introduzca un correo válido';
            }
            if (!data.nom) {
                err = 'Por favor introduzca el nombre';
            }
            if (!data.img) {
                err = 'Seleccione una imagen';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            const correo = await Clientes.query()
                //.select('cor_cli')
                .where({ cor_cli: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_cli == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un cliente con este correo' }, res, 200);

                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            const correo2 = await Proveedores.query()
                //.select('cor_pro')
                .where({ cor_pro: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_pro == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un conductor con este correo' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            if (valido) {
                const cliente = await Clientes.query()
                    .insert({
                        nom_cli: data.nom,
                        img_cli: data.img,
                        cor_cli: data.cor,
                        //tel_cli: data.tel,
                        //cedula_cli: data.cedula,
                        //dir_cli: data.dir_usu,
                        //lat_cli: data.lat,
                        //lon_cli: data.lon,
                        est_cli: 0,
                        //pas_cli: bcrypt.hashSync(data.pas, 8),
                        fec_reg: new Date(),
                        token: uuid.v1()
                    })
                    .catch(err => {
                        console.log(err);
                    })
                let token = jwt.sign({ id: cliente.id }, config.secret, {
                    expiresIn: 86400
                });
                if (cliente) {

                    const Tokencuenta = await Tokencuentas.query()
                        .insert({
                            est_token: 0,
                            fec_reg: new Date(),
                            token: uuid.v1(),
                            id_usuario: cliente.id
                        })
                        .catch(err => {
                            console.log(err);
                        })

                    mailer.enviarMail({
                        direccion: data.cor,
                        titulo: "Pijaos drive | Confirmar Cuenta",
                        mensaje: mail2({
                            nombre: data.nom,
                            email: data.cor,
                            tipo: 'clientes',
                            id: cliente.id,
                            token: Tokencuenta.token
                        })
                    });
                }
                cliente.tablausuario = "clientes";
                cliente.nom_usu = cliente.nom_cli;
                AR.enviarAuth(res, 200, token, cliente);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post(
    "/",
    upload.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.pas) {
                err = 'Por favor introduzca una contraseña';
            }
            if (!data.cor) {
                err = "Debe ingresar un correo electrónico.";
            }
            if (!data.tel) {
                err = "Debe ingresar un teléfono WhatsApp.";
            }
            if (!data.cedula) {
                err = "Debe ingresar una cédula.";
            }
            if (!data.dir_usu) {
                err = "Debe ingresar una dirección.";
            }
            if (!validarMail(data.cor)) {
                err = 'Por favor introduzca un correo válido';
            }
            if (!data.nom) {
                err = 'Por favor introduzca el nombre';
            }
            if (data.img) {

                if (data.img == "") {
                    err = 'Seleccione una imagen';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_cli__" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img = config.rutaArchivo(file);
            } else {
                err = 'Seleccione una imagen';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            const correo = await Clientes.query()
                //.select('cor_cli')
                .where({ cor_cli: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_cli == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un cliente con este correo' }, res, 200);

                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            const correo2 = await Proveedores.query()
                //.select('cor_pro')
                .where({ cor_pro: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_pro == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un conductor con este correo' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

                const ced_cli = await Clientes.query()
                //.select('cor_pro')
                .where({ cedula_cli: data.cedula })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cedula_cli == data.cedula) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un usuario con esta cedula' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

                const ced_pro = await Proveedores.query()
                //.select('cor_pro')
                .where({ cedula_pro: data.cedula })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cedula_pro == data.cedula) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un usuario con esta cedula' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            if (valido) {
                const cliente = await Clientes.query()
                    .insert({
                        nom_cli: data.nom,
                        img_cli: data.img,
                        cor_cli: data.cor,
                        tel_cli: data.tel,
                        cedula_cli: data.cedula,
                        dir_cli: data.dir_usu,
                        lat_cli: data.lat,
                        lon_cli: data.lon,
                        est_cli: 0,
                        pas_cli: bcrypt.hashSync(data.pas, 8),
                        fec_reg: new Date(),
                        token: uuid.v1()
                    })
                    .catch(err => {
                        console.log(err);
                    })
                let token = jwt.sign({ id: cliente.id }, config.secret, {
                    expiresIn: 86400
                });
                if (cliente) {

                    const Tokencuenta = await Tokencuentas.query()
                        .insert({
                            est_token: 0,
                            fec_reg: new Date(),
                            token: uuid.v1(),
                            id_usuario: cliente.id
                        })
                        .catch(err => {
                            console.log(err);
                        })

                    mailer.enviarMail({
                        direccion: data.cor,
                        titulo: "Pijaos drive | Confirmar Cuenta",
                        mensaje: mail2({
                            nombre: data.nom,
                            email: data.cor,
                            tipo: 'clientes',
                            id: cliente.id,
                            token: Tokencuenta.token
                        })
                    });
                }
                cliente.tablausuario = "clientes";
                cliente.nom_usu = cliente.nom_cli;
                AR.enviarAuth(res, 200, token, cliente);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.post(
    "/editar/imagen",
    upload.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {

            if (!data.id) {
                err = "Debe ingresar un id cliente.";
            }
            if (data.img) {

                if (data.img == "") {
                    err = 'Seleccione una imagen';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_cli__" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {

                });
                data.img = config.rutaArchivo(file);
            } else {
                err = 'Seleccione una imagen';
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            const cliente = await Clientes.query()
                .updateAndFetchById(data.id, {
                    img_cli: data.img,
                    token: uuid.v1()
                })
                .catch(err => {
                    console.log(err);
                })
            let token = jwt.sign({ id: cliente.id }, config.secret, {
                expiresIn: 86400
            });
            cliente.tablausuario = "clientes";
            cliente.nom_usu = cliente.nom_cli;
            AR.enviarAuth(res, 200, token, cliente);

        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post(
    "/editar",
    upload.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {

            if (!data.id) {
                err = "Debe ingresar un id cliente.";
            }
            if (!data.tel) {
                err = "Debe ingresar un teléfono WhatsApp.";
            }
            if (!data.cedula) {
                err = "Debe ingresar una cédula.";
            }

            if (!data.nom) {
                err = 'Por favor introduzca el nombre';
            }

            if (!data.dir_usu) {
                err = "Debe ingresar una dirección.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }




            const cliente = await Clientes.query()
                .updateAndFetchById(data.id, {
                    nom_cli: data.nom,
                    tel_cli: data.tel,
                    cedula_cli: data.cedula,
                    dir_cli: data.dir_usu,
                    lat_cli: data.lat,
                    lon_cli: data.lon,
                    token: uuid.v1()
                })
                .catch(err => {
                    console.log(err);
                })
            let token = jwt.sign({ id: cliente.id }, config.secret, {
                expiresIn: 86400
            });
            cliente.tablausuario = "clientes";
            cliente.nom_usu = cliente.nom_cli;
            AR.enviarAuth(res, 200, token, cliente);

        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/", sesi.validar_admin, async(req, res) => {
    await Clientes.query().then(clientes => {
        AR.enviarDatos(clientes, res);
    });
});
router.post("/editar-status",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del cliente'; }

            if (!data.est_cli) { err = "Debe ingresar el estatus."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }

            var datos = {
                "est_cli": data.est_cli
            };
            if (valido) {
                const cliente = await Clientes.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async cliente => {
                        cliente.msg = "Estatus del Cliente actualizado con éxito";
                        cliente.r = true;

                        var user = _.find(global.USUARIOSC, o => {
                            if (o.user.id == cliente.id && o.user.tablausuario == "clientes")
                                return o.user;
                        });
                        var socketReceptor = user ? user.socket : null;

                        if (socketReceptor) {
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", cliente);
                        }
                        await Notificacion.query()
                            .insert({
                                id_usuario_receptor: cliente.id,
                                tit_noti: "Cambios en tu Cuenta",
                                des_noti: "Tu cuenta ha sido cambiada a <b>" + data.stsT + "</b>, por los administradores de Pijaos drive. <br> Nota: <b>" + data.comentario + "</b>",
                                tipo_noti: "estatus-cli-push",
                                fec_reg_noti: new Date(),
                                est_noti: 0,
                                id_usable: cliente.id,
                                tablausuario: 'clientes'
                            })
                            .then(async noti => {
                                var datap = {
                                    id_usuario: noti.id_usuario_receptor,
                                    title: noti.tit_noti,
                                    body: noti.des_noti,
                                    id_usable: noti.id_usable,
                                    tipo_noti: noti.tipo_noti,
                                    tablausuario: noti.tablausuario
                                };
                                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                            });

                        AR.enviarDatos(cliente, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el estatus", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post("/enviar-push",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        data.tablausuario = 'clientes';
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del cliente'; }

            if (!data.msj) { err = "Debe ingresar el mensaje."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }


            if (valido) {
                //console.log(data.msj);
                var datap = {
                    id_usuario: data.id,
                    title: "Mensaje del Administrador",
                    body: data.msj,
                    tablausuario: "clientes",
                    tipo_noti: "push-tipo-mensaje"
                };

                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                IO.sockets.emit("Recibiralerta", data);
                AR.enviarDatos({ r: true, msg: "Mensaje enviado con éxito" }, res, 200);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Clientes.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
router.post(
    "/recuperar/clave",
    upload.none(),
    async(req, res, next) => {
        data = req.body;
        console.log(data);
        valido = false;
        let err = false;
        try {

            if (!data.cor) {
                err = "Debe ingresar un correo electrónico.";
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            const correo = await Clientes.query()
                //.select('cor_cli')
                .where({ cor_cli: data.cor })
                .skipUndefined()
                .then(async resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_cli == data.cor) {
                            valido = true;
                            var nuevaPass = "";
                            var caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                            for (var i = 0; i < 8; i++)
                                nuevaPass += caracteres.charAt(Math.floor(Math.random() * caracteres.length));

                            await Clientes.query()
                                .patchAndFetchById(resp[0].id, { pas_cli: bcrypt.hashSync(nuevaPass, 8) })
                                .catch(err => console.log(err));

                            mailer.enviarMail({
                                direccion: data.cor,
                                titulo: "Pijaos drive | Recuperar Cuenta",
                                mensaje: mail3({
                                    nombre: resp[0].nom_cli,
                                    email: data.cor,
                                    contrasena: nuevaPass
                                })
                            });
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            const correo2 = await Proveedores.query()
                //.select('cor_pro')
                .where({ cor_pro: data.cor })
                .skipUndefined()
                .then(async resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_pro == data.cor) {
                            valido = true;
                            var nuevaPass = "";
                            var caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                            for (var i = 0; i < 8; i++)
                                nuevaPass += caracteres.charAt(Math.floor(Math.random() * caracteres.length));

                            await Proveedores.query()
                                .patchAndFetchById(resp[0].id, { pas_pro: bcrypt.hashSync(nuevaPass, 8) })
                                .catch(err => console.log(err));

                            mailer.enviarMail({
                                direccion: data.cor,
                                titulo: "Pijaos drive | Recuperar Cuenta",
                                mensaje: mail3({
                                    nombre: resp[0].nom_pro,
                                    email: data.cor,
                                    contrasena: nuevaPass
                                })
                            });
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            if (valido) {
                return AR.enviarDatos({ r: true, msg: 'Verifique su cuenta de correo.' }, res, 200);
            } else {
                return AR.enviarDatos({ r: false, msg: 'No existe un usuario con este correo.' }, res, 200);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
module.exports = router;