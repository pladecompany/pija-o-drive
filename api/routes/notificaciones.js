//llamar al modelo de sudespecialidades
const Notificacion = require("../models/Notificaciones");
const Firebase = require("../models/Firebase");
var FirebaseB = require("../routes/firebase");
FirebaseB = new FirebaseB();
const AR = require("../ApiResponser");
const express = require("express");

var archivos = require("../Archivos");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var router = express.Router();


//nuevo usuario firebase
router.post('/agregar-eliminar-firebase', upload.none(), async(req, res) => {
    var obj = {
        'id_usuario': req.body.id_usuario,
        'token': req.body.tokenFirebase,
        'tablausuario': req.body.tablausuario
    };
    const eliminado = await Firebase.query().delete()
        .where({ token: obj.token })
        .catch(err => {
            console.log(err);
            res.send(err);
        });
    const insertado = await Firebase.query()
        .insert(obj)
        .then(fire => {
            AR.enviarDatos("0", res);
        })
        .catch(async err => {

            await Firebase.query()
                .patchAndFetchById(
                    obj.token, {
                        id_usuario: obj.id_usuario
                    })
                .then(firebase => {
                    firebase.msj = "Se editó correctamente";
                    AR.enviarDatos(firebase, res);
                })
                .catch(err => {
                    res.send(err);
                });
        });



});

router.get("/",
    archivos.none(),
    async(req, res) => {
        data = req.query;
        if (data.limit != null && data.limit != "")
            var limit = data.limit;
        else
            var limit = "3";

        var string = " DESC LIMIT " + limit + " ";

        if (data.limit == "todas")
            string = "";

        const Sol = await Notificacion.query()
            .where({ id_usuario_receptor: data.id, tablausuario: data.tipo, est_noti: 0 })
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });
router.get("/admin",
    archivos.none(),
    async(req, res) => {
        data = req.query;
        if (data.limit != null && data.limit != "")
            var limit = data.limit;
        else
            var limit = "5";

        var string = " DESC LIMIT " + limit + " ";

        if (data.limit == "todas")
            string = "";

        const Sol = await Notificacion.query()
            .where({ id_usuario_receptor: null, tablausuario: null, est_noti: 0 })
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });
router.get("/vista/:id", async(req, res) => {
    await Notificacion.query()
        .patchAndFetchById(
            req.params.id, {
                est_noti: 1
            })
        .then(notificacion => {
            AR.enviarDatos(notificacion, res);
        })
        .catch(err => {
            res.send(err);
        });
});

router.get("/del/:id", async(req, res) => {
    await Notificacion.query()
        .delete()
        .where({ id: req.params.id })
        .then(notificacion => {
            return AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        })
        .catch(err => {
            res.send(err);
        });
});


module.exports = router;