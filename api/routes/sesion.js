const config = require("../config");
const Admin = require("../models/Admin");
const Clientes = require("../models/Clientes"); 
const Proveedores = require("../models/Proveedores"); 

var Sesion = function(){

    this.validar_cli = async function(req, res, next){

        var tok = req.body.token || req.query.token || req.params.token;

         
        await Clientes.raw(
            "SELECT * FROM clientes WHERE  token='"+tok+"';"
        ).then(usuario => {
            if(usuario[0].length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {

            return res.sendStatus(401);
        });
        

    },
    this.validar_admin = async function(req, res, next){

        
        var tok = req.body.token || req.query.token || req.params.token;
        await Admin.raw(
            "SELECT * FROM admins WHERE  token='"+tok+"';"
        ).then(admins => {
               if(admins[0].length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {
            return res.sendStatus(401);
        });
        

    },
    this.validar_pro = async function(req, res, next){

        var tok = req.body.token   || req.query.token || req.params.token;

        
        await Proveedores.raw(
            "SELECT * FROM proveedores WHERE  token='"+tok+"';"
        ).then(admins => {
               if(admins[0].length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {
            return res.sendStatus(401);
        });
        

    }
}

module.exports = Sesion;
