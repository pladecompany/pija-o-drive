//llamar al modelo de Usuarios
const Solicitud_servicios = require("../models/Solicitud_servicios");
const Clientes = require("../models/Clientes");
const proveedores = require("../models/Proveedores");
const Reputacion = require("../models/Reputacion");
const DetallesP = require("../models/DetallesP");
const Notificacion = require("../models/Notificaciones");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");
const validarMail = require("../functions/validateMail");
const mailer = require("../mails/Mailer");
const mail2 = require("../mails/confirmacion");
//Llamar al helper de errores
const { AuthError, ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");
const _ = require("lodash");
var router = express.Router();

function BuscarSoli(id) {
    for (var i = 0; i < NUEVASSOLICITUDES.length; i++) {
        if (NUEVASSOLICITUDES[i].id == id) return true;
    }
    return false;
}
var KMCONFIG = 25;
getKilometros = function(lat1, lon1, lat2, lon2) {
    rad = function(x) { return x * Math.PI / 180; }
    var R = 6378.137; //Radio de la tierra en km
    var dLat = rad(lat2 - lat1);
    var dLong = rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d.toFixed(3); //Retorna tres decimales
}
router.post("/agregar",
    archivos.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.calculo) {
                err = "Imposible calcular el costo";
            }
            if (!data.id) {
                err = "Seleccione tipo de vehiculo";
            }
            if (!data.cliente.id) {
                err = "Ingrese un id cliente";
            }
            /*if (!data.carga) {
                err = "Seleccione que va a transportar";
            }
            if (!data.cantidad) {
                err = "Ingrese la cantidad";
            }*/
            if (!data.desdeText || !data.hastaText) {
                err = "Configure una ruta con origen y destino";
            }
            if (!data.desdeText) {
                err = "Seleccione la dirección de origen";
            }
            if (!data.hastaText) {
                err = "Seleccione la dirección de destino";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                var Soli_s = await Solicitud_servicios.query()
                    .insert({
                        id_cliente: data.cliente.id,
                        id_tipo_veh: data.id,
                        desdeText: data.desdeText,
                        hastaText: data.hastaText,
                        lat1: data.lat1,
                        lon1: data.lon1,
                        lat2: data.lat2,
                        lon2: data.lon2,
                        distancia_sol: JSON.stringify(data.distancia),
                        duracion_sol: JSON.stringify(data.duracion),
                        costo_sol: data.calculo,
                        carga_sol: "", //data.carga,
                        cantidad_sol: 0, //data.cantidad,
                        estatus_sol: 0,
                        fecha_sol: new Date()
                    })
                    .catch(err => {
                        console.log(err);
                    })
                const Soll = await Solicitud_servicios.query()
                    .where("id", Soli_s.id)
                    .catch(err => {
                        console.log(err);
                    });
                if (Soll.length > 0) {
                    Soli_s = Soll[Soll.length - 1];
                }
                if (!BuscarSoli(Soli_s.id))
                    NUEVASSOLICITUDES.push(Soli_s);

                Soli_s.r = true;
                Soli_s.msg = "Se registro correctamente!";

                var Proveedores = await proveedores.query()
                    .where({ estatus_verificado: 1, est_pro: 1 })
                    .catch(err => {
                        console.log(err);
                    });

                if (Proveedores) {


                    for (var i = 0; i < Proveedores.length; i++) {

                        if (Proveedores[i].Detalles.id_tipo_vehiculo == data.id) {
                            if (parseInt(getKilometros(Proveedores[i].lat_pro, Proveedores[i].lon_pro, Soli_s.lat1, Soli_s.lon1)) <= KMCONFIG) {

                                var user = _.find(global.USUARIOSC, o => {
                                    if (o.user.id == Proveedores[i].id && o.user.tablausuario == "proveedores")
                                        return o.user;
                                });

                                var socketReceptor = user ? user.socket : null;

                                if (socketReceptor) {
                                    global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-SOLICITUD", Soli_s);
                                }
                                await Notificacion.query()
                                    .insert({
                                        id_usuario_receptor: Proveedores[i].id,
                                        tit_noti: "Nueva solicitud de servicio, apresurate a tomarla",
                                        des_noti: "<b>CLIENTE: </b>" + data.cliente.nom_cli + " <br> <b>Solicitud de servicio <b> </b>DESDE: </b> " + Soli_s.desdeText + ", <b>HASTA: </b> " + Soli_s.hastaText,
                                        tipo_noti: "solicitud-nuevo-push",
                                        fec_reg_noti: new Date(),
                                        est_noti: 0,
                                        id_usable: Soli_s.id,
                                        tablausuario: 'proveedores'
                                    })
                                    .then(async noti => {

                                        var datap = {
                                            id_usuario: noti.id_usuario_receptor,
                                            title: noti.tit_noti,
                                            body: noti.des_noti,
                                            id_usable: noti.id_usable,
                                            tipo_noti: noti.tipo_noti,
                                            tablausuario: noti.tablausuario
                                        };

                                        Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                                    });
                            }
                        }
                    }
                }
                AR.enviarDatos(Soli_s, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/proveedor",
    archivos.none(),
    sesi.validar_pro, async(req, res) => {
        data = req.query;
        if (data.limit != null && data.limit != "")
            var limit = data.limit;
        else
            var limit = "3";

        var string = " DESC LIMIT " + limit + " ";

        if (data.limit == "todas")
            string = "";

        const Sol = await Solicitud_servicios.query()
            .where({ id_tipo_veh: data.id_tipo_vehiculo, id_proveedor: null, estatus_sol: 0 })
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });

        AR.enviarDatos(Sol, res);
    });

router.get("/propiasP",
    archivos.none(),
    sesi.validar_pro, async(req, res) => {
        data = req.query;
        if (data.limit != null && data.limit != "")
            var limit = data.limit;
        else
            var limit = "3";

        var string = " DESC LIMIT " + limit + " ";

        if (data.limit == "todas")
            string = "";
        var busqueda = {};
        if (data.solicitud) {
            busqueda = { id_proveedor: data.id, id: data.solicitud }
        } else {
            busqueda = { id_proveedor: data.id }
        }

        const Sol = await Solicitud_servicios.query()
            .where(busqueda)
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });
router.get("/propiasC",
    archivos.none(),
    sesi.validar_cli, async(req, res) => {
        data = req.query;
        if (data.limit != null && data.limit != "")
            var limit = data.limit;
        else
            var limit = "3";

        var string = " DESC LIMIT " + limit + " ";

        if (data.limit == "todas")
            string = "";

        var busqueda = {};
        if (data.solicitud) {
            busqueda = { id_cliente: data.id, id: data.solicitud }
        } else {
            busqueda = { id_cliente: data.id }
        }
        const Sol = await Solicitud_servicios.query()
            .where(busqueda)
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });

router.post("/tomar-solicitud",
    archivos.none(),
    sesi.validar_pro, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!BuscarSoli(data.id)) {
                err = "Esta solicitud ya está vencida.";
            }
            if (!data.id) {
                err = "Debe ingresar el id solicitud.";
            }
            if (!data.id_proveedor) {
                err = "Debe ingresar el id Conductor.";
            }
            const Sol = await Solicitud_servicios.query()
                .where("id", data.id)
                .catch(err => {
                    console.log(err);
                });
            if (Sol.length > 0) {

                var Solicitud = Sol[Sol.length - 1];
                if (Solicitud) {
                    console.log(Solicitud.id_proveedor, data.id_proveedor);
                    if (Solicitud.id_proveedor == data.id_proveedor) {
                        err = "Felicidades, ya la solicitud te pertenece, puedes ponerte en contacto con el cliente.";
                    } else {
                        if (Solicitud.estatus_sol == 5) {
                            return AR.enviarDatos({ r: false, msg: "Ésta solicitud fue cancelada por el cliente." }, res, 200);
                        }
                        if (Solicitud.id_proveedor && Solicitud.id_proveedor != data.id_proveedor) {
                            err = "Lo sentimos, la solicitud pertenece a otro conductor.";
                        }
                    }
                }
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const datos = {
                    id_proveedor: data.id_proveedor,
                    estatus_sol: 2, //1, antes era asi, ahora pasa a 2 simulando que el cliente acepta de una vez
                }
                await Solicitud_servicios.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async s => {
                        const Soll = await Solicitud_servicios.query()
                            .where("id", s.id)
                            .catch(err => {
                                console.log(err);
                            });

                        s = Soll[Soll.length - 1];

                        var user = _.find(global.USUARIOSC, o => {
                            if (o.user.id == s.id_cliente && o.user.tablausuario == "clientes")
                                return o.user;
                        });

                        var socketReceptor = user ? user.socket : null;

                        if (socketReceptor) {
                            s.tomada = true;
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", s);
                        }
                        await Notificacion.query()
                            .insert({
                                id_usuario_receptor: s.id_cliente,
                                tit_noti: "Cambios en su Solicitud / <b>PENDIENTE POR APROBAR</b>",
                                des_noti: "<b>Conductor:</b> " + data.chofer.nom_pro + " <br> <b>SOLICITUD DE SERVICIO, </b> <b>DESDE: </b> " + s.desdeText + ", <b>HASTA: </b> " + s.hastaText,
                                tipo_noti: "solicitud-cambios-push",
                                fec_reg_noti: new Date(),
                                est_noti: 0,
                                id_usable: s.id,
                                tablausuario: 'clientes'
                            })
                            .then(async noti => {

                                var datap = {
                                    id_usuario: noti.id_usuario_receptor,
                                    title: noti.tit_noti,
                                    body: noti.des_noti,
                                    id_usable: noti.id_usable,
                                    tipo_noti: noti.tipo_noti,
                                    tablausuario: noti.tablausuario
                                };

                                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                            });


                        s.msg = "Felicidades, ya la solicitud te pertenece, puedes ponerte en contacto con el cliente.";
                        s.r = true;
                         
                        return AR.enviarDatos(s, res, 200);
                    })
                    .catch(err => {
                        console.log(err);
                        res.send(err);
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.post("/aceptar-chofer-solicitud",
    archivos.none(),
    sesi.validar_cli, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar el id solicitud.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const datos = {
                    estatus_sol: 2
                }
                await Solicitud_servicios.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async s => {
                        var user = _.find(global.USUARIOSC, o => {
                            if (o.user.id == s.id_proveedor && o.user.tablausuario == "proveedores")
                                return o.user;
                        });

                        var socketReceptor = user ? user.socket : null;

                        if (socketReceptor) {
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", s);
                        }
                        await Notificacion.query()
                            .insert({
                                id_usuario_receptor: s.id_proveedor,
                                tit_noti: "Cambios en su Solicitud / <b>EN PROCESO</b>",
                                des_noti: "<b>CLIENTE</b>: " + s.Cliente.nom_cli + " <br> <b>Solicitud de servicio</b> <b>DESDE: </b> " + s.desdeText + ", <b>HASTA: </b> " + s.hastaText + "<br> fuíste aceptado para atender está solicitud.",
                                tipo_noti: "solicitud-cambios-push",
                                fec_reg_noti: new Date(),
                                est_noti: 0,
                                id_usable: s.id,
                                tablausuario: 'proveedores'
                            })
                            .then(async noti => {

                                var datap = {
                                    id_usuario: noti.id_usuario_receptor,
                                    title: noti.tit_noti,
                                    body: noti.des_noti,
                                    id_usable: noti.id_usable,
                                    tipo_noti: noti.tipo_noti,
                                    tablausuario: noti.tablausuario
                                };

                                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                            });


                        s.msg = "Cambios realizados.";
                        AR.enviarDatos(s, res);
                    })
                    .catch(err => {
                        console.log(err);
                        res.send(err);
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post("/mensaje-preestablecido",
    archivos.none(),
    sesi.validar_cli, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar el id solicitud.";
            }
            if (!data.id_proveedor) {
                err = "Debe ingresar el id proveedor.";
            }
            if (!data.tit) {
                err = "Debe ingresar el titulo.";
            }
            if (!data.des) {
                err = "Debe ingresar descripcion.";
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }



            var user = _.find(global.USUARIOSC, o => {
                if (o.user.id == data.id_proveedor && o.user.tablausuario == "proveedores")
                    return o.user;
            });

            var socketReceptor = user ? user.socket : null;

            if (socketReceptor) {
                global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", data);
            }
            await Notificacion.query()
                .insert({
                    id_usuario_receptor: data.id_proveedor,
                    tit_noti: "<b>Nuevo mensaje</b>",
                    des_noti: `El cliente <b>${data.usuario.nom_cli}</b> te envió el siguiente mensaje: <br> <b>${data.tit}</b> <br> <b>${data.des}</b>`,
                    tipo_noti: "solicitud-cambios-push",
                    fec_reg_noti: new Date(),
                    est_noti: 0,
                    id_usable: data.id,
                    tablausuario: 'proveedores'
                })
                .then(async noti => {

                    var datap = {
                        id_usuario: noti.id_usuario_receptor,
                        title: noti.tit_noti,
                        body: noti.des_noti,
                        id_usable: noti.id_usable,
                        tipo_noti: noti.tipo_noti,
                        tablausuario: noti.tablausuario
                    };

                    Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                    noti.msg = "Mensaje enviado.";
                    noti.r = true;
                    AR.enviarDatos(noti, res);

                });



        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.post("/rechazar-chofer-solicitud",
    archivos.none(),
    sesi.validar_cli, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar el id solicitud.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const datos = {
                    estatus_sol: 0,
                    puntuaciones: null,
                    id_proveedor: null,
                    fecha_sol: new Date()
                }
                await Solicitud_servicios.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async Soli_s => {


                        const Soll = await Solicitud_servicios.query()
                            .where("id", Soli_s.id)
                            .catch(err => {
                                console.log(err);
                            });

                        if (Soll.length > 0) {
                            Soli_s = Soll[Soll.length - 1];
                        }
                        if (!BuscarSoli(Soli_s.id))
                            NUEVASSOLICITUDES.push(Soli_s);


                        Soli_s.r = true;
                        Soli_s.msg = "Se registro correctamente!";

                        var Proveedores = await proveedores.query()
                            .where({ estatus_verificado: 1, est_pro: 1 })
                            .catch(err => {
                                console.log(err);
                            });

                        if (Proveedores) {


                            for (var i = 0; i < Proveedores.length; i++) {

                                if (Proveedores[i].Detalles.id_tipo_vehiculo == data.id) {
                                    if (parseInt(getKilometros(Proveedores[i].lat_pro, Proveedores[i].lon_pro, Soli_s.lat1, Soli_s.lon1)) <= KMCONFIG) {

                                        var user = _.find(global.USUARIOSC, o => {
                                            if (o.user.id == Proveedores[i].id && o.user.tablausuario == "proveedores")
                                                return o.user;
                                        });

                                        var socketReceptor = user ? user.socket : null;

                                        if (socketReceptor) {
                                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-SOLICITUD", Soli_s);
                                        }
                                        await Notificacion.query()
                                            .insert({
                                                id_usuario_receptor: Proveedores[i].id,
                                                tit_noti: "Nueva solicitud de servicio, apresurate a tomarla",
                                                des_noti: "<b>CLIENTE: </b>" + data.cliente.nom_cli + " <br> <b>Solicitud de servicio</b> <b>DESDE: </b> " + Soli_s.desdeText + ", <b>HASTA: </b> " + Soli_s.hastaText,
                                                tipo_noti: "solicitud-nuevo-push",
                                                fec_reg_noti: new Date(),
                                                est_noti: 0,
                                                id_usable: Soli_s.id,
                                                tablausuario: 'proveedores'
                                            })
                                            .then(async noti => {

                                                var datap = {
                                                    id_usuario: noti.id_usuario_receptor,
                                                    title: noti.tit_noti,
                                                    body: noti.des_noti,
                                                    id_usable: noti.id_usable,
                                                    tipo_noti: noti.tipo_noti,
                                                    tablausuario: noti.tablausuario
                                                };

                                                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                                            });
                                    }
                                }
                            }
                        }
                        AR.enviarDatos(Soli_s, res);
                    })
                    .catch(err => {
                        console.log(err);
                        res.send(err);
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.post("/puntuar-chofer-cliente", archivos.none(), async(req, res, next) => {
    data = req.body;

    valido = true;
    let err = false;
    try {
        if (!data.id) {
            err = "Debe ingresar el id solicitud.";
        }
        if (!data.id_proveedor && !data.id_cliente) {
            err = "Debe ingresar el id del proveedor o id del cliente.";
        }

        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        const inn = await Reputacion.query()
            .insert({
                id_cliente: data.tipo == "clientes" ? null : data.id_cliente,
                id_proveedor: data.tipo == "clientes" ? data.id_proveedor : null,
                voto1: data.voto1,
                voto2: data.voto2,
                voto3: data.voto3,
                comentario_rep: data.comentario_rep,
                fecha_rep: new Date(),
                id_solicitud: data.id
            })
            .then(async inn => {



                if (valido) {
                    const datos = {
                        puntuaciones: data.tipo == "clientes" ? 0 : 1
                    }
                    if (data.tipo == "clientes") {
                        datos.estatus_sol = 3;
                    }
                    await Solicitud_servicios.query()
                        .updateAndFetchById(data.id, datos)
                        .then(async s => {

                            var user = _.find(global.USUARIOSC, o => {
                                if (data.tipo == "clientes") {
                                    if (o.user.id == s.id_proveedor && o.user.tablausuario == "proveedores")
                                        return o.user;
                                } else {
                                    if (o.user.id == s.id_cliente && o.user.tablausuario == "clientes")
                                        return o.user;

                                }

                            });

                            var socketReceptor = user ? user.socket : null;

                            if (socketReceptor) {
                                global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", s);
                            }
                            await Notificacion.query()
                                .insert({
                                    id_usuario_receptor: data.tipo == "clientes" ? s.id_proveedor : s.id_cliente,
                                    tit_noti: "Nueva puntuación recibida",
                                    des_noti: "<b>CLIENTE: </b>" + data.cliente.nom_cli + " <br> <b>Solicitud de servicio</b> <b>DESDE: </b> " + Soli_s.desdeText + ", <b>HASTA: </b> " + Soli_s.hastaText,
                                    tipo_noti: "solicitud-cambios-push",
                                    fec_reg_noti: new Date(),
                                    est_noti: 0,
                                    id_usable: s.id,
                                    tablausuario: data.tipo == "clientes" ? "proveedores" : "clientes"
                                })
                                .then(async noti => {

                                    var datap = {
                                        id_usuario: noti.id_usuario_receptor,
                                        title: noti.tit_noti,
                                        body: noti.des_noti,
                                        id_usable: noti.id_usable,
                                        tipo_noti: noti.tipo_noti,
                                        tablausuario: noti.tablausuario
                                    };

                                    Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                                });


                            s.msg = "Cambios realizados";
                            AR.enviarDatos(s, res);
                        })
                        .catch(err => {
                            console.log(err);
                            res.send(err);
                        });
                }
            })
            .catch(err => {
                res.send(err);
            })
    } catch (err) {
        console.log(err);
        next(err);
    }
});
router.post("/cancelar-solicitud",
    archivos.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar el id solicitud.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const datos = {
                    estatus_sol: 5
                }
                await Solicitud_servicios.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async Soli_s => {


                        const Soll = await Solicitud_servicios.query()
                            .where("id", Soli_s.id)
                            .catch(err => {
                                console.log(err);
                            });

                        if (Soll.length > 0) {
                            Soli_s = Soll[Soll.length - 1];
                        }
                        if (BuscarSoli(Soli_s.id)) {
                            for (var i = 0; i < NUEVASSOLICITUDES.length; i++) {
                                if (NUEVASSOLICITUDES[i].id == Soli_s.id) {
                                    delete NUEVASSOLICITUDES[i];
                                    NUEVASSOLICITUDES.splice(i, 1);
                                }
                            }

                        }

                        Soli_s.r = true;
                        Soli_s.msg = "Solicitud cancelada...";
                        var user = _.find(global.USUARIOSC, o => {

                            if (o.user.id == Soli_s.id_cliente && o.user.tablausuario == "clientes")
                                return o.user;


                        });

                        var socketReceptor = user ? user.socket : null;

                        if (socketReceptor) {
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", Soli_s);
                        }
                        var user = _.find(global.USUARIOSC, o => {

                            if (o.user.id == Soli_s.id_proveedor && o.user.tablausuario == "proveedores")
                                return o.user;


                        });

                        var socketReceptor = user ? user.socket : null;

                        if (socketReceptor) {
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", Soli_s);
                        }
                        await Notificacion.query()
                            .insert({
                                id_usuario_receptor: Soli_s.id_cliente,
                                tit_noti: "Solicitud Cancelada",
                                des_noti: "<b>Solicitud de servicio</b> <b>DESDE: </b> " + Soli_s.desdeText + ", <b>HASTA: </b> " + Soli_s.hastaText,
                                tipo_noti: "solicitud-cambios-push",
                                fec_reg_noti: new Date(),
                                est_noti: 0,
                                id_usable: Soli_s.id,
                                tablausuario: "clientes"
                            })
                            .then(async noti => {

                                var datap = {
                                    id_usuario: noti.id_usuario_receptor,
                                    title: noti.tit_noti,
                                    body: noti.des_noti,
                                    id_usable: noti.id_usable,
                                    tipo_noti: noti.tipo_noti,
                                    tablausuario: noti.tablausuario
                                };

                                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                            });
                        if (Soli_s.id_proveedor) {
                            await Notificacion.query()
                                .insert({
                                    id_usuario_receptor: Soli_s.id_proveedor,
                                    tit_noti: "Solicitud Cancelada",
                                    des_noti: "<b>Solicitud de servicio</b> <b>DESDE: </b> " + Soli_s.desdeText + ", <b>HASTA: </b> " + Soli_s.hastaText,
                                    tipo_noti: "solicitud-cambios-push",
                                    fec_reg_noti: new Date(),
                                    est_noti: 0,
                                    id_usable: Soli_s.id,
                                    tablausuario: "proveedores"
                                })
                                .then(async noti => {

                                    var datap = {
                                        id_usuario: noti.id_usuario_receptor,
                                        title: noti.tit_noti,
                                        body: noti.des_noti,
                                        id_usable: noti.id_usable,
                                        tipo_noti: noti.tipo_noti,
                                        tablausuario: noti.tablausuario
                                    };

                                    Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                                });
                        }

                        AR.enviarDatos(Soli_s, res);
                    })
                    .catch(err => {
                        console.log(err);
                        res.send(err);
                    });
            }

        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post("/llegue",
    archivos.none(),
    sesi.validar_pro, async(req, res, next) => {
        data = req.body;

        await Solicitud_servicios.query()
            .updateAndFetchById(data.id, { estatus_sol: 6 })
            .catch(err => {
                return res.send(err);
            });

        var user = _.find(global.USUARIOSC, o => {
            if (o.user.id == data.Cliente.id && o.user.tablausuario == "clientes")
                return o.user;
        });

        var socketReceptor = user ? user.socket : null;

        if (socketReceptor) {
            data.llegada = true;
            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", data);
        }
        await Notificacion.query()
            .insert({
                id_usuario_receptor: data.Cliente.id,
                tit_noti: "Ya su conductor llegó, por favor salga.",
                des_noti: "<b>Conductor:</b> " + data.Proveedor.nom_pro,
                tipo_noti: "solicitud-cambios-push",
                fec_reg_noti: new Date(),
                est_noti: 0,
                id_usable: data.id,
                tablausuario: 'clientes'
            })
            .then(async noti => {

                var datap = {
                    id_usuario: noti.id_usuario_receptor,
                    title: noti.tit_noti,
                    body: noti.des_noti,
                    id_usable: noti.id_usable,
                    tipo_noti: noti.tipo_noti,
                    tablausuario: noti.tablausuario
                };

                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

            });
        AR.enviarDatos(data, res);
    });
router.post("/finviaje",
    archivos.none(),
    sesi.validar_pro, async(req, res, next) => {
        data = req.body;

        await Solicitud_servicios.query()
            .updateAndFetchById(data.id, { estatus_sol: 7 })
            .catch(err => {
                return res.send(err);
            });

        var user = _.find(global.USUARIOSC, o => {
            if (o.user.id == data.Cliente.id && o.user.tablausuario == "clientes")
                return o.user;
        });

        var socketReceptor = user ? user.socket : null;

        if (socketReceptor) {
            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", data);
        }
        await Notificacion.query()
            .insert({
                id_usuario_receptor: data.Cliente.id,
                tit_noti: "Viaje finalizado.",
                des_noti: "Cuando reciba su valoración podrá calificar al <b>Conductor:</b> " + data.Proveedor.nom_pro,
                tipo_noti: "solicitud-cambios-push",
                fec_reg_noti: new Date(),
                est_noti: 0,
                id_usable: data.id,
                tablausuario: 'clientes'
            })
            .then(async noti => {

                var datap = {
                    id_usuario: noti.id_usuario_receptor,
                    title: noti.tit_noti,
                    body: noti.des_noti,
                    id_usable: noti.id_usable,
                    tipo_noti: noti.tipo_noti,
                    tablausuario: noti.tablausuario
                };

                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

            });
        AR.enviarDatos(data, res);
    });
router.get("/propiasP/admin",
    archivos.none(),
    async(req, res) => {
        data = req.query;

        var string = " ASC LIMIT 50 ";
        var date = new Date(data.ano + '-' + data.mes + '-01');
        var mes = data.mes;
        if (mes < 10)
            mes = "0" + mes;
        var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
        var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        primerDia = primerDia.getDate();
        ultimoDia = ultimoDia.getDate();
        primerDia = data.ano + "-" + mes + "-" + primerDia + " 00:00:00";
        ultimoDia = data.ano + "-" + mes + "-" + ultimoDia + " 00:00:00";
        const Sol = await Solicitud_servicios.query()
            .where({ id_proveedor: data.id })
            .whereBetween("fecha_sol", [primerDia, ultimoDia])
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });
router.get("/propiasC/admin",
    archivos.none(),
    async(req, res) => {
        data = req.query;

        var string = " ASC LIMIT 50 ";
        var date = new Date(data.ano + '-' + data.mes + '-01');
        var mes = data.mes;
        if (mes < 10)
            mes = "0" + mes;
        var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
        var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        primerDia = primerDia.getDate();
        ultimoDia = ultimoDia.getDate();
        primerDia = data.ano + "-" + mes + "-" + primerDia + " 00:00:00";
        ultimoDia = data.ano + "-" + mes + "-" + ultimoDia + " 00:00:00";
        //console.log(primerDia+"--"+ultimoDia);
        const Sol = await Solicitud_servicios.query()
            .where({ id_cliente: data.id })
            .where({ estatus_sol: 3 })
            .whereBetween("fecha_sol", [primerDia, ultimoDia])
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });
module.exports = router;