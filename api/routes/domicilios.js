//llamar al modelo de sudespecialidades
const Domis = require("../models/Domicilios");

const AR = require("../ApiResponser");
const express = require("express");

var archivos = require("../Archivos");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var router = express.Router();


router.post("/add",
    archivos.none(),
    sesi.validar_cli, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar el cliente.";
            }
            if (!data.lat) {
                err = "Debe ingresar latitud.";
            }
            if (!data.lon) {
                err = "Debe ingresar longitud.";
            }
            if (!data.nom_dom) {
                err = "Debe ingresar Descripción: alias.";
            }
            if (!data.dir_dom) {
                err = "Debe ingresar dirección.";
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {

                const domis = await Domis.query()
                    .insert({
                        id_cliente: data.id,
                        nom_dom: data.nom_dom,
                        dir_dom: data.dir_dom,
                        lat_dom: data.lat,
                        lon_dom: data.lon,
                    })
                    .catch(err => {
                        console.log(err);
                    })



                domis.r = true;
                domis.msg = "Se registro correctamente!";
                AR.enviarDatos(domis, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/",
    archivos.none(),
    async(req, res) => {
        data = req.query;
        if (data.limit != null && data.limit != "")
            var limit = data.limit;
        else
            var limit = "3";

        var string = " DESC LIMIT " + limit + " ";

        if (data.limit == "todas")
            string = "";

        const Sol = await Domis.query()
            .where({ id_cliente: data.id })
            .orderByRaw(' id ' + string)
            .catch(err => {
                res.send(err);
            });


        AR.enviarDatos(Sol, res);
    });


router.get("/del/:id", async(req, res) => {
    await Domis.query()
        .delete()
        .where({ id: req.params.id })
        .then(Domis => {
            return AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        })
        .catch(err => {
            res.send(err);
        });
});


module.exports = router;