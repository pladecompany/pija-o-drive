const Tipovehiculo = require("../models/Tipovehiculo");
const Horario = require("../models/Horario");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");
//Llamar al helper de errores
const { ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
const jsonfile = require('jsonfile');
var fs = require('fs');

var file = __dirname+ '/../tarifas.json'

router.get("/",archivos.none(), async (req, res,next) => {
  var files = jsonfile.readFileSync(file);
  AR.enviarDatos(files, res);
});

router.post("/config",archivos.none(),sesi.validar_admin, async (req, res,next) => {
    err = false;
    data = req.body;
    if (data.bande.length <1) {
      err = 'Introduzca el banderazo'
    }
    if (data.km_min.length <1) {
      err = 'Introduzca los Km mínimos'
    }
    if (data.pre_km.length <1) {
      err = 'Introduzca el precio mínimo'
    }
    if(err){return AR.enviarDatos({r: false, msg: err},res, 200);}
    jsonfile.readFile(file, function(err, obj) {
      var fileObj = obj;

      fileObj.banderazo=parseFloat(req.body.bande);
      fileObj.km=parseFloat(req.body.km_min);
      fileObj.precio=parseFloat(req.body.pre_km);

      jsonfile.writeFile(file, fileObj, function(err,obj) {
        if (err) throw err;
      fileObj.msg = "Tarifa actualizada con éxito"
      AR.enviarDatos(fileObj, res);
      });
    });

});

router.post("/",
  archivos.none(),
  sesi.validar_admin, async (req, res,next) => {
  // read in the JSON file
    err = false;
    data = req.body;
    //console.log(data);
    dtarifas = data.des;
    htarifas = data.has;
    ctarifas = data.pre_tar;
    mtarifas = data.pre_min;
    tarvacios = 0;
    await dtarifas.forEach( async function(valor, indice, array) {
      if(valor=="")
        tarvacios++;   
    });
    await htarifas.forEach( async function(valor, indice, array) {
      if(valor=="")
        tarvacios++;   
    });
    await ctarifas.forEach( async function(valor, indice, array) {
      if(valor=="")
        tarvacios++;   
    });
    await mtarifas.forEach( async function(valor, indice, array) {
      if(valor=="")
        tarvacios++;   
    });
    if (tarvacios > 0) err = "No debe quedar ningun campo vacio.";
    if(err) return AR.enviarDatos({r: false, msj: err}, res);

    const eliminado = await Horario.query().delete().where({ id_tipo_vehiculo: data.id_auto });
    
    await ctarifas.forEach( async function(valor, indice, array) {
      var datos = {
        "id_tipo_vehiculo" : data.id_auto,
        "preciokm" : valor,
        "preciomin" : mtarifas[indice],
        "hor_des" : dtarifas[indice],
        "hor_has" : htarifas[indice]
      };
      const tipov = await Horario.query()
      .insert(datos)
      .catch(err=>{
        console.log(err);
      })
    });
    AR.enviarDatos({r: true, msj: "Horario de Tarifa actualizada con éxito"}, res);

});

router.get("/:id" , sesi.validar_admin,async (req, res) => {
  await Horario.query().where("id_tipo_vehiculo", req.params.id).then(horario => {
    AR.enviarDatos(horario, res);
  });
});



module.exports = router;
