//llamar al modelo de Usuarios
const Mensajes = require("../models/Mensajes");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Notificacion = require("../models/Notificaciones");
var Firebase = require("../routes/firebase");
Firebase = new Firebase(); 

const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");

//Llamar al helper de errores
const { AuthError,ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");
 
var router = express.Router();

router.post("/", 
  archivos.none(), 
  sesi.validar_admin, async (req, res, next) => {
    data = req.body;
     
    valido = true;
    let err = false;
    try{
      if(!data.des){
        err= "Por favor introduzca la descripción.";
      }
      if(!data.tit){
        err='Por favor introduzca el titulo';
      }
      if(err){
          return AR.enviarDatos({r: false, msg: err},res, 200);
      }
      
      if(valido){
          const msj = await Mensajes.query()
            .insert({
              tit_men: data.tit,
              des_men:data.des
            })
            .catch(err=>{
              console.log(err);
            })
            
            
            
            msj.r = true;
            msj.msg = "Se registro correctamente!";
            AR.enviarDatos(msj,res);
        }
      }
    catch (err) {
      console.log(err);
    next(err);
  }
});

router.get("/" , sesi.validar_admin,async (req, res) => {
    await Mensajes.query().then(msj => {
        AR.enviarDatos(msj, res);
    });
});
router.get("/cliente" , sesi.validar_cli,async (req, res) => {
    await Mensajes.query().then(msj => {
        AR.enviarDatos(msj, res);
    });
});
router.post("/editar", 
  archivos.none(), 
  sesi.validar_admin,async (req, res, next) => {

    data = req.body;
    valido = true;
    let err = false;
    try{

      if(!data.id){ err='Debes enviar el id del mensaje'; }

      if(!data.des){
        err= "Por favor introduzca la descripción.";
      }
      if(!data.tit){
        err='Por favor introduzca el titulo';
      }

        if(err){ return AR.enviarDatos({r: false, msg: err},res, 200); }

        var datos = {
          "tit_men" : data.tit,
          "des_men" :data.des
        };
        if(valido){
            const msj = await Mensajes.query()
            .updateAndFetchById(data.id, datos)
            .then(async msj => {
              msj.msg = "Registro actualizado con éxito";
              msj.r = true;
              AR.enviarDatos(msj, res);
            })
            .catch(err=>{
              //respuesta error
              console.log(err);
              return AR.enviarDatos({msg: "Error al actualizar el mensaje", error: err}, res, 200);
            })

        }
    }
    catch (err) {
        console.log(err);
        next(err);
    }
});
router.post("/masivos",
  archivos.none(),
  sesi.validar_admin, async (req, res,next) => {
    err = false;
    data = req.body;
    //console.log(data);
    clista = data.condi;
    tit = data.tit;
    des = data.des;

    vacios = 0;
    await clista.forEach( async function(valor, indice, array) {
      if(valor=="")
        vacios++;   
    });
    if (vacios > 0) err = "Debes seleccionar al menos un Usuario.";
    if(err) return AR.enviarDatos({r: false, msj: err}, res);

    
    await clista.forEach( async function(valor, indice, array) {
      var datos = {
        id_usuario_receptor: indice,
          tit_noti: tit,
          des_noti: des,
          tipo_noti: "mensaje-masivo",
          fec_reg_noti: new Date(),
          est_noti: 0,
          id_usable: null,
          tablausuario: valor
      };
      await Notificacion.query()
        .insert({
          id_usuario_receptor: indice,
          tit_noti: tit,
          des_noti: des,
          tipo_noti: "mensaje-masivo",
          fec_reg_noti: new Date(),
          est_noti: 0,
          id_usable: null,
          tablausuario: valor
        })
        .then(async noti => {
          var datap = {
                id_usuario: noti.id_usuario_receptor,
                title: noti.tit_noti,
                body: noti.des_noti,
                id_usable: noti.id_usable,
                tipo_noti: noti.tipo_noti,
                tablausuario: noti.tablausuario
          };
          Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

        });
    });
    AR.enviarDatos({r: true, msj: "Mensaje enviado con éxito"}, res);

});

router.get("/delete/:id",
  archivos.none(),
  sesi.validar_admin, async (req, res) => {
  const eliminado = await Mensajes.query()
    .delete()
    .where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });
  if (eliminado) AR.enviarDatos({msj:"Registro eliminado exitosamente"}, res);
  else AR.enviarDatos("0", res);
});
module.exports = router;