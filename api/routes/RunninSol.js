const config = require("../config");
const Solicitud_servicios = require("../models/Solicitud_servicios");
const Notificacion = require("../models/Notificaciones");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
const moment = require("moment");
const _ = require("lodash");

moment.locale("es");
var RunninSol = function(){

    this.SolicitudesVencidas = async function(){
      var Solis =NUEVASSOLICITUDES;
       
      for (var i = 0; i < Solis.length; i++) {
            fechau = moment(new Date(), "YYYY-MM-DD H:mm:ss");
            fechad = moment(Solis[i].fecha_sol, "YYYY-MM-DD H:mm:ss");
            
            if(fechau>=fechad){
              minutos = fechau.diff(fechad, 'minutes');

              if(minutos>=5) {
                const Soll = await Solicitud_servicios.query()
                .where("id",Solis[i].id)
                .catch(err => {
                    console.log(err);
                });
                if(Soll.length>0){
                    Soli_s = Soll[Soll.length-1];
                    if(!Soli_s.id_proveedor){
                       
                   
                      await Solicitud_servicios.query()
                      .updateAndFetchById(Soli_s.id,{
                                      id_proveedor: null,
                                      estatus_sol: 4,
                                      puntuaciones: null
                                      });

                      delete NUEVASSOLICITUDES[i];
                      NUEVASSOLICITUDES.splice(i, 1);
                          var user = _.find(global.USUARIOSC, o => {
                              if(o.user.id == Soli_s.id_cliente && o.user.tablausuario == "clientes")
                              return o.user;
                          });

                          var socketReceptor = user ? user.socket : null;

                          if (socketReceptor) {
                            Soli_s.vencida = true;
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", Soli_s);
                          }
                            await Notificacion.query()
                                        .insert({
                                          id_usuario_receptor: Soli_s.id_cliente,
                                          tit_noti: "Cambios en su Solicitud",
                                          des_noti:"Su solicitud se venció <br> Solicitud de servicio <b>desde</b> " + Soli_s.desdeText + ", <b>hasta</b> " + Soli_s.hastaText,
                                          tipo_noti: "solicitud-cambios-push",
                                          fec_reg_noti: new Date(),
                                          est_noti: 0,
                                          id_usable: Soli_s.id,
                                          tablausuario: 'clientes'
                                        })
                                        .then(async noti => {
                                          
                                          var datap = {
                                            id_usuario: noti.id_usuario_receptor,
                                            title: noti.tit_noti,
                                            body: noti.des_noti,
                                            id_usable: noti.id_usable,
                                            tipo_noti: noti.tipo_noti,
                                            tablausuario: noti.tablausuario
                                          };
                                          
                                          Firebase.enviarPushNotification(datap, function(datan){ console.log(datan); });
                                          
                                });


                    } else {
                      delete NUEVASSOLICITUDES[i];
                      NUEVASSOLICITUDES.splice(i, 1);
                    }
                  }
              }
            }
      }
    }
}

module.exports = RunninSol;
