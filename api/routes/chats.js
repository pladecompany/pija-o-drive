//llamar al modelo de Chats
const Chats = require("../models/Chats");
const AR = require("../ApiResponser");
const express = require("express");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

var router = express.Router();

var sesion = require("./sesion");
var sesi = new sesion();
//Listado de usuarios en el chat
router.get('/admin/obtener-usuarios', upload.none(), async (req, res) => {
	data = req.query
    usuarios  = await Chats.raw("select id,(select mensaje from chats where envio=c.envio and id_usuario=c.id_usuario order by fecha desc limit 1) as mensaje,(select envio from chats where envio=c.envio and id_usuario=c.id_usuario order by fecha desc limit 1) as envio,(select fecha from chats where envio=c.envio and id_usuario=c.id_usuario order by fecha desc limit 1)as hora,c.id_usuario,(select count(visto) from chats where envio=c.envio and id_usuario=c.id_usuario and visto=0 and tabla_usuario = 'admin')as pendientes,if(c.envio = 1,(select nom_cli from clientes where id=c.id_usuario),(select nom_pro from proveedores where id=c.id_usuario))as nombre,if(c.envio = 1,(select img_cli from clientes where id=c.id_usuario),(select img_pro from proveedores where id=c.id_usuario))as img  from chats c  group by c.envio,c.id_usuario order by max(c.fecha) desc;")
    .catch(err=>{

    })
    AR.enviarDatos(usuarios[0], res);
    
});
router.get('/admin/obtener-chats', upload.none(), async (req, res) => {
	data = req.query;
    envio = data.envio;
    mensajes  = await Chats.raw("select id,mensaje,tabla_usuario,if(envio = 1,(select img_cli from clientes where id="+data.id_usuario+"),(select img_pro from proveedores where id="+data.id_usuario+"))as img,fecha as hora from chats where id_usuario="+data.id_usuario+" and envio="+data.envio+" order by fecha desc;")
    .catch(err=>{

    })
    visto = await Chats.raw("update chats set visto=1 where id_usuario="+data.id_usuario+" and envio="+envio+";")
    .then(res=>{
        
    })

    AR.enviarDatos(mensajes[0], res);
    
}); 

router.get('/admin/pendientes', upload.none(), async (req, res) => {
    data = req.query
    usuarios  = await Chats.raw("select count(visto) as pendientes from chats where tabla_usuario='admin' and visto=0;")
    .catch(err=>{

    })
    AR.enviarDatos(usuarios[0], res);
    
});

module.exports = router;

