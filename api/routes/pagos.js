//llamar al modelo de Usuarios
const Pagos = require("../models/Pagos");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");

//Llamar al helper de errores
const { AuthError, ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");

var router = express.Router();

router.post("/",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.pre) {
                err = "Debe ingresar el precio.";
            }
            if (!data.fecha) {
                err = 'Debes seleccionar la fecha.';
            }
            if (!data.pla) {
                err = "Debes seleccionar el plan.";
            }
            if (!data.pro) {
                err = 'Debes seleccionar el conductor.';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                fech = data.fecha + " 00:00:00";
                const pagos = await Pagos.query()
                    .insert({
                        id_proveedor: data.pro,
                        plan: data.pla,
                        valor: data.pre,
                        estatus: 0,
                        concepto: data.con,
                        fecha_reg: fech
                    })
                    .catch(err => {
                        console.log(err);
                    })



                pagos.r = true;
                pagos.msg = "Se registro correctamente!";
                AR.enviarDatos(pagos, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/", sesi.validar_admin, async(req, res) => {
    await Pagos.query().then(pagos => {
        AR.enviarDatos(pagos, res);
    });
});
router.post("/editar",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del pago'; }

            if (!data.pre) {
                err = "Debe ingresar el precio.";
            }
            if (!data.fecha) {
                err = 'Debes seleccionar la fecha.';
            }
            if (!data.pla) {
                err = "Debes seleccionar el plan.";
            }
            if (!data.pro) {
                err = 'Debes seleccionar el conductor.';
            }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }
            fech = data.fecha + " 00:00:00";
            var datos = {
                "id_proveedor": data.pro,
                "plan": data.pla,
                "valor": data.pre,
                //"estatus": 0,
                "concepto": data.con,
                "fecha_reg": fech
            };
            if (valido) {
                const pagos = await Pagos.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async pagos => {
                        pagos.msg = "Registro actualizado con éxito";
                        pagos.r = true;
                        AR.enviarDatos(pagos, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el pago", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post("/editar-status",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del pago'; }

            if (!data.estatus) { err = "Debe ingresar el estatus."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }

            var datos = {
                "estatus": data.estatus
            };
            if (valido) {
                const pagos = await Pagos.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async pagos => {
                        pagos.msg = "Estatus del Pago actualizado con éxito";
                        pagos.r = true;
                        AR.enviarDatos(pagos, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el estatus", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Pagos.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
module.exports = router;