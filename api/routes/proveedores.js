//llamar al modelo de Usuarios
const Clientes = require("../models/Clientes");
const Proveedores = require("../models/Proveedores");
const DetallesP = require("../models/DetallesP");
const Tokencuentas = require("../models/Tokencuentas");
const Solicitud_servicios = require("../models/Solicitud_servicios");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const _ = require("lodash");
const Notificacion = require("../models/Notificaciones");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");
const validarMail = require("../functions/validateMail");
const mailer = require("../mails/Mailer");
const mail2 = require("../mails/confirmacion");
//Llamar al helper de errores
const { AuthError, ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");

const upload = multer({
    limits: {
        fieldSize: 8 * 1024 * 1024,
    }
});
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");

var router = express.Router();
router.post(
    "/ubicacionActual", upload.none(), async(req, res) => {
        data = req.body;

        update = {
            lat_pro: data.lat,
            lon_pro: data.lon
        };
        //console.log(data);
        await Proveedores.query()
            .updateAndFetchById(data.id_usuario, update)
            .then(async usuario => {

                const pr = await Proveedores.query()
                    .findById(usuario.id)
                    .catch(err => {
                        console.log(err);
                    });
                pr.tablausuario = "proveedores";
                pr.nom_usu = pr.nom_pro;

                const Soll = await Solicitud_servicios.query()
                    .where({ id_proveedor: usuario.id, estatus_sol: 2 })
                    .catch(err => {
                        console.log(err);
                    });
                const Solld = await Solicitud_servicios.query()
                    .where({ id_proveedor: usuario.id, estatus_sol: 7, puntuaciones: null })
                    .catch(err => {
                        console.log(err);
                    });
                if (Soll.length > 0) {
                    pr.Activas = Soll.length;
                }
                if (Solld.length > 0) {
                    pr.Sinvalorar = Solld.length;
                    pr.SinvalorarId = Solld[0].id;
                }

                AR.enviarDatos(pr, res);
            })
            .catch(err => {
                res.send(err);
            });
    }
);
router.post(
    "/Actualizar-sesion", upload.none(), async(req, res) => {
        data = req.body;

        const pr = await Proveedores.query()
            .findById(data.id)
            .catch(err => {
                res.send(err);
            });
        pr.tablausuario = "proveedores";
        pr.nom_usu = pr.nom_pro;
        return AR.enviarDatos(pr, res);
    }
);
router.get(
    "/verificar", upload.none(), async(req, res) => {
        data = req.query;


        if (!data.token) {
            return AR.enviarDatos({ msg: "Token no detectado." }, res, 200);
        }

        update = {
            est_pro: 1
        };
        updatee = {
            est_token: 1
        };
        await Tokencuentas.query()
            .where("token", data.token)
            .update(updatee)
            .then(async u => {

                var usuario = {};
                if (u == 1) {
                    const token = await Tokencuentas.query()
                        .where({ token: data.token })
                        .then(async resp => {

                            if (resp.length != 0) {
                                if (resp[0].token == data.token) {
                                    await Proveedores.query()
                                        .updateAndFetchById(resp[0].id_usuario, update)
                                    await Tokencuentas.query()
                                        .delete()
                                        .where("token", data.token)
                                }
                            }
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    usuario.r = u;
                    usuario.msg = "Cuenta verificada exitosamente.";
                } else {
                    usuario.r = u;
                    usuario.msg = "Token vencido.";
                }

                AR.enviarDatos(usuario, res, 200);
            })
            .catch(err => {
                res.send(err);
            });
    }
);
router.post(
    "/Oauth",
    upload.none(),
    async(req, res, next) => {
        data = req.body;
        data.nom = data.name;
        data.img = data.picture.data.url;
        valido = true;
        let err = false;
        try {

            if (!data.cor) {
                err = "Debe ingresar un correo electrónico.";
            }



            if (!validarMail(data.cor)) {
                err = 'Por favor introduzca un correo válido';
            }
            if (!data.nom) {
                err = 'Por favor introduzca el nombre';
            }
            if (!data.img) {
                err = 'Seleccione una imagen';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            const correo = await Clientes.query()
                //.select('cor_cli')
                .where({ cor_cli: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_cli == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un cliente con este correo' }, res, 200);

                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            const correo2 = await Proveedores.query()
                //.select('cor_pro')
                .where({ cor_pro: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_pro == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un conductor con este correo' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            if (valido) {
                const prove = await Proveedores.query()
                    .insert({
                        nom_pro: data.nom,
                        img_pro: data.img,
                        cor_pro: data.cor,
                        //tel_pro: data.tel,
                        //cedula_pro: data.cedula,
                        //dir_pro: data.dir_usu,
                        //lat_pro: data.lat,
                        //lon_pro: data.lon,
                        est_pro: 0,
                        //pas_pro: bcrypt.hashSync(data.pas, 8),
                        fec_reg: new Date(),
                        token: uuid.v1()
                    })
                    .catch(err => {
                        console.log(err);
                    })
                let token = jwt.sign({ id: prove.id }, config.secret, {
                    expiresIn: 86400
                });
                if (prove) {
                    const Tokencuenta = await Tokencuentas.query()
                        .insert({
                            est_token: 0,
                            fec_reg: new Date(),
                            token: uuid.v1(),
                            id_usuario: prove.id
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    mailer.enviarMail({
                        direccion: data.cor,
                        titulo: "Pijaos drive | Confirmar Cuenta",
                        mensaje: mail2({
                            nombre: data.nom,
                            email: data.cor,
                            tipo: 'proveedores',
                            id: prove.id,
                            token: Tokencuenta.token

                        })
                    });
                }
                const pr = await Proveedores.query()
                    .findById(prove.id)
                    .catch(err => {
                        console.log(err);
                    });
                pr.tablausuario = "proveedores";
                pr.nom_usu = pr.nom_pro;
                await Notificacion.query()
                    .insert({
                        tit_noti: "Nuevo Conductor",
                        des_noti: "Hay un Conductor nuevo por verificar (" + pr.nom_pro + ").",
                        tipo_noti: "verificar-chofer-admin",
                        fec_reg_noti: new Date(),
                        est_noti: 0,
                        id_usable: prove.id
                    })
                    .then(async noti => {
                        IO.sockets.emit("Recibiralerta", noti);
                    });
                AR.enviarAuth(res, 200, token, pr);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post(
    "/",
    upload.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.pas) {
                err = 'Por favor introduzca una contraseña';
            }
            if (!data.cor) {
                err = "Debe ingresar un correo electrónico.";
            }
            if (!data.tel) {
                err = "Debe ingresar un teléfono WhatsApp.";
            }
            if (!data.cedula) {
                err = "Debe ingresar una cédula.";
            }
            if (!data.dir_usu) {
                err = "Debe ingresar una dirección.";
            }
            if (!validarMail(data.cor)) {
                err = 'Por favor introduzca un correo válido';
            }
            if (!data.nom) {
                err = 'Por favor introduzca el nombre';
            }
            if (data.img) {

                if (data.img == "") {
                    err = 'Seleccione una imagen';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_pro__" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img = config.rutaArchivo(file);
            } else {
                err = 'Seleccione una imagen';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            const correo = await Clientes.query()
                //.select('cor_cli')
                .where({ cor_cli: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_cli == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un cliente con este correo' }, res, 200);

                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            const correo2 = await Proveedores.query()
                //.select('cor_pro')
                .where({ cor_pro: data.cor })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cor_pro == data.cor) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un conductor con este correo' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
             const ced_cli = await Clientes.query()
                //.select('cor_pro')
                .where({ cedula_cli: data.cedula })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cedula_cli == data.cedula) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un usuario con esta cedula' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
                
            const ced_pro = await Proveedores.query()
                //.select('cor_pro')
                .where({ cedula_pro: data.cedula })
                .skipUndefined()
                .then(resp => {

                    if (resp.length != 0) {
                        if (resp[0].cedula_pro == data.cedula) {
                            valido = false;

                            return AR.enviarDatos({ r: false, msg: 'Ya existe un usuario con esta cedula' }, res, 200);
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            if (valido) {
                const prove = await Proveedores.query()
                    .insert({
                        nom_pro: data.nom,
                        img_pro: data.img,
                        cor_pro: data.cor,
                        tel_pro: data.tel,
                        cedula_pro: data.cedula,
                        dir_pro: data.dir_usu,
                        lat_pro: data.lat,
                        lon_pro: data.lon,
                        est_pro: 0,
                        pas_pro: bcrypt.hashSync(data.pas, 8),
                        fec_reg: new Date(),
                        token: uuid.v1()
                    })
                    .catch(err => {
                        console.log(err);
                    })
                let token = jwt.sign({ id: prove.id }, config.secret, {
                    expiresIn: 86400
                });
                if (prove) {
                    const Tokencuenta = await Tokencuentas.query()
                        .insert({
                            est_token: 0,
                            fec_reg: new Date(),
                            token: uuid.v1(),
                            id_usuario: prove.id
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    mailer.enviarMail({
                        direccion: data.cor,
                        titulo: "Pijaos drive | Confirmar Cuenta",
                        mensaje: mail2({
                            nombre: data.nom,
                            email: data.cor,
                            tipo: 'proveedores',
                            id: prove.id,
                            token: Tokencuenta.token

                        })
                    });
                }
                const pr = await Proveedores.query()
                    .findById(prove.id)
                    .catch(err => {
                        console.log(err);
                    });
                pr.tablausuario = "proveedores";
                pr.nom_usu = pr.nom_pro;
                await Notificacion.query()
                    .insert({
                        tit_noti: "Nuevo Conductor",
                        des_noti: "Hay un Conductor nuevo por verificar (" + pr.nom_pro + ").",
                        tipo_noti: "verificar-chofer-admin",
                        fec_reg_noti: new Date(),
                        est_noti: 0,
                        id_usable: prove.id
                    })
                    .then(async noti => {
                        IO.sockets.emit("Recibiralerta", noti);
                    });
                AR.enviarAuth(res, 200, token, pr);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post(
    "/editar/imagen",
    upload.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar un id conductor.";
            }
            if (data.img) {

                if (data.img == "") {
                    err = 'Seleccione una imagen';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_pro__" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img = config.rutaArchivo(file);
            } else {
                err = 'Seleccione una imagen';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }


            const prove = await Proveedores.query()
                .updateAndFetchById(data.id, {
                    img_pro: data.img,
                    token: uuid.v1()
                })
                .catch(err => {
                    console.log(err);
                })
            let token = jwt.sign({ id: prove.id }, config.secret, {
                expiresIn: 86400
            });

            const pr = await Proveedores.query()
                .findById(prove.id)
                .catch(err => {
                    console.log(err);
                });
            pr.tablausuario = "proveedores";
            pr.nom_usu = pr.nom_pro;
            AR.enviarAuth(res, 200, token, pr);

        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post(
    "/editar",
    upload.none(),
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.id) {
                err = "Debe ingresar un id conductor.";
            }
            if (!data.tel) {
                err = "Debe ingresar un teléfono WhatsApp.";
            }
            if (!data.cedula) {
                err = "Debe ingresar una cédula.";
            }
            if (!data.nom) {
                err = 'Por favor introduzca el nombre';
            }
            if (!data.dir_usu) {
                err = "Debe ingresar una dirección.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }


            const prove = await Proveedores.query()
                .updateAndFetchById(data.id, {
                    nom_pro: data.nom,
                    tel_pro: data.tel,
                    cedula_pro: data.cedula,
                    dir_pro: data.dir_usu,
                    lat_pro: data.lat,
                    lon_pro: data.lon,
                    token: uuid.v1()
                })
                .catch(err => {
                    console.log(err);
                })
            let token = jwt.sign({ id: prove.id }, config.secret, {
                expiresIn: 86400
            });

            const pr = await Proveedores.query()
                .findById(prove.id)
                .catch(err => {
                    console.log(err);
                });
            pr.tablausuario = "proveedores";
            pr.nom_usu = pr.nom_pro;
            AR.enviarAuth(res, 200, token, pr);

        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post(
    "/detalles/agregar",
    upload.none(),
    sesi.validar_pro,
    async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {



            if (!data.id) {
                err = 'Por favor ingrese el conductor';
            }
            if (data.img1) {

                if (data.img1 == "") {
                    err = 'Debe seleccionar una imagen de frente';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_pro_1_" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img1 = config.rutaArchivo(file);
            } else {
                err = 'Debe seleccionar una imagen de frente';
            }

            //DOS
            if (data.img2) {

                if (data.img2 == "") {
                    err = 'Debe seleccionar una imagen de lateral';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_pro_2_" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img2.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img2 = config.rutaArchivo(file);
            } else {
                err = 'Debe seleccionar una imagen de lateral';
            }
            //TRES
            if (data.img3) {

                if (data.img3 == "") {
                    err = 'Debe seleccionar una imagen de trasera';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_pro_3_" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img3.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img3 = config.rutaArchivo(file);
            } else {
                err = 'Debe seleccionar una imagen de trasera';
            }

            if (!data.id_tipo_vehiculo) {
                err = 'Por favor ingrese el tipo de vehículo';
            }

            if (!data.marca) {
                err = 'Por favor introduzca una marca';
            }
            if (!data.matricula) {
                err = 'Por favor introduzca una matricula';
            }
            if (!data.referencia) {
                err = 'Por favor introduzca una referencia personal';
            }
            if (!data.tel_referencia) {
                err = 'Por favor ingrese el teléfono de la referencia personal';
            }


            //cuatro
            if (data.img4) {

                if (data.img4 == "") {
                    err = 'Debe seleccionar una imagen de su cedula';
                }
                if (err) {
                    return AR.enviarDatos({ r: false, msg: err }, res, 200);
                }
                var file = "img_pro_4_" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img4.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img4 = config.rutaArchivo(file);
            } else {
                err = 'Debe seleccionar una imagen de su cedula';
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }
            const prove = await DetallesP.query()
                .insert({
                    id_proveedor: data.id,
                    id_tipo_vehiculo: data.id_tipo_vehiculo,
                    img_cedula: data.img4,
                    referencia_personal: data.referencia,
                    tel_referencia: data.tel_referencia,
                    marca_veh: data.marca,
                    matri_veh: data.matricula,
                    img_fre: data.img1,
                    img_lat: data.img2,
                    img_tra: data.img3
                })
                .catch(err => {
                    //console.log(err);
                })
            const pr = await Proveedores.query()
                .findById(data.id)
                .catch(err => {
                    console.log(err);
                });
            pr.tablausuario = "proveedores";
            pr.nom_usu = pr.nom_pro;
            AR.enviarDatos(pr, res);


        } catch (err) {
            console.log(err);
            next(err);
        }

    });

router.get("/", sesi.validar_admin, async(req, res) => {
    await Proveedores.query().then(proveedor => {
        AR.enviarDatos(proveedor, res);
    });
});
router.post("/editar-plan",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.idp) { err = 'Debes enviar el id del conductor'; }

            if (!data.id_plan) { err = "Debes seleccionar un plan."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }

            var datos = {
                "id_plan": data.id_plan
            };
            if (valido) {
                const proveedor = await Proveedores.query()
                    .updateAndFetchById(data.idp, datos)
                    .then(async proveedor => {
                        proveedor.msg = "Plan del Conductor actualizado con éxito";
                        proveedor.r = true;
                        AR.enviarDatos(proveedor, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el plan", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.post("/editar-status",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del conductor'; }

            if (!data.estatus_verificado) { err = "Debe ingresar el estatus."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }

            var datos = {
                "estatus_verificado": data.estatus_verificado
            };
            if (valido) {
                const proveedor = await Proveedores.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async proveedor => {
                        proveedor.msg = "Estatus del Conductor actualizado con éxito";
                        proveedor.r = true;
                        if (data.estatus_verificado == 1) {
                            var user = _.find(global.USUARIOSC, o => {
                                if (o.user.id == proveedor.id && o.user.tablausuario == "proveedores")
                                    return o.user;
                            });

                            var socketReceptor = user ? user.socket : null;

                            if (socketReceptor) {
                                global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", proveedor);
                            }
                            await Notificacion.query()
                                .insert({
                                    id_usuario_receptor: proveedor.id,
                                    tit_noti: "Verificar cuenta / Conductor oficial",
                                    des_noti: "Su cuenta como Conductor oficial ha sido verficada.",
                                    tipo_noti: "verificar-pro-push",
                                    fec_reg_noti: new Date(),
                                    est_noti: 0,
                                    id_usable: proveedor.id,
                                    tablausuario: 'proveedores'
                                })
                                .then(async noti => {

                                    var datap = {
                                        id_usuario: noti.id_usuario_receptor,
                                        title: noti.tit_noti,
                                        body: noti.des_noti,
                                        id_usable: noti.id_usable,
                                        tipo_noti: noti.tipo_noti,
                                        tablausuario: noti.tablausuario
                                    };

                                    Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                                });
                        }

                        AR.enviarDatos(proveedor, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el estatus", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });





router.post("/editar-status-normal",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del conductor'; }

            if (!data.est_pro) { err = "Debe ingresar el estatus."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }

            var datos = {
                "est_pro": data.est_pro
            };
            if (valido) {
                const proveedor = await Proveedores.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async proveedor => {
                        proveedor.msg = "Estatus del Conductor actualizado con éxito";
                        proveedor.r = true;

                        var user = _.find(global.USUARIOSC, o => {
                            if (o.user.id == proveedor.id && o.user.tablausuario == "proveedores")
                                return o.user;
                        });

                        var socketReceptor = user ? user.socket : null;

                        if (socketReceptor) {
                            global.IO.to(socketReceptor).emit("RECIBIR-NUEVA-NOTIFICACION", proveedor);
                        }
                        await Notificacion.query()
                            .insert({
                                id_usuario_receptor: proveedor.id,
                                tit_noti: "Cambios en su Cuenta",
                                des_noti: "Su cuenta ha sido cambiada a <b>" + data.stsT + "</b>, por los administradores de Pijaos drive. <br> Nota: <b>" + data.comentario + "</b>",
                                tipo_noti: "estatus-pro-push",
                                fec_reg_noti: new Date(),
                                est_noti: 0,
                                id_usable: proveedor.id,
                                tablausuario: 'proveedores'
                            })
                            .then(async noti => {

                                var datap = {
                                    id_usuario: noti.id_usuario_receptor,
                                    title: noti.tit_noti,
                                    body: noti.des_noti,
                                    id_usable: noti.id_usable,
                                    tipo_noti: noti.tipo_noti,
                                    tablausuario: noti.tablausuario
                                };

                                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });

                            });


                        AR.enviarDatos(proveedor, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el estatus", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Proveedores.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
router.post("/enviar-push",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        data.tablausuario = 'proveedores';
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del proveedor'; }

            if (!data.msj) { err = "Debe ingresar el mensaje."; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }


            if (valido) {
                //console.log(data.msj);
                var datap = {
                    id_usuario: data.id,
                    title: "Mensaje del Administrador",
                    body: data.msj,
                    tablausuario: "proveedores",
                    tipo_noti: "push-tipo-mensaje"
                };

                Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
                IO.sockets.emit("Recibiralerta", data);
                AR.enviarDatos({ r: true, msg: "Mensaje enviado con éxito" }, res, 200);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
module.exports = router;