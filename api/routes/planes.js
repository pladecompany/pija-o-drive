//llamar al modelo de Usuarios
const Planes = require("../models/Planes");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
 
const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");

//Llamar al helper de errores
const { AuthError,ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");
 
var router = express.Router();

router.post("/", 
  archivos.none(), 
  sesi.validar_admin, async (req, res, next) => {
    data = req.body;
     
    valido = true;
    let err = false;
    try{
      if(!data.pre){
        err= "Debe ingresar el precio.";
      }
      if(!data.des){
        err= "Debe ingresar el beneficio.";
      }
      if(!data.tit){
        err='Por favor introduzca el titulo';
      }
      if(err){
          return AR.enviarDatos({r: false, msg: err},res, 200);
      }
      
      if(valido){
          const prove = await Planes.query()
            .insert({
              tit_pla: data.tit,
              des_pla:data.des,
              pre_pla: data.pre
            })
            .catch(err=>{
              console.log(err);
            })
            
            
            
            prove.r = true;
            prove.msg = "Se registro correctamente!";
            AR.enviarDatos(prove,res);
        }
      }
    catch (err) {
      console.log(err);
    next(err);
  }
});

router.get("/" , sesi.validar_admin,async (req, res) => {
    await Planes.query().then(proveedor => {
        AR.enviarDatos(proveedor, res);
    });
});
router.post("/editar", 
  archivos.none(), 
  sesi.validar_admin,async (req, res, next) => {

    data = req.body;
    valido = true;
    let err = false;
    try{

      if(!data.id){ err='Debes enviar el id del plan'; }

      if(!data.tit){
        err='Por favor introduzca el titulo';
      }
      if(!data.des){
        err= "Debe ingresar el beneficio";
      }
      if(!data.pre){
        err= "Debe ingresar el precio.";
      }

        if(err){ return AR.enviarDatos({r: false, msg: err},res, 200); }

        var datos = {
          "tit_pla" : data.tit,
          "des_pla" :data.des,
          "pre_pla" : data.pre
        };
        if(valido){
            const plan = await Planes.query()
            .updateAndFetchById(data.id, datos)
            .then(async plan => {
              plan.msg = "Registro actualizado con éxito";
              plan.r = true;
              AR.enviarDatos(plan, res);
            })
            .catch(err=>{
              //respuesta error
              console.log(err);
              return AR.enviarDatos({msg: "Error al actualizar el plan", error: err}, res, 200);
            })

        }
    }
    catch (err) {
        console.log(err);
        next(err);
    }
});
router.get("/delete/:id",
  archivos.none(),
  sesi.validar_admin, async (req, res) => {
  const eliminado = await Planes.query()
    .delete()
    .where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });
  if (eliminado) AR.enviarDatos({msj:"Registro eliminado exitosamente"}, res);
  else AR.enviarDatos("0", res);
});
module.exports = router;