//llamar al modelo de Usuarios

const Admin = require("../models/Admin");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
const validarMail = require("../functions/validateMail");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();
 
// sesi.validar_usu O validar_admin validar sesion

//cambiar clave admin
router.post("/cambiar-clave", sesi.validar_admin, upload.none(), async (req, res, next) => {
    data = req.body;
    err =  false;
    try {
      if(data.pass_new!=data.pass_new_con){
        err ='Las contraseñas no coinciden'
        
      }
      if(!data.pass_new_con){
        err ='Por favor introduzca el repetir contraseña'
        
      }
      if(!data.pass_new){
        err ='Por favor introduzca la nueva contraseña'
        
      }
      if(!data.pass){
        err = 'Por favor introduzca la contraseña actual'
        
      }

      if(err) return AR.enviarDatos({r: false, msj: err}, res);
        const usuario = await Admin.query()
            .findOne("id", data.id)
            .throwIfNotFound()
            .catch(err => console.log(err));

            let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pass);

                if (!passwordIsValid){
                  return AR.enviarDatos({r: false, msj: 'Su contraseña actual es incorrecta.'}, res);
                }
                var obj={
                    pass: bcrypt.hashSync(req.body.pass_new, 8)
                }
                await Admin.query()
                .patchAndFetchById(
                    data.id,obj)
                .then(admin => {
                    admin.msj = "Ha cambiado su contraseña satisfactoriamente.";
                    admin.r = true;
                    AR.enviarDatos(admin, res);
                })
                .catch(err => {
                    res.send(err);
                });
    } catch (err) {
        next(err);
    }
});

router.get("/" , sesi.validar_admin,async (req, res) => {
    await Admin.query().then(admin => {
        AR.enviarDatos(admin, res);
    });
});
router.post("/", 
  upload.none(), 
  sesi.validar_admin, async (req, res, next) => {
    data = req.body;
     
    valido = true;
    let err = false;
    try{
      if(!data.cor){
        err= "Debe ingresar el correo.";
      }
      if (!validarMail(data.cor)) {
        err = 'Por favor introduzca un correo válido';
      }
      if(data.pass.length<5){
        err= "Debe ingresar la contraseña.";
      }
      if(err){
          return AR.enviarDatos({r: false, msg: err},res, 200);
      }
      
      if(valido){
          const prove = await Admin.query()
            .insert({
              correo: data.cor,
              pass: bcrypt.hashSync(data.pass, 8),
              tip_adm: data.tip
            })
            .catch(err=>{
              console.log(err);
            })
            
            
            
            prove.r = true;
            prove.msg = "Se registro correctamente!";
            AR.enviarDatos(prove,res);
        }
      }
    catch (err) {
      console.log(err);
    next(err);
  }
});

router.post("/editar", 
  upload.none(), 
  sesi.validar_admin,async (req, res, next) => {

    data = req.body;
    valido = true;
    let err = false;
    try{

      if(!data.id){ err='Debes enviar el id del Administrador'; }

      if(!data.cor){
        err= "Debe ingresar el correo.";
      }
      if (!validarMail(data.cor)) {
        err = 'Por favor introduzca un correo válido';
      }
      if(data.pass.length>0 && data.pass.length<5){
        err= "Debe ingresar la contraseña.";
      }
      if(err){
          return AR.enviarDatos({r: false, msg: err},res, 200);
      }

        if(err){ return AR.enviarDatos({r: false, msg: err},res, 200); }

        if(data.pass.length>0){
          var datos = {
            "correo" : data.cor,
            "pass" :bcrypt.hashSync(data.pass, 8),
            "tip_adm" : data.tip
          };
        }else{
          var datos = {
            "correo" : data.cor,
            "tip_adm" : data.tip
          };
        }
        
        if(valido){
            const admin = await Admin.query()
            .updateAndFetchById(data.id, datos)
            .then(async admin => {
              admin.msg = "Registro actualizado con éxito";
              admin.r = true;
              AR.enviarDatos(admin, res);
            })
            .catch(err=>{
              //respuesta error
              console.log(err);
              return AR.enviarDatos({msg: "Error al actualizar el Admin, el correo ya existe", error: err}, res, 200);
            })

        }
    }
    catch (err) {
        console.log(err);
        next(err);
    }
});
router.get("/delete/:id",
  upload.none(),
  sesi.validar_admin, async (req, res) => {
  const eliminado = await Admin.query()
    .delete()
    .where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });
  if (eliminado) AR.enviarDatos({msj:"Registro eliminado exitosamente"}, res);
  else AR.enviarDatos("0", res);
});
module.exports = router;
