//llamar al modelo de Usuarios
const Tipovehiculo = require("../models/Tipovehiculo");
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const moment = require("moment");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");

const jsonfile = require('jsonfile');

//Llamar al helper de errores
const { AuthError, ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
var uuid = require("uuid");
const fs = require("fs");
const crypto = require("crypto");

var router = express.Router();

router.post("/agregar",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.nombre_veh) {
                err = "Debe ingresar el nombre.";
            }
            if (!data.icono_veh) {
                err = "Debe ingresar un icono.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const tipov = await Tipovehiculo.query()
                    .insert({
                        nombre_veh: data.nombre_veh,
                        icono_veh: data.icono_veh
                    })
                    .catch(err => {
                        console.log(err);
                    })



                tipov.r = true;
                tipov.msg = "Se registro correctamente!";
                AR.enviarDatos(tipov, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/panel", sesi.validar_admin, async(req, res) => {
    await Tipovehiculo.query()
        .then(tipov => {
            AR.enviarDatos(tipov, res);
        });
});
router.post("/obtener", sesi.validar_cli, async(req, res) => {
    await Tipovehiculo.query()
        .where('id', req.body.veh)
        .where('est_veh', 1)
        .then(tipov => {
            var t = {};
            if (tipov.length > 0) {
                t = tipov[0];
                t.tarifas = jsonfile.readFileSync(__dirname + '/../tarifas.json');
                if (t.preciokm <= 0 || !t.tarifas) {
                    t.r = false;
                    t.msg = "Transporte no disponible en éste horario.";
                }
            } else {
                t.r = false;
                t.msg = "Transporte no disponible.";
            }

            AR.enviarDatos(t, res);
        })
        .catch(err => {
            AR.enviarDatos({ r: false, msg: "Transporte no disponible." }, res);
        })
});

router.get("/app", async(req, res) => {
    await Tipovehiculo.query()
        .where('est_veh', 1)
        .then(tipov => {
            AR.enviarDatos(tipov, res);
        });
});

router.post("/app", async(req, res) => {
    await Tipovehiculo.query()
        .where('est_veh', 1)
        .then(tipov => {
            AR.enviarDatos(tipov, res);
        });
});
router.post("/editar",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del tipo de vehiculo'; }

            if (!data.nombre_veh) {
                err = "Debe ingresar el nombre.";
            }
            if (!data.icono_veh) {
                err = "Debe ingresar un icono.";
            }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }

            var datos = {
                "nombre_veh": data.nombre_veh,
                "icono_veh": data.icono_veh,
                "est_veh": data.est_veh
            };
            if (valido) {
                const tipov = await Tipovehiculo.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async tipov => {
                        tipov.msg = "Registro actualizado con éxito";
                        tipov.r = true;
                        AR.enviarDatos(tipov, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el plan", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Tipovehiculo.query()
            .delete()
            .where({ id: req.params.id })
            .then(async eliminado => {
                return AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
            })
            .catch(err => {

                return AR.enviarDatos("0", res);
            });


    });
module.exports = router;