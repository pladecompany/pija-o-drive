//llamar al modelo de Usuarios
const Clientes = require("../models/Clientes");
const Proveedores = require("../models/Proveedores");
const Admin = require("../models/Admin");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

// sesi.validar_usu O validar_admin validar sesion
//Logear un Admin
router.post("/admin", upload.none(), async(req, res, next) => {
    data = req.body;
    try {
        let err = false;
        if (!data.correo) err = "Debe ingresar un correo electrónico.";
        if (!data.pass || data.pass.length < 5) err = "Debe ingresar una contraseña.";

        if (err) return AR.enviarDatos({ r: false, msj: err }, res);

        await Admin.query()
            .findOne("correo", data.correo)
            .throwIfNotFound()
            .then(async usuario => {
                let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pass);

                if (!passwordIsValid) err = "Su contraseña es incorrecta.";

                if (err) return AR.enviarDatos({ r: false, msj: err }, res);

                usuario.token = uuid.v1();
                console.log(usuario.token);
                usuario.r = true;
                await Admin.query()
                    .updateAndFetchById(usuario.id, {
                        token: usuario.token
                    });
                //Admin.raw("UPDATE admins SET token='"+usuario.token+"' WHERE correo='"+usuario.correo+"';").then(usuario => {});;
                return AR.enviarAuth(res, true, null, usuario);
            })
            .catch(err => {
                if (err instanceof AuthError) next(err);
                //else console.log(err);
                else
                    AR.enviarError(
                        "No se encontró un admin con este correo.",
                        res,
                        404
                    );
            });
    } catch (err) {
        next(err);
    }
});

router.post("/app", upload.none(), async(req, res, next) => {
    data = req.body;

    try {
        let err = false;
        if (!data.correo) err = "Debe ingresar un correo electrónico.";
        else if (!data.pass || data.pass.length < 5) err = "Debe ingresar una contraseña.";

        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        const correo = await Clientes.query()
            .where({ cor_cli: data.correo })
            .catch(err => {
                console.log(err);
            });

        const correo2 = await Proveedores.query()
            .where({ cor_pro: data.correo })
            .catch(err => {

            });
        if (correo.length != 0) {
            if (correo[0].cor_cli == data.correo) {

                await Clientes.query()
                    .findOne("cor_cli", data.correo)
                    .throwIfNotFound()
                    .then(async usuario => {
                        entro = true;
                        let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pas_cli);


                        if (!passwordIsValid)
                            return AR.enviarDatos({ r: false, msg: "Su contraseña es incorrecta." }, res, 200);

                        if (usuario.est_cli == 2)
                            return AR.enviarError(
                                "No puede iniciar sesión porque su usuario está bloqueado.",
                                res,
                                403
                            );
                        else {
                            usuario.token = uuid.v1();
                            await Clientes.raw(
                                "UPDATE clientes SET token='" + usuario.token + "' WHERE cor_cli='" + usuario.cor_cli + "';"
                            ).then(usuario => {

                            });
                            const pr = await Clientes.query()
                                .findById(usuario.id)
                                .catch(err => {
                                    console.log(err);
                                });
                            pr.tablausuario = "clientes";
                            pr.nom_usu = pr.nom_cli;
                            return AR.enviarAuth(res, 200, usuario.token, pr);

                        }

                    })
                    .catch(err => {

                    });
            }
        }

        if (correo2.length != 0) {
            if (correo2[0].cor_pro == data.correo) {
                await Proveedores.query()
                    .findOne("cor_pro", data.correo)
                    .throwIfNotFound()
                    .then(async usuario => {

                        let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pas_pro);


                        if (!passwordIsValid)
                            return AR.enviarDatos({ r: false, msg: "Su contraseña es incorrecta." }, res, 200);


                        if (usuario.est_pro == 2)
                            return AR.enviarError(
                                "No puede iniciar sesión porque su usuario está bloqueado.",
                                res,
                                403
                            );
                        else {

                            usuario.token = uuid.v1();
                            await Proveedores.raw(
                                "UPDATE proveedores SET token='" + usuario.token + "' WHERE cor_pro='" + usuario.cor_pro + "';"
                            ).then(usuario => {

                            });
                            const pr = await Proveedores.query()
                                .findById(usuario.id)
                                .catch(err => {
                                    console.log(err);
                                });
                            pr.tablausuario = "proveedores";
                            pr.nom_usu = pr.nom_pro;
                            return AR.enviarAuth(res, 200, usuario.token, pr);

                        }
                        return;
                    })
                    .catch(err => {

                    });
            }
        }
        if (correo.length <= 0 && correo2.length <= 0) {

            return AR.enviarError(
                "No se encontró un usuario con este correo.",
                res,
                404
            );
        }


    } catch (err) {
        next(err);
    }
});
router.post("/app/Oauth", upload.none(), async(req, res, next) => {
    data = req.body;

    try {
        let err = false;
        if (!data.correo) err = "Debe ingresar un correo electrónico.";
        //else if (!data.pass || data.pass.length < 5) err = "Debe ingresar una contraseña.";

        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        const correo = await Clientes.query()
            .where({ cor_cli: data.correo })
            .catch(err => {
                console.log(err);
            });
        const correo2 = await Proveedores.query()
            .where({ cor_pro: data.correo })
            .catch(err => {

            });
        if (correo.length != 0) {
            if (correo[0].cor_cli == data.correo) {

                await Clientes.query()
                    .findOne("cor_cli", data.correo)
                    .throwIfNotFound()
                    .then(async usuario => {

                        entro = true;
                        //let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pas_cli);


                        //if (!passwordIsValid)
                        //    return AR.enviarDatos({ r: false, msg: "Su contraseña es incorrecta." }, res, 200);

                        if (usuario.est_cli == 2)
                            return AR.enviarError(
                                "No puede iniciar sesión porque su usuario está bloqueado.",
                                res,
                                403
                            );
                        else {

                            usuario.token = uuid.v1();
                            usuario.nom_cli = data.name;
                            usuario.img_cli = data.picture.data.url;
                            updatee = {
                                token: usuario.token,
                                nom_cli: usuario.nom_cli,
                                img_cli: usuario.img_cli
                            };
                            await Clientes.query()
                                .where("cor_cli", usuario.cor_cli)
                                .update(updatee)
                                .then(async u => {

                                })
                                .catch(err => {
                                    console.log(err);
                                });
                            const pr = await Clientes.query()
                                .findById(usuario.id)
                                .catch(err => {
                                    console.log(err);
                                });
                            pr.tablausuario = "clientes";
                            pr.nom_usu = pr.nom_cli;

                            return AR.enviarAuth(res, 200, usuario.token, pr);

                        }

                    })
                    .catch(err => {

                    });
            }
        }

        if (correo2.length != 0) {
            if (correo2[0].cor_pro == data.correo) {
                await Proveedores.query()
                    .findOne("cor_pro", data.correo)
                    .throwIfNotFound()
                    .then(async usuario => {

                        //let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pas_pro);


                        //if (!passwordIsValid)
                        //    return AR.enviarDatos({ r: false, msg: "Su contraseña es incorrecta." }, res, 200);


                        if (usuario.est_pro == 2)
                            return AR.enviarError(
                                "No puede iniciar sesión porque su usuario está bloqueado.",
                                res,
                                403
                            );
                        else {

                            usuario.token = uuid.v1();
                            usuario.nom_pro = data.name;
                            usuario.img_pro = data.picture.data.url;

                            updatee = {
                                token: usuario.token,
                                nom_pro: usuario.nom_pro,
                                img_pro: usuario.img_pro
                            };
                            await Proveedores.query()
                                .where("cor_pro", usuario.cor_pro)
                                .update(updatee)
                                .then(async u => {

                                })
                                .catch(err => {
                                    console.log(err);
                                });
                            const pr = await Proveedores.query()
                                .findById(usuario.id)
                                .catch(err => {
                                    console.log(err);
                                });
                            pr.tablausuario = "proveedores";
                            pr.nom_usu = pr.nom_pro;
                            return AR.enviarAuth(res, 200, usuario.token, pr);

                        }
                        return;
                    })
                    .catch(err => {

                    });
            }
        }
        if (correo.length <= 0 && correo2.length <= 0) {
            return AR.enviarError(
                "No se encontró un usuario con este correo.",
                res,
                404
            );
        }


    } catch (err) {
        next(err);
    }
});
module.exports = router;