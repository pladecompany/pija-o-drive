const funciones = require("./funciones");
 

const MensajeModel = require("../models/Chats");
var Firebase = require("../routes/firebase");
Firebase = new Firebase();

module.exports = function(socket) {
  return async function(data) {
 
    
    
    

    const _mensaje = await MensajeModel.query()
      .insert({
        id: null,
        id_usuario: data.id_usuario,
        mensaje: data.mensaje,
        fecha: new Date(),
        visto: 0,
        tabla_usuario: data.tabla_usuario,
        envio: data.envio
      })
      .catch(err => console.log(err));

      const _mensaje_nombre = await MensajeModel.raw("select c.id,id_usuario,mensaje,fecha as hora,envio,(select count(visto) from chats where envio=c.envio and id_usuario=c.id_usuario and visto=0 and tabla_usuario = 'admin')as pendientes,if(c.envio = 1,(select nom_cli from clientes where id=c.id_usuario),(select nom_pro from proveedores where id=c.id_usuario))as nombre,if(c.envio = 1,(select img_cli from clientes where id=c.id_usuario),(select img_pro from proveedores where id=c.id_usuario))as img from chats c where id="+_mensaje.id+";")
        .catch(err=>console.log(err));
    if(_mensaje){
           if(data.tabla_usuario!="admin"){
            //enviar emit al cliente o profesional
              let socketReceptor = funciones.encontrarSocket(data.id_usuario, data.tablausuario);

                

              if (socketReceptor) {

                
                global.IO.to(socketReceptor).emit("RECIBIRMENSAJE", _mensaje_nombre[0][0]);
                //socket.broadcast.to(socketReceptor).emit("RECIBIRMENSAJE", _mensaje);

              } else {
                var dataf = {
                  id_usuario: data.id_usuario,
                  title: "NUEVO MENSAJE DE SOPORTE",
                  body: data.mensaje,
                  id_usable: _mensaje.id,
                  tipo_noti: "nuevo-mensaje-chat",
                  tablausuario: data.tabla_usuario
                };
                Firebase.enviarPushNotification(dataf, function(push){ console.log(push); });
              }




           } else {
            //enviar emit al admin
            IO.sockets.emit("RECIBIRMENSAJE-ADMIN", _mensaje_nombre[0][0]);
           }



    }
 
  }
}
