"use strict";

const fs = require("fs");
//const ruta = (__dirname + "/../assets/logo.png")
//var bitmap = fs.readFileSync(ruta)

const config = require("../config");

//const logo = bitmap.toString('base64');

module.exports = function(data) {
    return `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
 

<body style="background-color: #fff; -webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;width: 100%!important;">

    <table style="background-color: #fff; width: 100%;font-family: HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;">
        <tr>
            <td style="margin: 0 auto!important;padding: 0;display: block!important;max-width: 600px!important;clear: both!important;">
                <div style="margin: 0 auto;padding: 15px;max-width: 600px;display: block;">
                    <table style="background-color: #fff;border: 2px solid #e1bdff; width: 100%;">
                        <tr>
                            <td style="padding: 30px 10px;">
                                <center><img src="${config.dominioWeb}static/img/logo.png" style="width: 200px;"></center>
                            </td>
                        </tr>
                        <tr><td></td></tr>
                        <tr style="margin: 0;width: 100%;background: #e8ebff;">
                            <td style="padding: 20px;">
                            <h3 style="line-height: 1.1;margin-bottom: 15px;color: #008CFF;font-weight: 500;font-size: 27px;">Pijaos drive</h3>
                                <h3 style="line-height: 1.1;margin-bottom: 15px;color: #008CFF;font-weight: 500;font-size: 27px;">Hola, ${data.nombre}</h3>
                                <p style="margin-bottom: 10px;font-weight: normal;font-size: 18px;line-height: 1.6;color: #565656;">Su acceso ha sido restaurado con éxito</p>
                                
                                
                                <table width="100%" style="width: 100%;">
                                    <tr>
                                        <td>
                                        <p>Su nueva contraseña es: <b>${
                                            data.contrasena
                                        }</b></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>`;
};