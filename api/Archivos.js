const multer = require("multer");
const crypto = require("crypto");

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, __dirname + "/public/uploads/");
    },
    filename: function(req, file, cb) {
        console.log("Archivo: "+ file.originalname + " | " + file.mimetype);
        let customFileName = crypto.randomBytes(18).toString("hex"),
            fileExtension = file.originalname.split(".")[1]; // get file extension from original file name
        cb(null, customFileName + "." + fileExtension);
    }
});

module.exports = multer({
    storage: storage,
    onError: function(err, next) {
        console.log("error", err);
        next(err);
    }
});
