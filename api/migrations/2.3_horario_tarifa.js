exports.up = knex => {
    return knex.schema.createTable("horario_tarifa", table => {
        table.increments("id").primary();
        table.integer("id_tipo_vehiculo").unsigned().references("id").inTable("tipovehiculo").onDelete("cascade");
        table.time("hor_des");
        table.time("hor_has");
        table.float("preciokm", 50, 2).defaultTo(0);
        table.float("preciomin", 50, 2).defaultTo(0);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("horario_tarifa");
};