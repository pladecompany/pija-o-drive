exports.up = knex => {
    return knex.schema.createTable("plan", table => {
        table.increments("id").primary();
        table.string("tit_pla");
        table.string("des_pla");
        table.float("pre_pla", 50, 2).defaultTo(0);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("plan");
};