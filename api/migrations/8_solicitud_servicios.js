exports.up = knex => {
    return knex.schema.createTable("solicitud_servicios", table => {
        table.increments("id").primary();
        table.integer("id_cliente").unsigned().references("id").inTable("clientes").onDelete("cascade");
        table.integer("id_proveedor").unsigned().references("id").inTable("proveedores").onDelete("cascade");
        table.integer("id_tipo_veh").unsigned().references("id").inTable("tipovehiculo");
        table.string("desdeText");
        table.string("hastaText");
        table.string("lat1");
        table.string("lon1");
        table.string("lat2");
        table.string("lon2");
        table.string("distancia_sol");
        table.string("duracion_sol");
        table.float("costo_sol", 50, 2);
        table.string("carga_sol");
        table.integer("cantidad_sol");
        table.integer("estatus_sol"); // 0 pendiente , 1 esperando aceptacion cliente , 2 en proceso, 3 finalizada , 4 vencida
        table.datetime("fecha_sol");
        table.integer("puntuaciones"); // 0 cliente, 1 proveedor

    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("solicitud_servicios");
};