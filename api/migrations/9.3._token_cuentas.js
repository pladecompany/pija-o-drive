exports.up = knex => {
  return knex.schema.createTable("token_cuentas", table => {
    table.increments("id").primary();
    table.string("token",150).unique();
    table.datetime("fec_reg");
    table.integer("est_token");
    table.integer("id_usuario");
  });
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("token_cuentas");
};
