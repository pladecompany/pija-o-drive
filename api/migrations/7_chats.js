exports.up = knex => {
  return knex.schema.createTable("chats", table => {
    table.increments("id").primary();
    table.integer("id_usuario").nullable();
    table.integer("id_usuario_recibe");
    table.text("mensaje");
    table.datetime("fecha");
    table.integer("visto");
    table.string("tabla_usuario");//cliente o proveedor
    table.integer("envio");//cliente o proveedor
  });
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("chats");
};
