exports.up = knex => {
    return knex.schema.createTable("tipovehiculo", table => {
        table.increments("id").primary();
        table.string("nombre_veh");
        table.string("icono_veh");
        table.integer("est_veh").defaultTo(1); // 0 inactivo y 1 activo
        table.float("preciokm", 50, 2).defaultTo(0);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("tipovehiculo");
};