exports.up = function(knex, Promise) {
    return knex.schema.createTable("mis_domicilios", table => {
        table.increments("id").primary();
        table.integer("id_cliente").unsigned().references("id").inTable("clientes").onDelete("cascade");
        table.string("nom_dom", 50);
        table.string("dir_dom");
        table.string("lat_dom", 100).defaultTo(null);
        table.string("lon_dom", 100).defaultTo(null);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists("mis_domicilios");
};