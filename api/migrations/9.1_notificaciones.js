exports.up = knex => {
    return knex.schema.createTable("notificaciones", table => {
        table.increments("id").primary();
        table.integer("id_usuario_receptor").nullable();
        table.string("tit_noti");
        table.text("des_noti");
        table.string("tipo_noti"); // limpieza autos, encargado asignado
        table.datetime("fec_reg_noti");
        table.integer("est_noti"); // 0 No leida, 1 leida
        table.integer("id_usable").nullable();
        table.string("tablausuario").nullable();
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("notificaciones");
};
