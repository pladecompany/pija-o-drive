exports.up = knex => {
    return knex.schema.createTable("mensajes_pre", table => {
        table.increments("id").primary();
        table.string("tit_men");
        table.string("des_men");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("mensajes_pre");
};