exports.up = knex => {
    return knex.schema.createTable("reputacion", table => {
        table.increments("id").primary();
        table.integer("id_cliente").unsigned().references("id").inTable("clientes").onDelete("cascade");
        table.integer("id_proveedor").unsigned().references("id").inTable("proveedores").onDelete("cascade");
        table.integer("voto1"); // en base a 5 estrellas
        table.integer("voto2"); // en base a 5 estrellas
        table.integer("voto3"); // en base a 5 estrellas
        table.string("comentario_rep");
        table.datetime("fecha_rep");
        table.integer("id_solicitud").unsigned().references("id").inTable("solicitud_servicios");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("reputacion");
};