exports.up = knex => {
  return knex.schema.createTable("clientes", table => {
    table.increments("id").primary();
    table.string("nom_cli");
    table.string("tel_cli", 15);
    table.string("cedula_cli",20).unique();
    table.string("dir_cli");
    table.string("cor_cli", 150).unique();
    table.string("pas_cli");
    table.integer("est_cli").defaultTo(0);
    table.integer("push_cli").defaultTo(0);
    table.string("whatsapp_cli",20).unique();
    table.string("img_cli");
    table.string("lat_cli", 100).defaultTo(null);
    table.string("lon_cli", 100).defaultTo(null);
    table.string("token",150).unique();
    table.datetime("fec_reg");
  }).raw('ALTER TABLE clientes AUTO_INCREMENT = 2000');
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("clientes");
};
