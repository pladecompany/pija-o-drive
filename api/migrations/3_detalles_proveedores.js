exports.up = knex => {
    return knex.schema.createTable("detalles_proveedores", table => {
        table.increments("id").primary();
        table.integer("id_proveedor").unsigned().unique().references("id").inTable("proveedores").onDelete("cascade");
        table.integer("id_tipo_vehiculo").unsigned().references("id").inTable("tipovehiculo");
        table.string("img_cedula");
        table.string("referencia_personal");
        table.string("tel_referencia", 15);
        table.string("marca_veh");
        table.string("matri_veh");
        table.string("img_fre");
        table.string("img_lat");
        table.string("img_tra");

    });
};
exports.down = knex => {
    return knex.schema.dropTableIfExists("detalles_proveedores");
};