exports.up = knex => {
    return knex.schema.createTable("admins", table => {
        table.increments("id").primary();
        table.string("user");
        table.string("correo", 150).unique();
        table.string("pass");
        table.string("token", 150).unique();
        table.integer("tip_adm");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("admins");
};
