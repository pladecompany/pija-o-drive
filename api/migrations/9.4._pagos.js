exports.up = knex => {
    return knex.schema.createTable("pagos", table => {
        table.increments("id").primary();
        table.integer("id_proveedor").unsigned().references("id").inTable("proveedores").onDelete("cascade");
        table.string("plan");
        table.float("valor", 50, 2).defaultTo(0);
        table.integer("estatus");
        table.string("concepto");
        table.datetime("fecha_reg");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("pagos");
};