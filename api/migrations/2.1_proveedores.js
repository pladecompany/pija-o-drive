exports.up = knex => {
  return knex.schema.createTable("proveedores", table => {
    table.increments("id").primary();
    table.string("nom_pro");
    table.string("tel_pro", 15);
    table.string("cedula_pro",20).unique();
    table.string("dir_pro");
    table.string("cor_pro", 150).unique();
    table.string("pas_pro");
    table.integer("est_pro").defaultTo(0);
    table.integer("estatus_verificado").defaultTo(0);
    table.integer("push_pro").defaultTo(0);
    table.string("whatsapp_pro",20).unique();
    table.integer("id_plan").unsigned().references("id").inTable("plan");
    table.string("img_pro");
    table.string("lat_pro", 100).defaultTo(null);
    table.string("lon_pro", 100).defaultTo(null);
    table.string("token",150).unique();
    table.datetime("fec_reg");
  }).raw('ALTER TABLE proveedores AUTO_INCREMENT = 1000');
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("proveedores");
};
