<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
include_once('ruta.php');
?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Trabajo en belleza">
	<meta name="keywords" content="Trabajo en belleza">
	<meta name="author" content="Trabajo en belleza">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- For iPhone -->
	<meta name="msapplication-TileColor" content="#00bcd4">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<title>PijasoDRIVE</title>
	<link rel="shortcut icon" href="../static/img/favicon.png" />
	<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">

	<!-- CORE CSS   --> 
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/materialize.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/style.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/font-awesome/css/all.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/responsive.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/select2/select2.min.css"/>

	<script type="text/javascript" src="../static/js/jquery.min.js"></script>
	<script src="../static/js/materialize.min.js"></script>
	
	<script type="text/javascript">
		if(window.localStorage.getItem("user") == null)
		{
			window.location = "../";	
		}
	</script>
</head>

<style type="text/css">
	.side-nav{
		width: 250px !important;
	}
</style>

<body>
	<header>
		<nav class="red lighten-2" role="navigation">
			<div class="nav-wrapper">
				<a class="brand-logo" href="?op=inicio">
					<img src="../static/img/logo.png">
				</a>

				<a class="button-collapse" data-activates="nav_mobile" href="javascript:;">
				</a>

				<ul class="right ">
					<li>
						<a class="dropdown-trigger" data-target="menu"  href="#">
							<i class="fa fa-th"></i>
						</a>
						<ul id='menu' class='dropdown-content dp2 mCustomScrollbar' data-mcs-theme="dark">
							<li class="" id="menu-filter">
								<div class="row">
									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=inicio">
											<i class="fa fa-home fa-2x m-0"></i>
											<div class="center m-0 inDiv">Inicio</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=clientes">
											<i class="fa fa-users fa-2x m-0"></i>
											<div class="center m-0 inDiv">Clientes</div>
										</a>
									</div>

									<div class="col s4 p-0 center" id="ver-admin" style="display:none;">
										<a class="p-0" href="?op=administradores">
											<i class="fa fa-lock fa-2x m-0"></i>
											<div class="center m-0 inDiv">Administradores</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=mensajesmasivos">
											<i class="fa fa-comments fa-2x m-0"></i>
											<div class="center m-0 inDiv">Mensajes masivos</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=conductores">
											<i class="fa fa-address-card fa-2x m-0"></i>
											<div class="center m-0 inDiv">Conductores</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=controldepagos">
											<i class="fas fa-book-open fa-2x m-0"></i>
											<div class="center m-0 inDiv">Control de pagos</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=chat">
											<i class="fa fa-comments fa-2x m-0"></i>
											<div class="center m-0 inDiv">Chat</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=tarifas">
											<i class="fas fa-server fa-2x m-0"></i>
											<div class="center m-0 inDiv">Tarifas</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=planes">
											<i class="fa fa-layer-group fa-2x m-0"></i>
											<div class="center m-0 inDiv">Planes</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=tipovehiculo">
											<i class="fa fa-car fa-2x m-0"></i>
											<div class="center m-0 inDiv">Tipo de vehículo</div>
										</a>
									</div>

									<div class="col s4 p-0 center">
										<a class="p-0" href="?op=mensaje">
											<i class="fa fa-comments fa-2x m-0"></i>
											<div class="center m-0 inDiv">Mensajes prestablecidos</div>
										</a>
									</div>
								</div>
							</li>
						</ul>
					</li>

					<li>
						<a href="?op=notificaciones" class="dropdown-button" id="icon_noti" href="javascript:;">
							<i class="fa fa-bell-slash" id="icon_notifi"></i>
						</a>
					</li>

					<li>
						<a class="dropdown-trigger" data-target="configuracion">
							<img src="../static/img/user.png" class="img-nav circle" id="img-perfil" >
						</a>
						
						<ul id="configuracion" class="dropdown-content dp3">
							<li><a href="?op=cambiarclave">Cambiar contraseña</a></li>
							<li><a href="#modal-salir" class="waves-effect waves-block waves-light modal-trigger">Salir</a></li>
						</ul>
					</li>
				</ul>

				<ul class="right">
                </ul>
			</div>
		</nav>
	</header>

	<div id="modal-salir" class="modal" style="width:50%;">
		<div class="modal-content text-center">
			<center><img src="../static/img/logo.png" width="15%"></center>
			<h3> ¿Desea cerrar sesión?</h3>
			<a href="salir.php" id="" class="btn btn-orange modal-action bt_salir" style="min-width: 100px;">Sí</a>
			<button class="btn btn-orange modal-action modal-close" style="min-width: 100px;">No</button>
		</div>
	</div>
	
	<?php
		include_once($ruta);
	?>

	<footer class="footer down">
		<div class="row p-0">
			<div class="col s12 text-center">
				<p class="text-bold">Copyright 2019 pijaodrive</p>
			</div>
		</div>
	</footer>

	<script src="../static/js/menu.js"></script>
	<script src="../static/js/main.js"></script>
	<script src="../static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script src="../static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script src="../static/lib/JTables/js/dataTables.responsive.min.js"></script>
	<script src="../static/lib/select2/select2.min.js"></script>
	<script src='../static/js/backend-panel/general.js'></script>
	<script src='../static/js/socket/socket.js'></script>
	<script src='../static/js/backend-panel/socket_events.js'></script>

	<script>
		$(document).ready(function() {
			$('.select2').select2();
		});
	</script>

	<script>
		(function($){
			$(window).on("load",function(){
				//$(".content").mCustomScrollbar();
			});
		})(jQuery);
	</script>

	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "../static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
			datanoti = {
				limit:5,
	            ida:user.id,
	            token:user.token
	        }
			$.ajax({
	            url : dominio+'notificaciones/admin',
	            type: 'GET',
	            data:datanoti,
	            success:function(data){
	            	if(data.length>0){
	            		$("#icon_notifi").removeClass("fa-bell-slash");
	            		$("#icon_notifi").addClass("fa-bell");
	            	}
	                
	            }

	        })
		} );
	</script>

	<script>
		var acentos=function(cadena)
			{
				var chars={
					"á":"a", "é":"e", "í":"i", "ó":"o", "ú":"u",
					"à":"a", "è":"e", "ì":"i", "ò":"o", "ù":"u", "ñ":"n",
					"Á":"A", "É":"E", "Í":"I", "Ó":"O", "Ú":"U",
					"À":"A", "È":"E", "Ì":"I", "Ò":"O", "Ù":"U", "Ñ":"N"}
				var expr=/[áàéèíìóòúùñ]/ig;
				var res=cadena.replace(expr,function(e){return chars[e]});
				return res;
			}
	</script>
</body>
</html>
