<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">MENSAJES MASIVOS</h5>
	
	<br>
	<div class="row">
		<div class="col s3">
			<div class="form-group">
				<label class="text-bold">Tipo de usuario</label>
				<select id="tip_usu" name="tip_usu" class="input-field">
					<option value="">Seleccionar</option>
					<option value="1">Conductores</option>
					<option value="2">Clientes</option>
				</select>
			</div>
		</div>
		<!--<div class="col s3 fil_usu">
			<div class="form-group">
				<label class="text-bold">Buscar por</label>
				<select id="bus_usu" disabled name="bus_usu" class="input-field">
					<option value="">Seleccionar</option>
					<option value="1">Viajes</option>
					<option value="2">Valoración(Amabilidad)</option>
				</select>
			</div>
		</div>
		<div class="col s3 fil_con" style="display:none;">
			<div class="form-group">
				<label class="text-bold">Buscar por</label>
				<select id="bus_con" disabled name="bus_con" class="input-field" >
					<option value="">Seleccionar</option>
					<option value="1">Viajes</option>
					<option value="2">Valoración</option>
					<option value="3">Limpieza</option>
					<option value="4">Amabilidad</option>
					<option value="5">Puntualidad</option>
				</select>
			</div>
		</div>
		<div class="col s2">
			<div class="form-group">
				<label class="text-bold">Puntuación/Cantidad</label>
				<select id="pun" disabled name="pun" class="input-field">
					<option value="">Seleccionar</option>
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
				<input id="can" name="can" type="text" class="form-app limpiar_form number" style="display:none;" placeholder="">
			</div>
		</div>
		<div class="col s2">
			<div class="form-group">
				<label class="text-bold">Condición</label>
				<select id="cond" name="cond" class="input-field">
					<option value="">Seleccionar</option>
					<option value="1">Menor</option>
					<option value="2">Menor e Igual</option>
					<option value="3">Igual</option>
					<option value="4">Mayor</option>
					<option value="5">Mayor e Igual</option>
				</select>
			</div>
		</div>-->
		<div class="col s1">
			<br>
			<a href="#" onclick="return false" class="btn btn-orange btn-bus">Buscar</a>
		</div>
		<div class="col s1">
			<br>
			<a href="#modal" onclick="return false" class="btn btn-orange modal-trigger">Mensaje</a>
		</div>
	</div>
	<form action="" method="" id="form_msj" onsubmit="return false;">
		<table class="table display dt-responsive tabla" id="table" cellspacing="0" width="100%">
			<thead class="">
				<tr>
					<th>#</th>
					<th>COD</th>
					<th>Nombre</th>
					<th>Correo</th>
					<th>Tipo de usuario</th>
					<th>Viajes</th>
					<th>Valoración</th>
					<th class="txt-val">Limpieza / Amabilidad / Puntualidad</th>
				</tr>
			</thead>

			<tbody>
				
			</tbody>
		</table>
	</form>
</section>

<!-- Modal -->
<div id="modal" class="modal">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"><i class="fa fa-envelope fa-2x m-0"></i> Mensaje Masivo</h6>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<input type="hidden" class="limpiar_form" name="id_admin" id="id_admin" value=""> 
			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Titulo</label>
						<input id="tit" name="tit" type="text" class="form-app limpiar_form" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Descripción</label>
						<textarea id="des" name="des" class="form-app limpiar_form"></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange btn-save">Enviar</a>
				</div>
			</div>
		</form>
	</div>
</div>
