<section class="mt-4 mb-4 p-4">
	<div class="row">
		<div class="col s12 m4 offset-m4">
			<div class="card boxPerfil">
				<div class="boxHead">
					<h6><i class="fa fa-user icon ml-2"></i> CAMBIAR CONTRASEÑA</h6>
				</div>

				<div class="p-2">
					<form action="" method="" id="cambiar_clave">
						<div class="row">
							<div class="col s12 flexCenter">
								<img src="../static/img/user.png" alt="" class="boxPerfil--img" id="perfil">
							</div>
						</div>

						<div class="row mb-0">
							<div class="col s12">
								<div class="input-field">
									<p for="con_cla">Contraseña actual</p>
									<input type="password" id="con_cla" name="pass" class="form-app">
								</div>
							</div>
						</div>

						<div class="row mb-0">
							<div class="col s12">
								<div class="input-field">
									<p for="nue_cla">Nueva contraseña</p>
									<input type="password" id="nue_cla" name="pass_new" class="form-app">
								</div>
							</div>
						</div>

						<div class="row mb-0">
							<div class="col s12">
								<div class="input-field">
									<p for="rep_cla">Repetir contraseña</p>
									<input type="password" id="rep_cla" name="pass_new_con" class="form-app">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 text-center pb-2">
								<button class="btn waves-effect btn-orange">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/backend-panel/cambiarclaveadmin.js"></script>