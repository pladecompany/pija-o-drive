<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">TARIFAS</h5>

	<div class="row pt-4">
		<div class="col s12 m10 offset-m1">
			
				<div class="row">
					<div id="listar_vehiculos"></div>
				</div>
		</div>
		<div class="col s12 m10 offset-m1">
				<div class="row">
					<form action="" method="" id="form_tarifa_con" onsubmit="return false;">
						<div class="col s12 m2 text-center offset-m3">
							<h6 class="text-bold">Banderazo $</h6> 
							<input type="text" name="bande" class="number text-center" id="bande" value=""> 
						</div>

						<div class="col s12 m2 text-center">
							<h6 class="text-bold">Km Mínimos</h6>
							<input type="text" name="km_min" class="number text-center" id="km_min" value=""> 
						</div>

						<div class="col s12 m2 text-center">
							<h6 class="text-bold">Precio Mínimo $</h6>
							<input type="text" name="pre_km" class="number text-center" id="pre_km" value=""> 
						</div>
						<div class="col s12 center">
							<a href="#" onclick="return false" class="btn btn-orange btn-save-tar">Guardar</a>
						</div>
					</form>
				</div>
		</div>
	</div>



</section>
<!-- Modal -->
<div id="horario" class="modal modalFull">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"><i class="fa fa-calendar fa-2x m-0"></i> Horario (<span class="tit-hor"></span>)</h6>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form_tarifa" onsubmit="return false;">
			
			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange add-horario"><i class="fa fa-plus m-0"></i> Nuevo Horario</a>
				</div>
			</div>
			<hr>
			
			<input type="hidden" name="id_auto" id="id_auto" value=""> 
			<div class="row list_hor">
					
			</div>
			
			<hr>
			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange btn-save-hor">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>
