<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">TIPOS DE VEHICULOS</h5>
	<div class="text-right mb-3">
		<a href="#modalVehiculos" class="btn btn-orange BTNuevo"><i class="fa fa-plus"></i> Nuevo</a>

	</div>
	<br>
	<table class="table display dt-responsive tablaTipo" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Img</th>
				<th>Nombre</th>
				<th>Acciones</th>
			</tr>
		</thead>

		<tbody>
			
		</tbody>
	</table>
</section>

<!-- Modal -->
<div id="modalVehiculos" class="modal">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"><i class="fa fa-car fa-2x m-0"></i> Tipo de vehículos</h6>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<input type="hidden" name="id_tipo_ve" id="id_tipo_ve">
			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Nombre</label>
						<input id="nombre_veh" name="nombre_veh" type="text" class="form-app" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<label class="text-bold">Selecciona tipo de vehículo</label>
					<select id="icono_veh" name="icono_veh" class="input-field">
						 <option value="bicycle.png" data-icon="../static/img/transporte/bicycle.png">Bicicleta</option>
						 <option value="car.png" data-icon="../static/img/transporte/car.png">Carro</option>
						 <option value="crane-truck.png" data-icon="../static/img/transporte/crane-truck.png">Camión Grua</option>
						 <option value="pickup-truck.png" data-icon="../static/img/transporte/pickup-truck.png">Pickup</option>
						 <option value="scooter.png" data-icon="../static/img/transporte/scooter.png">Scooter</option>
						 <option value="truck.png" data-icon="../static/img/transporte/truck.png">Camión</option>
						 <option value="monopatin.png" data-icon="../static/img/transporte/monopatin.png">Monopatin</option>
						 <option value="motodelivery.png" data-icon="../static/img/transporte/motodelivery.png">Moto Delivery</option>
						 <option value="motorbike.png" data-icon="../static/img/transporte/motorbike.png">Moto Bike</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange btn-save btsave hide BTsubmit" tipo="1">Guardar</a>
					<a href="#" onclick="return false" class="btn btn-orange btn-save btedit hide BTsubmit" tipo="2">Editar</a>
				</div>
			</div>
		</form>
	</div>
</div>

