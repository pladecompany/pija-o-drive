<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">PLANES</h5>
	<div class="text-right mb-3">
		<a href="#" class="btn btn-orange" onclick='$("#form").modal("open");$(".limpiar_form").val("");'><i class="fa fa-plus"></i> Nuevo</a>

	</div>
	<br>
	<table class="table display dt-responsive plan" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Titulo</th>
				<th>Beneficios</th>
				<th>Precio</th>
				<th>Acciones</th>
			</tr>
		</thead>

		<tbody>
			
		</tbody>
	</table>
</section>
<!-- Modal -->
<div id="form" class="modal">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"><i class="fa fa-layer-group fa-2x m-0"></i> Plan</h6>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<input type="hidden" class="limpiar_form" name="id_plan" id="id_plan" value=""> 
			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Titulo</label>
						<input id="tit" name="tit" type="text" class="form-app text limpiar_form" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Beneficios</label>
						<textarea class="textarea limpiar_form" id="des" name="des"></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Precio ($)</label>
						<input id="pre" name="pre" type="text" class="form-app number limpiar_form" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange btn-save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>

