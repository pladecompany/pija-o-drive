<?php
  
?>
<page>
	<div style="background: #e30016; height: 20px;text-align:center;">
		<h4 style="color:#fff;">CONDUCTOR</h4>
	</div>
    <br>
	<table align="center" style="width:100%;max-width:100%;margin-top: 10px; border:0px;" cellspacing="0">
		<tr style="color: #fff;">
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;" >NOMBRE</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;" >TELÉFONO</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;" >CÉDULA</th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $data['nom_pro'] ?></td>
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $data['tel_pro'] ?></td>
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $data['cedula_pro'] ?></td>
		</tr>
		
		<tr style="color: #fff;">
				<th colspan="3" style="width:99%;text-align:center;background: #3a3a3a;padding: 7px;" >DIRECCIÓN</th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td colspan="3" style="width:99%;text-align:center;padding: 7px;"><?php echo $data['dir_pro'] ?></td>
		</tr>

		<tr style="color: #fff;">
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">EMAIL</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">WHATSAPP</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">ESTATUS</th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $data['cor_pro'] ?></td>
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $data['whatsapp_pro'] ?></td>
				<td style="width:33%;text-align:center;padding: 7px;">
					<?php 
						if($data['est_pro']==1)
							echo 'ACTIVO';
						else if($data['est_pro']==2)
							echo 'BLOQUEADO';
						else
							echo 'PAUSADO';
					?>
				</td>
		</tr>

		<tr style="color: #fff;">
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">ESTATUS CHOFER</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">PLAN</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">COD</th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td style="width:33%;text-align:center;padding: 7px;">
					<?php 
						if($data['estatus_verificado']==1)
							echo 'VERIFICADO';
						else if($data['estatus_verificado']==2)
							echo 'RECHAZADO';
						else
							echo 'PENDIENTE';
					?>
				</td>
				<td style="width:33%;text-align:center;padding: 7px;">
					<?php echo $plan; ?>
				</td>
				<td style="width:33%;text-align:center;padding: 7px;">0</td>
		</tr>

		<tr style="color: #fff;">
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">VIAJES</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">PUNTOS</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;"></th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td style="width:33%;text-align:center;padding: 7px;">
					<?php 
						if($data['Detalles']){
							if($data['Detalles']['Viajes_total'])
								echo $data['Detalles']['Viajes_total'];
							else
								echo "0";
						}else
							echo "0";
					?>
				</td>
				<td style="width:33%;text-align:center;padding: 7px;">
					<?php 
						if($data['Detalles']){
							if($data['Detalles']['Reputacion'])
								echo $data['Detalles']['Reputacion'];
							else
								echo "0";
						}else
							echo "0";
					?>
				</td>
				<td style="width:33%;text-align:center;padding: 7px;"></td>
		</tr>

		<tr style="color: #fff;">
				<th colspan="3" style="width:99%;text-align:center;background: #3a3a3a;padding: 7px;" >TIPO VEHÍCULO</th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td colspan="3" style="width:99%;text-align:center;padding: 7px;">
					<?php 
						if($data['Detalles']){
							if($data['Detalles']['Vehiculo'])
								echo $data['Detalles']['Vehiculo']['nombre_veh'];
							else
								echo "Sin registrar";
						}else
							echo "Sin registrar";

					 ?>
				</td>
		</tr>
		<?php
			if($data['Detalles']){
				$mar=$data['Detalles']['marca_veh'];
				$mat=$data['Detalles']['matri_veh'];
				$ref=$data['Detalles']['referencia_personal'];
			}else{
				$mar='Sin registrar';
				$mat='Sin registrar';
				$ref='Sin registrar';
			}
		?>
		<tr style="color: #fff;">
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">MARCA VEHÍCULO</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">MATRICULA VEHÍCULO</th>
				<th style="width:33%;text-align:center;background: #3a3a3a;padding: 7px;">REFERENCIA PERSONAL</th>
		</tr>
		<tr style="color: rgba(0, 0, 0, 0.87);">
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $mar ?></td>
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $mat ?></td>
				<td style="width:33%;text-align:center;padding: 7px;"><?php echo $ref ?></td>
		</tr>
	</table>

	
	<br>

	


	<br>
	<br>
</page>
