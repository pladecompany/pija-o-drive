<style type="text/css">
	.cir-sts{
		width: 2rem;
    	height: 2rem;
    	display: inline-flex;
	}
	td, th{
		text-align: center;
	}
	th:nth-child(1), th:nth-child(2){
       	padding: 10px 26px 10px 10px !important;
    }
    td:nth-child(1), td:nth-child(2){
       	padding: 10px 18px 10px 18px !important;
    }
</style>
<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">CONDUCTORES</h5>
	<div class="row mb-0">
		<div class="col s4 m2 p-1 offset-m3">
	        <h6 class="center"><span class="trans-act cir-sts"></span> Activo</h6> 
	    </div>
	    <div class="col s4 m2 p-1 ">
	        <h6 class="center"><span class="trans-blo cir-sts"></span> Bloqueado</h6> 
	    </div>
	    <div class="col s4 m2 p-1 ">
	        <h6 class="center"><span class="trans-pau cir-sts"></span> Pausado</h6> 
	    </div>
	</div>
	<table class="table display dt-responsive prove" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>COD</th>
				<th>Nombre</th>
				<th>Vehículo</th>
				<th>Teléfono</th>
				<th>Cédula</th>
				<th>Viajes</th>
				<th>Puntos</th>
				<th>Estatus</th>
				<th>Estatus chofer</th>
				<!--<th>PUSH</th>
				<th>Whatsapp</th>-->
				<th style="width: 100px;">Plan</th>
				<th style="width: 150px;">Acciones</th>
			</tr>
		</thead>

		<tbody>
		</tbody>
	</table>
</section>

<div id="status1" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> <span class="txt-obs">Cambiar Estatus del Chofer</span></span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>
	<div class="modal-content">
		<div class="col s12">
			<h6 class="text-bold">Estatus final del conductor</h6>
			<input type="hidden" name="id_cli1" id="id_cli1" value=""> 
			<select id="sts_edit1" name="sts_edit1" class="browser-default input-field">
				<option value="1">Verificado</option>
				<option value="2">Rechazado</option>
				<option value="0">Pendiente</option>
			</select>
		</div>
		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange save-sts-cli1">Guardar</a>
		</div>
	</div>
</div>
<div id="status" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> <span class="txt-obs">Cambiar Estatus</span></span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>
	<div class="modal-content">
		<div class="col s12">
			<h6 class="text-bold">Estatus del conductor</h6>
			<input type="hidden" name="id_cli" id="id_cli" value=""> 
			<select id="sts_edit" name="sts_edit" class="browser-default input-field">
				<option value="1">Activo</option>
				<option value="2">Bloqueado</option>
				<option value="0">Pausado</option>
			</select>
		</div>
		<div class="col s12">
			<h6 class="text-bold">Comentario</h6>
			 <textarea name="" class="input-field" id="comentario"></textarea>
			
		</div>
		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange save-sts-cli">Guardar</a>
		</div>
	</div>
</div>

<div id="planes" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> <span class="txt-obs">Cambiar Plan</span></span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>
	<div class="modal-content">
		<div class="col s12">
			<h6 class="text-bold">Planes</h6>
			<input type="hidden" name="id_prove_plan" id="id_prove_plan" value=""> 
			<select id="plan_edit" name="plan_edit" class="browser-default input-field">
			</select>
		</div>
		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange save-plan-pro">Guardar</a>
		</div>
	</div>
</div>

<div id="modalProveedor" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> </span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>

	<div class="modal-content">
		<div class="row">
			<div class="col s6">
				<ul class="collection with-header">
					<li class="collection-header">
						<h4 class="clr_orange">Datos</h4>
					</li>
					<li class="collection-item"><b>Nombre: </b> <span class="nom_pro"></span></li>
					<li class="collection-item"><b>Cédula: </b> <span class="ced_pro"></span></li>
					<li class="collection-item"><b>Teléfono Whatsapp: </b> <span class="tlf_pro"></span></li>
					<li class="collection-item"><b>Dirección: </b> <span class="dir_pro"></span></li>
					<li class="collection-item"><b>Email: </b> <span class="ema_pro"></span></li>
					<li class="collection-item"><b>Quejas: </b> <span class="que_pro"></span></li>
					<li class="collection-item"><b>Viajes: </b> <span class="via_pro"></span></li>
					<li class="collection-item"><b>Puntos: </b> <span class="pun_pro"></span></li>
					<li class="collection-item"><b>Estatus: </b> <span class="sts_pro"></span></li>
					<li class="collection-item"><b>Estatus chofer: </b> <span class="stsf_pro"></span></li>
					<!--<li class="collection-item"><b>Whatsapp: </b> <span class="wha_pro"></span></li>-->
					<li class="collection-item"><b>COD: </b> <span class="cod_pro"></span></li>
					<li class="collection-item"><b>Documentos: </b> <span class="doc_pro"></span></li>
					<li class="collection-item"><b>Plan: </b> <span class="pla_pro"></span></li>
					<li class="collection-item"><b>Tipo Vehículo: </b> <span class="tip_pro"></span></li>
					<li class="collection-item"><b>Marca Vehículo: </b> <span class="mar_pro"></span></li>
					<li class="collection-item"><b>Matricula Vehículo: </b> <span class="mat_pro"></span></li>
					<li class="collection-item"><b>Referencia personal: </b> <span class="ref_pro"></span></li>
					<li class="collection-item"><b>Teléfono (referencia personal): </b> <span class="tel_referencia"></span></li>
					
				</ul>

			</div>

			<div class="col s6">
				<div class="row">
					<div class="col s12 center">
						<label>Imagen</label><br>
						<span class="ver_img1"></span>
					</div>
					<div class="col s12 center ver-com-pro">
						<br>
						<label>Fotos del Vehículo:</label>
						<br>
						<label>Frente</label><br>
						<span class="ver_img2"></span>
					</div>
					<div class="col s12 center ver-com-pro">
						<label>Lateral</label><br>
						<span class="ver_img3"></span>
					</div>
					<div class="col s12 center ver-com-pro">
						<label>Trasera</label><br>
						<span class="ver_img4"></span>
					</div>
					<div class="col s12 center ver-com-pro">
						<br>
						<label>Cédula</label><br>
						<span class="ver_img5"></span>
					</div>
				</div>
			</div>
		</div>

		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange modal-close">Cerrar</a>
		</div>
	</div>
</div>

<div id="puntos" class="modal modalFull">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<span class="ml-3"> <span class="txt-obs name-puntos"></span></span>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
		</div>
	</div>
	<div class="modal-content">
		<div class="row">
			<div class="col s6">
				<input type="hidden" id="id_reg_pun" value=""> 
				<h6 class="text-bold">Mes</h6>
				<?php $mesact= date("m");?>
				<select id="mes_pun" class="browser-default input-field">
						<option value="1" <?php if($mesact==1) echo "selected";?>>Enero</option>
						<option value="2" <?php if($mesact==2) echo "selected";?>>Febrero</option>
						<option value="3" <?php if($mesact==3) echo "selected";?>>Marzo</option>
						<option value="4" <?php if($mesact==4) echo "selected";?>>Abril</option>
						<option value="5" <?php if($mesact==5) echo "selected";?>>Mayo</option>
						<option value="6" <?php if($mesact==6) echo "selected";?>>Junio</option>
						<option value="7" <?php if($mesact==7) echo "selected";?>>Julio</option>
						<option value="8" <?php if($mesact==8) echo "selected";?>>Agosto</option>
						<option value="9" <?php if($mesact==9) echo "selected";?>>Septiembre</option>
						<option value="10" <?php if($mesact==10) echo "selected";?>>Octubre</option>
						<option value="11" <?php if($mesact==11) echo "selected";?>>Noviembre</option>
						<option value="12" <?php if($mesact==12) echo "selected";?>>Diciembre</option>
				</select>
				<br>
			</div>
			<div class="col s6">
				<h6 class="text-bold">Año</h6>
				<?php $anoact= date("Y");?>
				<select id="ano_pun" class="browser-default input-field">
						<?php 

	                        for ($i=$anoact; $i >= 2019; $i--) { 
	                        	if($anoact==$i)
	                            	echo '<option value="'.$i.'" selected>'.$i.'</option>';
	                            else
	                            	echo '<option value="'.$i.'">'.$i.'</option>';
	                        }
	                    ?>
				</select>
				<br>
			</div>
		</div>
		<div class="col s12 text-center">
			<table class="table display dt-responsive" id="table_p" cellspacing="0" width="100%">
				<thead class="">
					<tr>
						<th class="text-center">No.</th>
						<th class="text-center">Cliente</th>
						<th class="text-center">Teléfono</th>
						<th class="text-center">Limpieza</th>
						<th class="text-center">Amabilidad</th>
						<th class="text-center">Puntualidad</th>
						<th class="text-center">Comentario</th>
					</tr>
				</thead>

				<tbody>
				</tbody>
			</table>
			<form method="post"  id="formpuntaje" target="blank" action="vistas/pdf_php_puntaje_p.php">
				<textarea class="hide" name="tablainfo" style="display: none;" id="tablainfopuntos" required="required"></textarea>
			</form>
			<a href="#" class="btn btn-orange" id="btn_pdf_punta" onclick='$("#formpuntaje")[0].submit();'>Exportar <i class="fa fa-file-pdf"></i></a>
			<a href="#" class="btn btn-orange modal-close">Cerrar</a>
		</div>
	</div>
</div>
<div id="mensaje" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> <span class="txt-obs">Enviar Mensaje</span></span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>
	<div class="modal-content">
		<div class="col s12">
			<h6 class="text-bold">Mensaje</h6>
			<input type="hidden" name="id_cli_msj" id="id_cli_msj" value=""> 
			<textarea id="msj_cli" name="msj_cli" class="form-app"></textarea>
		</div>
		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange save-msj-cli">Enviar</a>
		</div>
	</div>
</div>
<form method="post" style="display: none;" id="formprint" target="blank" action="vistas/pdf_php_proveedor.php">
	<textarea class="hide" name="tablainfo" id="tablainfo" required="required"></textarea>
	<textarea class="hide" name="tablaplan" id="tablaplan" required="required"></textarea>
</form>