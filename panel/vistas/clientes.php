<style type="text/css">
	.cir-sts{
		width: 2rem;
    	height: 2rem;
    	display: inline-flex;
	}
	td, th{
		text-align: center;
	}
	th:nth-child(1), th:nth-child(2){
       	padding: 10px 26px 10px 10px !important;
    }
    td:nth-child(1), td:nth-child(2){
       	padding: 10px 18px 10px 18px !important;
    }
</style>
<section class="mt-4 p-4">
	<h5 class="clr_orange center">CLIENTES</h5>
	<div class="row mb-0">
		<div class="col s2 offset-s3">
	        <h6 class="center"><span class="trans-act cir-sts"></span> Activo</h6> 
	    </div>
	    <div class="col s2 ">
	        <h6 class="center"><span class="trans-blo cir-sts"></span> Bloqueado</h6> 
	    </div>
	    <div class="col s2 ">
	        <h6 class="center"><span class="trans-pau cir-sts"></span> Pausado</h6> 
	    </div>
	</div>
	
	<table class="table display dt-responsive clientes" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>COD</th>
				<th>Imagen</th>
				<th>Nombre</th>
				<th>Teléfono</th>
				<th>Cédula</th>
				<th>Email</th>
				<th>Viajes</th>
				<th>Puntos</th>
				<th>Estatus</th>
				<!--<th>PUSH</th>
				<th>Whatsapp</th>-->
				<th style="width: 150px;">Acciones</th>
			</tr>
		</thead>

		<tbody>
		</tbody>
	</table>
</section>
<div id="status" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> <span class="txt-obs">Cambiar Estatus</span></span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>
	<div class="modal-content">
		<div class="col s12">
			<h6 class="text-bold">Estatus</h6>
			<input type="hidden" name="id_cli" id="id_cli" value=""> 
			<select id="sts_edit" name="sts_edit" class="browser-default input-field">
				<option value="1">Activo</option>
				<option value="2">Bloqueado</option>
				<option value="0">Pausado</option>
			</select>
		</div>
		<div class="col s12">
			<h6 class="text-bold">Comentario</h6>
			 <textarea name="" class="input-field" id="comentario"></textarea>
		</div>
		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange save-sts-cli">Guardar</a>
		</div>
	</div>
</div>
<div id="mensaje" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3"> <span class="txt-obs">Enviar Mensaje</span></span>
		<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
	</div>
	<div class="modal-content">
		<div class="col s12">
			<h6 class="text-bold">Mensaje</h6>
			<input type="hidden" name="id_cli_msj" id="id_cli_msj" value=""> 
			<textarea id="msj_cli" name="msj_cli" class="form-app"></textarea>
		</div>
		<div class="text-right">
			<br>
			<a href="#" class="btn btn-orange save-msj-cli">Enviar</a>
		</div>
	</div>
</div>
<div id="puntos" class="modal modalFull">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<span class="ml-3" > <span class="txt-obs name-puntos"></span></span>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x"></i></a>
		</div>
	</div>
	<div class="modal-content">
		<div class="row">
			<div class="col s6">
				<input type="hidden" id="id_reg_pun" value=""> 
				<h6 class="text-bold">Mes</h6>
				<?php $mesact= date("m");?>
				<select id="mes_pun" class="browser-default input-field">
						<option value="1" <?php if($mesact==1) echo "selected";?>>Enero</option>
						<option value="2" <?php if($mesact==2) echo "selected";?>>Febrero</option>
						<option value="3" <?php if($mesact==3) echo "selected";?>>Marzo</option>
						<option value="4" <?php if($mesact==4) echo "selected";?>>Abril</option>
						<option value="5" <?php if($mesact==5) echo "selected";?>>Mayo</option>
						<option value="6" <?php if($mesact==6) echo "selected";?>>Junio</option>
						<option value="7" <?php if($mesact==7) echo "selected";?>>Julio</option>
						<option value="8" <?php if($mesact==8) echo "selected";?>>Agosto</option>
						<option value="9" <?php if($mesact==9) echo "selected";?>>Septiembre</option>
						<option value="10" <?php if($mesact==10) echo "selected";?>>Octubre</option>
						<option value="11" <?php if($mesact==11) echo "selected";?>>Noviembre</option>
						<option value="12" <?php if($mesact==12) echo "selected";?>>Diciembre</option>
				</select>
				<br>
			</div>
			<div class="col s6">
				<h6 class="text-bold">Año</h6>
				<?php $anoact= date("Y");?>
				<select id="ano_pun" class="browser-default input-field">
						<?php 

	                        for ($i=$anoact; $i >= 2019; $i--) { 
	                        	if($anoact==$i)
	                            	echo '<option value="'.$i.'" selected>'.$i.'</option>';
	                            else
	                            	echo '<option value="'.$i.'">'.$i.'</option>';
	                        }
	                    ?>
				</select>
				<br>
			</div>
		</div>
		<div class="col s12 text-center">
			<table class="table display dt-responsive" id="table_p" cellspacing="0" width="100%">
				<thead class="">
					<tr>
						<th class="text-center">No.</th>
						<th class="text-center">Chofer</th>
						<th class="text-center">Teléfono</th>
						<th class="text-center">Amabilidad</th>
						<th class="text-center">Comentario</th>
					</tr>
				</thead>

				<tbody>
				</tbody>
			</table>
			<form method="post"  id="formpuntaje" target="blank" action="vistas/pdf_php_puntaje_c.php">
				<textarea class="hide" name="tablainfo" style="display: none;" id="tablainfopuntos" required="required"></textarea>
			</form>
			<a href="#" class="btn btn-orange" id="btn_pdf_punta" onclick='$("#formpuntaje")[0].submit();'>Exportar <i class="fa fa-file-pdf"></i></a>
			<a href="#" class="btn btn-orange modal-close">Cerrar</a>
		</div>
	</div>
</div>
<form method="post" style="display: none;" id="formprint" target="blank" action="vistas/pdf_php_cliente.php">
	<textarea class="hide" name="tablainfo" id="tablainfo" required="required"></textarea>
</form>