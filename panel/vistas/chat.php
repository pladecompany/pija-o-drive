<div class="pl-5 pr-5">
	<div class="row boxChat my-5 mb-5">
		<div class="col s12 m3">
			<div class="boxChat__list" id="listado">
				<div class="input-field input-search">
					<input type="search" id="bus_men" name="bus_men" class="form-app" placeholder="Buscar contacto">
					<span class="fa fa-search icon_buscar"></span>
				</div>

				<div class="listado">
				
				</div>
				<!--<div class="boxChat__cont">
					<div class="row mb-0">
						<div class="col s3">
							<img src="../static/img/user.png" class="boxChat--img img-cover w100">
						</div>

						<div class="col s9">
							<p>Daniela manzanilla</p>
						</div>
					</div>
				</div>

				<div class="boxChat__cont">
					<div class="row mb-0">
						<div class="col s3">
							<img src="../static/img/user.png" class="boxChat--img img-cover w100">
						</div>

						<div class="col s9">
							<p>Daniela manzanilla</p>
						</div>
					</div>
				</div>-->
			</div>
		</div>

		<div class="col s12 m9">
			<div class="card-content init" id="init">
				<h5 class="titulo">
					No ha seleccionado ninguna conversación
				</h5>
			</div>
			<div class="card boxMensajes" id="men" style="display: none;">
				<div class="boxMensajes__head">
					<i class="fa fa-angle-left fa-2x" id="btn_goList"></i>
					<img src="../static/img/user.png" width="40px" id="msj-chat" class="circle img-cover" style="width: 40px;height:40px;">
					<h5 class="boxChat--name" id="nombre"></h5>
				</div>

				<div class="boxMensajes__cont" id="content_messages">
					<input type="number" id="id_usuario" name="" style="display: none;">
					<input type="number" id="envio" name="" style="display: none;">
					
					<ol class="boxChat--msj">
						<!--<div id="mensajes"><center><i class="fa fa-spinner fa-spin"></i></center></div>-->
						<div id="mensajes">
							<!--<li class="boxChat__left">
								<div class="avatar">
									<img src="../static/img/user.png" class="img-cover w100" draggable="false">
								</div>

								<div class="msg">
									<p class="m-0">Puede alguien ayudarme ?</p>
									<span class="timeago" title="2019-07-17T13:23:18.000Z">Hace 5 días</span>
								</div>
							</li>

							<li class="boxChat__right">
								<div class="avatar">
									<img src="../static/img/user.png" class="img-cover w100" draggable="false">
								</div>
								
								<div class="msg">
									<p class="m-0">Hola si?</p>
									<span class="timeago" title="2019-07-22 15:08:00">Hace menos de un minuto</span>
								</div>
							</li>-->
						</div>
					</ol>
				</div>

				<div class="card-footer bottom" id="bottom">
					<form id="form" method="POST" onsubmit="return false;">
						<div class="row m-0 p-3" style="border-top: 1px solid #ddd;">
							<div class="col s10">
								<style type="text/css">
									#enviar:focus {
										outline: none !important;
									}
								</style>
								<div contenteditable="true" id="enviar" class="boxTexto">
								</div>
							</div>

							<div class="col s2">
								<button type="button" onclick="return false" class="btn btn-orange send">
									<i class="fa fa-angle-right"></i>
								</button>
							</div>	
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if($_GET['envio']!='' && $_GET['id_usuario']!='' && $_GET['nombre']!=''){
		echo '<input type="hidden" value="'.$_GET['envio'].'" id="enviobuscar">';
		echo '<input type="hidden" value="'.$_GET['id_usuario'].'" id="id_usuariobuscar">';
		echo '<input type="hidden" value="'.$_GET['nombre'].'" id="nombrebuscar">';
	}else
		echo '<input type="hidden" value="0" id="enviobuscar">';
?>
<script type="text/javascript">
	/*$(document).ready(function(){
		$(".boxChat__cont").click(function(){
			$(".boxMensajes").show();
			$(".boxChat__list").hide();
		});
 
		$("#btn_goList").click(function(){
			$(".boxMensajes").hide();
			$(".boxChat__list").show();
		});
	});*/
</script>

