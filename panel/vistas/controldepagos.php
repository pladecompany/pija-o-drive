<style type="text/css">
	.select2-container{
		width: 100% !important;
	}
</style>
<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">CONTROL DE PAGOS</h5>
	<div class="text-right mb-3">
		<a href="#" class="btn btn-orange" onclick='$("#form").modal("open");$(".limpiar_form").val("");$("#pro").attr("disabled",false);$("#pro").val("").trigger("change");'><i class="fa fa-plus"></i> Nuevo</a>
	</div>
	<table class="table display dt-responsive pagost" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Imagen</th>
				<th>Nombre</th>
				<th>Cedula</th>
				<th>Teléfono</th>
				<th>Plan</th>
				<th>Monto</th>
				<th>Fecha</th>
				<th>¿Pago?</th>
				<th>Acciones</th>
			</tr>
		</thead>

		<tbody>
			<!--<tr>
				<td>1</td>
				<td>Imagen</td>
				<td>Nombre</td>
				<td>Código</td>
				<td>Plan</td>
				<td>Estatus</td>
				<td>Whatsapp</td>
				<td>PUSH</td>
				<td>Pagos realizados</td>
				<td>
					<a href="" class="btn btn-orange"><i class="fa fa-eye"></i></a>
					<a href="" class="btn btn-orange"><i class="fa fa-trash"></i></a>
				</td>
			</tr>-->
		</tbody>
	</table>
</section>


<!-- Modal -->
<div id="form" class="modal modalFull">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"><i class="fa fa-layer-group fa-2x m-0"></i> Pagos</h6>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<input type="hidden" class="limpiar_form" name="id_pago" id="id_pago" value=""> 
			<div class="row">
				<div class="col s12 m6">
					<div class="form-group">
						<label>Conductor</label>
						<select id="pro" name="pro" class="select browser-default input-field select2">
							<option value=""></option>
						</select>
					</div>
				</div>

				<div class="col s12 m6">
					<div class="form-group">
						<input id="pla" name="pla" type="hidden" class="form-app number limpiar_form" placeholder="">
					</div>
				</div>

				<div class="col s12 m6">
					<div class="form-group">
						<label>Concepto</label>
						<textarea class="textarea limpiar_form" id="con" name="con"></textarea>
					</div>
				</div>

				<div class="col s12 m6">
					<div class="form-group">
						<label>Fecha</label>
						<input name="fecha" id="fecha" type="text" class="datepicker limpiar_form">
					</div>
				</div>

				<div class="col s12 m6">
					<div class="form-group">
						<label>Monto ($)</label>
						<input id="pre" name="pre" type="text" class="form-app number limpiar_form" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange btn-save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>
