<section class="mt-4 mb-4 p-4">
	<div class="row">
		<div class="col s12 m6 offset-m3">
			<div class="card boxPerfil">
				<div class="boxHead">
					<h6><i class="fa fa-user icon ml-2"></i> MI PERFIL</h6>
				</div>

				<div class="p-2">
					<form action="" method="post" id="update_perfil">
						<div class="row">
							<div class="flexCenter">
								<div class="file-field input-field">
									<img class="" src="../static/img/user.png" onerror="this.src='../static/img/user.png'" id="imagen">
									<div class="btn btn-blue text-center" style="position: absolute;bottom: 0px;right: 0px;">
										<i class="fa fa-upload upload-button"></i>
										<input type="file">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 m6">
								<div class="input-field">
									<p class="text-bold" for="nom_usu">Nombre</p>
									<input type="text" id="nombre" name="nom_usu" class="form-app">
								</div>
							</div>
							
							<div class="col s12 m6">
								<div class="input-field">
									<p class="text-bold" for="ape_usu">Apellido</p>
									<input type="text" id="apellido" name="ape_usu" class="form-app">
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p class="text-bold" for="ape_usu">Correo</p>
									<input type="email" id="correo" name="cor_usu" class="form-app">
								</div>
							</div>
							
							<div class="col s12 m6">
								<div class="input-field">
									<p class="text-bold" for="tel_usu">Teléfono</p>
									<input type="text" id="telefono" name="tel_usu" class="form-app">
								</div>
							</div>

							<div class="col s12 m12">
								<select class="browser-default input-field" id="sexo" name="sex_usu">
									<option value="Femenino" selected>Femenino</option>
									<option value="Masculino">Masculino</option>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col s12 text-center pb-2">
								<button class="btn waves-effect btn-orange">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../static/js/jquery.min.js"></script>
</section>