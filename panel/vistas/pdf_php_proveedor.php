<?php
  error_reporting(0);
  $data = json_decode($_POST['tablainfo'], true);
  $plan = $_POST['tablaplan'];
  
    session_start();
    require '../../vendor/autoload.php';
    use Spipu\Html2Pdf\Html2Pdf;
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
try {
     
    $nom = "Cliente__" . $data['id'] ."__.pdf";
    ob_start();
    include dirname(__FILE__).'/pdf_impresion_proveedor.php';
    $html = ob_get_clean();
    $html2pdf = new Html2Pdf('P', 'A4', 'es',true,'UTF-8');
    $html2pdf->writeHTML($html);
    $html2pdf->output($nom);
} catch (Html2PdfException $e) {
    $html2pdf->clean();
    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}

?>
