<section class="mt-4 mb-4 p-4">
	<h5 class="clr_orange center">ADMINISTRADORES</h5>
	<div class="text-right mb-3">
		<a href="#modalAdmin" class="btn btn-orange modal-trigger"><i class="fa fa-plus"></i> Nuevo</a>

	</div>
	<br>
	<table class="table display dt-responsive admin" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Correo</th>
				<th>Tipo de usuario</th>
				<th>Acciones</th>
			</tr>
		</thead>

		<tbody>
			<!--<tr>
				<td>#</td>
				<td>Correo</td>
				<td>Administrador / normal</td>
				<td>
					<a class="btn btn-orange" id=""><i class="fa fa-eye"></i></a> 
					<a class="btn btn-orange" id=""><i class="fa fa-edit"></i></a> 
					<a class="btn btn-orange" id=""><i class="fa fa-trash"></i></a>
				</td>
			</tr>-->
		</tbody>
	</table>
</section>

<!-- Modal -->
<div id="modalAdmin" class="modal">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"><i class="fa fa-lock fa-2x m-0"></i> Administrador</h6>
			<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<input type="hidden" class="limpiar_form" name="id_admin" id="id_admin" value=""> 
			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Correo</label>
						<input id="cor_adm" name="cor_adm" type="text" class="form-app limpiar_form" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Contraseña</label>
						<input id="con_adm" name="con_adm" type="password" class="form-app limpiar_form" placeholder="">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label class="text-bold">Tipo de usuario</label>
						<select id="tip_adm" name="tip_adm" class="input-field">
							<option value="1">Administrador</option>
							<option value="2">Normal</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-orange btn-save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>

