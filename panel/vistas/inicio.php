<div class="container">
	<section class="mt-4">
		<div class="row">
			<div id="listar_vehiculos">

			</div>
			<!--<div class="col s12 m12 l6">
				<div class="row border flexCenter m-2">
					<div class="col s12 m2 flexCenter">
						 <img src="../static/img/transporte/bicycle.png" class="trans--img">
					</div>

					<div class="col s12 m10">
						 <div class="row mb-0">
						 	<div class="col s12 m3">
						 		<br><h5 class="center veh_1">0</h5>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-act veh_1_1">0</span></h6> 
						 		<h6 class="center">Activo</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-blo veh_1_2">0</span></h6> 
						 		<h6 class="center">Bloqueado</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-pau veh_1_0">0</span></h6> 
						 		<h6 class="center">Pausado</h6>
						 	</div>
						 </div>
					</div>
				</div>
			</div>

			<div class="col s12 m12 l6">
				<div class="row border flexCenter m-2">
					<div class="col s12 m2 flexCenter">
						 <img src="../static/img/transporte/car.png" class="trans--img">
					</div>

					<div class="col s12 m10">
						 <div class="row mb-0">
						 	<div class="col s12 m3">
						 		<br><h5 class="center veh_2">0</h5>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-act veh_2_1">0</span></h6> 
						 		<h6 class="center">Activo</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-blo veh_2_2">0</span></h6> 
						 		<h6 class="center">Bloqueado</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-pau veh_2_0">0</span></h6> 
						 		<h6 class="center">Pausado</h6>
						 	</div>
						 </div>
					</div>
				</div>
			</div>

			<div class="col s12 m12 l6">
				<div class="row border flexCenter m-2">
					<div class="col s12 m2 flexCenter">
						 <img src="../static/img/transporte/crane-truck.png" class="trans--img">
					</div>

					<div class="col s12 m10">
						 <div class="row mb-0">
						 	<div class="col s12 m3">
						 		<br><h5 class="center veh_4">0</h5>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-act veh_4_1">0</span></h6> 
						 		<h6 class="center">Activo</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-blo veh_4_2">0</span></h6> 
						 		<h6 class="center">Bloqueado</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-pau veh_4_0">0</span></h6> 
						 		<h6 class="center">Pausado</h6>
						 	</div>
						 </div>
					</div>
				</div>
			</div>

			<div class="col s12 m12 l6">
				<div class="row border flexCenter m-2">
					<div class="col s12 m2 flexCenter">
						 <img src="../static/img/transporte/pickup-truck.png" class="trans--img">
					</div>

					<div class="col s12 m10">
						 <div class="row mb-0">
						 	<div class="col s12 m3">
						 		<br><h5 class="center veh_5">0</h5>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-act veh_5_1">0</span></h6> 
						 		<h6 class="center">Activo</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-blo veh_5_2">0</span></h6> 
						 		<h6 class="center">Bloqueado</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-pau veh_5_0">0</span></h6> 
						 		<h6 class="center">Pausado</h6>
						 	</div>
						 </div>
					</div>
				</div>
			</div>

			<div class="col s12 m12 l6">
				<div class="row border flexCenter m-2">
					<div class="col s12 m2 flexCenter">
						 <img src="../static/img/transporte/scooter.png" class="trans--img">
					</div>

					<div class="col s12 m10">
						 <div class="row mb-0">
						 	<div class="col s12 m3">
						 		<br><h5 class="center veh_3">0</h5>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-act veh_3_1">0</span></h6> 
						 		<h6 class="center">Activo</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-blo veh_3_2">0</span></h6> 
						 		<h6 class="center">Bloqueado</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-pau veh_3_0">0</span></h6> 
						 		<h6 class="center">Pausado</h6>
						 	</div>
						 </div>
					</div>
				</div>
			</div>

			<div class="col s12 m12 l6">
				<div class="row border flexCenter m-2">
					<div class="col s12 m2 flexCenter">
						 <img src="../static/img/transporte/truck.png" class="trans--img">
					</div>

					<div class="col s12 m10">
						 <div class="row mb-0">
						 	<div class="col s12 m3">
						 		<br><h5 class="center veh_6">0</h5>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-act veh_6_1">0</span></h6> 
						 		<h6 class="center">Activo</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-blo veh_6_2">0</span></h6> 
						 		<h6 class="center">Bloqueado</h6>
						 	</div>

						 	<div class="col s12 m3">
						 		<h6 class="center"><span class="trans-pau veh_6_0">0</span></h6> 
						 		<h6 class="center">Pausado</h6>
						 	</div>
						 </div>
					</div>
				</div>
			</div>-->
		</div>
	</section>
</div>
<script src='../static/js/backend-panel/controlinicio.js'></script>