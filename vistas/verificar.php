<style type="text/css">
		.divcuenta{
			background: #f1f1f1;
			height: 100vh;
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.boxcuenta{
			background: #fff;
			border: 2px solid #ddd;
			border-radius: .8rem;
			font-family: 'Roboto', sans-serif !important;
		}
		.boxcuenta--text{
			background: #001f3b;
			color: #fff;
			border-radius: 0rem 0rem .8rem .8rem;
		}

		.boxcuenta--logo{
			padding: 1rem;
			text-align: center;
		}

		.boxcuenta--text{
			padding: 1rem;
			margin: 0px;
			color: #fff;
		}

		@media only screen and (min-width: 751px){
			.boxcuenta{
				width: 50%;
			}
			.boxcuenta--logo img{
				width: 20%;
			}
		}

		@media (min-width: 426px) and (max-width: 750px){
			.boxcuenta{
				width: 60%;
				margin-top: 6rem;
				margin-bottom: 5rem;
			}
			.boxcuenta--logo img{
				width: 40%;
			}
			.boxcuenta h1{
				font-size: 26px;
			}
		}

		@media only screen and (max-width: 425px){
			.boxcuenta{
				width: 60%;
				margin-top: 3rem;
				margin-bottom: 3rem;
			}
			.boxcuenta--logo img{
				width: 50%;
			}
			.boxcuenta h1{
				font-size: 26px;
			}
		}
	</style>

	<div class="divcuenta">
		<center>
			<div class="boxcuenta">
				<div class="boxcuenta--logo">
					<img src="static/img/logo.png">
				</div>

				<div class="boxcuenta--text">
					<h1>BIENVENIDO</h1>
					<div id="previewPublisher" class="gallery main-gallery flickity-enabled" style="text-align: center;width:70%!important;">
							<i class="fa fa-spinner fa-spin" id="spin" style="font-size:40px;color:white;"></i>
					</div>
				</div>
			</div>
		</center>
	</div>
 
 
<script  type="text/javascript" src="static/js/backend-panel/general.js"></script>
<script src='static/js/axios.min.js'></script>"
<script type="text/javascript">
	
	axios.get(dominio+"<?php echo explode('-',$_GET['op'])[1]; ?>/verificar?token=<?php echo $_GET['token']; ?>", {})
      .then(function (response) {
         		$('#spin').remove();
				$('#previewPublisher').html(`<h6>${response.data.msg}</h6>`);
      })
      .catch(function (error) {
       
      });
</script>
