<div class="flexCenter vh100">
	<div class="boxAccount__form">
		<form method="post" action="#" class="login-form" id="" onsubmit="return false;">
			
			<h4 class="center text-bold boxAccount--title">Iniciar sesión</h4>

			<div class="boxAccount__body">
				<div class="row mb-0">
					<div class="col s12">
						<div class="input-field">
							<p>Correo electrónico</p>
							<input id="correo" name="correo" type="text" class="form-app">
						</div>
					</div>
					
					<div class="col s12">
						<div class="input-field">
							<p>Contraseña</p>
							<input id="pass" name="pass" type="password" class="form-app">
						</div>
					</div>
				</div>

				<div class="row mb-0">
					<div class="col s12">
						<p>
						<label>
							<input name="condi" id="condi" type="radio" value="1"/>
							<span>Recuerdame</span>
						</label>
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col s12">
						<button type="submit" id="btn_entrar" class="btn btn-orange waves-effect w100">Ingresar <i class="fa" id="login1"></i></button>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						<b>¿Olvidaste tu contraseña? <a href="?op=recuperar">Recuperar</a></b>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
