<div class="flexCenter vh100">
	<div class="boxAccount__form">
		<form method="post" action="#" class="login-form" id="login_entrar" onsubmit="return false;">
			
			<h4 class="center text-bold boxAccount--title">Recuperar </h4>
			
			<div class="boxAccount__body">
				<div class="row mb-0">
					<div class="col s12">
						<div class="input-field">
							<p>Correo electrónico</p>
							<input id="correo" name="correo" type="text" class="form-app">
						</div>
					</div>
				</div>

				<div class="row mt-5">
					<div class="col s12">
						<button type="submit" id="btn_entrar" class="btn btn-orange waves-effect w100">Enviar <i class="fa" id="login1"></i></button>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						<b>¿Ya tienes cuenta? <a href="?op=iniciarsesion">Iniciar sesión</a></b>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="static/js/back/login.js"></script>
