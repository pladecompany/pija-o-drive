<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
include_once('ruta.php');
?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Trabajo en belleza">
	<meta name="keywords" content="Trabajo en belleza">
	<meta name="author" content="Trabajo en belleza">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- For iPhone -->
	<meta name="msapplication-TileColor" content="#00bcd4">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<title>Pijaos DRIVE</title>
	<link rel="shortcut icon" href="static/img/favicon.png" />
	<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">

	<!-- CORE CSS   --> 
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/materialize.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/scroll.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/style.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/font-awesome/css/all.min.css">
</head>

<body>
<nav class="fixed">
	<div class="nav-wrapper">
		<div class="col s12">
			<a href="#!" class="brand-logo">
				<img src="static/img/logo.png">
			</a>
		</div>
	</div>
</nav>


	<div>
		<?php
		include_once($ruta);
		?>
	</div>

	<footer class="footer down">
		<div class="row p-0">
			<div class="col s12 center">
				<p class="text-bold">Copyright 2019 pijaosDrive</p>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="static/js/jquery.min.js"></script>
	<script src="static/js/materialize.min.js"></script>
	<script src="static/js/menu.js"></script>
	<script src="static/js/main.js"></script>
	<script src="static/lib/select2/select2.min.js"></script>
	<script src='static/js/axios.min.js'></script>
	<script src="static/js/backend-panel/login.js"></script>
</body>
</html>
